﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    static class ModAdapter
    {
        private static ModAdapter_HAR har;
        private static IGenitalAccess genitals;

        public static bool IsRJWLoaded => ModLister.AnyFromListActive(new List<string>() { "rim.job.world" });

        public static ModAdapter_HAR HAR
        {
            get
            {
                if (har == null)
                {
                    har = new ModAdapter_HAR();
                }
                return har;
            }
        }
        public static IGenitalAccess Genitals
        {
            get
            {
                if(genitals == null)
                {
                    if (IsRJWLoaded)
                    {
                        string filePath = ReflectionUtility.ModDirectory + "/RJWGenitalAccess/RJWGenitalAccess.dll";
                        Assembly rjwAssembly = Assembly.LoadFrom(filePath);
                        //Log.Message(rjwAssembly.ToString());
                        Type genitalAccessType = rjwAssembly.GetType("RJWGenitalAccess.GenitalAccess");
                        genitals = (IGenitalAccess) Activator.CreateInstance(genitalAccessType);
                    }
                    else
                    {
                        string filePath = ReflectionUtility.ModDirectory + "/LightGenitals/Assemblies/LightGenitals.dll";
                        Assembly lgAssembly = Assembly.LoadFrom(filePath);
                        Type genitalAccessType = lgAssembly.GetType("LightGenitals.GenitalAccess");
                        genitals = (IGenitalAccess)Activator.CreateInstance(genitalAccessType);
                    }
                }
                return genitals;
            }
        }
    }
}
