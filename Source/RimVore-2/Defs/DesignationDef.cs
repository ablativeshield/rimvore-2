﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class Designation : IExposable
    {
        public DesignationDef def;
        public Pawn pawn;
        public bool enabledAuto;
        public bool enabledManual;
        public bool isSetManually;

        public Designation() { }

        public Designation(Pawn pawn, DesignationDef def)
        {
            this.pawn = pawn;
            this.def = def;
            enabledManual = false;
            isSetManually = false;
            CalculateEnabledAuto();
        }

        public Texture2D CurrentIcon() => def.CurrentIcon(this);
        public string CurrentTip() => def.CurrentTip(this);

        public void CalculateEnabledAuto()
        {
            enabledAuto = RV2Mod.settings.rules.DesignationActive(pawn, def);
        }

        public bool IsEnabled()
        {
            if (def.IsBlocked(pawn))
            {
                return false;
            }
            if (isSetManually)
            {
                return enabledManual;
            }
            CalculateEnabledAuto();
            return enabledAuto;
        }

        // auto -> manual on -> manual off -> <repeat>
        public void Cycle()
        {
            // can't cycle if designation is blocked
            if (def.IsBlocked(pawn))
            {
                return;
            }
            VoreInteractionManager.Reset();
            if (isSetManually)
            {
                if (enabledManual)
                {
                    enabledManual = false;
                }
                else
                {
                    isSetManually = false;
                    CalculateEnabledAuto();
                }
            }
            else
            {
                enabledManual = true;
                isSetManually = true;
            }
        }

        public void ExposeData()
        {
            Scribe_Defs.Look(ref def, "def");
            Scribe_References.Look(ref pawn, "pawn");
            Scribe_Values.Look(ref enabledAuto, "enabledAuto");
            Scribe_Values.Look(ref enabledManual, "enabledManual");
            Scribe_Values.Look(ref isSetManually, "isSetManually");
        }
    }

    public class DesignationDef : Def
    {
        public IdentifierRole assignedTo;
        public bool lethal = false;
        public string iconPathEnabledManually;
        public string iconPathDisabledManually;
        public string iconPathEnabledAutomatically;
        public string iconPathDisabledAutomatically;
        public string iconPathBlocked;
        public string tipEnabledManually;
        public string tipDisabledManually;
        public string tipEnabledAutomatically;
        public string tipDisabledAutomatically;
        public string tipBlocked;

        /// <param name="reason">if translateReason=true - translated reason with pawn name, else translationkey</param>
        /// <param name="translateReason">default TRUE, there may be scenarios where the placeholder {0} needs to be replaced with something that is not the pawns name (default behaviour)</param>
        public bool IsEnabledFor(Pawn pawn, out string reason)
        {
            bool enabled = SaveStorage.DataStore?.GetPawnData(pawn)?.Designations?.TryGetValue(this)?.IsEnabled() == true;
            if (!enabled)
            {
                reason = "RV2_VoreInvalidReasons_DesignationMissing".Translate(pawn.Label, label);
                return false;
            }
            reason = null;
            return true;
        }

        public bool IsEnabledFor(Pawn predator, Pawn prey, out string reason)
        {
            if (assignedTo == IdentifierRole.Both || assignedTo == IdentifierRole.Predator)
            {
                // Log.Message("checking predator for designation " + designation.defName);
                if (!IsEnabledFor(predator, out reason))
                {
                    // var dict = SaveStorage.DataStore?.GetPawnData(predator)?.Designations;
                    // Log.Message("not valid, dict: " + dict == null ? "NULL" : LogUtility.ToString(dict));
                    return false;
                }
            }
            if (assignedTo == IdentifierRole.Both || assignedTo == IdentifierRole.Prey)
            {
                // Log.Message("checking prey for designation " + designation.defName);
                if (!IsEnabledFor(prey, out reason))
                {
                    // var dict = SaveStorage.DataStore?.GetPawnData(predator)?.Designations;
                    // Log.Message("not valid, dict: " + dict == null ? "NULL" : LogUtility.ToString(dict));
                    return false;
                }
            }

            reason = null;
            return true;
        }

        public bool CanBeAssignedTo(IdentifierRole role)
        {
            // if either points to prey and pred, always true
            if(role == IdentifierRole.Both || assignedTo == IdentifierRole.Both)
            {
                return true;
            }
            // otherwise the role must match
            return role == assignedTo;
        }

        public Texture2D CurrentIcon(Pawn pawn)
        {
            Designation designation = SaveStorage.DataStore?.GetPawnData(pawn)?.Designations.SingleOrDefault(kvp => kvp.Key == this).Value;
            return CurrentIcon(designation);
        }
        public Texture2D CurrentIcon(Designation designation)
        {
            Pawn pawn = designation.pawn;
            string path;
            if (IsBlocked(pawn)) path = iconPathBlocked;
            else if (designation.isSetManually)
            {
                if(designation.enabledManual) path = iconPathEnabledManually;
                else path = iconPathDisabledManually;
            }
            else
            {
                if(designation.enabledAuto) path = iconPathEnabledAutomatically;
                else path = iconPathDisabledAutomatically;
            }

            return ContentFinder<Texture2D>.Get(path);
        }

        public string CurrentTip(Pawn pawn)
        {
            Designation designation = SaveStorage.DataStore?.GetPawnData(pawn)?.Designations.SingleOrDefault(kvp => kvp.Key == this).Value;
            return CurrentTip(designation);
        }
        public string CurrentTip(Designation designation)
        {
            if (designation == null)
            {
                return null;
            }
            Pawn pawn = designation.pawn;
            if (IsBlocked(pawn)) return tipBlocked;
            else if (designation.isSetManually)
            {
                if(designation.enabledManual) return tipEnabledManually;
                else return tipDisabledManually;
            }
            else
            {
                if(designation.enabledAuto) return tipEnabledAutomatically;
                else return tipDisabledAutomatically;
            }
        }

        public bool IsBlocked(Pawn pawn)
        {
            QuirkManager pawnQuirks = pawn.QuirkManager(false);
            if(pawnQuirks == null)
            {
                return false;
            }
            return pawnQuirks.HasDesignationBlock(this);
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (iconPathEnabledManually == null)
            {
                yield return "Required field \"iconPathEnabledManually\" not set";
            }
            if (iconPathDisabledManually == null)
            {
                yield return "Required field \"iconPathDisabledManually\" not set";
            }
            if (iconPathEnabledAutomatically == null)
            {
                yield return "Required field \"iconPathEnabledAutomatically\" not set";
            }
            if (iconPathDisabledAutomatically == null)
            {
                yield return "Required field \"iconPathDisabledAutomatically\" not set";
            }
            if (iconPathBlocked == null)
            {
                yield return "Required field \"iconPathBlocked\" not set";
            }
            if (tipEnabledManually == null)
            {
                yield return "Required field \"tipEnabledManually\" not set";
            }
            if (tipDisabledManually == null)
            {
                yield return "Required field \"tipDisabledManually\" not set";
            }
            if (tipEnabledAutomatically == null)
            {
                yield return "Required field \"tipEnabledAutomatically\" not set";
            }
            if (tipDisabledAutomatically == null)
            {
                yield return "Required field \"tipDisabledAutomatically\" not set";
            }
            if (tipBlocked == null)
            {
                yield return "Required field \"tipBlocked\" not set";
            }
        }
    }
}
