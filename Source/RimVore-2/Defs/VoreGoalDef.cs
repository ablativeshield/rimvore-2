﻿using System;
using System.Collections.Generic;
using Verse;
using RimWorld;
using UnityEngine;

namespace RimVore2
{
    public class VoreGoalDef : Def, IPreferrable
    {
        public string RMBMenuTranslationKey;
        public List<DesignationDef> requiredDesignations = new List<DesignationDef>();
        public bool IsLethal => requiredDesignations.Any(designation => designation.lethal);
        public List<TargetedRequirements> requirements;
        public TaleDef goalFinishTale;
        public RecordDef goalFinishRecordPredator;
        public RecordDef goalFinishRecordPrey;
        public List<RulePackDef> relatedRulePacks;


        //Tab UI information
        public string IconPath;
        private Texture2D icon;
        public Texture2D Icon
        {
            get
            {
                if (icon == null)
                {
                    if (!IconPath.NullOrEmpty())
                    {
                        icon = ContentFinder<Texture2D>.Get(IconPath);
                        
                    }
                    else
                    {
                        if (IsLethal)
                            icon = UIUtility.SkullButtonTexture;
                        else
                            icon = UIUtility.HeartButtonTexture;
                    }
                }
                return icon;
            }
        }

        public bool IsValid(Pawn predator, Pawn prey, out string reason)
        {
            //Log.Message("Checking voreGoalDef for validity");
            if (!predator.CanBePredator(out reason))
            {
                return false;
            }
            if(!prey.CanBePrey(out reason))
            {
                return false;
            }
            if(IsLethal && !RV2Mod.settings.features.FatalVoreEnabled)
            {
                reason = "RV2_VoreInvalidReasons_FatalDisabled".Translate();
                return false;
            }
            if (!IsLethal && !RV2Mod.settings.features.EndoVoreEnabled)
            {
                reason = "RV2_VoreInvalidReasons_EndoDisabled".Translate();
                return false;
            }
            // predator is always required for vore, but the user doesn't need to set that in the Defs
            if (!requiredDesignations.Contains(DesignationDefOf.predator))
            {
                requiredDesignations.Add(DesignationDefOf.predator);
            }
            bool allDesignationsValid = true;
            // Log.Message("Checking designations: " + string.Join(", ", requiredDesignations.ConvertAll(d => d.defName)));
            foreach(DesignationDef designation in requiredDesignations)
            {
                bool isDesignationValid = designation.IsEnabledFor(predator, prey, out string subReason);
                // Log.Message("Designation " + designation.defName + " is valid ? " + isDesignationValid);
                allDesignationsValid &= isDesignationValid;
                if (subReason != null)
                {
                    // if reason not set yet, set it, otherwise, add to it
                    reason = reason == null ? subReason : reason + " + " + subReason;
                }
            }
            //bool allDesignationsValid = validDesignations.TrueForAll(designation => IsDesignationValid(designation, predator, prey, reasons.Add(out reason));
            if (!allDesignationsValid)
            {
                return false;
            }

            if(!AreVoreEnablersValid(predator, prey, out reason))
            {
                return false;
            }

            if (!requirements.NullOrEmpty())
            {
                foreach(TargetedRequirements requirement in requirements)
                {
                    if(!requirement.FulfillsRequirements(predator, prey, out reason))
                    {
                        return false;
                    }
                }
            }

            reason = null;
            return true;
        }

        private bool AreVoreEnablersValid(Pawn predator, Pawn prey, out string reason)
        {
            if(!RV2Mod.settings.features.VoreQuirksEnabled)
            {
                reason = null;
                return true;
            }
            List<VoreTargetSelectorRequest> requests = new List<VoreTargetSelectorRequest>()
            {
                new VoreTargetSelectorRequest(true)
                {
                    voreGoal = this
                },
                new VoreTargetSelectorRequest(true)
                {
                    role = VoreRole.Prey,
                    raceType = prey.GetRaceType(),
                    voreGoal = this
                }
            };
            if(!VoreValidator.PredatorPassesVoreEnablerSelectors(predator, requests))
            {
                reason = "RV2_RequirementInvalidReasons_MissingQuirk".Translate();
                return false;
            }
            reason = null;
            return true;
        }

        public float GetPreference(Pawn pawn, VoreRole role, ModifierOperation modifierOperation = ModifierOperation.Multiply)
        {
            IdentifierRole identRole;
            if (role == VoreRole.Predator) identRole = IdentifierRole.Predator;
            else if (role == VoreRole.Prey) identRole = IdentifierRole.Prey;
            else throw new NotImplementedException("No support for feeder vore for preference calculation");

            List<DesignationDef> applicableDesignations = requiredDesignations
                .FindAll(d => d.CanBeAssignedTo(identRole));
            foreach(DesignationDef designation in applicableDesignations)
            {
                if(!designation.IsEnabledFor(pawn, out _))
                {
                    return -1f;
                }
            }

            return pawn.PreferenceFor(this, role, modifierOperation);
        }

        public bool IsObsessed(Pawn pawn, VoreRole role)
        {
            return GetPreference(pawn, role) >= 99;
        }

        public void IncrementRecords(Pawn predator, Pawn prey)
        {
            if(goalFinishRecordPredator != null)
            {
                predator.records?.Increment(goalFinishRecordPredator);
            }
            if(goalFinishRecordPrey != null)
            {
                prey.records?.Increment(goalFinishRecordPrey);
            }
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (!requirements.NullOrEmpty())
            {
                foreach(TargetedRequirements requirement in requirements)
                {
                    foreach(string error in requirement.ConfigErrors())
                    {
                        yield return error;
                    }
                }
            }
        }
    }
}