﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace RimVore2
{
    public class VorePathDef : Def
    {
        public VoreTypeDef voreType;
        public VoreGoalDef voreGoal;

        public TaleDef initTale;
        public TaleDef exitTale;
        public string actionDescription;
        public VoreProduct voreProduct;
        public PostVoreMemoryDef postVoreMemories;
        public List<VoreStageDef> stages;
        public List<TargetedRequirements> requirements;
        public bool feedsPredator = false;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (stages == null || stages.Count < 1)
            {
                yield return "required list \"stages\" not provided";
            }
            if (voreType == null)
            {
                yield return "required field \"voreType\" not provided";
            }
            if (voreGoal == null)
            {
                yield return "required field \"voreGoal\" not provided";
            }
            if(requirements != null)
            {
                foreach(TargetedRequirements requirement in requirements)
                {
                    foreach(string error in requirement.ConfigErrors())
                    {
                        yield return error;
                    }
                }
            }
        }

        public List<ThingDef> ValidVoreProductContainers => voreProduct?.selectableContainers?.FindAll(container => container.GetModExtension<VoreContainerExtension>()?.IsValid() == true);

        public ThingDef FirstValidContainer => ValidVoreProductContainers.NullOrEmpty() ? null : ValidVoreProductContainers.First();

        public override string ToString()
        {
            return "VoreType: " + voreType +
                "|VoreGoal: " + voreGoal +
                "|Path: " + string.Join("\n", stages);
        }

        public bool IsValid(Pawn predator, Pawn prey, out string reason, bool isForAuto = false, bool ignoreRules = false)
        {
            //Log.Message("Checking vorePathDef for validity");
            // if the prey is null, do not call any checking methods with the prey. The prey can be null if this method is called to retrieve all generally valid paths for a predator
            bool preyCanBeChecked = prey != null;
            // check settings
            if(!ignoreRules && !RV2Mod.settings.rules.VorePathEnabled(predator, IdentifierRole.Predator, this, isForAuto))
            {
                reason = "RV2_VoreInvalidReasons_PredatorRuleBlockingPath".Translate();
                return false;
            }
            if (!AreVoreEnablersValid(predator, prey, out reason))
            {
                return false;
            }
            // valid between pred and prey
            if (preyCanBeChecked)
            {
                if(!RV2Mod.settings.rules.VorePathEnabled(prey, IdentifierRole.Prey, this, isForAuto))
                {
                    reason = "RV2_VoreInvalidReasons_PreyRuleBlockingPath".Translate();
                    return false;
                }
                if (!voreGoal.IsValid(predator, prey, out reason))
                {
                    return false;
                }
                if(!voreType.IsValid(predator, prey, out reason))
                {
                    return false;
                }
                if(!predator.CanVore(prey, out reason))
                {
                    return false;
                }
            }
            if(!AreStagesValid(predator, prey, out reason))
            {
                return false;
            }
            if (!requirements.NullOrEmpty())
            {
                foreach(TargetedRequirements requirement in requirements)
                {
                    // if any requirement is not passed, path is not valid
                    if(!requirement.FulfillsRequirements(predator, prey, out reason))
                    {
                        return false;
                    }
                }
            }
            reason = null;
            return true;
        }

        private bool AreVoreEnablersValid(Pawn predator, Pawn prey, out string reason)
        {
            if (!RV2Mod.settings.features.VoreQuirksEnabled)
            {
                reason = null;
                return true;
            }
            List<VoreTargetSelectorRequest> requests = new List<VoreTargetSelectorRequest>()
            {
                new VoreTargetSelectorRequest(true) {
                    voreGoal = this.voreGoal,
                    voreType = this.voreType
                }
            };
            if(prey != null)
            {
                List<VoreTargetSelectorRequest> preyRequests = new List<VoreTargetSelectorRequest>()
                {
                    new VoreTargetSelectorRequest(true) {
                        role = VoreRole.Prey,
                        raceType = prey.GetRaceType(),
                        voreGoal = this.voreGoal,
                        voreType = this.voreType
                    },
                    new VoreTargetSelectorRequest(true) {
                        role = VoreRole.Prey,
                        raceType = prey.GetRaceType()
                    }
                };
                requests.AddRange(preyRequests);
            }
            if (!VoreValidator.PredatorPassesVoreEnablerSelectors(predator, requests))
            {
                reason = "RV2_RequirementInvalidReasons_MissingQuirk".Translate();
                return false;
            }
            reason = null;
            return true;
        }

        public bool AreStagesValid(Pawn predator, Pawn prey, out string reason)
        {
            foreach (VoreStageDef stage in stages)
            {
                if (!stage.requirements.NullOrEmpty())
                {
                    foreach(TargetedRequirements requirement in stage.requirements)
                    {
                        if (!requirement.FulfillsRequirements(predator, prey, out reason))
                        {
                            return false;
                        }
                    }
                }
            }
            reason = null;
            return true;
        }
    }

    public class PostVoreMemoryDef
    {
        public ThoughtDef predatorPostVoreSuccess;
        public ThoughtDef predatorPostVoreInterrupted;
        public ThoughtDef preyPostVoreSuccess;
        public ThoughtDef preyPostVoreInterrupted;
    }
}