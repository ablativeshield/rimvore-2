﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class RV2Settings : ModSettings
    {
        public string LastSavedVersion;

        public SettingsContainer_Debug debug;
        public SettingsContainer_Features features;
        public SettingsContainer_FineTuning fineTuning;
        public SettingsContainer_Cheats cheats;
        public SettingsContainer_Sounds sounds;
        public SettingsContainer_Quirks quirks;
        public SettingsContainer_Rules rules;
        protected static List<SettingsContainer> settingsContainers = new List<SettingsContainer>();
        public RV2Settings()
        {
            debug = new SettingsContainer_Debug();
            features = new SettingsContainer_Features();
            fineTuning = new SettingsContainer_FineTuning();
            cheats = new SettingsContainer_Cheats();
            sounds = new SettingsContainer_Sounds();
            quirks = new SettingsContainer_Quirks();
            rules = new SettingsContainer_Rules();
            settingsContainers.Add(debug);
            settingsContainers.Add(features);
            settingsContainers.Add(fineTuning);
            settingsContainers.Add(cheats);
            settingsContainers.Add(sounds);
            settingsContainers.Add(quirks);
            settingsContainers.Add(rules);
        }

        public void Reset()
        {
            if (!settingsContainers.NullOrEmpty())
            {
                settingsContainers.ForEach(c => c.Reset());
            }
        }

        public void DefsLoaded()
        {
            if (!settingsContainers.NullOrEmpty())
            {
                settingsContainers.ForEach(c => c.DefsLoaded());
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref LastSavedVersion, "LastSavedVersion");
            if (!settingsContainers.NullOrEmpty())
            {
                settingsContainers.ForEach(c => c.ExposeData());
            }
        }
    }
}

