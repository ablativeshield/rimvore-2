﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public class VoreRulePreset : IExposable
    {
        public Dictionary<VoreRuleTarget, VoreRule> rules;
        public VoreRulePreset() { }
        public VoreRulePreset(Dictionary<VoreRuleTarget, VoreRule> rules)
        {
            this.rules = new Dictionary<VoreRuleTarget, VoreRule>(rules);
        }

        public void ExposeData()
        {
            ScribeUtilities.ScribeVariableDictionary(ref rules, "rules", LookMode.Deep, LookMode.Deep);
        }
    }

    public static class RulePresets
    {
        /// <summary>
        /// Scribing is fucking dogshit, I can't use a LookMode to scribe a collection, so I have to encapsulate this dict and use LookMode.Deep
        /// </summary>
        
        public static KeyValuePair<string, VoreRulePreset> NabbersChoice()
        {
            Dictionary<VoreRuleTarget, VoreRule> rules = new Dictionary<VoreRuleTarget, VoreRule>();
            VoreRuleTarget target1 = new VoreRuleTarget()
            {
                targetType = IdentifierType.Everyone,
                targetRole = IdentifierRole.Both
            };
            VoreRule rule1 = new VoreRule(RuleState.On) { };
            rule1.DesignationStates[DesignationDefOf.fatal.defName] = RuleState.Off;
            rules.Add(target1, rule1);
            VoreRuleTarget target2 = new VoreRuleTarget()
            {
                customName = "Wild animals ignore age and only vore orally",
                targetType = IdentifierType.Relation,
                targetRelation = RelationKind.WildAnimal,
                targetRole = IdentifierRole.Predator
            };
            VoreRule rule2 = new VoreRule(RuleState.Copy)
            {
                ConsiderMinimumAge = RuleState.Off,
                UseVorePathRules = RuleState.On
            };
            rule2.DesignationStates[DesignationDefOf.fatal.defName] = RuleState.On;
            // disable all paths except for oral
            foreach (VorePathRule pathRule in rule2.AllPathRules())
            {
                if (pathRule.VorePath.voreType != DefDatabase<VoreTypeDef>.GetNamed("Oral"))
                {
                    pathRule.Enabled = false;
                }
            }
            rules.Add(target2, rule2);
            VoreRuleTarget target3 = new VoreRuleTarget()
            {
                customName = "Colony animals ignore age",
                targetType = IdentifierType.Relation,
                targetRelation = RelationKind.ColonyAnimal
            };
            VoreRule rule3 = new VoreRule(RuleState.Copy)
            {
                ConsiderMinimumAge = RuleState.Off
            };
            rules.Add(target3, rule3);
            VoreRuleTarget target4 = new VoreRuleTarget()
            {
                customName = "Don't vore visitors",
                targetType = IdentifierType.Relation,
                targetRelation = RelationKind.Visitor
            };
            VoreRule rule4 = new VoreRule(RuleState.Copy) { };
            rule4.DesignationStates[DesignationDefOf.endo.defName] = RuleState.Off;
            rule4.DesignationStates[DesignationDefOf.fatal.defName] = RuleState.Off;
            rules.Add(target4, rule4);
            VoreRuleTarget target5 = new VoreRuleTarget()
            {
                customName = "Gloves off for prisoners",
                targetType = IdentifierType.Relation,
                targetRelation = RelationKind.Prisoner
            };
            VoreRule rule5 = new VoreRule() { };
            rule5.DesignationStates[DesignationDefOf.fatal.defName] = RuleState.On;
            rules.Add(target5, rule5);
            return new KeyValuePair<string, VoreRulePreset>("Nabbers' Recommendation", new VoreRulePreset(rules));
        }
    }
}
