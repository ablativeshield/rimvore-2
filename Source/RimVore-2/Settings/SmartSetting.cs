﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace RimVore2
{
    public interface ISmartSetting : IExposable
    {
        void Reset();
        void DoSetting(Listing_Standard list);
        void DefsLoaded();
    }

    public abstract class SmartSetting<T> : ISmartSetting
    {
        protected const float buttonSize = 16f;
        protected const float buttonPadding = 4f;
        protected string labelKey;
        protected string tooltipKey;
        public T value;
        protected T defaultValue;

        protected float ButtonSizeWithPadding => buttonSize + buttonPadding * 2;
        protected string Label => labelKey.Translate();
        protected string Tooltip => tooltipKey?.Translate();


        public SmartSetting() { }

        public SmartSetting(string labelKey, T value, T defaultValue, string tooltipKey = null)
        {
            this.labelKey = labelKey;
            this.value = value;
            this.tooltipKey = tooltipKey;
            this.defaultValue = defaultValue;
        }
        protected abstract float RequiredHeight(Listing_Standard list);

        public abstract void DoSetting(Listing_Standard list);

        public bool IsInvalid()
        {
            return labelKey == null;
        }

        public void DecorateSetting(Listing_Standard list, out Rect settingsRect)
        {
            Rect rect = list.GetRect(RequiredHeight(list));
            if (Mouse.IsOver(rect))
            {
                Widgets.DrawHighlight(rect);
            }
            if(Tooltip != null)
            {
                TooltipHandler.TipRegion(rect, Tooltip);
            }
            ResetButton(rect, out settingsRect);
        }

        protected void ResetButton(Rect rect, out Rect settingsRect)
        {
            UIUtility.SplitRectVertically(rect, out Rect outerButtonRect, out settingsRect, ButtonSizeWithPadding);
            bool isDefault = value.GetHashCode() == defaultValue.GetHashCode();
            // for some reason this causes odd behaviour when moving the slider through the default value, so we draw a phantom button instead
            // might have something to do with the additional potential event created by ButtonInvisible, so this is a bit of a mess
            // try it out if you don't believe me :^)
            //if (isDefault)
            //{
            //    return;
            //}
            Texture2D tex = isDefault ? UIUtility.BlankButtonTexture : UIUtility.ResetButtonTexture;
            UIUtility.TextureInCenter(outerButtonRect, tex, out Rect buttonRect, buttonSize);
            if (Widgets.ButtonInvisible(buttonRect, !isDefault))
            {
                if (!isDefault)
                {
                    SoundDefOf.Tick_Low.PlayOneShotOnCamera(null);
                    Reset();
                }
            }
        }

        public void Reset()
        {
            value = defaultValue;
        }

        public virtual void DefsLoaded() { }

        public virtual void ExposeData()
        {
            Scribe_Values.Look(ref labelKey, "labelKey");
            Scribe_Values.Look(ref tooltipKey, "tooltipKey");
        }
    }

    public class BoolSmartSetting : SmartSetting<bool>
    {
        public BoolSmartSetting() : base() { }

        public BoolSmartSetting(string labelKey, bool value, bool defaultValue, string tooltipKey = null) : base(labelKey, value, defaultValue, tooltipKey) { }

        protected override float RequiredHeight(Listing_Standard list)
        {
            return Text.CalcHeight(Label, list.ColumnWidth - ButtonSizeWithPadding * 2);
        }

        /// <summary>
        /// This is basically a re-implementation of base games checkbox - but with a reset button on the left
        /// </summary>
        public override void DoSetting(Listing_Standard list)
        {
            base.DecorateSetting(list, out Rect rect);
            UIUtility.SplitRectVertically(rect, out Rect labelRect, out Rect checkboxRect, -1, ButtonSizeWithPadding);
            Widgets.Label(labelRect, Label);
            Texture2D checkboxTexture = value == true ? UIUtility.CheckOnTexture : UIUtility.CheckOffTexture;
            UIUtility.TextureInCenter(checkboxRect, checkboxTexture, out _);
            if(Widgets.ButtonInvisible(rect))
            {
                SoundDefOf.Tick_Low.PlayOneShotOnCamera(null);
                value = !value;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref value, "value");
            Scribe_Values.Look(ref defaultValue, "defaultValue");
        }
    }

    public class FloatSmartSetting : SmartSetting<float>
    {
        const float textBoxWidth = 50f;
        float minValue;
        float maxValue;
        string format = "0.00";
        string extraLabel = "";

        public FloatSmartSetting() : base() { }

        public FloatSmartSetting(string labelKey, float value, float defaultValue, float minValue = 0, float maxValue = 999, string tooltipKey = null, string format = "0.00", string extraLabel = "") : base(labelKey, value, defaultValue, tooltipKey)
        {
            this.minValue = minValue;
            this.maxValue = maxValue;
            this.format = format;
            this.extraLabel = extraLabel;
        }

        protected override float RequiredHeight(Listing_Standard list)
        {
            float labelWidth = list.ColumnWidth - ButtonSizeWithPadding - textBoxWidth;
            return Text.CalcHeight(Label, labelWidth) + Text.CalcHeight(" ", labelWidth);
        }

        public override void DoSetting(Listing_Standard list)
        {
            base.DecorateSetting(list, out Rect rect);
            UIUtility.FancySlider(rect, Label, ref value, minValue, maxValue, format, extraLabel);
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref value, "value");
            Scribe_Values.Look(ref defaultValue, "defaultValue");
            Scribe_Values.Look(ref minValue, "minValue", 0);
            Scribe_Values.Look(ref maxValue, "maxValue", 999);
            Scribe_Values.Look(ref format, "format", "0.00");
            Scribe_Values.Look(ref extraLabel, "extraLabel", "");
        }
    }

    public class EnumSmartSetting<T> : SmartSetting<T> where T : struct, IConvertible
    {
        List<T> whiteList;
        List<T> blackList;
        public Func<T, string> valuePresenter;
        FloatMenu buttonMenu;
        FloatMenu ButtonMenu
        {
            get
            {
                if (buttonMenu == null)
                {
                    List<FloatMenuOption> options = ValidOptions()   // conver each valid enum value into a selectable float menu option
                        .Select(enumValue =>
                        {
                            string valueLabel = valuePresenter == null ? enumValue.ToString() : valuePresenter(enumValue).ToString();
                            Action valueAction = () => value = enumValue;
                            return new FloatMenuOption(valueLabel, valueAction);
                        })
                        .ToList();
                    buttonMenu = new FloatMenu(options);
                }
                return buttonMenu;
            }
        }

        public EnumSmartSetting() : base() { }

        public EnumSmartSetting(string labelKey, T value, T defaultValue, string tooltipKey = null, List<T> whiteList = null, List<T> blackList = null) : base(labelKey, value, defaultValue, tooltipKey)
        {
            this.whiteList = whiteList;
            this.blackList = blackList;
        }

        private string ButtonLabel => valuePresenter == null ? value.ToString() : valuePresenter(value).ToString();
        private float ButtonWidth => Text.CalcSize(" " + ButtonLabel.ToString() + " ").x; 

        protected override float RequiredHeight(Listing_Standard list)
        {
            float labelWidth = list.ColumnWidth - ButtonWidth;
            return Text.CalcHeight(Label, labelWidth);
        }

        public override void DoSetting(Listing_Standard list)
        {
            base.DecorateSetting(list, out Rect rect);
            UIUtility.SplitRectVertically(rect, out Rect labelRect, out Rect buttonRect, -1, ButtonWidth);
            Widgets.Label(labelRect, Label);

            if (Widgets.ButtonText(buttonRect, ButtonLabel))
            {
                Find.WindowStack.Add(ButtonMenu);
            }
        }

        public IEnumerable<T> ValidOptions()
        {
            foreach (T value in typeof(T).GetEnumValues())
            {
                if (blackList?.Contains(value) == true)
                {
                    continue;
                }
                if (whiteList?.Contains(value) == false)
                {
                    continue;
                }
                yield return value;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref value, "value");
            Scribe_Values.Look(ref defaultValue, "defaultValue");
            Scribe_Collections.Look(ref whiteList, "whiteList", LookMode.Value);
            Scribe_Collections.Look(ref blackList, "blackList", LookMode.Value);
        }
    }

    public class DefSmartSetting<T> : SmartSetting<T> where T : Def, new()
    {
        string unresolvedValue;
        string unresolvedDefaultValue;
        List<T> whiteList;
        List<T> blackList;
        FloatMenu buttonMenu;
        FloatMenu ButtonMenu
        {
            get
            {
                if (buttonMenu == null)
                {
                    List<FloatMenuOption> options = ValidOptions()   // conver each valid enum value into a selectable float menu option
                        .Select(def =>
                        {
                            Action valueAction = () => value = def;
                            return new FloatMenuOption(def.label, valueAction);
                        })
                        .ToList();
                    buttonMenu = new FloatMenu(options);
                }
                return buttonMenu;
            }
        }

        public DefSmartSetting() : base() { }

        public DefSmartSetting(string labelKey, T value, T defaultValue, string tooltipKey = null, List<T> whiteList = null, List<T> blackList = null) : base(labelKey, value, defaultValue, tooltipKey)
        {
            this.whiteList = whiteList;
            this.blackList = blackList;
        }

        private float ButtonWidth => Text.CalcSize(value.ToString() + "    ").x;

        protected override float RequiredHeight(Listing_Standard list)
        {
            float labelWidth = list.ColumnWidth - ButtonWidth;
            return Text.CalcHeight(Label, labelWidth);
        }

        public override void DoSetting(Listing_Standard list)
        {
            base.DecorateSetting(list, out Rect rect);
            UIUtility.SplitRectVertically(rect, out Rect labelRect, out Rect buttonRect, -1, ButtonWidth);
            Widgets.Label(labelRect, Label);

            if (Widgets.ButtonText(buttonRect, value.label))
            {
                Find.WindowStack.Add(ButtonMenu);
            }
        }

        public IEnumerable<T> ValidOptions()
        {
            foreach (T value in DefDatabase<T>.AllDefsListForReading)
            {
                if (blackList?.Contains(value) == true)
                {
                    continue;
                }
                if (whiteList?.Contains(value) == false)
                {
                    continue;
                }
                yield return value;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            if(Scribe.mode == LoadSaveMode.Saving)
            {
                unresolvedValue = value.defName;
                Scribe_Values.Look(ref unresolvedValue, "unresolvedValue");
                unresolvedDefaultValue = defaultValue.defName;
                Scribe_Values.Look(ref unresolvedDefaultValue, "unresolvedDefaultValue");
            }
            else if(Scribe.mode == LoadSaveMode.LoadingVars)
            {
                Scribe_Values.Look(ref unresolvedValue, "unresolvedValue");
                Scribe_Values.Look(ref unresolvedDefaultValue, "unresolvedDefaultValue");
            }
            Scribe_Collections.Look(ref whiteList, "whiteList", LookMode.Value);
            Scribe_Collections.Look(ref blackList, "blackList", LookMode.Value);
        }

        public override void DefsLoaded()
        {
            if(unresolvedValue != null)
            {
                value = DefDatabase<T>.GetNamed(unresolvedValue);
            }
            if(unresolvedDefaultValue != null)
            {
                defaultValue = DefDatabase<T>.GetNamed(unresolvedDefaultValue);
            }
        }
    }
}
