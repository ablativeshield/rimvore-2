﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HugsLib.Core;
using Verse;

namespace RimVore2
{
    public static class SettingsUpdater
    {
        public static void UpdateSettings()
        {
            Version currentVersion = GetVersion();
            Version lastVersion = RV2Mod.settings.LastSavedVersion == null ? new Version(0, 0) : new Version(RV2Mod.settings.LastSavedVersion);
            //Log.Message("Last saved version: " + lastVersion + " current version: " + currentVersion);
            // settings refactor to SmartSetting classes
            if (lastVersion < new Version("0.6.0"))
            {
                UpdateVersionZero();
            }

            RV2Mod.settings.LastSavedVersion = currentVersion.ToString();
            RV2Mod.settings.Write();
        }

        private static void UpdateVersionZero()
        {
            NukeSettings();
            Log.Warning("RV2 Settings have been wiped due to outdated version / fresh install. Easier than updating for both you and me, trust me. -Nabber");
        }

        public static void NukeSettings()
        {
            LoadedModManager.WriteModSettings(RV2Mod.mod.Content.FolderName, typeof(RV2Mod).Name, null);
            RV2Mod.settings.Write();
        }

        public static Version GetVersion()
        {
            //Log.Message("all package ids: " + String.Join(", ", LoadedModManager.RunningModsListForReading.Select(m => m.PackageId)));
            ModContentPack pack = LoadedModManager.RunningModsListForReading.Find(mod => mod.PackageId == "nabber.rimvore2");
            Version version = ManifestFile.TryParse(pack)?.Version;
            if(version == null)
            {
                Log.Warning("Version could not be parsed correctly, returning 0.0");
                version = new Version(0, 0);
            }
            return version;
        }
    }
}
