﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class Window_RuleEditor : Window
    {
        private readonly VoreRule rule;
        private readonly VoreRule oldRule;
        private readonly VoreRuleTarget target;
        private readonly VoreRuleTarget oldTarget;
        private readonly bool isFirstRule = false;

        public void DoWindow()
        {
            absorbInputAroundWindow = true;
            focusWhenOpened = true;
            draggable = false;
            onlyOneOfTypeAllowed = true;
        }

        public Window_RuleEditor(VoreRuleTarget target, VoreRule rule, bool isFirstRule = false)
        {
            this.rule = rule;
            this.oldRule = this.rule.Copy();
            this.target = target;
            this.oldTarget = this.target.Copy();
            this.isFirstRule = isFirstRule;
        }

        public override Vector2 InitialSize
        {
            get
            {
                return new Vector2(UI.screenWidth * 0.4f, UI.screenHeight * 0.6f);
            }
        }

        public override void DoWindowContents(Rect inRect)
        {
            float exitButtonHeight = 40f;
            Listing_Standard list = new Listing_Standard()
            {
                ColumnWidth = inRect.width
            };
            list.Begin(inRect);
            if (!isFirstRule)
            {
                target.DoTargetControls(list, isFirstRule);
                list.GapLine();
            }
            Rect controlRect = list.GetRect(inRect.height - list.CurHeight - exitButtonHeight);
            rule.DoControls(controlRect, target.targetRole, isFirstRule);
            Rect exitButtonRect = list.GetRect(exitButtonHeight);
            UIUtility.DoSaveCancelButtons(exitButtonRect, Save, Cancel);
            list.End();
        }

        private void Save()
        {

            RV2Mod.settings.rules.NotifyStale();
            Close();
        }
        private void Cancel()
        {
            target.CopyInto(oldTarget);
            rule.CopyInto(oldRule);
            Close();
        }
    }



}