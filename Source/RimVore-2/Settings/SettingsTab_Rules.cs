﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class SettingsTab_Rules : SettingsTab
    {
        public SettingsTab_Rules(string label, Action clickedAction, bool selected) : base(label, clickedAction, selected) { }
        public SettingsTab_Rules(string label, Action clickedAction, Func<bool> selected) : base(label, clickedAction, selected) { }


        public override SettingsContainer AssociatedContainer => RV2Mod.settings.rules;
        public SettingsContainer_Rules Rules => (SettingsContainer_Rules)AssociatedContainer;

        private bool heightStale = true;
        private float height = 0f;
        private Vector2 scrollPosition;
        public override void FillRect(Rect inRect)
        {
            #region scrollViewStart
#if v1_2
            Listing_Standard list = new Listing_Standard()
            {
                ColumnWidth = inRect.width
            };
            list.Begin(inRect);
            Rect outerRect = list.GetRect(inRect.height - list.CurHeight); ;
            list.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Rect innerRect);
#else
            Rect outerRect = inRect;
            UIUtility.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Listing_Standard list);
#endif
            #endregion

            DoSettingsButtons(list);
            list.GapLine();


            DoHeaderRow(list);
            list.GapLine();
            for (int i = 0; i < Rules.Rules.ToList().Count; i++)
            {
                KeyValuePair<VoreRuleTarget, VoreRule> rule = Rules.Rules.ElementAt(i);
                DoRuleRow(list, i, rule);
            }

            if (list.ButtonImage(UIUtility.AddButtonTexture, UIUtility.ImageButtonWithLabelSize, UIUtility.ImageButtonWithLabelSize))
            {
                Rules.Rules.Add(new VoreRuleTarget(), new VoreRule(RuleState.Copy));
                Rules.NotifyStale();
                height += Text.LineHeight;
            }

            #region scrollViewEnd
#if v1_2
            list.EndScrollView(ref height, ref heightStale, ref innerRect);
            list.End();
#else
            list.EndScrollView(ref height, ref heightStale);
#endif
            #endregion
        }

        private void DoSettingsButtons(Listing_Standard list)
        {
            string resetButtonLabel = "RV2_Settings_Rules_Reset".Translate();
            string savePresetLabel = "RV2_Settings_Rules_SavePreset".Translate();
            string loadPresetLabel = "RV2_Settings_Rules_LoadPreset".Translate();
            string removePresetLabel = "RV2_Settings_Rules_RemovePreset".Translate();
            Vector2 resetButtonSize = Text.CalcSize(UIUtility.ButtonStringPadding + resetButtonLabel + UIUtility.ButtonStringPadding);
            Vector2 savePresetSize = Text.CalcSize(UIUtility.ButtonStringPadding + savePresetLabel + UIUtility.ButtonStringPadding);
            Vector2 loadPresetSize = Text.CalcSize(UIUtility.ButtonStringPadding + loadPresetLabel + UIUtility.ButtonStringPadding);
            Vector2 removePresetSize = Text.CalcSize(UIUtility.ButtonStringPadding + removePresetLabel + UIUtility.ButtonStringPadding);
            float maxRequiredHeight = Mathf.Max(resetButtonSize.y, savePresetSize.y, loadPresetSize.y, removePresetSize.y);
            Rect rowRect = list.GetRect(maxRequiredHeight);
            float buttonPadding = 5f;
            Rect resetButtonRect = new Rect(rowRect.x, rowRect.y, resetButtonSize.x, rowRect.height);
            Rect savePresetRect = new Rect(resetButtonRect.x + resetButtonRect.width + buttonPadding, rowRect.y, savePresetSize.x, rowRect.height);
            Rect loadPresetRect = new Rect(savePresetRect.x + savePresetRect.width + buttonPadding, rowRect.y, loadPresetSize.x, rowRect.height);
            Rect removePresetRect = new Rect(loadPresetRect.x + loadPresetRect.width + buttonPadding, rowRect.y, removePresetSize.x, rowRect.height);
            if (Widgets.ButtonText(resetButtonRect, resetButtonLabel))
            {
                Rules.Reset();
            }
            if (Widgets.ButtonText(savePresetRect, savePresetLabel))
            {
                OpenSavePresetMenu();
            }
            if (Rules.Presets.Count >= 1)
            {
                if (Widgets.ButtonText(loadPresetRect, loadPresetLabel))
                {
                    OpenLoadPresetMenu();
                }
                if (Widgets.ButtonText(removePresetRect, removePresetLabel))
                {
                    OpenRemovePresetMenu();
                }
            }
        }
        private void DoHeaderRow(Listing_Standard list)
        {
            int fontSize = Text.CurFontStyle.fontSize;
            Text.CurFontStyle.fontSize = 18;
            int columnCount = 3;
            string nameLabel = "RV2_Settings_Rules_Name".Translate();
            string controlsLabel = "RV2_Settings_Rules_Controls".Translate();
            float columnWidth = list.ColumnWidth / 3;
            float requiredHeight = Mathf.Max(Text.CalcHeight(nameLabel, columnWidth), Text.CalcHeight(controlsLabel, columnWidth));
            Rect rowRect = list.GetRect(requiredHeight);
            List<Rect> columns = UIUtility.CreateColumns(rowRect, columnCount, out _, 0, 0, 5f);
            Widgets.Label(columns[0], nameLabel);
            DoQuickAccessHeader(columns[1]);
            //Widgets.Label(columns[1], quickAccessLabel);
            Widgets.Label(columns[2], controlsLabel);
            Text.CurFontStyle.fontSize = fontSize;
        }
        private static void DoQuickAccessHeader(Rect inRect)
        {
            List<Texture2D> buttons = new List<Texture2D>() {
                ContentFinder<Texture2D>.Get("Widget/auto_vore")
            };
            List<string> buttonTooltips = new List<string>()
            {
                "RV2_Settings_Rules_AllowedToAutoVore".Translate()
            };
            foreach (DesignationDef designation in RV2_Common.VoreDesignations)
            {
                buttons.Add(ContentFinder<Texture2D>.Get(designation.iconPathEnabledAutomatically));
                buttonTooltips.Add(designation.description);
            }
            UIUtility.ButtonImages(inRect, buttons, new List<Action>(), buttonTooltips, null, 24f);
        }
        
        private void DoRuleRow(Listing_Standard list, int index, KeyValuePair<VoreRuleTarget, VoreRule> rule)
        {
            string ruleLabel = (index + 1) + ": " + rule.Key.GetName();
            int columnCount = 3;
            float rowHeight = Text.CalcHeight(ruleLabel, list.ColumnWidth / columnCount);
            Rect rowRect = list.GetRect(rowHeight);
            List<Rect> columns = UIUtility.CreateColumns(rowRect, columnCount, out _, 0, 0, 5f);
            Widgets.Label(columns[0], ruleLabel);
            bool isFirstRule = index == 0;
            DoRuleQuickAccessButtons(columns[1], rule, isFirstRule);
            DoRuleControlButtons(columns[2], index, rule);
        }

        private void DoRuleQuickAccessButtons(Rect inRect, KeyValuePair<VoreRuleTarget, VoreRule> rule, bool isFirstRule = false)
        {
            VoreRule ruleValue = rule.Value;
            VoreRuleTarget target = rule.Key;
            List<Texture2D> buttons = new List<Texture2D>();
            List<Action> buttonActions = new List<Action>();
            List<string> buttonTooltips = new List<string>();
            //bool displayPreyButtons = target.targetRole == IdentifierRole.Both || target.targetRole == IdentifierRole.Prey;
            //bool displayPredatorButtons = target.targetRole == IdentifierRole.Both || target.targetRole == IdentifierRole.Predator;

            // auto vore
            buttons.Add(GetStateTexture(ruleValue.AllowedInAutoVore));
            buttonTooltips.Add(GetStateTooltip(ruleValue.AllowedInAutoVore));
            // first rule cycles between ON and OFF, all other rules can also COPY
            if (isFirstRule)
            {
                if (ruleValue.AllowedInAutoVore == RuleState.On)
                {
                    buttonActions.Add(delegate ()
                    {
                        ruleValue.AllowedInAutoVore = RuleState.Off;
                        Rules.NotifyStale();
                    });
                }
                else
                {
                    buttonActions.Add(delegate ()
                    {
                        ruleValue.AllowedInAutoVore = RuleState.On;
                        Rules.NotifyStale();
                    });
                }
            }
            else
            {
                buttonActions.Add(delegate ()
                {
                    ruleValue.AllowedInAutoVore = ruleValue.AllowedInAutoVore.Next();
                    Rules.NotifyStale();
                });
            }
            List<DesignationDef> designations = RV2_Common.VoreDesignations;

            foreach (DesignationDef designation in designations)
            {
                // if the current designation does not apply to the rule, add a blank space
                if (!designation.CanBeAssignedTo(target.targetRole))
                {
                    buttons.Add(UIUtility.BlankButtonTexture);
                }
                // otherwise do the state button
                else
                {
                    string designationKey = designation.defName;
                    // take the rules current state
                    RuleState currentState = ruleValue.DesignationStates.TryGetValue(designationKey, RuleState.On);
                    buttons.Add(GetStateTexture(currentState));
                    buttonTooltips.Add(GetStateTooltip(currentState));
                    // special handling for first rule, switch between ON and OFF
                    if (isFirstRule)
                    {
                        if (currentState == RuleState.On)
                        {
                            buttonActions.Add(delegate ()
                            {
                                ruleValue.DesignationStates.SetOrAdd(designationKey, RuleState.Off);
                                Rules.NotifyStale();
                            });
                        }
                        else
                        {
                            buttonActions.Add(delegate ()
                            {
                                ruleValue.DesignationStates.SetOrAdd(designationKey, RuleState.On);
                                Rules.NotifyStale();
                            });
                        }
                    }
                    // normal handling for other rules - cycle through enum
                    else
                    {
                        buttonActions.Add(delegate ()
                        {
                            ruleValue.DesignationStates.SetOrAdd(designationKey, currentState.Next());
                            Rules.NotifyStale();
                        });
                    }
                }
            }
            UIUtility.ButtonImages(inRect, buttons, buttonActions, buttonTooltips, null, 24f);
        }

        public static void DoLabeledRuleStateControl(Listing_Standard list, string label, ref RuleState currentState, string tooltip = null)
        {
            string stateTooltip = GetStateTooltip(currentState);
            if (tooltip == null)
            {
                tooltip = stateTooltip;
            }
            else if (stateTooltip != null)
            {
                tooltip += "\n\n" + stateTooltip;
            }

            list.LabeledCheckbox(label, ref currentState, SettingsContainer_Rules.ruleStateIcons, tooltip, 24f);
        }

        public void DoRuleStateControl(Rect inRect, ref RuleState currentState, string tooltip = null)
        {
            UIUtility.Checkbox(inRect, ref currentState, SettingsContainer_Rules.ruleStateIcons, tooltip);
        }

        private static string GetStateTooltip(RuleState state)
        {
            switch (state)
            {
                case RuleState.Copy:
                    return "RV2_Settings_Rules_RuleStateCopy".Translate();
                case RuleState.Off:
                    return "RV2_Settings_Rules_RuleStateOff".Translate();
                case RuleState.On:
                    return "RV2_Settings_Rules_RuleStateOn".Translate();
                default:
                    return null;
            }
        }

        public Texture2D GetStateTexture(RuleState state)
        {
            switch (state)
            {
                case RuleState.Copy:
                    return UIUtility.CopyTexture;
                case RuleState.Off:
                    return UIUtility.CheckOffTexture;
                case RuleState.On:
                    return UIUtility.CheckOnTexture;
                default:
                    return default(Texture2D);
            }
        }

        private void DoRuleControlButtons(Rect inRect, int index, KeyValuePair<VoreRuleTarget, VoreRule> rule)
        {

            Action moveDown = () => Rules.MoveRuleDown(index);
            Action moveUp = () => Rules.MoveRuleDown(index);
            // the first entry must never be removed or moved!
            bool isFirstEntry = index == 0;
            //Action edit = () => Find.WindowStack.Add(new Window_RuleEditor(rule.Key, rule.Value));
            Action edit = delegate ()
            {
                //rules.Remove(rule.Key);
                Find.WindowStack.Add(new Window_RuleEditor(rule.Key, rule.Value, isFirstEntry));
                //rules.Add(rule.Key, rule.Value);
                //SetRulesStale();
            };
            Action remove = delegate ()
            {
                Rules.Rules.Remove(rule.Key);
                Rules.NotifyStale();
            };
            List<Texture2D> buttons = new List<Texture2D> { UIUtility.EditButtonTexture };
            List<Action> buttonActions = new List<Action>() { edit };
            List<string> buttonTooltips = new List<string>() { "RV2_Settings_Edit".Translate() };
            // the second rule should not have a MoveUp button
            bool canBeMovedUp = index > 1;
            bool isLastEntry = rule.Equals(Rules.Rules.Last());
            if (canBeMovedUp)
            {
                buttons.Add(UIUtility.MoveUpButtonTexture);
                buttonActions.Add(moveUp);
                buttonTooltips.Add("RV2_Settings_MoveUp".Translate());
            }
            if (!isLastEntry && !isFirstEntry)
            {
                buttons.Add(UIUtility.MoveDownButtonTexture);
                buttonActions.Add(moveDown);
                buttonTooltips.Add("RV2_Settings_MoveDown".Translate());
            }
            if (Rules.Rules.Count != 1 && !isFirstEntry)
            {
                buttons.Add(UIUtility.RemoveButtonTexture);
                buttonActions.Add(remove);
                buttonTooltips.Add("RV2_Settings_Remove".Translate());
            }
            UIUtility.ButtonImages(inRect, buttons, buttonActions, buttonTooltips, null, 20f);
            //list.ButtonImages(buttons, buttonActions, buttonTooltips, null, (index + 1) + ". " + rule.Key.GetName());
        }
        
        #region preset menu
        private void OpenSavePresetMenu()
        {
            Action<string> save = delegate (string content)
            {
                if (string.IsNullOrEmpty(content))
                {
                    return;
                }
                Rules.Presets.SetOrAdd(content, new VoreRulePreset(Rules.Rules));
            };
            Action cancel = () => { };
            Window_CustomTextField textFieldWindow = new Window_CustomTextField("", save, cancel);
            Find.WindowStack.Add(textFieldWindow);
        }

        private void OpenLoadPresetMenu()
        {
            Action<string> load = delegate (string presetName)
            {
                RV2Log.Message("Loading preset " + presetName);
                if (Rules.Presets.ContainsKey(presetName))
                {
                    Rules.Rules = new Dictionary<VoreRuleTarget, VoreRule>(Rules.Presets[presetName].rules);
                    Rules.NotifyStale();
                }
            };
            List<FloatMenuOption> options = new List<FloatMenuOption>();
            foreach (string presetName in Rules.Presets.Keys.ToList())
            {
                options.Add(new FloatMenuOption(presetName, () => load(presetName)));
            }
            Find.WindowStack.Add(new FloatMenu(options));
        }

        private void OpenRemovePresetMenu()
        {
            Action<string> remove = delegate (string presetName)
            {
                RV2Log.Message("Removing preset " + presetName);
                if (Rules.Presets.ContainsKey(presetName))
                {
                    Rules.Presets.Remove(presetName);
                }
            };
            List<FloatMenuOption> options = new List<FloatMenuOption>();
            foreach (string presetName in Rules.Presets.Keys.ToList())
            {
                options.Add(new FloatMenuOption(presetName, () => remove(presetName)));
            }
            Find.WindowStack.Add(new FloatMenu(options));
        }
        #endregion
    }
}
