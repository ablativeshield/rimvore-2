﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class RV2Mod : Mod
    {
        public static RV2Settings settings;
        public static RV2Mod mod;
        private static int currentTabIndex;
        private static List<TabRecord> tabs;
        private SettingsTab CurrentTab => tabs.Count > currentTabIndex ? (SettingsTab)tabs[currentTabIndex] : null;

        public RV2Mod(ModContentPack content) : base(content)
        {
            settings = GetSettings<RV2Settings>();  // create !static! settings
            mod = this;
        }

        public void DefsLoaded()
        {
            settings.DefsLoaded();
        }

        public override string SettingsCategory()
        {
            return "RV2_Settings_Category".Translate();
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            InitializeTabs();
            Listing_Standard list = new Listing_Standard();
            list.Begin(inRect);

            Rect tabRect = new Rect(inRect);
            Rect drawRect = new Rect(inRect);
            drawRect = drawRect.ContractedBy(10f);
            drawRect.height -= 30f;
            TabDrawer.DrawTabs(tabRect, tabs, 1);
            CurrentTab?.FillRect(drawRect);

            list.End();
        }

        private void InitializeTabs()
        {
            if(tabs == null)
            {
                tabs = new List<TabRecord>();
            }
            if (Prefs.DevMode)
            {
                AddTab(new SettingsTab_Debug("RV2_Settings_TabNames_Debug".Translate(), null, null));
            }
            AddTab(new SettingsTab_Features("RV2_Settings_TabNames_Features".Translate(), null, null));
            AddTab(new SettingsTab_FineTuning("RV2_Settings_TabNames_FineTuning".Translate(), null, null));
            AddTab(new SettingsTab_Sounds("RV2_Settings_TabNames_Sounds".Translate(), null, null));
            AddTab(new SettingsTab_Cheats("RV2_Settings_TabNames_Cheats".Translate(), null, null));
            AddTab(new SettingsTab_Rules("RV2_Settings_TabNames_Rules".Translate(), null, null));
            AddTab(new SettingsTab_Quirks("RV2_Settings_TabNames_Quirks".Translate(), null, null));

        }
        Func<SettingsTab, Action> clickedAction = (SettingsTab tab) => () => currentTabIndex = tabs.IndexOf(tab);
        Func<SettingsTab, Func<bool>> selectedGetter = (SettingsTab tab) => () => currentTabIndex == tabs.IndexOf(tab);
        // the RecordTab constructor is bad, so we call it with NULL clickedAction and selectedGetter in order to add them afterwards with reference to the created tab
        void AddTab(SettingsTab tab)
        {
            // each tab type can only exist once
            if(tabs.Any(t => t.GetType() == tab.GetType()))
            {
                // Log.Message("tabs already contains tab of type " + tab.GetType().ToString());
                return;
            }
            tab.clickedAction = clickedAction(tab);
            tab.selectedGetter = selectedGetter(tab);
            tabs.Add(tab);
        }

    }
}
