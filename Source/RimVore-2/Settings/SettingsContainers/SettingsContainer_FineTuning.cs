﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class SettingsContainer_FineTuning : SettingsContainer
    {
        public SettingsContainer_FineTuning() { }

        public BoolSmartSetting showInvalidRMBOptions;
        public BoolSmartSetting forbidDigestedCorpse;
        public EnumSmartSetting<DefaultStripSetting> defaultStripBehaviour;
        public BoolSmartSetting reformHumanoidsAsNewborn;
        public BoolSmartSetting manhuntersVoreDownedPawns;
        public FloatSmartSetting huntingAnimalsVoreChance;
        public FloatSmartSetting raiderVorenappingChance;
        public FloatSmartSetting mTBVoreProposalCreation;
        public FloatSmartSetting forcedVoreChanceOnFailedProposal;
        public BoolSmartSetting prisonersMustConsentToProposal;
        public BoolSmartSetting autoAcceptAnimal;
        public FloatSmartSetting autoAcceptAnimalSkill;
        public BoolSmartSetting autoAcceptSocial;
        public FloatSmartSetting autoAcceptSocialSkillDifference;
        public EnumSmartSetting<NotificationType> proposalDeniedNotification;
        public EnumSmartSetting<NotificationType> proposalAcceptedNotification;
        public EnumSmartSetting<NotificationType> fatalProposalAcceptedNotification;
       
        public bool ShowInvalidRMBOptions => showInvalidRMBOptions.value;
        public bool ForbidDigestedCorpse => forbidDigestedCorpse.value;
        public DefaultStripSetting DefaultStripBehaviour => defaultStripBehaviour.value;
        public bool ReformHumanoidsAsNewborn => reformHumanoidsAsNewborn.value;
        public bool ManhuntersVoreDownedPawns => manhuntersVoreDownedPawns.value;
        public float HuntingAnimalsVoreChance => huntingAnimalsVoreChance.value / 100;
        public float RaiderVorenappingChance => raiderVorenappingChance.value / 100;
        public float MTBVoreProposalCreation => mTBVoreProposalCreation.value;
        public float ForcedVoreChanceOnFailedProposal => forcedVoreChanceOnFailedProposal.value / 100;
        public bool PrisonersMustConsentToProposal => prisonersMustConsentToProposal.value;
        public bool AutoAcceptAnimal => autoAcceptAnimal.value;
        public int AutoAcceptAnimalSkill => Mathf.RoundToInt(autoAcceptAnimalSkill.value);
        public bool AutoAcceptSocial => autoAcceptSocial.value;
        public int AutoAcceptSocialSkillDifference => Mathf.RoundToInt(autoAcceptSocialSkillDifference.value);
        public NotificationType ProposalDeniedNotification => proposalDeniedNotification.value;
        public NotificationType ProposalAcceptedNotification => proposalAcceptedNotification.value;
        public NotificationType FatalProposalAcceptedNotification => fatalProposalAcceptedNotification.value;

        public override void EnsureSmartSettingDefinition()
        {
            if (showInvalidRMBOptions == null || showInvalidRMBOptions.IsInvalid())
                showInvalidRMBOptions = new BoolSmartSetting("RV2_Settings_General_DisplayUnavailableRMBOptions", true, true, "RV2_Settings_General_DisplayUnavailableRMBOptions_Tip");
            if (forbidDigestedCorpse == null || forbidDigestedCorpse.IsInvalid())
                forbidDigestedCorpse = new BoolSmartSetting("RV2_Settings_FineTuning_ForbidDigestedCorpse", false, false, "RV2_Settings_FineTuning_DefaultStripBehaviour_Tip");
            if (defaultStripBehaviour == null || defaultStripBehaviour.IsInvalid())
                defaultStripBehaviour = new EnumSmartSetting<DefaultStripSetting>("RV2_Settings_FineTuning_DefaultStripBehaviour", DefaultStripSetting.Random, DefaultStripSetting.Random, "RV2_Settings_FineTuning_DefaultStripBehaviour_Tip");
            if (reformHumanoidsAsNewborn == null || reformHumanoidsAsNewborn.IsInvalid())
                reformHumanoidsAsNewborn = new BoolSmartSetting("RV2_Settings_FineTuning_ReformHumanoidsAsNewborn", true, true, "RV2_Settings_FineTuning_ReformHumanoidsAsNewborn_Tip");
            if (manhuntersVoreDownedPawns == null || manhuntersVoreDownedPawns.IsInvalid())
                manhuntersVoreDownedPawns = new BoolSmartSetting("RV2_Settings_FineTuning_ManhuntersVoreDownedPawns", true, true, "RV2_Settings_FineTuning_ManhuntersVoreDownedPawn_Tip");
            if (huntingAnimalsVoreChance == null || huntingAnimalsVoreChance.IsInvalid())
                huntingAnimalsVoreChance = new FloatSmartSetting("RV2_Settings_FineTuning_HuntingAnimalsVoreChance", 50f, 50f, 0f, 100f, "RV2_Settings_FineTuning_HuntingAnimalsVoreChance_Tip", "0", "%");
            if (raiderVorenappingChance == null || raiderVorenappingChance.IsInvalid())
                raiderVorenappingChance = new FloatSmartSetting("RV2_Settings_FineTuning_RaiderVorenappingChance", 59f, 50f, 0f, 100f, "RV2_Settings_FineTuning_RaiderVorenappingChance_Tip", "0", "%");
            if (mTBVoreProposalCreation == null || mTBVoreProposalCreation.IsInvalid())
                mTBVoreProposalCreation = new FloatSmartSetting("RV2_Settings_FineTuning_MTBVoreProposalCreation", 12f, 12f, 1f, 999f, "RV2_Settings_FineTuning_MTBVoreProposalCreation_Tip", "0");
            if (forcedVoreChanceOnFailedProposal == null || forcedVoreChanceOnFailedProposal.IsInvalid())
                forcedVoreChanceOnFailedProposal = new FloatSmartSetting("RV2_Settings_FineTuning_ForcedVoreChanceOnFailedProposal", 5f, 5f, 0f, 100f, "RV2_Settings_FineTuning_ForcedVoreChanceOnFailedProposal_Tip", "0", "%");
            if (prisonersMustConsentToProposal == null || prisonersMustConsentToProposal.IsInvalid())
                prisonersMustConsentToProposal = new BoolSmartSetting("RV2_Settings_FineTuning_PrisonersMustConsentToProposal", false, false, "RV2_Settings_FineTuning_PrisonersMustConsentToProposal_Tip");
            if (autoAcceptAnimal == null || autoAcceptAnimal.IsInvalid())
                autoAcceptAnimal = new BoolSmartSetting("RV2_Settings_FineTuning_AutoAcceptAnimal", true, true, "RV2_Settings_FineTuning_AutoAcceptAnimal_Tip");
            if (autoAcceptAnimalSkill == null || autoAcceptAnimalSkill.IsInvalid())
                autoAcceptAnimalSkill = new FloatSmartSetting("RV2_Settings_FineTuning_AutoAcceptAnimal", 15f, 15f, 1f, 20f, "RV2_Settings_FineTuning_AutoAcceptAnimal_Tip", "0");
            if (autoAcceptSocial == null || autoAcceptSocial.IsInvalid())
                autoAcceptSocial = new BoolSmartSetting("RV2_Settings_FineTuning_AutoAcceptSocial", true, true, "RV2_Settings_FineTuning_AutoAcceptSocial_Tip");
            if (autoAcceptSocialSkillDifference == null || autoAcceptSocialSkillDifference.IsInvalid())
                autoAcceptSocialSkillDifference = new FloatSmartSetting("RV2_Settings_FineTuning_AutoAcceptSocialSkillDifference", 12f, 12f, 1f, 20f, "RV2_Settings_FineTuning_AutoAcceptSocialSkillDifference_Tip", "0");
            if (proposalDeniedNotification == null || proposalDeniedNotification.IsInvalid())
                proposalDeniedNotification = new EnumSmartSetting<NotificationType>("RV2_Settings_FineTuning_ProposalDeniedNotification", NotificationType.MessageThreatSmall, NotificationType.MessageThreatSmall);
            if (proposalAcceptedNotification == null || proposalAcceptedNotification.IsInvalid())
                proposalAcceptedNotification = new EnumSmartSetting<NotificationType>("RV2_Settings_FineTuning_ProposalAcceptedNotification", NotificationType.MessageThreatSmall, NotificationType.MessageThreatSmall);
            if (fatalProposalAcceptedNotification == null || fatalProposalAcceptedNotification.IsInvalid())
                fatalProposalAcceptedNotification = new EnumSmartSetting<NotificationType>("RV2_Settings_FineTuning_FatalProposalAcceptedNotification", NotificationType.MessageThreatSmall, NotificationType.MessageThreatSmall);
        }

        public override void Reset()
        {
            showInvalidRMBOptions.Reset();
            forbidDigestedCorpse.Reset();
            defaultStripBehaviour.Reset();
            reformHumanoidsAsNewborn.Reset();
            manhuntersVoreDownedPawns.Reset();
            huntingAnimalsVoreChance.Reset();
            raiderVorenappingChance.Reset();
            mTBVoreProposalCreation.Reset();
            forcedVoreChanceOnFailedProposal.Reset();
            prisonersMustConsentToProposal.Reset();
            autoAcceptAnimal.Reset();
            autoAcceptAnimalSkill.Reset();
            autoAcceptSocial.Reset();
            autoAcceptSocialSkillDifference.Reset();
            proposalDeniedNotification.Reset();
            proposalAcceptedNotification.Reset();
            fatalProposalAcceptedNotification.Reset();
        }

        private bool heightStale = true;
        private float height = 0f;
        private Vector2 scrollPosition;
        public void FillRect(Rect inRect)
        {
            #region scrollViewStart
#if v1_2
            Listing_Standard list = new Listing_Standard()
            {
                ColumnWidth = inRect.width
            };
            list.Begin(inRect);
            Rect outerRect = list.GetRect(inRect.height - list.CurHeight); ;
            list.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Rect innerRect);
#else
            Rect outerRect = inRect;
            UIUtility.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Listing_Standard list);
#endif
            #endregion

            if (list.ButtonText("RV2_Settings_Reset".Translate()))
                Reset();

            showInvalidRMBOptions.DoSetting(list);
            forbidDigestedCorpse.DoSetting(list);
            defaultStripBehaviour.DoSetting(list);
            reformHumanoidsAsNewborn.DoSetting(list);
            manhuntersVoreDownedPawns.DoSetting(list);
            huntingAnimalsVoreChance.DoSetting(list);
            raiderVorenappingChance.DoSetting(list);
            mTBVoreProposalCreation.DoSetting(list);
            forcedVoreChanceOnFailedProposal.DoSetting(list);
            prisonersMustConsentToProposal.DoSetting(list);
            autoAcceptAnimal.DoSetting(list);
            if (AutoAcceptAnimal)
                autoAcceptAnimalSkill.DoSetting(list);
            autoAcceptSocial.DoSetting(list);
            if (AutoAcceptSocial)
                autoAcceptSocialSkillDifference.DoSetting(list);
            proposalDeniedNotification.DoSetting(list);
            proposalAcceptedNotification.DoSetting(list);
            fatalProposalAcceptedNotification.DoSetting(list);

            #region scrollViewEnd
#if v1_2
            list.EndScrollView(ref height, ref heightStale, ref innerRect);
            list.End();
#else
            list.EndScrollView(ref height, ref heightStale);
#endif
            #endregion
        }

        public override void DefsLoaded()
        {
            base.DefsLoaded();
            defaultStripBehaviour.valuePresenter = (DefaultStripSetting s) => s == DefaultStripSetting.AlwaysStrip ? "RV2_Settings_General_StrippingAlways".Translate() : s == DefaultStripSetting.NeverStrip ? "RV2_Settings_General_StrippingNever".Translate() : "RV2_Settings_General_StrippingRandom".Translate();
            Func<NotificationType, string> notificationPresenter = (NotificationType n) =>
            {
                switch (n)
                {
                    case NotificationType.MessageNeutral:
                        return "RV2_Settings_MessageNeutral".Translate();
                    case NotificationType.MessageThreatSmall:
                        return "RV2_Settings_MessageThreatSmall".Translate();
                    case NotificationType.MessageThreatBig:
                        return "RV2_Settings_MessageThreatBig".Translate();
                    case NotificationType.Letter:
                        return "RV2_Settings_Letter".Translate();
                    case NotificationType.LetterThreatSmall:
                        return "RV2_Settings_LetterThreatSmall".Translate();
                    case NotificationType.LetterThreatBig:
                        return "RV2_Settings_LetterThreatBig".Translate();
                    default:
                        return n.ToString();
                }
            };
            proposalDeniedNotification.valuePresenter = notificationPresenter;
            proposalAcceptedNotification.valuePresenter = notificationPresenter;
            fatalProposalAcceptedNotification.valuePresenter = notificationPresenter;
        }

        public override void ExposeData()
        {
            Scribe_Deep.Look(ref showInvalidRMBOptions, "showInvalidRMBOptions", new object[0]);
            Scribe_Deep.Look(ref forbidDigestedCorpse, "forbidDigestedCorpse", new object[0]);
            Scribe_Deep.Look(ref defaultStripBehaviour, "defaultStripBehaviour", new object[0]);
            Scribe_Deep.Look(ref reformHumanoidsAsNewborn, "reformHumanoidsAsNewborn", new object[0]);
            Scribe_Deep.Look(ref manhuntersVoreDownedPawns, "manhuntersVoreDownedPawns", new object[0]);
            Scribe_Deep.Look(ref huntingAnimalsVoreChance, "huntingAnimalsVoreChance", new object[0]);
            Scribe_Deep.Look(ref raiderVorenappingChance, "raiderVorenappingChance", new object[0]);
            Scribe_Deep.Look(ref mTBVoreProposalCreation, "mTBVoreProposalCreation", new object[0]);
            Scribe_Deep.Look(ref forcedVoreChanceOnFailedProposal, "forcedVoreChanceOnFailedProposal", new object[0]);
            Scribe_Deep.Look(ref prisonersMustConsentToProposal, "prisonersMustConsentToProposal", new object[0]);
            Scribe_Deep.Look(ref autoAcceptAnimal, "autoAcceptAnimal", new object[0]);
            Scribe_Deep.Look(ref autoAcceptAnimalSkill, "autoAcceptAnimalSkill", new object[0]);
            Scribe_Deep.Look(ref autoAcceptSocial, "autoAcceptSocial", new object[0]);
            Scribe_Deep.Look(ref autoAcceptSocialSkillDifference, "autoAcceptSocialSkillDifference", new object[0]);
            Scribe_Deep.Look(ref proposalDeniedNotification, "proposalDeniedNotification", new object[0]);
            Scribe_Deep.Look(ref proposalAcceptedNotification, "proposalAcceptedNotification", new object[0]);
            Scribe_Deep.Look(ref fatalProposalAcceptedNotification, "fatalProposalAcceptedNotification", new object[0]);

            PostExposeData();
        }
    }
}
