﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class SettingsContainer_Debug : SettingsContainer
    {
        private BoolSmartSetting logging;
        private BoolSmartSetting verboseLogging;
        private BoolSmartSetting passPredatorTicks;
        private FloatSmartSetting hediffLabelRefreshInterval;

        public SettingsContainer_Debug() { }
        public bool Logging => logging.value;
        public bool VerboseLogging => verboseLogging.value;
        public bool PassPredatorTicks => passPredatorTicks.value;
        public int HediffLabelRefreshInterval => (int)hediffLabelRefreshInterval.value;

        public override bool DevModeOnly => true;

        public override void Reset()
        {
            logging.Reset();
            verboseLogging.Reset();
            passPredatorTicks.Reset();
            hediffLabelRefreshInterval.Reset();
        }

        public override void EnsureSmartSettingDefinition()
        {
            if (logging == null || logging.IsInvalid())
                logging = new BoolSmartSetting("RV2_Settings_Debug_Logging", false, false);
            if (verboseLogging == null || verboseLogging.IsInvalid())
                verboseLogging = new BoolSmartSetting("RV2_Settings_Debug_VerboseLogging", false, false);
            if (passPredatorTicks == null || passPredatorTicks.IsInvalid())
                passPredatorTicks = new BoolSmartSetting("RV2_Settings_Debug_PassPredatorTick", true, true);
            if (hediffLabelRefreshInterval == null || hediffLabelRefreshInterval.IsInvalid())
                hediffLabelRefreshInterval = new FloatSmartSetting("RV2_Settings_Debug_LabelRefresh", 150, 150, 1, 9999, null, "#");
        }

        public void FillRect(Rect inRect)
        {
            Listing_Standard list = new Listing_Standard();
            list.Begin(inRect);

            if (list.ButtonText("RV2_Settings_Reset".Translate()))
                Reset();

            logging.DoSetting(list);
            verboseLogging.DoSetting(list);
            passPredatorTicks.DoSetting(list);
            hediffLabelRefreshInterval.DoSetting(list);

            list.End();
        }

        public override void ExposeData()
        {
            Scribe_Deep.Look(ref logging, "logging", new object[0]);
            Scribe_Deep.Look(ref verboseLogging, "verboseLogging", new object[0]);
            Scribe_Deep.Look(ref passPredatorTicks, "passPredatorTicks", new object[0]);
            Scribe_Deep.Look(ref hediffLabelRefreshInterval, "hediffLabelRefreshInterval", new object[0]);

            PostExposeData();
        }
    }
}
