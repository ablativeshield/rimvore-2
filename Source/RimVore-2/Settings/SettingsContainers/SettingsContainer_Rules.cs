﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class SettingsContainer_Rules : SettingsContainer
    {
        public SettingsContainer_Rules() { }

        public Dictionary<string, VoreRulePreset> Presets = new Dictionary<string, VoreRulePreset>();
        private Dictionary<VoreRuleTarget, VoreRule> rules;
        public Dictionary<VoreRuleTarget, VoreRule> Rules
        {
            get
            {
                if (rules.EnumerableNullOrEmpty())
                {
                    rules = new Dictionary<VoreRuleTarget, VoreRule>()
                    {
                        {  new VoreRuleTarget(){ targetType = IdentifierType.Everyone }, new VoreRule(RuleState.On) }
                    };
                }
                return rules;
            }
            set
            {
                NotifyStale();
                rules = value;
            }
        }
        public static List<Texture2D> ruleStateIcons;

        public void NotifyStale()
        {
            cachedPawnRules.Clear();
            RV2_Patch_UI_Widget_GetGizmos.NotifyAllStale();
            RecacheAllDesignations();
            VoreInteractionManager.Reset();
        }

        public void MoveRuleDown(int index)
        {
            Rules = Rules
                .Move(index, index + 1)
                .ToDictionary();
        }

        public void MoveRuleUp(int index)
        {
            Rules = Rules
                .Move(index, index - 1)
                .ToDictionary();
        }

        private void RecacheAllDesignations()
        {
            try
            {
                IEnumerable<PawnData> pawnDataList = SaveStorage.DataStore?.PawnData?.Values
                .Where(pd => pd.IsValid);
                if (pawnDataList.EnumerableNullOrEmpty())
                {
                    RV2Log.Message("No pawn data to recache", true);
                    return;
                }
                RV2Log.Message("Recaching pawndatas: " + string.Join(", ", pawnDataList.Select(pd => pd.Pawn?.LabelShort)), true);
                foreach (PawnData pawnData in pawnDataList)
                {
                    RV2Log.Message("Recaching " + pawnData.Pawn?.LabelShort + " designations", true);
                    List<DesignationDef> designations = RV2_Common.VoreDesignations;
                    if (designations.NullOrEmpty())
                    {
                        RV2Log.Error("No designationDefs to recache for");
                        return;
                    }
                    foreach (DesignationDef designation in designations)
                    {
                        string key = designation.defName;
                        Func<VoreRule, RuleState> stateGetter = (VoreRule rule) => rule.DesignationStates.TryGetValue(key);
                        VoreRule appliedRule = GetFinalRule(pawnData.Pawn, IdentifierRole.Both, stateGetter);
                        if (appliedRule == null)
                        {
                            RV2Log.Message("Fatal error, no rule to recalculate designations with", true, true, "Settings");
                            return;
                        }
                        bool designationActive = appliedRule.DesignationStates.TryGetValue(key) == RuleState.On;
                        /*if (!pawnData.Designations.ContainsKey(designation))
                        {
                            pawnData.Designations.Add(designation, default(Designation));
                        }*/
                        pawnData.Designations[designation].enabledAuto = designationActive;
                    }
                }
            }
            catch (Exception e)
            {
                RV2Log.Error("The accursed RecacheAllDesignations fucked up. Pawn designations were not properly recached, reason: " + e);
            }
        }

        #region rule retrieval
        // I hate this, but this is the best solution I can think of to provide a multi-key dictionary
        private class RuleCacheable
        {
            readonly Pawn pawn;
            readonly IdentifierRole role;
            readonly Func<VoreRule, RuleState> stateGetter;
            public RuleCacheable(Pawn pawn, IdentifierRole role, Func<VoreRule, RuleState> stateGetter)
            {
                this.pawn = pawn;
                this.role = role;
                this.stateGetter = stateGetter;
            }

        }

        public IEnumerable<VoreRule> ApplicableRules(Pawn pawn, IdentifierRole role)
        {
            return Rules
               .Where(rule => rule.Key.AppliesTo(pawn, role))
               .Select(rule => rule.Value);
        }

        private readonly Dictionary<RuleCacheable, VoreRule> cachedPawnRules = new Dictionary<RuleCacheable, VoreRule>();
        public VoreRule GetFinalRule(IEnumerable<VoreRule> applicableRules, Func<VoreRule, RuleState> stateGetter)
        {
            // Log.Message(string.Join(", ", applicableRules.Select(rule => stateGetter(rule).ToString())));
            applicableRules = applicableRules
                .Where(rule => stateGetter(rule) != RuleState.Copy);
            if (applicableRules.EnumerableNullOrEmpty())
            {
                string errorMessage = "RimVore2: NO FINAL RULE FOUND, this is a critical error, forcing all rules to be reset and using first rule.";
                NotificationUtility.DoNotification(NotificationType.MessageThreatBig, errorMessage);
                Reset();
                return Rules.First().Value;
            }
            return applicableRules
                .Last(rule => stateGetter(rule) == RuleState.On || stateGetter(rule) == RuleState.Off);
        }


        public VoreRule GetFinalRule(Pawn pawn, IdentifierRole role, Func<VoreRule, RuleState> stateGetter)
        {
            RuleCacheable cacheKey = new RuleCacheable(pawn, role, stateGetter);
            if (cachedPawnRules.ContainsKey(cacheKey))
            {
                RV2Log.Message("Getting cached final rule for pawn " + pawn.LabelShort + ", role " + role + " stategetter hash: " + stateGetter.GetHashCode(), true, true);
                return cachedPawnRules[cacheKey];
            }
            VoreRule finalRule = GetFinalRule(ApplicableRules(pawn, role), stateGetter);
            cachedPawnRules.SetOrAdd(cacheKey, finalRule);
            return finalRule;
        }
        #endregion

        #region outside communication

        public bool DesignationActive(Pawn pawn, DesignationDef designation)
        {
            RV2Log.Message("calculating enabled designation for " + pawn.LabelShort, true, true);
            string key = designation.defName;
            Func<VoreRule, RuleState> stateGetter = (VoreRule rule) => rule.DesignationStates.TryGetValue(key);
            VoreRule finalRule = GetFinalRule(pawn, designation.assignedTo, stateGetter);
            // Log.Message(LogUtility.ToString(finalRule.DesignationStates));
            return finalRule.DesignationStates.TryGetValue(key) == RuleState.On;
        }
        public bool VorePathEnabled(Pawn pawn, IdentifierRole role, VorePathDef path, bool isForAuto = false)
        {
            Func<VoreRule, RuleState> stateGetter = (VoreRule r) => r.UseVorePathRules;
            VoreRule rule = GetFinalRule(pawn, role, stateGetter);
            VorePathRule pathRule = rule.GetPathRule(path.defName);
            bool enabled = pathRule.Enabled;
            if (isForAuto)
            {
                // Log.Message("Checking for auto-vore validity, is auto enabled? " + pathRule.AutoVoreEnabled);
                enabled &= pathRule.AutoVoreEnabled;
            }
            return enabled;
        }
        public ThingDef GetContainer(Pawn predator, VorePathDef pathDef)
        {
            Func<VoreRule, RuleState> stateGetter = (VoreRule rule) => rule.UseVorePathRules;
            VoreRule finalRule = GetFinalRule(predator, IdentifierRole.Predator, stateGetter);
            VorePathRule pathRule = finalRule.GetPathRule(pathDef.defName);
            ThingDef container = pathRule.Container;
            return container;
        }
        public bool HasValidAge(Pawn pawn, IdentifierRole role = IdentifierRole.Both)
        {
            int requiredAge = GetValidAge(pawn, role);
            return pawn.ageTracker?.AgeBiologicalYears >= requiredAge;
        }
        public int GetValidAge(Pawn pawn, IdentifierRole role = IdentifierRole.Both)
        {
            Func<VoreRule, RuleState> ageStateGetter = (VoreRule rule) => rule.ConsiderMinimumAge;
            VoreRule activeRule = GetFinalRule(pawn, role, ageStateGetter);
            bool needsAgeCheck = activeRule.ConsiderMinimumAge == RuleState.On;
            if (!needsAgeCheck)
            {
                return -1;
            }
            return activeRule.MinimumAge;
        }
        public bool AllowedInAutoVore(Pawn pawn) => GetFinalRule(pawn, IdentifierRole.Both, (VoreRule rule) => rule.AllowedInAutoVore).AllowedInAutoVore == RuleState.Off;
        public CorpseProcessingType GetCorpseProcessingType(Pawn pawn, VorePathDef path)
        {
            VoreRule finalRule = RV2Mod.settings.rules.GetFinalRule(pawn, IdentifierRole.Predator, (VoreRule rule) => rule.UseVorePathRules);
            string pathKey = path.defName;
            if (finalRule != null)
            {
                VorePathRule pathRule = finalRule.GetPathRule(pathKey);
                return pathRule.CorpseProcessingType;
            }

            QuirkManager quirkManager = pawn.QuirkManager(false);
            if (quirkManager?.HasSpecialFlag("DestroyPawn") == true)
            {
                return CorpseProcessingType.Destroy;
            }

            return CorpseProcessingType.Dessicate;
        }

        public bool CanBeProposedTo(Pawn pawn, IdentifierRole role = IdentifierRole.Both)
        {
            Func<VoreRule, RuleState> proposalStateGetter = (VoreRule rule) => rule.CanBeProposedTo;
            VoreRule activeRule = GetFinalRule(pawn, role, proposalStateGetter);
            bool canBeProposedTo = activeRule.CanBeProposedTo == RuleState.On;
            return canBeProposedTo;
        }

        public bool CanForceFailedProposal(Pawn initiator, Pawn target, IdentifierRole initiatorRole = IdentifierRole.Both, IdentifierRole targetRole = IdentifierRole.Both)
        {
            Func<VoreRule, RuleState> initiatorStateGetter = (VoreRule rule) => rule.CanForceFailedProposals;
            Func<VoreRule, RuleState> targetStateGetter = (VoreRule rule) => rule.CanBeForcedFailedProposals;
            VoreRule initiatorRule = GetFinalRule(initiator, initiatorRole, initiatorStateGetter);
            VoreRule targetRule = GetFinalRule(target, targetRole, targetStateGetter);
            bool valid = initiatorRule.CanForceFailedProposals == RuleState.On && targetRule.CanBeForcedFailedProposals == RuleState.On;
            //Log.Message(initiator.LabelShort + " can vore " + target.LabelShort + " ? " + valid);
            return valid;
        }

        public bool CanBeForcedIfDowned(Pawn pawn, IdentifierRole role = IdentifierRole.Both)
        {
            Func<VoreRule, RuleState> ruleStateGetter = (VoreRule rule) => rule.CanBeForcedIfDowned;
            VoreRule activeRule = GetFinalRule(pawn, role, ruleStateGetter);
            return activeRule.CanBeForcedIfDowned == RuleState.On;
        }

        #endregion

        public override void Reset()
        {
            rules = null;
            NotifyStale();
        }

        public override void ExposeData()
        {
            ScribeUtilities.ScribeVariableDictionary(ref rules, "rules", LookMode.Deep, LookMode.Deep);
            ScribeUtilities.ScribeVariableDictionary(ref Presets, "presets", LookMode.Value, LookMode.Deep);
        }

        public override void DefsLoaded()
        {
            ruleStateIcons = new List<Texture2D>()
            {
                UIUtility.CheckOnTexture,
                UIUtility.CheckOffTexture,
                UIUtility.CopyTexture
            };
            foreach (VoreRule rule in Rules.Values)
            {
                rule.DefsLoaded();
            }
        }

        public override void EnsureSmartSettingDefinition()
        {
            // nothing to do here for rules settings
        }
    }
}
