﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class SettingsContainer_Features : SettingsContainer
    {
        public SettingsContainer_Features() { }

        private BoolSmartSetting voreQuirksEnabled;
        private BoolSmartSetting fatalVoreEnabled;
        private BoolSmartSetting endoVoreEnabled;
        private BoolSmartSetting feederVoreEnabled;
        private BoolSmartSetting animalsEnabled;
        private BoolSmartSetting showVoreTab;
        private BoolSmartSetting showRMBVoreMenu;
        private BoolSmartSetting showGizmo;
        private BoolSmartSetting scatEnabled;
        private BoolSmartSetting bonesEnabled;
        private BoolSmartSetting backstoryGenitalsEnabled;

        public bool VoreQuirksEnabled => voreQuirksEnabled.value;
        public bool FatalVoreEnabled => fatalVoreEnabled.value;
        public bool EndoVoreEnabled => endoVoreEnabled.value;
        public bool FeederVoreEnabled => feederVoreEnabled.value;
        public bool AnimalsEnabled => animalsEnabled.value;
        public bool ShowVoreTab => showVoreTab.value;
        public bool ShowRMBVoreMenu => showRMBVoreMenu.value;
        public bool ShowGizmo => showGizmo.value;
        public bool ScatEnabled => scatEnabled.value;
        public bool BonesEnabled => bonesEnabled.value;
        public bool BackstoryGenitalsEnabled => backstoryGenitalsEnabled.value;


        public override void Reset()
        {
            voreQuirksEnabled.Reset();
            fatalVoreEnabled.Reset();
            endoVoreEnabled.Reset();
            feederVoreEnabled.Reset();
            animalsEnabled.Reset();
            showVoreTab.Reset();
            showRMBVoreMenu.Reset();
            showGizmo.Reset();
            scatEnabled.Reset();
            bonesEnabled.Reset();
            backstoryGenitalsEnabled.Reset();
        }

        public override void EnsureSmartSettingDefinition()
        {
            if (voreQuirksEnabled == null || voreQuirksEnabled.IsInvalid())
                voreQuirksEnabled = new BoolSmartSetting("RV2_Settings_Features_VoreQuirksEnabled", true, true, "RV2_Settings_Features_VoreQuirksEnabled_Tip");
            if (fatalVoreEnabled == null || fatalVoreEnabled.IsInvalid())
                fatalVoreEnabled = new BoolSmartSetting("RV2_Settings_Features_FatalVoreEnabled", true, true, "RV2_Settings_Features_FatalVoreEnabled_Tip");
            if (endoVoreEnabled == null || endoVoreEnabled.IsInvalid())
                endoVoreEnabled = new BoolSmartSetting("RV2_Settings_Features_EndoVoreEnabled", true, true, "RV2_Settings_Features_EndoVoreEnabled_Tip");
            if (feederVoreEnabled == null || feederVoreEnabled.IsInvalid())
                feederVoreEnabled = new BoolSmartSetting("RV2_Settings_Features_FeederVoreEnabled", true, true, "RV2_Settings_Features_FeederVoreEnabled_Tip");
            if (animalsEnabled == null || animalsEnabled.IsInvalid())
                animalsEnabled = new BoolSmartSetting("RV2_Settings_Features_AnimalsEnabled", true, true, "RV2_Settings_Features_AnimalsEnabled_Tip");
            if (showVoreTab == null || showVoreTab.IsInvalid())
                showVoreTab = new BoolSmartSetting("RV2_Settings_Features_ShowVoreTab", true, true, "RV2_Settings_Features_ShowVoreTab_Tip");
            if (showRMBVoreMenu == null || showRMBVoreMenu.IsInvalid())
                showRMBVoreMenu = new BoolSmartSetting("RV2_Settings_Features_ShowRMBVoreMenu", true, true, "RV2_Settings_Features_ShowRMBVoreMenu_Tip");
            if (showGizmo == null || showGizmo.IsInvalid())
                showGizmo = new BoolSmartSetting("RV2_Settings_Features_ShowGizmo", true, true, "RV2_Settings_Features_ShowGizmo_Tip");
            if (scatEnabled == null || scatEnabled.IsInvalid())
                scatEnabled = new BoolSmartSetting("RV2_Settings_Features_ScatEnabled", true, true, "RV2_Settings_Features_ScatEnabled_Tip");
            if (bonesEnabled == null || bonesEnabled.IsInvalid())
                bonesEnabled = new BoolSmartSetting("RV2_Settings_Features_BonesEnabled", true, true, "RV2_Settings_Features_BonesEnabled_Tip");
            if (backstoryGenitalsEnabled == null || backstoryGenitalsEnabled.IsInvalid())
                backstoryGenitalsEnabled = new BoolSmartSetting("RV2_Settings_Features_BackstoryGenitalsEnabled", true, true, "RV2_Settings_Features_BackstoryGenitalsEnabled_Tip");
        }

        private bool heightStale = true;
        private float height = 0f;
        private Vector2 scrollPosition;
        public void FillRect(Rect inRect)
        {
            #region scrollViewStart
#if v1_2
            Listing_Standard list = new Listing_Standard()
            {
                ColumnWidth = inRect.width
            };
            list.Begin(inRect);
            Rect outerRect = list.GetRect(inRect.height - list.CurHeight); ;
            list.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Rect innerRect);
#else
            Rect outerRect = inRect;
            UIUtility.MakeAndBeginScrollView(outerRect, height, ref scrollPosition, out Listing_Standard list);
#endif
            #endregion

            if (list.ButtonText("RV2_Settings_Reset".Translate()))
                Reset();

            voreQuirksEnabled.DoSetting(list);
            fatalVoreEnabled.DoSetting(list);
            endoVoreEnabled.DoSetting(list);
            feederVoreEnabled.DoSetting(list);
            animalsEnabled.DoSetting(list);
            showVoreTab.DoSetting(list);
            showRMBVoreMenu.DoSetting(list);
            showGizmo.DoSetting(list);
            scatEnabled.DoSetting(list);
            bonesEnabled.DoSetting(list);
            backstoryGenitalsEnabled.DoSetting(list);

            #region scrollViewEnd
#if v1_2
            list.EndScrollView(ref height, ref heightStale, ref innerRect);
            list.End();
#else
            list.EndScrollView(ref height, ref heightStale);
#endif
            #endregion
        }

        public override void ExposeData()
        {
            Scribe_Deep.Look(ref voreQuirksEnabled, "VoreQuirksEnabled", new object[0]);
            Scribe_Deep.Look(ref fatalVoreEnabled, "FatalVoreEnabled", new object[0]);
            Scribe_Deep.Look(ref endoVoreEnabled, "EndoVoreEnabled", new object[0]);
            Scribe_Deep.Look(ref feederVoreEnabled, "FeederVoreEnabled", new object[0]);
            Scribe_Deep.Look(ref animalsEnabled, "AnimalsEnabled", new object[0]);
            Scribe_Deep.Look(ref showVoreTab, "ShowVoreTab", new object[0]);
            Scribe_Deep.Look(ref showRMBVoreMenu, "ShowRMBVoreMenu", new object[0]);
            Scribe_Deep.Look(ref showGizmo, "ShowGizmo", new object[0]);
            Scribe_Deep.Look(ref scatEnabled, "ScatEnabled", new object[0]);
            Scribe_Deep.Look(ref bonesEnabled, "BonesEnabled", new object[0]);
            Scribe_Deep.Look(ref backstoryGenitalsEnabled, "BackstoryGenitalsEnabled", new object[0]);

            PostExposeData();
        }
    }

}
