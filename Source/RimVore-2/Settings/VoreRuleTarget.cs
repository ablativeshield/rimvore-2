﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public class VoreRuleTarget : IExposable
    {
        public IdentifierType targetType;
        public string targetRace;
        public ThingDef TargetRace => targetRace == null ? null : ThingDef.Named(targetRace);
        public string targetAnimal;
        public PawnKindDef TargetAnimal => targetAnimal == null ? null : PawnKindDef.Named(targetAnimal);
        public RelationKind targetRelation;
        public FoodTypeFlags targetFoodType;
        public IdentifierRole targetRole;
        public Gender targetGender;
        public string customName = "";

        public void CopyInto(VoreRuleTarget target)
        {
            targetType = target.targetType;
            targetRace = target.targetRace;
            targetAnimal = target.targetAnimal;
            targetRelation = target.targetRelation;
            targetFoodType = target.targetFoodType;
            targetRole = target.targetRole;
            targetGender = target.targetGender;
            customName = string.Copy(target.customName);
        }

        public VoreRuleTarget Copy()
        {
            return new VoreRuleTarget()
            {
                targetType = this.targetType,
                targetRace = this.targetRace,
                targetAnimal = this.targetAnimal,
                targetRelation = this.targetRelation,
                targetFoodType = this.targetFoodType,
                targetRole = this.targetRole,
                targetGender = this.targetGender,
                customName = string.Copy(this.customName)
            };
        }

        public string GetTargetTypeKey()
        {
            switch (targetType)
            {
                case IdentifierType.Everyone:
                    return "Base";
                case IdentifierType.Relation:
                    return targetRelation.ToString();
                case IdentifierType.Race:
                    return targetRace;
                case IdentifierType.Animal:
                    return targetAnimal;
                case IdentifierType.FoodTypeFlag:
                    return targetFoodType.ToString();
                case IdentifierType.Gender:
                    return targetGender.ToString();
                default:
                    return null;
            }
        }

        public string GetName()
        {
            if (customName == null || customName == "")
            {
                return this.ToString();
            }
            return customName;
        }

        public void DoTargetControls(Listing_Standard list, bool isFirstRule = false)
        {
            list.ColumnWidth = list.ColumnWidth / 2;
            var oldFontSize = Text.CurFontStyle.fontSize;
            Text.CurFontStyle.fontSize = 20;
            Action<IdentifierType> identifierTypeSelection = (IdentifierType newType) => targetType = newType;
            list.HeaderLabel("RV2_Settings_Rules_TargetHeader".Translate());

            customName = list.DoLabelledTextField("RV2_Settings_Rules_Name".Translate(), customName);
            list.EnumLabeled("RV2_Settings_Rules_IdentifierType".Translate(), targetType, identifierTypeSelection, "RV2_Settings_Rules_IdentifierType_Tip".Translate());
            //list.CreateLabelledDropDownForEnum("RV2_Settings_Rules_IdentifierType".Translate(), identifierTypeSelection, targetType, null, "RV2_Settings_Rules_IdentifierType_Tip".Translate());
            List<string> options = new List<string>();
            switch (targetType)
            {
                case IdentifierType.Everyone:

                    break;
                case IdentifierType.Relation:
                    Action<RelationKind> relationSelection = (RelationKind newRelation) => targetRelation = newRelation;
                    List<RelationKind> relationBlacklist = new List<RelationKind>()
                    {
                        RelationKind.Invalid
                    };
                    list.EnumLabeled("", targetRelation, relationSelection, null, null, null, relationBlacklist);
                    //list.CreateLabelledDropDownForEnum("", relationSelection, targetRelation, null, null, null, null, null, relationBlacklist);
                    break;
                case IdentifierType.Race:
                    Action<ThingDef> raceSelection = (ThingDef newRace) => targetRace = newRace.defName;
                    Func<ThingDef, string> raceLabelGetter = (ThingDef race) => race.LabelCap;
                    Func<ThingDef, string> raceTooltipGetter = (ThingDef race) => race.description;
                    list.CreateLabelledDropDown(ModAdapter.HAR.SortedAlienRaces, TargetRace, raceLabelGetter, raceSelection, null, "", null, raceTooltipGetter);
                    break;
                case IdentifierType.Animal:
                    Action<PawnKindDef> animalSelection = (PawnKindDef newAnimal) => targetAnimal = newAnimal.defName;
                    Func<PawnKindDef, string> animalLabelGetter = (PawnKindDef animal) => animal.LabelCap;
                    Func<PawnKindDef, string> animalTooltipGetter = (PawnKindDef animal) => animal.description;
                    list.CreateLabelledDropDown(RV2_Common.AnimalRaces, TargetAnimal, animalLabelGetter, animalSelection, null, "", null, animalTooltipGetter);
                    break;
                case IdentifierType.FoodTypeFlag:
                    Action<FoodTypeFlags> foodTypeSelection = (FoodTypeFlags foodType) => targetFoodType = foodType;
                    Func<FoodTypeFlags, string> foodTypeLabelGetter = (FoodTypeFlags foodType) => foodType.ToString();
                    List<FoodTypeFlags> foodTypeWhiteList = new List<FoodTypeFlags>()
                    {
                        FoodTypeFlags.VegetableOrFruit,
                        FoodTypeFlags.Meat,
                        FoodTypeFlags.Corpse,
                        FoodTypeFlags.Seed,
                        FoodTypeFlags.AnimalProduct,
                        FoodTypeFlags.Plant,
                        FoodTypeFlags.Tree,
                        FoodTypeFlags.Meal,
                        FoodTypeFlags.Processed
                    };
                    list.EnumLabeled("", targetFoodType, foodTypeSelection, null, foodTypeLabelGetter, foodTypeWhiteList);
                    //list.CreateLabelledDropDownForEnum("", foodTypeSelection, targetFoodType, null, null, null, null, null, null, foodTypeWhiteList);
                    break;
                case IdentifierType.Gender:
                    Action<Gender> genderSelection = (Gender newGender) => targetGender = newGender;
                    Func<Gender, string> genderLabelGetter = (Gender gender) => gender.ToString();
                    list.EnumLabeled("", targetGender, genderSelection);
                    break;
                default:
                    RV2Log.Warning("Unknown IdentifierType");
                    return;
            }
            Action<IdentifierRole> identifierRoleSelection = (IdentifierRole newRole) => targetRole = newRole;
            list.EnumLabeled("RV2_Settings_Rules_IdentifierRole".Translate(), targetRole, identifierRoleSelection, "RV2_Settings_Rules_IdentifierRole_Tip".Translate());
            //list.CreateLabelledDropDownForEnum("RV2_Settings_Rules_IdentifierRole".Translate(), identifierRoleSelection, targetRole, null, "RV2_Settings_Rules_IdentifierRole_Tip".Translate());
            Text.CurFontStyle.fontSize = oldFontSize;
            list.ColumnWidth *= 2;
        }

        public override string ToString()
        {
            if (targetType == IdentifierType.Everyone) return targetType.ToString() + " - " + targetRole;
            return targetType.ToString() + " - " + GetTargetTypeKey() + " - " + targetRole;
        }

        private bool AppliesToRole(IdentifierRole role)
        {
            // if the rule applies to both, it doesn't matter which role the requestor has, apply the rule
            if (this.targetRole == IdentifierRole.Both)
            {
                return true;
            }
            if(role == IdentifierRole.Both)
            {
                return true;
            }
            // otherwise compare the requestors role with the rules role
            return role == this.targetRole;
        }

        public bool AppliesTo(Pawn pawn, IdentifierRole role)
        {
            if(pawn == null)
            {
                RV2Log.Warning("AppliesTo() called for null pawn, returning false");
                return false;
            }
            if (!AppliesToRole(role))
            {
                return false;
            }
            switch (targetType)
            {
                case IdentifierType.Everyone:
                    return true;
                case IdentifierType.Relation:
                    List<RelationKind> relations;
                    if(!pawn.TryGetRelationKind(out relations))
                    {
                        return false;
                    }
                    return relations.Contains(this.targetRelation);
                case IdentifierType.Race:
                    return this.TargetRace == pawn.def;
                case IdentifierType.Animal:
                    return this.TargetAnimal == pawn.kindDef;
                case IdentifierType.FoodTypeFlag:
                    return pawn.RaceProps?.Eats(this.targetFoodType) == true;
                case IdentifierType.Gender:
                    return pawn.gender == targetGender;
                default:
                    RV2Log.Warning("Fall-through for AppliesTo() triggered, returning false");
                    return false;
            }
        }

        public void ExposeData()
        {
            Scribe_Values.Look(ref targetType, "targetType");
            Scribe_Values.Look(ref targetRace, "targetRace");
            Scribe_Values.Look(ref targetAnimal, "targetAnimal");
            Scribe_Values.Look(ref targetRelation, "targetRelation");
            Scribe_Values.Look(ref targetFoodType, "targetFoodType");
            Scribe_Values.Look(ref targetRole, "targetRole");
            Scribe_Values.Look(ref customName, "customName");
        }
    }
}

// check why first rule is not being detected for paths, all other rules appear to use the everyone rule, except for paths