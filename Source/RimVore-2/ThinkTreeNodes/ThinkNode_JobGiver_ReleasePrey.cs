﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;


namespace RimVore2
{
    public class ThinkNode_JobGiver_ReleasePrey : ThinkNode_JobGiver
    {
        protected override Job TryGiveJob(Pawn pawn)
        {
            // TODO rjw integration for sexual partner search to dump cock vore into
            PawnData pawnData = SaveStorage.DataStore.GetPawnData(pawn);
            Pawn releasePawn = pawnData.VoreTracker.VoreTrackerRecords
                .FindAll(record => record.HasReachedEnd)
                .RandomElement()
                .Prey;
            IntVec3 targetPosition = PositionUtility.GetPreyReleasePosition(pawn);
            RV2Log.Message("Calculated target position " + targetPosition, "Jobs");
            Job releaseJob = JobMaker.MakeJob(VoreJobDefOf.RV2_ReleasePrey, targetPosition, releasePawn);
            return releaseJob;
        }
    }
}
