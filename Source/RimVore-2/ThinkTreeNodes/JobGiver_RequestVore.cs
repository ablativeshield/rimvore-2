﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using RimWorld;

namespace RimVore2
{
    public class JobGiver_RequestVore : ThinkNode_JobGiver
    {
        protected override Job TryGiveJob(Pawn pawn)
        {
            RV2Log.Message("Tried to give request-vore job!", true, false, "AutoVore");
            Map map = pawn.Map;
            if(map == null)
            {
                RV2Log.Message("Early exit: no map determinable", true, false, "AutoVore");
                return null;
            }
            List<Pawn> validTargets = map.mapPawns.FreeColonists;
            validTargets.AddDistinctRange(map.mapPawns.PrisonersOfColony);
            if (RV2Mod.settings.features.AnimalsEnabled)
            {
                validTargets.AddDistinctRange(GetColonyAnimals(map));
            }
            validTargets = validTargets
                .Where(t => !t.InAggroMentalState    // do not propose to hostile pawns
                && pawn.CanReserveAndReach(t, PathEndMode.Touch, Danger.Some)    // check pathing and reservation ability
                && (pawn.CanVore(t, out _) || t.CanVore(pawn, out _))
                && RV2Mod.settings.rules.CanBeProposedTo(t))
                .ToList();    // check if vore is even possible between the pawns
            
            // remove self from targets
            validTargets.Remove(pawn);
            if (validTargets.NullOrEmpty())
            {
                RV2Log.Message("Early exit: no targets at all", true, false, "AutoVore");
                return null;
            }
            Pawn target = RollForTarget(pawn, validTargets);
            if(target == null)
            {
                RV2Log.Message("Early exit: no target determinable", true, false, "AutoVore");
                return null;
            }
            VoreInteraction interaction = new VoreInteraction(pawn, target);
            if (!interaction.TryCalculatePreferredPath(true))
            {
                return null;
            }
            VorePathDef pathDef = interaction.PickedPath;
            RV2Log.Message("Picked path for vore proposal: " + pathDef.defName, true, false, "AutoVore");
            VoreProposal proposal = new VoreProposal(interaction.Predator, interaction.Prey, pawn, target, pathDef);
            VoreJob job = VoreJobMaker.MakeJob(VoreJobDefOf.RV2_ProposeVore, target);
            job.targetA = target;
            job.Proposal = proposal;
            job.VorePath = new VorePath(proposal.VorePath);
            job.Initiator = pawn;
            return job;
        }

        [Obsolete("Move to TargetingUtility")]
        private Pawn RollForTarget(Pawn pawn, List<Pawn> targets)
        {
            Dictionary<Pawn, float> weightedTargets;
            if (!TryMakeWeightedPreferences(pawn, targets, out weightedTargets))
            {
                return null;
            }
            RV2Log.Message("weighted targets: " + LogUtility.ToString(weightedTargets), true, false, "AutoVore");

            Pawn target = weightedTargets.RandomElementByWeight(kvp => kvp.Value).Key;
            RV2Log.Message("Picked target " + target.LabelShort, true, false, "AutoVore");
            return target;
        }

        

        private VoreGoalDef RollForGoal(Pawn pawn, VoreInteraction interaction, VoreRole role)
        {
            Dictionary<VoreGoalDef, float> weightedGoals;
            if (!TryMakeWeightedPreferences<VoreGoalDef>(pawn, interaction.ValidGoals(), role, out weightedGoals))
            {
                return null;
            }

            RV2Log.Message("Weighted goals: " + LogUtility.ToString(weightedGoals), true, false, "AutoVore");
            VoreGoalDef pickedGoal = weightedGoals.RandomElementByWeight(weightedGoal => weightedGoal.Value).Key;
            RV2Log.Message("Picked " + pickedGoal, true, false, "AutoVore");
            return pickedGoal;
        }

        private VoreTypeDef RollForType(Pawn pawn, VoreInteraction interaction, VoreGoalDef goal, VoreRole role)
        {
            Dictionary<VoreTypeDef, float> weightedTypes;
            if (!TryMakeWeightedPreferences<VoreTypeDef>(pawn, interaction.ValidTypes(goal), role, out weightedTypes))
            {
                return null;
            }

            RV2Log.Message("Weighted types: " + LogUtility.ToString(weightedTypes), true, false, "AutoVore");
            VoreTypeDef pickedType = weightedTypes.RandomElementByWeight(weightedType => weightedType.Value).Key;
            RV2Log.Message("Picked " + pickedType, true, false, "AutoVore");
            return pickedType;
        }

        private List<Pawn> GetColonyAnimals(Map map)
        {
            return map.mapPawns.AllPawnsSpawned
                .FindAll(pawn => 
                    pawn.RaceProps?.Animal == true
                    && pawn.Faction == Find.FactionManager.OfPlayer
                );
        }

        private bool TryMakeWeightedPreferences<T>(Pawn pawn, IEnumerable<T> preferenceSource, VoreRole role, out Dictionary<T, float> weightedPreferences) where T : IPreferrable
        {
            weightedPreferences = new Dictionary<T, float>();
            if (preferenceSource.EnumerableNullOrEmpty())
            {
                return false;
            }
            foreach(T source in preferenceSource)
            {
                float preference = source.GetPreference(pawn, role, ModifierOperation.Add);
                if(preference >= 0)
                {
                    //preference++;
                    weightedPreferences.Add(source, preference);
                }
                else
                {
                    RV2Log.Message("Preference for " + typeof(T).ToString() + " is below 0, not adding to valid target list", true, false, "AutoVore");
                }
            }
            return !weightedPreferences.EnumerableNullOrEmpty();
        }
        private bool TryMakeWeightedPreferences(Pawn pawn, IEnumerable<Pawn> targets, out Dictionary<Pawn, float> weightedPreferences)
        {
            weightedPreferences = new Dictionary<Pawn, float>();
            if(targets.EnumerableNullOrEmpty())
            {
                return false;
            }
            foreach(Pawn target in targets)
            {
                float preference = pawn.PreferenceFor(target, ModifierOperation.Add);
                if(preference >= 0)
                {
                    //preference++;
                    weightedPreferences.Add(target, preference);
                }
                else
                {
                    RV2Log.Message("Preference for target is below 0, not adding to valid target list", true, false, "AutoVore");
                }
            }
            return !weightedPreferences.EnumerableNullOrEmpty();
        }

        
    }
}
