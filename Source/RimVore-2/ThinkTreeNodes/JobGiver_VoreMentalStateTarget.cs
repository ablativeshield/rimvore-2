﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace RimVore2
{
    public class JobGiver_VoreMentalStateTarget : ThinkNode_JobGiver
    {
        protected override Job TryGiveJob(Pawn pawn)
        {
            //Log.Message("JobGiver VoreMentalState enter");
            if (!(pawn.MentalState is MentalState_VoreTargeter mentalState))
            {
                RV2Log.Message("Mental state is not VoreTargeter", "MentalStates");
                return null;
            }
            Pawn target = mentalState.CurrentTarget;
            if(target == null)
            {
                RV2Log.Message("No target provided by mental state", "MentalStates");
                return null;
            }
            // if the target has been vored, we need to wait until we recalculate
            if (!target.Spawned)
            {
                return null;
            }
            List<VoreGoalDef> goalWhitelist = mentalState.VoreMentalStateDef.goalWhitelist;
            List<VoreGoalDef> goalBlacklist = mentalState.VoreMentalStateDef.goalBlacklist;
            List<VoreTypeDef> typeWhitelist = mentalState.VoreMentalStateDef.typeWhitelist;
            List<VoreTypeDef> typeBlacklist = mentalState.VoreMentalStateDef.typeBlacklist;
            List<DesignationDef> designationWhitelist = mentalState.VoreMentalStateDef.designationWhitelist;
            List<DesignationDef> designationBlacklist = mentalState.VoreMentalStateDef.designationBlacklist;
            VoreRole initiatorRole = mentalState.InitiatorRole;
            VoreInteraction interaction = new VoreInteraction(pawn, target, null, initiatorRole, false, typeBlacklist, typeWhitelist, goalBlacklist, goalWhitelist, designationBlacklist, designationWhitelist);
            RV2Log.Message(interaction.ToString(), "MentalStates");
            VorePathDef path = interaction.ValidPaths().RandomElementWithFallback();
            if(path == null)
            {
                RV2Log.Message("No path available for interaction", "MentalStates");
                return null;
            }
            JobDef voreJobDef = initiatorRole.GetInitJobDefFor();
            VoreJob job = VoreJobMaker.MakeJob(voreJobDef, target);
            job.targetA = target;
            job.VorePath = new VorePath(path);
            job.Initiator = pawn;
            RV2Log.Message("Giving job "+ job.ToString(), "MentalStates");
            return job;
        }
    }
}
