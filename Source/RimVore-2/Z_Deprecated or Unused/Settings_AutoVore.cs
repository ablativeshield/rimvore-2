﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using UnityEngine;
//using Verse;
//using RimWorld;

//namespace RimVore2
//{
//    public class Settings_AutoVore : ModSettings
//    {
//        public static float AnimalHuntingChanceToBeVore = 0.5f;
//        public static float RaiderKidnapChanceToBeVore = 0.5f;
//        public static bool CanManhuntersVore = true;
//        public static bool AutoAcceptAnimal = true;
//        public static bool AutoAcceptHumanoid = true;
//        public static bool SocialAutoAcceptBasedOnDifference = true;
//        public static bool PrisonersCanBeVored = true;
//        public static bool PrisonerCanDenyProposal = false;
//        public static float DefaultMtbHoursVoreRequests = 12f;
//        public static NotificationType NotificationOnDeniedVoreProposal = NotificationType.MessageThreatSmall;
//        public static NotificationType NotificationOnAcceptedVoreProposal = NotificationType.MessageThreatSmall;
//        public static NotificationType NotificationOnAcceptedFatalVoreProposal = NotificationType.MessageThreatSmall;
//        public static float FailedProposalForceVoreChance = 0.05f;

//        public static void Reset()
//        {
//            AnimalHuntingChanceToBeVore = 0.5f;
//            RaiderKidnapChanceToBeVore = 0.5f;
//            CanManhuntersVore = true;
//            AutoAcceptAnimal = true;
//            AutoAcceptHumanoid = true;
//            SocialAutoAcceptBasedOnDifference = true;
//            PrisonersCanBeVored = true;
//            PrisonerCanDenyProposal = false;
//            DefaultMtbHoursVoreRequests = 12f;
//            NotificationOnDeniedVoreProposal = NotificationType.MessageThreatSmall;
//            NotificationOnAcceptedVoreProposal = NotificationType.MessageThreatSmall;
//            NotificationOnAcceptedFatalVoreProposal = NotificationType.MessageThreatSmall;
//            FailedProposalForceVoreChance = 0.05f;
//    }

//        public static void DoSettingsWindowContents(Rect inRect)
//        {
//            List<Rect> columns = UIUtility.CreateColumns(inRect, 3, out float columnWidth, 0, 30, 12);
//            DoGeneralAutoVore(columns[0]);
//            DoVoreProposalSettings(columns[1]);
//            DoFineTuningSettings(columns[2]);
//        }

//        protected static Vector2 generalAutoVoreScrollPosition;
//        protected static float generalAutoVoreHeight = 0f;
//        protected static bool generalAutoVoreHeightStale = true;
//        protected static void DoGeneralAutoVore(Rect inRect)
//        {
//#if v1_2
//            Listing_Standard list = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            list.Begin(inRect);
//            Rect outerGeneralAutoVoreRect = list.GetRect(inRect.height - list.CurHeight);
//            list.MakeAndBeginScrollView(outerGeneralAutoVoreRect, generalAutoVoreHeight, ref generalAutoVoreScrollPosition, out Rect innerGeneralAutoVoreRect);
//#else
//            Rect outerGeneralAutoVoreRect = inRect;
//            UIUtility.MakeAndBeginScrollView(outerGeneralAutoVoreRect, generalAutoVoreHeight, ref generalAutoVoreScrollPosition, out Listing_Standard list);
//#endif
//            if (list.ButtonText("RV2_Settings_AutoVore_Reset".Translate()))
//            {
//                Reset();
//            }
//            AnimalHuntingChanceToBeVore = list.DoLabelledSlider(AnimalHuntingChanceToBeVore, 0, 1, "RV2_Settings_AutoVore_HuntingAnimalVoreChance".Translate(), "RV2_Settings_AutoVore_HuntingAnimalVoreChance_Tip".Translate(), UIUtility.PercentagePresenter);
//            RaiderKidnapChanceToBeVore = list.DoLabelledSlider(RaiderKidnapChanceToBeVore, 0, 1, "RV2_Settings_AutoVore_RaiderKidnapVoreChance".Translate(), "RV2_Settings_AutoVore_RaiderKidnapVoreChance_Tip".Translate(), UIUtility.PercentagePresenter);
//            list.CheckboxLabeled("RV2_Settings_AutoVore_ManhuntersVore".Translate(), ref CanManhuntersVore, "RV2_Settings_AutoVore_ManhuntersVore_Tip".Translate());
//#if v1_2
//            list.EndScrollView(ref generalAutoVoreHeight, ref generalAutoVoreHeightStale, ref innerGeneralAutoVoreRect);
//            list.End();
//#else
//            list.EndScrollView(ref generalAutoVoreHeight, ref generalAutoVoreHeightStale);
//#endif
//        }

//        protected static Vector2 voreProposalScrollPosition;
//        protected static float voreProposalHeight = 0f;
//        protected static bool voreProposalHeightStale = true;
//        protected static void DoVoreProposalSettings(Rect inRect)
//        {
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            outerList.Begin(inRect);
//            Rect outerVoreProposalsRect = outerList.GetRect(inRect.height - outerList.CurHeight);
//#if v1_2
//            Listing_Standard innerList = outerList;
//            outerList.MakeAndBeginScrollView(outerVoreProposalsRect, voreProposalHeight, ref voreProposalScrollPosition, out Rect innerVoreProposalRect);
//#else
//            UIUtility.MakeAndBeginScrollView(outerVoreProposalsRect, voreProposalHeight, ref voreProposalScrollPosition, out Listing_Standard innerList);
//#endif
//            innerList.CheckboxLabeled("RV2_Settings_AutoVore_AnimalAutoAccept".Translate(), ref AutoAcceptAnimal, "RV2_Settings_AutoVore_AnimalAutoAccept_Tip".Translate());
//            innerList.CheckboxLabeled("RV2_Settings_AutoVore_SocialAutoAccept".Translate(), ref AutoAcceptHumanoid, "RV2_Settings_AutoVore_SocialAutoAccept_Tip".Translate());
//            if (AutoAcceptHumanoid)
//            {
//                innerList.CheckboxLabeled("RV2_Settings_AutoVore_SocialAutoAcceptDifference".Translate(), ref SocialAutoAcceptBasedOnDifference, "RV2_Settings_AutoVore_SocialAutoAcceptDifference_Tip".Translate());
//            }
//            innerList.CheckboxLabeled("RV2_Settings_AutoVore_PrisonersCanbeVored".Translate(), ref PrisonersCanBeVored);
//            if (PrisonersCanBeVored)
//            {
//                innerList.CheckboxLabeled("RV2_Settings_AutoVore_PrisonersMustAgree".Translate(), ref PrisonerCanDenyProposal, "RV2_Settings_AutoVore_PrisonersMustAgree_Tip".Translate());
//            }
//            innerList.CreateLabelledDropDownForEnum<NotificationType>("RV2_Settings_AutoVore_ProposalDeniedNotificationType".Translate(), (NotificationType newType) => NotificationOnDeniedVoreProposal = newType, NotificationOnDeniedVoreProposal);
//            innerList.CreateLabelledDropDownForEnum<NotificationType>("RV2_Settings_AutoVore_ProposalAcceptedNotificationType".Translate(), (NotificationType newType) => NotificationOnAcceptedVoreProposal = newType, NotificationOnAcceptedVoreProposal);
//            innerList.CreateLabelledDropDownForEnum<NotificationType>("RV2_Settings_AutoVore_FatalProposalAcceptedNotificationType".Translate(), (NotificationType newType) => NotificationOnAcceptedFatalVoreProposal = newType, NotificationOnAcceptedFatalVoreProposal);
//#if v1_2
//            outerList.EndScrollView(ref voreProposalHeight, ref voreProposalHeightStale, ref innerVoreProposalRect);
//#else
//            innerList.EndScrollView(ref voreProposalHeight, ref voreProposalHeightStale);
//#endif
//            outerList.End();
//        }

//        protected static Vector2 fineTuningScrollPosition;
//        protected static float fineTuningHeight = 0f;
//        protected static bool fineTuningHeightStale = true;
//        protected static void DoFineTuningSettings(Rect inRect)
//        {
//#if v1_2
//            Listing_Standard list = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            list.Begin(inRect);
//            Rect outerFineTuningsRect = list.GetRect(inRect.height - list.CurHeight);
//            list.MakeAndBeginScrollView(outerFineTuningsRect, fineTuningHeight, ref fineTuningScrollPosition, out Rect innerFineTuningRect);
//#else
//            Rect outerFineTuningsRect = inRect;
//            UIUtility.MakeAndBeginScrollView(outerFineTuningsRect, fineTuningHeight, ref fineTuningScrollPosition, out Listing_Standard list);
//#endif
//            Func<float, string> timePresenter = delegate (float hours)
//            {
//                if (hours < 24) return Math.Round(hours) + " hours";
//                else return Math.Round(hours / 24) + " days, " + Math.Round(hours % 24) + " hours";
//            };
//            DefaultMtbHoursVoreRequests = list.DoLabelledSlider(DefaultMtbHoursVoreRequests, 0.016f, 336f, "RV2_Settings_AutoVore_MtbVoreRequests".Translate(), "RV2_Settings_AutoVore_MtbVoreRequests_Tip".Translate(), timePresenter);
//            FailedProposalForceVoreChance = list.DoLabelledSlider(FailedProposalForceVoreChance, 0, 1, "RV2_Settings_AutoVOre_FailedProposalForceVoreChance".Translate(), "RV2_Settings_AutoVOre_FailedProposalForceVoreChance_Tip".Translate(), UIUtility.PercentagePresenter);
//#if v1_2
//            list.EndScrollView(ref fineTuningHeight, ref fineTuningHeightStale, ref innerFineTuningRect);
//            list.End();
//#else
//        list.EndScrollView(ref fineTuningHeight, ref fineTuningHeightStale);
//#endif
//        }

//        public override void ExposeData()
//        {
//            base.ExposeData();
//            Scribe_Values.Look(ref AnimalHuntingChanceToBeVore, "AnimalHuntingChanceToBeVore", AnimalHuntingChanceToBeVore, true);
//            Scribe_Values.Look(ref RaiderKidnapChanceToBeVore, "RaiderKidnapChanceToBeVore", RaiderKidnapChanceToBeVore, true);
//            Scribe_Values.Look(ref CanManhuntersVore, "CanManhuntersVore", CanManhuntersVore, true);

//            Scribe_Values.Look(ref AutoAcceptAnimal, "AutoAcceptAnimal", AutoAcceptAnimal, true);
//            Scribe_Values.Look(ref AutoAcceptHumanoid, "AutoAcceptHumanoid", AutoAcceptHumanoid, true);
//            Scribe_Values.Look(ref SocialAutoAcceptBasedOnDifference, "SocialAutoAcceptBasedOnDifference", SocialAutoAcceptBasedOnDifference, true);
//            Scribe_Values.Look(ref PrisonersCanBeVored, "PrisonersCanBeVored", PrisonersCanBeVored, true);
//            Scribe_Values.Look(ref PrisonerCanDenyProposal, "PrisonerCanDenyProposal", PrisonerCanDenyProposal, true);
//            Scribe_Values.Look(ref NotificationOnDeniedVoreProposal, "NotificationOnDeniedVoreProposal", NotificationOnDeniedVoreProposal, true);
//            Scribe_Values.Look(ref NotificationOnAcceptedVoreProposal, "NotificationOnAcceptedVoreProposal", NotificationOnAcceptedVoreProposal, true);
//            Scribe_Values.Look(ref NotificationOnAcceptedFatalVoreProposal, "NotificationOnAcceptedFatalVoreProposal", NotificationOnAcceptedFatalVoreProposal, true);

//            Scribe_Values.Look(ref DefaultMtbHoursVoreRequests, "DefaultMtbHoursVoreRequests", DefaultMtbHoursVoreRequests, true);
//            Scribe_Values.Look(ref FailedProposalForceVoreChance, "FailedProposalForceVoreChance", FailedProposalForceVoreChance, true);
//        }
//    }
//}