﻿//using System;
//using UnityEngine;
//using Verse;

//namespace RimVore2
//{
//    // TODO refactor the separate categories to be one category with multiple tabs like MapDesigner
//    public class SettingsController_General : Mod
//    {
//        public SettingsController_General(ModContentPack content) : base(content)
//        {
//            GetSettings<Settings_General>();
//        }

//        public override string SettingsCategory()
//        {
//            return "RV2_Settings_General_Category".Translate();
//        }

//        public override void DoSettingsWindowContents(Rect inRect)
//        {
//            Settings_General.DoSettingsWindowContents(inRect);
//        }
//    }

//    public class SettingsController_Quirks : Mod
//    {
//        public SettingsController_Quirks(ModContentPack content) : base(content)
//        {
//            GetSettings<Settings_Quirks>();
//        }

//        public override string SettingsCategory()
//        {
//            return "RV2_Settings_Quirks_Category".Translate();
//        }

//        public override void DoSettingsWindowContents(Rect inRect)
//        {
//            Settings_Quirks.DoSettingsWindowContents(inRect);
//        }
//    }
//    public class SettingsController_AutoVore : Mod
//    {
//        public SettingsController_AutoVore(ModContentPack content) : base(content)
//        {
//            GetSettings<Settings_AutoVore>();
//        }

//        public override string SettingsCategory()
//        {
//            return "RV2_Settings_AutoVore_Category".Translate();
//        }

//        public override void DoSettingsWindowContents(Rect inRect)
//        {
//            Settings_AutoVore.DoSettingsWindowContents(inRect);
//        }
//    }
//    public class SettingsController_Rules : Mod
//    {
//        public SettingsController_Rules(ModContentPack content) : base(content)
//        {
//            GetSettings<Settings_Rules>();
//        }

//        public override string SettingsCategory()
//        {
//            return "RV2_Settings_Rules_Category".Translate();
//        }

//        public override void DoSettingsWindowContents(Rect inRect)
//        {
//            Settings_Rules.DoSettingsWindowContents(inRect);
//        }
//    }
//}
