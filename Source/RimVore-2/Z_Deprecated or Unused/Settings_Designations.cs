﻿//using System.Collections.Generic;
//using UnityEngine;
//using Verse;
//using AlienRace;
//using System.Linq;

//namespace RimVore2
//{
//    class Settings_Designations : ModSettings
//    {
//        public static int minimumAge = 18;
//        // could key this on the actual Def, but why bother? We don't need any actual values except for an identifier
//        public static Dictionary<string, VoreDesignationOptions> RaceOptions = new Dictionary<string, VoreDesignationOptions>();
//        public static Dictionary<string, VoreDesignationOptions> RelationOptions = new Dictionary<string, VoreDesignationOptions>();
//        public static Dictionary<string, VoreDesignationOptions> AnimalOptions = new Dictionary<string, VoreDesignationOptions>();


//        protected static Vector2 relationsScrollPosition;
//        protected static float relationsScrollHeight = 0f;
//        protected static bool relationsScrollHeightStale = true;

//        protected static Vector2 racesScrollPosition;
//        protected static float racesScrollHeight = 0f;
//        protected static bool racesScrollHeightStale = true;

//        protected static Vector2 animalsScrollPosition;
//        protected static float animalsScrollHeight = 0f;
//        protected static bool animalsScrollHeightStale = true;

//        public static void Reset()
//        {
//            RaceOptions = new Dictionary<string, VoreDesignationOptions>();
//            RelationOptions = new Dictionary<string, VoreDesignationOptions>();
//            AnimalOptions = new Dictionary<string, VoreDesignationOptions>();
//            relationsScrollHeight = 0f;
//            relationsScrollHeightStale = true;
//            racesScrollHeight = 0f;
//            racesScrollHeightStale = true;
//            animalsScrollHeight = 0f;
//            animalsScrollHeightStale = true;
//        }

//        protected static int ColumnCount
//        {
//            get
//            {
//                int count = 2;
//                if (Settings_General.AnimalsCanVore) count++;
//                return count;
//            }
//        }

//        public static void DoSettingsWindowContents(Rect inRect)
//        {
//            List<Rect> columns = UIUtility.CreateColumns(inRect, ColumnCount, out float columnWidth);
//            Listing_Standard list = new Listing_Standard()
//            {
//                ColumnWidth = columnWidth
//            };
//            DoRelationsSettings(columns[0]);
//            DoRaceSettings(columns[1]);
//            if(ColumnCount < 3)
//            {
//                return;
//            }
//            DoAnimalsSettings(columns[2]);

//        }

//        private static void DoRelationsSettings(Rect inRect)
//        {
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            outerList.Begin(inRect);
//            outerList.ColumnWidth = inRect.width;
//            if (outerList.ButtonText("RV2_Settings_Designations_ResetDesignations".Translate()))
//            {
//                Reset();
//            }
//            outerList.Label("RV2_Settings_Designations_MinimumAge".Translate() + ": " + minimumAge.ToString());
//            minimumAge = (int)outerList.Slider(minimumAge, 0, 99);
//            outerList.Gap();
//            outerList.Label("RV2_Settings_Designations_ColonyRelationOptions".Translate());
//            outerList.GapLine();
//            Rect outerRect = outerList.GetRect(inRect.height - outerList.CurHeight);
//#if v1_2
//            Listing_Standard innerList = outerList;
//            outerList.MakeAndBeginScrollView(outerRect, relationsScrollHeight, ref relationsScrollPosition, out Rect innerRect);
//#else
//            UIUtility.MakeAndBeginScrollView(outerRect, relationsScrollHeight, ref relationsScrollPosition, out Listing_Standard innerList);
//#endif
//            foreach (string relationKind in RV2_Common.RelationKinds)
//            {
//                if (!RelationOptions.ContainsKey(relationKind))
//                {
//                    RelationOptions.Add(relationKind, new VoreDesignationOptions());
//                }
//                VoreDesignationOptions options = RelationOptions[relationKind];
//                // we need to force base to always override so the user HAS to set something
//                if (relationKind == "Base")
//                {
//                    options.OverridesOthers = true;
//                }
//                // if animals are disabled, do not show any relation like "wild" or "colonist" animals
//                if (!Settings_General.AnimalsCanVore && relationKind.Contains("Animal"))
//                {
//                    continue;
//                }
//                // Basically cheating, but we assume that all translation keys are provided!
//                CreateDesignationOverrideEntry(("RV2_Settings_Designations_RelationKey_" + relationKind).Translate(), innerList, options, ref relationsScrollHeightStale);
//            }
//#if v1_2
//            outerList.EndScrollView(ref relationsScrollHeight, ref relationsScrollHeightStale, ref innerRect);
//#else
//            innerList.EndScrollView(ref relationsScrollHeight, ref relationsScrollHeightStale);
//#endif
//            outerList.End();
//        }

//        private static void DoRaceSettings(Rect inRect)
//        {
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            outerList.Begin(inRect);
//            outerList.ColumnWidth = inRect.width;
//            outerList.Label("RV2_Settings_Designations_RaceVoreOptions".Translate());
//            outerList.GapLine();
//            List<ThingDef_AlienRace> sortedRaces = ModAdapter.HAR.LoadedAlienRaces.OrderBy(x => x.LabelCap.ToString()).ToList();
//            Rect outerRect = outerList.GetRect(inRect.height - outerList.CurHeight);
//#if v1_2
//            outerList.MakeAndBeginScrollView(outerRect, racesScrollHeight, ref racesScrollPosition, out Rect innerRect);
//            Listing_Standard innerList = outerList;
//#else
//            UIUtility.MakeAndBeginScrollView(outerRect, racesScrollHeight, ref racesScrollPosition, out Listing_Standard innerList);
//#endif
//            foreach (ThingDef_AlienRace raceDef in sortedRaces)
//            {
//                CreateDesignationOverrideEntryForDef(raceDef, RaceOptions, innerList, ref racesScrollHeightStale);
//            }
//#if v1_2
//            outerList.EndScrollView(ref racesScrollHeight, ref racesScrollHeightStale, ref innerRect);
//            outerList.ColumnWidth = inRect.width;
//#else
//            innerList.EndScrollView(ref racesScrollHeight, ref racesScrollHeightStale);
//#endif
//            outerList.End();
//        }

//        private static void DoAnimalsSettings(Rect inRect)
//        {
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            if (Settings_General.AnimalsCanVore)
//            {
//                outerList.Begin(inRect);
//                outerList.ColumnWidth = inRect.width;
//                outerList.Label("RV2_Settings_Designations_AnimalVoreOptions".Translate());
//                outerList.GapLine();
//                Rect outerRect = outerList.GetRect(inRect.height - outerList.CurHeight);//UIUtility.CreateOuterScrollRect(list, columns[2]);
//#if v1_2
//                outerList.MakeAndBeginScrollView(outerRect, animalsScrollHeight, ref animalsScrollPosition, out Rect innerRect);
//                Listing_Standard innerList = outerList;
//#else
//                UIUtility.MakeAndBeginScrollView(outerRect, animalsScrollHeight, ref animalsScrollPosition, out Listing_Standard innerList);
//#endif
//                List<PawnKindDef> sortedAnimals = RV2_Common.AnimalRaces.OrderBy(x => x.LabelCap.ToString()).ToList();
//                foreach (PawnKindDef animalDef in sortedAnimals)
//                {
//                    CreateDesignationOverrideEntryForDef(animalDef, AnimalOptions, innerList, ref animalsScrollHeightStale);
//                }
//#if v1_2
//                outerList.EndScrollView(ref animalsScrollHeight, ref animalsScrollHeightStale, ref innerRect);
//#else
//                innerList.EndScrollView(ref animalsScrollHeight, ref animalsScrollHeightStale);
//#endif
//                outerList.End();

//            }
//        }

//        private static void CreateDesignationOverrideEntryForDef(Def def, Dictionary<string, VoreDesignationOptions> optionDictionary, Listing_Standard list, ref bool isStale)
//        {
//            if (!optionDictionary.ContainsKey(def.defName))
//            {
//                optionDictionary.Add(def.defName, new VoreDesignationOptions());
//            }
//            VoreDesignationOptions options = optionDictionary[def.defName];
//            CreateDesignationOverrideEntry(def.LabelCap, list, options, ref isStale);
//        }

//        private static void CreateDesignationOverrideEntry(string label, Listing_Standard list, VoreDesignationOptions options, ref bool isStale)
//        {
//            bool originalOverridesOthers = options.OverridesOthers;
//            list.CheckboxLabeled(label, ref options.OverridesOthers);
//            if(originalOverridesOthers != options.OverridesOthers)
//            {
//                isStale = true;
//            }
//            //list.Label(race.LabelCap);
//            if (!options.OverridesOthers)
//            {
//                return;
//            }
//            list.Indent(12f);
//            // with the indentation the checkbox will be inside of the scrollbar, so reduce the width a bit 
//            list.ColumnWidth -= 20f;
//            bool originalShowMiniumumAgeCheckbox = options.IsPredatorAuto || options.IsEndoPreyAuto || options.IsFatalPreyAuto;
//            list.CheckboxLabeled("RV2_Settings_Designations_DesignatedPredator".Translate(), ref options.IsPredatorAuto);
//            list.CheckboxLabeled("RV2_Settings_Designations_DesignatedEndoPrey".Translate(), ref options.IsEndoPreyAuto);
//            list.CheckboxLabeled("RV2_Settings_Designations_DesignatedFatalPrey".Translate(), ref options.IsFatalPreyAuto);
//            bool showMiniumumAgeCheckbox = options.IsPredatorAuto || options.IsEndoPreyAuto || options.IsFatalPreyAuto;
//            if (originalShowMiniumumAgeCheckbox != showMiniumumAgeCheckbox)
//            {
//                isStale = true;
//            }
//            if (showMiniumumAgeCheckbox)
//            {
//                list.CheckboxLabeled("RV2_Settings_Designations_IgnoreMinimumAge".Translate(), ref options.IgnoreMinimumAge);
//            }
//            list.ColumnWidth += 20f;
//            list.Outdent(12f);
//            list.Gap();
//        }

//        public override void ExposeData()
//        {
//            base.ExposeData();
//            Scribe_Values.Look(ref minimumAge, "minimumAge", 18);
//            // yea, relations are not variable per se, but it fits in the flow and makes future extensions easier
//            ScribeUtilities.ScribeVariableDictionary(ref RelationOptions, "RelationOptions", LookMode.Value, LookMode.Deep);
//            ScribeUtilities.ScribeVariableDictionary(ref RaceOptions, "RaceOptions", LookMode.Value, LookMode.Deep);
//            ScribeUtilities.ScribeVariableDictionary(ref AnimalOptions, "AnimalOptions", LookMode.Value, LookMode.Deep);
//        }
//    }
//}
