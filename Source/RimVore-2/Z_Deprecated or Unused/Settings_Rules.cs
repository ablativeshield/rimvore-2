﻿// using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Verse;
//using RimWorld;
//using UnityEngine;

//namespace RimVore2
//{
//    public class Settings_Rules : ModSettings
//    {
//        public static Dictionary<string, VoreRulePreset> Presets = new Dictionary<string, VoreRulePreset>();
//        private static Dictionary<VoreRuleTarget, VoreRule> rules;
//        public static Dictionary<VoreRuleTarget, VoreRule> Rules
//        {
//            get
//            {
//                if(rules.EnumerableNullOrEmpty())
//                {
//                    rules = new Dictionary<VoreRuleTarget, VoreRule>()
//                    {
//                        {  new VoreRuleTarget(){ targetType = IdentifierType.Everyone }, new VoreRule(RuleState.On) }
//                    };
//                }
//                return rules;
//            }
//            set
//            {
//                SetRulesStale();
//                rules = value;
//            }
//        }
//        public static void Reset()
//        {
//            rules = null;
//            SetRulesStale();
//            rulesHeightStale = true;
//        }

//        public static void SetRulesStale()
//        {
//            //Log.Message("Set rules to stale");
//            cachedGetFinalRule.Clear();
//            //cachedPawnDesignations.Clear();
//            RV2_Patch_UI_Widget_GetGizmos.NotifyAllStale();
//            RecacheAllDesignations();
//        }

//        // UI Section

//        public static Vector2 rulesScrollPosition;
//        public static float rulesHeight = 0f;
//        public static bool rulesHeightStale = true;
//        private static readonly Func<int, Action> moveUp = (int index) => delegate ()
//        {
//            Rules = Rules
//                .Move(index, index - 1)
//                .ToDictionary();
//            SetRulesStale();
//        };
//        private static readonly Func<int, Action> moveDown = (int index) => delegate ()
//        {
//            Rules = Rules
//                .Move(index, index + 1)
//                .ToDictionary();
//            SetRulesStale();
//        };

//        // must be kept in Sync with Enums.RuleState !
//        public static List<Texture2D> ruleStateIcons;

//        public static void DefsLoaded()
//        {
//            ruleStateIcons =  new List<Texture2D>()
//            {
//                UIUtility.CheckOnTexture,
//                UIUtility.CheckOffTexture,
//                UIUtility.CopyTexture
//            };
//            foreach(VoreRule rule in Rules.Values)
//            {
//                rule.DefsLoaded();
//            }
//            UpdateOldNabberPreset();
//        }

//        /// <summary>
//        /// has become necessary due to a difference in rule composition, old nabber presets will always cause errors
//        /// </summary>
//        private static void UpdateOldNabberPreset()
//        {
//            // TODO
//        }

//        public static void DoSettingsWindowContents(Rect inRect)
//        {
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            outerList.Begin(inRect);
//            outerList.HeaderLabel("RV2_Settings_Rules_RulesHeader".Translate(), false);
//            DoSettingsButtons(outerList);
//            outerList.GapLine();
//            Rect rulesOuterRect = outerList.GetRect(inRect.height - outerList.CurHeight - 50);
//#if v1_2
//            Listing_Standard innerList = outerList;
//            outerList.MakeAndBeginScrollView(rulesOuterRect, rulesHeight, ref rulesScrollPosition, out Rect rulesInnerRect);
//#else
//            UIUtility.MakeAndBeginScrollView(rulesOuterRect, rulesHeight, ref rulesScrollPosition, out Listing_Standard innerList);
//#endif
//            DoHeaderRow(innerList);
//            innerList.GapLine();
//            for (int i = 0; i < Rules.ToList().Count; i++)
//            {
//                KeyValuePair<VoreRuleTarget, VoreRule> rule = Rules.ElementAt(i);
//                DoRuleRow(innerList, i, rule);
//            }

//            if (innerList.ButtonImage(UIUtility.AddButtonTexture, UIUtility.ImageButtonWithLabelSize, UIUtility.ImageButtonWithLabelSize))
//            {
//                Rules.Add(new VoreRuleTarget(), new VoreRule(RuleState.Copy));
//                SetRulesStale();
//                rulesHeight += Text.LineHeight;
//            }
//#if v1_2
//            outerList.EndScrollView(ref rulesHeight, ref rulesHeightStale, ref rulesInnerRect);
//#else
//            innerList.EndScrollView(ref rulesHeight, ref rulesHeightStale);
//#endif
//            outerList.End();
//        }

//        private static void DoSettingsButtons(Listing_Standard list)
//        {
//            string resetButtonLabel = "RV2_Settings_Rules_Reset".Translate();
//            string savePresetLabel = "RV2_Settings_Rules_SavePreset".Translate();
//            string loadPresetLabel = "RV2_Settings_Rules_LoadPreset".Translate();
//            string removePresetLabel = "RV2_Settings_Rules_RemovePreset".Translate();
//            Vector2 resetButtonSize = Text.CalcSize(UIUtility.ButtonStringPadding + resetButtonLabel + UIUtility.ButtonStringPadding);
//            Vector2 savePresetSize = Text.CalcSize(UIUtility.ButtonStringPadding + savePresetLabel + UIUtility.ButtonStringPadding);
//            Vector2 loadPresetSize = Text.CalcSize(UIUtility.ButtonStringPadding + loadPresetLabel + UIUtility.ButtonStringPadding);
//            Vector2 removePresetSize = Text.CalcSize(UIUtility.ButtonStringPadding + removePresetLabel + UIUtility.ButtonStringPadding);
//            float maxRequiredHeight = Mathf.Max(resetButtonSize.y, savePresetSize.y, loadPresetSize.y, removePresetSize.y);
//            Rect rowRect = list.GetRect(maxRequiredHeight);
//            float buttonPadding = 5f;
//            Rect resetButtonRect = new Rect(rowRect.x, rowRect.y, resetButtonSize.x, rowRect.height);
//            Rect savePresetRect = new Rect(resetButtonRect.x + resetButtonRect.width + buttonPadding, rowRect.y, savePresetSize.x, rowRect.height);
//            Rect loadPresetRect = new Rect(savePresetRect.x + savePresetRect.width + buttonPadding, rowRect.y, loadPresetSize.x, rowRect.height);
//            Rect removePresetRect = new Rect(loadPresetRect.x + loadPresetRect.width + buttonPadding, rowRect.y, removePresetSize.x, rowRect.height);
//            if (Widgets.ButtonText(resetButtonRect, resetButtonLabel))
//            {
//                Reset();
//            }
//            if (Widgets.ButtonText(savePresetRect, savePresetLabel))
//            {
//                OpenSavePresetMenu();
//            }
//            if(Presets.Count >= 1)
//            {
//                if (Widgets.ButtonText(loadPresetRect, loadPresetLabel))
//                {
//                    OpenLoadPresetMenu();
//                }
//                if (Widgets.ButtonText(removePresetRect, removePresetLabel))
//                {
//                    OpenRemovePresetMenu();
//                }
//            }
//        }

//        private static void DoHeaderRow(Listing_Standard list)
//        {
//            int fontSize= Text.CurFontStyle.fontSize;
//            Text.CurFontStyle.fontSize = 18;
//            int columnCount = 3;
//            string nameLabel = "RV2_Settings_Rules_Name".Translate();
//            string quickAccessLabel = "RV2_Settings_Rules_QuickAccess".Translate();
//            string controlsLabel = "RV2_Settings_Rules_Controls".Translate();
//            float columnWidth = list.ColumnWidth / 3;
//            float requiredHeight = Mathf.Max(Text.CalcHeight(nameLabel, columnWidth), Text.CalcHeight(quickAccessLabel, columnWidth), Text.CalcHeight(controlsLabel, columnWidth));
//            Rect rowRect = list.GetRect(requiredHeight);
//            List<Rect> columns = UIUtility.CreateColumns(rowRect, columnCount, out _, 0, 0, 5f);
//            Widgets.Label(columns[0], nameLabel);
//            DoQuickAccessHeader(columns[1]);
//            //Widgets.Label(columns[1], quickAccessLabel);
//            Widgets.Label(columns[2], controlsLabel);
//            Text.CurFontStyle.fontSize = fontSize;
//        }

//        private static void DoQuickAccessHeader(Rect inRect)
//        {
//            List<Texture2D> buttons = new List<Texture2D>() {
//                ContentFinder<Texture2D>.Get("Widget/auto_vore")
//            };
//            List<string> buttonTooltips = new List<string>()
//            {
//                "RV2_Settings_Rules_AllowedToAutoVore".Translate()
//            };
//            foreach(DesignationDef designation in RV2_Common.VoreDesignations)
//            {
//                buttons.Add(ContentFinder<Texture2D>.Get(designation.iconPathEnabledAutomatically));
//                buttonTooltips.Add(designation.description);
//            }
//            UIUtility.ButtonImages(inRect, buttons, new List<Action>(), buttonTooltips, null, 24f);
//        }

//        private static void DoRuleRow(Listing_Standard list, int index, KeyValuePair<VoreRuleTarget, VoreRule> rule)
//        {
//            string ruleLabel = (index + 1) + ": " + rule.Key.GetName();
//            int columnCount = 3;
//            float rowHeight = Text.CalcHeight(ruleLabel, list.ColumnWidth / columnCount);
//            Rect rowRect = list.GetRect(rowHeight);
//            List<Rect> columns = UIUtility.CreateColumns(rowRect, columnCount, out _, 0, 0, 5f);
//            Widgets.Label(columns[0], ruleLabel);
//            bool isFirstRule = index == 0;
//            DoRuleQuickAccessButtons(columns[1], rule, isFirstRule);
//            DoRuleControlButtons(columns[2], index, rule);
//        }

//        private static void DoRuleQuickAccessButtons(Rect inRect, KeyValuePair<VoreRuleTarget, VoreRule> rule, bool isFirstRule = false)
//        {
//            VoreRule ruleValue = rule.Value;
//            VoreRuleTarget target = rule.Key;
//            List<Texture2D> buttons = new List<Texture2D>();
//            List<Action> buttonActions = new List<Action>();
//            List<string> buttonTooltips = new List<string>();
//            //bool displayPreyButtons = target.targetRole == IdentifierRole.Both || target.targetRole == IdentifierRole.Prey;
//            //bool displayPredatorButtons = target.targetRole == IdentifierRole.Both || target.targetRole == IdentifierRole.Predator;

//            // auto vore
//            buttons.Add(GetStateTexture(ruleValue.AllowedInAutoVore));
//            buttonTooltips.Add(GetStateTooltip(ruleValue.AllowedInAutoVore));
//            // first rule cycles between ON and OFF, all other rules can also COPY
//            if (isFirstRule)
//            {
//                if (ruleValue.AllowedInAutoVore == RuleState.On)
//                {
//                    buttonActions.Add(delegate () 
//                    {
//                        ruleValue.AllowedInAutoVore = RuleState.Off;
//                        SetRulesStale();
//                    }); 
//                }
//                else
//                {
//                    buttonActions.Add(delegate () {
//                        ruleValue.AllowedInAutoVore = RuleState.On;
//                        SetRulesStale();
//                    }); 
//                }
//            }
//            else
//            {
//                buttonActions.Add(delegate () 
//                {
//                    ruleValue.AllowedInAutoVore = ruleValue.AllowedInAutoVore.Next();
//                    SetRulesStale();
//                }); 
//            }
//            List<DesignationDef> designations = RV2_Common.VoreDesignations;

//            foreach(DesignationDef designation in designations)
//            {
//                // if the current designation does not apply to the rule, add a blank space
//                if (!designation.CanBeAssignedTo(target.targetRole))
//                {
//                    buttons.Add(UIUtility.BlankButtonTexture);
//                }
//                // otherwise do the state button
//                else
//                {
//                    string designationKey = designation.defName;
//                    // take the rules current state
//                    RuleState currentState = ruleValue.DesignationStates.TryGetValue(designationKey, RuleState.On);
//                    buttons.Add(GetStateTexture(currentState));
//                    buttonTooltips.Add(GetStateTooltip(currentState));
//                    // special handling for first rule, switch between ON and OFF
//                    if (isFirstRule)
//                    {
//                        if (currentState == RuleState.On)
//                        {
//                            buttonActions.Add(delegate ()
//                            {
//                                ruleValue.DesignationStates.SetOrAdd(designationKey, RuleState.Off);
//                                SetRulesStale();
//                            });
//                        }
//                        else
//                        {
//                            buttonActions.Add(delegate ()
//                            {
//                                ruleValue.DesignationStates.SetOrAdd(designationKey, RuleState.On);
//                                SetRulesStale();
//                            });
//                        }
//                    }
//                    // normal handling for other rules - cycle through enum
//                    else
//                    {
//                        buttonActions.Add(delegate ()
//                        {
//                            ruleValue.DesignationStates.SetOrAdd(designationKey, currentState.Next());
//                            SetRulesStale();
//                        });
//                    }
//                }
//            }
//            UIUtility.ButtonImages(inRect, buttons, buttonActions, buttonTooltips, null, 24f);
//        }

//        public static void DoLabeledRuleStateControl(Listing_Standard list, string label, ref RuleState currentState, string tooltip = null)
//        {
//            string stateTooltip = GetStateTooltip(currentState);
//            if (tooltip == null)
//            {
//                tooltip = stateTooltip;
//            }
//            else if (stateTooltip != null)
//            {
//                tooltip += "\n\n" + stateTooltip;
//            }

//            list.LabeledCheckbox(label, ref currentState, Settings_Rules.ruleStateIcons, tooltip, 24f);
//        }

//        public static void DoRuleStateControl(Rect inRect, ref RuleState currentState, string tooltip = null)
//        {
//            UIUtility.Checkbox(inRect, ref currentState, Settings_Rules.ruleStateIcons, tooltip);
//        }

//        private static string GetStateTooltip(RuleState state)
//        {
//            switch (state)
//            {
//                case RuleState.Copy:
//                    return "RV2_Settings_Rules_RuleStateCopy".Translate();
//                case RuleState.Off:
//                    return "RV2_Settings_Rules_RuleStateOff".Translate();
//                case RuleState.On:
//                    return "RV2_Settings_Rules_RuleStateOn".Translate();
//                default:
//                    return null;
//            }
//        }

//        public static Texture2D GetStateTexture(RuleState state)
//        {
//            switch (state)
//            {
//                case RuleState.Copy:
//                    return UIUtility.CopyTexture;
//                case RuleState.Off:
//                    return UIUtility.CheckOffTexture;
//                case RuleState.On:
//                    return UIUtility.CheckOnTexture;
//                default:
//                    return default(Texture2D);
//            }
//        }

//        private static void DoRuleControlButtons(Rect inRect, int index, KeyValuePair<VoreRuleTarget, VoreRule> rule)
//        {
//            // the first entry must never be removed or moved!
//            bool isFirstEntry = index == 0;
//            //Action edit = () => Find.WindowStack.Add(new Window_RuleEditor(rule.Key, rule.Value));
//            Action edit = delegate ()
//            {
//                //rules.Remove(rule.Key);
//                Find.WindowStack.Add(new Window_RuleEditor(rule.Key, rule.Value, isFirstEntry));
//                //rules.Add(rule.Key, rule.Value);
//                //SetRulesStale();
//            };
//            Action remove = delegate () {
//                Rules.Remove(rule.Key);
//                SetRulesStale();
//            };
//            List<Texture2D> buttons = new List<Texture2D> { UIUtility.EditButtonTexture };
//            List<Action> buttonActions = new List<Action>() { edit };
//            List<string> buttonTooltips = new List<string>() { "RV2_Settings_Edit".Translate() };
//            // the second rule should not have a MoveUp button
//            bool canBeMovedUp = index > 1;
//            bool isLastEntry = rule.Equals(Rules.Last());
//            if (canBeMovedUp)
//            {
//                buttons.Add(UIUtility.MoveUpButtonTexture);
//                buttonActions.Add(moveUp(index));
//                buttonTooltips.Add("RV2_Settings_MoveUp".Translate());
//            }
//            if (!isLastEntry && !isFirstEntry)
//            {
//                buttons.Add(UIUtility.MoveDownButtonTexture);
//                buttonActions.Add(moveDown(index));
//                buttonTooltips.Add("RV2_Settings_MoveDown".Translate());
//            }
//            if (Rules.Count != 1 && !isFirstEntry)
//            {
//                buttons.Add(UIUtility.RemoveButtonTexture);
//                buttonActions.Add(remove);
//                buttonTooltips.Add("RV2_Settings_Remove".Translate());
//            }
//            UIUtility.ButtonImages(inRect, buttons, buttonActions, buttonTooltips, null, 20f);
//            //list.ButtonImages(buttons, buttonActions, buttonTooltips, null, (index + 1) + ". " + rule.Key.GetName());
//        }

//        public override void ExposeData()
//        {
//            base.ExposeData();
//            ScribeUtilities.ScribeVariableDictionary(ref rules, "rules", LookMode.Deep, LookMode.Deep);
//            ScribeUtilities.ScribeVariableDictionary(ref Presets, "presets", LookMode.Value, LookMode.Deep);
//        }

//        // logic section

//        private static void OpenSavePresetMenu()
//        {
//            Action<string> save = delegate (string content)
//            {
//                if (string.IsNullOrEmpty(content))
//                {
//                    return;
//                }
//                Presets.SetOrAdd(content, new VoreRulePreset(Rules));
//            };
//            Action cancel = () => { };
//            Window_CustomTextField textFieldWindow = new Window_CustomTextField("", save, cancel);
//            Find.WindowStack.Add(textFieldWindow);
//        }

//        private static void OpenLoadPresetMenu()
//        {
//            Action<string> load = delegate (string presetName)
//            {
//                RV2Log.Message("Loading preset " + presetName);
//                if (Presets.ContainsKey(presetName))
//                {
//                    Rules = new Dictionary<VoreRuleTarget, VoreRule>(Presets[presetName].rules);
//                    SetRulesStale();
//                }
//            };
//            List<FloatMenuOption> options = new List<FloatMenuOption>();
//            foreach(string presetName in Presets.Keys.ToList())
//            {
//                options.Add(new FloatMenuOption(presetName, () => load(presetName)));
//            }
//            Find.WindowStack.Add(new FloatMenu(options));
//        }

//        private static void OpenRemovePresetMenu()
//        {
//            Action<string> remove = delegate (string presetName)
//            {
//                RV2Log.Message("Removing preset " + presetName);
//                if (Presets.ContainsKey(presetName))
//                {
//                    Presets.Remove(presetName);
//                }
//            };
//            List<FloatMenuOption> options = new List<FloatMenuOption>();
//            foreach (string presetName in Presets.Keys.ToList())
//            {
//                options.Add(new FloatMenuOption(presetName, () => remove(presetName)));
//            }
//            Find.WindowStack.Add(new FloatMenu(options));
//        }

//        public static int GetValidAge(Pawn pawn, IdentifierRole role = IdentifierRole.Both)
//        {
//            Func<VoreRule, RuleState> ageStateGetter = (VoreRule rule) => rule.ConsiderMinimumAge;
//            VoreRule activeRule = GetFinalRule(pawn, role, ageStateGetter);
//            bool needsAgeCheck = activeRule.ConsiderMinimumAge == RuleState.On;
//            if (!needsAgeCheck)
//            {
//                return 0;
//            }
//            return activeRule.MinimumAge;
//        }

//        public static bool HasValidAge(Pawn pawn, IdentifierRole role = IdentifierRole.Both)
//        {
//            int requiredAge = GetValidAge(pawn, role);
//            return pawn.ageTracker?.AgeBiologicalYears >= requiredAge;
//        }

//        public static bool IsAllowedForAuto(Pawn pawn, IdentifierRole role)
//        {
//            Func<VoreRule, RuleState> autoStateGetter = (VoreRule rule) => rule.AllowedInAutoVore;
//            return GetFinalRule(pawn, role, autoStateGetter).AllowedInAutoVore == RuleState.On;
//        }

//        //private static Dictionary<int, bool> cachedVorePathEnabled = new Dictionary<int, bool>();
//        public static bool VorePathEnabled(Pawn pawn, IdentifierRole role, VorePathDef path, bool isForAuto = false)
//        {
//            Func<VoreRule, RuleState> stateGetter = (VoreRule r) => r.UseVorePathRules;
//            VoreRule rule = GetFinalRule(pawn, role, stateGetter);
//            VorePathRule pathRule = rule.GetPathRule(path.defName);
//            bool enabled = pathRule.Enabled;
//            if (isForAuto)
//            {
//                // Log.Message("Checking for auto-vore validity, is auto enabled? " + pathRule.AutoVoreEnabled);
//                enabled &= pathRule.AutoVoreEnabled;
//            }
//            return enabled;
//        }

//        public static bool DesignationActive(Pawn pawn, DesignationDef designation)
//        {
//            RV2Log.Message("calculating enabled designation for " + pawn.LabelShort, true, true);
//            string key = designation.defName;
//            Func<VoreRule, RuleState> stateGetter = (VoreRule rule) => rule.DesignationStates.TryGetValue(key);
//            VoreRule finalRule = GetFinalRule(pawn, designation.assignedTo, stateGetter);
//            // Log.Message(LogUtility.ToString(finalRule.DesignationStates));
//            return finalRule.DesignationStates.TryGetValue(key) == RuleState.On;
//        }

//        public static void RecacheAllDesignations()
//        {
//            try
//            {
//                IEnumerable<PawnData> pawnDataList = SaveStorage.DataStore?.PawnData?.Values
//                .Where(pd => pd.IsValid);
//                if (pawnDataList.EnumerableNullOrEmpty())
//                {
//                    RV2Log.Message("No pawn data to recache", true);
//                    return;
//                }
//                RV2Log.Message("Recaching pawndatas: " + string.Join(", ", pawnDataList.Select(pd => pd.Pawn?.LabelShort)), true);
//                foreach (PawnData pawnData in pawnDataList)
//                {
//                    RV2Log.Message("Recaching " + pawnData.Pawn?.LabelShort + " designations", true);
//                    List<DesignationDef> designations = RV2_Common.VoreDesignations;
//                    if (designations.NullOrEmpty())
//                    {
//                        RV2Log.Error("No designationDefs to recache for");
//                        return;
//                    }
//                    foreach (DesignationDef designation in designations)
//                    {
//                        string key = designation.defName;
//                        Func<VoreRule, RuleState> stateGetter = (VoreRule rule) => rule.DesignationStates.TryGetValue(key);
//                        VoreRule appliedRule = GetFinalRule(pawnData.Pawn, IdentifierRole.Both, stateGetter);
//                        if (appliedRule == null)
//                        {
//                            RV2Log.Message("Fatal error, no rule to recalculate designations with", true, true, "Settings");
//                            return;
//                        }
//                        bool designationActive = appliedRule.DesignationStates.TryGetValue(key) == RuleState.On;
//                        /*if (!pawnData.Designations.ContainsKey(designation))
//                        {
//                            pawnData.Designations.Add(designation, default(Designation));
//                        }*/
//                        pawnData.Designations[designation].enabledAuto = designationActive;
//                    }
//                }
//            }
//            catch(Exception e)
//            {
//                RV2Log.Error("The accursed RecacheAllDesignations fucked up. Pawn designations were not properly recached, reason: " + e);
//            }
//        }

//        //private static Dictionary<int, ThingDef> cachedGetContainer = new Dictionary<int, ThingDef>();
//        public static ThingDef GetContainer(Pawn predator, VorePathDef pathDef)
//        {
//            Func<VoreRule, RuleState> stateGetter = (VoreRule rule) => rule.UseVorePathRules;
//            VoreRule finalRule = GetFinalRule(predator, IdentifierRole.Predator, stateGetter);
//            VorePathRule pathRule = finalRule.GetPathRule(pathDef.defName);
//            ThingDef container = pathRule.Container;
//            return container;
//        }

//        public static void ValidateContainers()
//        {
//            Rules.ForEach(rule => rule.Value.ValidateContainers());
//        }

//        public static IEnumerable<VoreRule> ApplicableRules(Pawn pawn, IdentifierRole role)
//        {
//            return Rules
//               .Where(rule => rule.Key.AppliesTo(pawn, role))
//               .Select(rule => rule.Value);
//        }

//        public static VoreRule GetFinalRule(IEnumerable<VoreRule> applicableRules, Func<VoreRule, RuleState> stateGetter)
//        {
//            // Log.Message(string.Join(", ", applicableRules.Select(rule => stateGetter(rule).ToString())));
//            applicableRules = applicableRules
//                .Where(rule => stateGetter(rule) != RuleState.Copy);
//            if (applicableRules.EnumerableNullOrEmpty())
//            {
//                string errorMessage = "RimVore2: NO FINAL RULE FOUND, this is a critical error, forcing all rules to be reset and using first rule.";
//                NotificationUtility.DoNotification(NotificationType.MessageThreatBig, errorMessage);
//                Reset();
//                return Rules.First().Value;
//            }
//            return applicableRules
//                .Last(rule => stateGetter(rule) == RuleState.On || stateGetter(rule) == RuleState.Off);
//        }
        
//        // I hate this, but this is the best solution I can think of to provide a multi-key dictionary
//        private class RuleCacheable
//        {
//            readonly Pawn pawn;
//            readonly IdentifierRole role;
//            readonly Func<VoreRule, RuleState> stateGetter;
//            public RuleCacheable(Pawn pawn, IdentifierRole role, Func<VoreRule, RuleState> stateGetter)
//            {
//                this.pawn = pawn;
//                this.role = role;
//                this.stateGetter = stateGetter;
//            }

//        }

//        private readonly static Dictionary<RuleCacheable, VoreRule> cachedGetFinalRule = new Dictionary<RuleCacheable, VoreRule>();
//        public static VoreRule GetFinalRule(Pawn pawn, IdentifierRole role, Func<VoreRule, RuleState> stateGetter)
//        {
//            RuleCacheable cacheKey = new RuleCacheable(pawn, role, stateGetter);
//            if (cachedGetFinalRule.ContainsKey(cacheKey))
//            {
//                RV2Log.Message("Getting cached final rule for pawn " + pawn.LabelShort + ", role " + role + " stategetter hash: " + stateGetter.GetHashCode(), true, true);
//                return cachedGetFinalRule[cacheKey];
//            }
//            VoreRule finalRule = GetFinalRule(ApplicableRules(pawn, role), stateGetter);
//            cachedGetFinalRule.SetOrAdd(cacheKey, finalRule);
//            return finalRule;
//        }
//    }
//}
