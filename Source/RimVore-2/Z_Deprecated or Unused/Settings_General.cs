﻿//using System.Collections.Generic;
//using UnityEngine;
//using Verse;
//using System.Linq;
//using System;
///// <summary>
///// Complaints can be directed at dev/null
///// Or give me a tutorial on Rimworld UIs that doesn't suck
///// </summary>
//namespace RimVore2
//{
//    public class Settings_General : ModSettings
//    {
//        public static bool Logging = false;
//        public static bool VerboseLogging = false;
//        public static bool DesignatorGizmoEnabled = true;
//        public static bool RMBVoreMenuEnabled = true;
//        public static bool AnimalsCanVore = true;
//        public static bool FatalVoreEnabled = true;
//        public static bool EndoVoreEnabled = true;
//        public static float VoreSpeedUserMultiplier = 1f;
//        public static bool ScatEnabled = true;
//        public static bool BonesEnabled = true;
//        public static bool QuirksEnabled = true;
//        public static DefaultStripSetting DefaultStrippingBehaviour = DefaultStripSetting.Random;
//        public static float MinimumVoreCapacity = 0f;
//        public static bool DisplayInvalidRMBOptions = true;
//        public static bool FeederVore = true;
//        public static bool ForbidDigestedCorpse = false;
//        //public static bool PassNormalTicksDuringVore = false;
//        public static bool BackstoriesCanAddSexualParts = true;
//        public static bool CanStealNeedFromPawnWithoutNeed = false;
//        public static int MaximumTraitsForReformedPawns = 3;
//        public static bool ReformHumanoidsAsNewborns = true;
//        public static float DefaultInternalTemperature = 36.5f;
//        public static bool PreventPreyFromStarving = false;
//        public static bool ShowVoreTab = true;
//        public static bool SpawnEmptyVoreContainers = false;

//        //public static Dictionary<string, bool> EnabledVorePaths = new Dictionary<string, bool>();
//        //public static Dictionary<string, string> VorePathResultingContainer = new Dictionary<string, string>();
//        public static bool SoundsEnabled = true;
//        public static float SoundVolumeMultiplier = 1f;
//        public static Dictionary<string, bool> EnabledVoreSounds = new Dictionary<string, bool>();
//        public static string lastlogmessage = "";

//        // I haven't found a great solution to store these, there is probably a fancy way though
//        protected static Vector2 generalScrollPosition;
//        protected static float generalScrollHeight = 0f;
//        protected static bool generalScrollHeightStale = true;

//        protected static Vector2 pathsScrollPosition;
//        protected static float pathsScrollHeight = 0f;
//        protected static bool pathsScrollHeightStale = true;

//        protected static Vector2 soundsScrollPosition;
//        protected static float soundsScrollHeight = 0f;
//        protected static bool soundsScrollHeightStale = true;

//        public static void Setup()
//        {
//            //ValidateContainers();
//        }

//        public static void ResetAllSettings()
//        {
//            Reset();
//            Settings_Quirks.Reset();
//            Settings_AutoVore.Reset();
//        }

//        public static void Reset()
//        {
//            Logging = false;
//            VerboseLogging = false;
//            DesignatorGizmoEnabled = true;
//            RMBVoreMenuEnabled = true;
//            AnimalsCanVore = true;
//            FatalVoreEnabled = true;
//            EndoVoreEnabled = true;
//            VoreSpeedUserMultiplier = 1f;
//            ScatEnabled = true;
//            BonesEnabled = true;
//            QuirksEnabled = true;
//            DefaultStrippingBehaviour = DefaultStripSetting.Random;
//            MinimumVoreCapacity = 0f;
//            DisplayInvalidRMBOptions = true;
//            FeederVore = true;
//            //EnabledVorePaths.Clear();
//            //VorePathResultingContainer.Clear();
//            SoundsEnabled = true;
//            SoundVolumeMultiplier = 1f;
//            EnabledVoreSounds.Clear();
//            lastlogmessage = "";
//            generalScrollHeight = 0f;
//            generalScrollHeightStale = true;
//            pathsScrollHeight = 0f;
//            pathsScrollHeightStale = true;
//            soundsScrollHeight = 0f;
//            soundsScrollHeightStale = true;
//            ForbidDigestedCorpse = false;
//            //PassNormalTicksDuringVore = false;
//            BackstoriesCanAddSexualParts = true;
//            CanStealNeedFromPawnWithoutNeed = false;
//            MaximumTraitsForReformedPawns = 3;
//            ReformHumanoidsAsNewborns = true;
//            DefaultInternalTemperature = 36.5f;
//            PreventPreyFromStarving = false;
//            ShowVoreTab = true;
//            SpawnEmptyVoreContainers = false;

//            Setup();
//        }

//        public static void DoSettingsWindowContents(Rect inRect)
//        {
//            int columnCount = 2;
//            List<Rect> columns = UIUtility.CreateColumns(inRect, columnCount, out float columnWidth);

//            Listing_Standard list = new Listing_Standard()
//            {
//                ColumnWidth = columns[0].width
//            };

//            // first column
//            DoGeneralSettings(columns[0]);

//            // second column
//            DoSoundSettings(columns[1]);
//        }

//        protected static void DoGeneralSettings(Rect inRect)
//        {
//#if v1_2
//            Listing_Standard list = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            list.Begin(inRect);
//            Rect outerRect = list.GetRect(inRect.height - list.CurHeight); ;
//            list.MakeAndBeginScrollView(outerRect, generalScrollHeight, ref generalScrollPosition, out Rect innerRect);
//#else
//            Rect outerRect = inRect;
//            UIUtility.MakeAndBeginScrollView(outerRect, generalScrollHeight, ref generalScrollPosition, out Listing_Standard list);
//#endif
//            if (list.ButtonText("RV2_Settings_General_ResetAllSettings".Translate()))
//            {
//                ResetAllSettings();
//            }
//            if(list.ButtonText("RV2_Settings_General_ResetGeneralSettings".Translate()))
//            {
//                Reset();
//            }
//            if (Prefs.DevMode)
//            {
//                list.CheckboxLabeled("RV2_Settings_General_Logging".Translate(), ref Logging);
//                list.CheckboxLabeled("RV2_Settings_General_VerboseLogging".Translate(), ref VerboseLogging);
//                list.Gap();
//            }
//            list.CheckboxLabeled("RV2_Settings_General_DesignatorGizmoEnabled".Translate(), ref DesignatorGizmoEnabled);
//            list.Gap();
//            list.CheckboxLabeled("RV2_Settings_General_AnimalsCanVore".Translate(), ref AnimalsCanVore);
//            list.Gap();
//            list.CheckboxLabeled("RV2_Settings_General_RMBVoreMenuEnabled".Translate(), ref RMBVoreMenuEnabled);
//            list.Gap();
//            list.CheckboxLabeled("RV2_Settings_General_QuirksEnabled".Translate(), ref QuirksEnabled);
//            list.CheckboxLabeled("RV2_Settings_General_FatalVoreEnabled".Translate(), ref FatalVoreEnabled);
//            list.CheckboxLabeled("RV2_Settings_General_EndoVoreEnabled".Translate(), ref EndoVoreEnabled);
//            Func<float, string> speedMultiplierPresenter = delegate (float value)
//            {
//                return Math.Round(value, 1) + "x";
//            };
//            VoreSpeedUserMultiplier = list.DoLabelledSlider(VoreSpeedUserMultiplier, 0.1f, 10f, "RV2_Settings_General_VoreSpeedMultiplier".Translate(), null, speedMultiplierPresenter);
//            list.Gap();
//            DoScatBonesToggleSettings(list);
//            list.Gap();
//            DoStrippingDropDown(list);
//            Func<float, string> minimumCapacityPresenter = delegate (float value)
//            {
//                if (value == 0) return "RV2_Settings_None".Translate();
//                else if (value == RV2_Common.VoreStorageCapacityToBeConsideredInfinite) return "RV2_Settings_Infinite".Translate();
//                else return Math.Round(value, 1).ToString();
//            };
//            MinimumVoreCapacity = list.DoLabelledSlider(MinimumVoreCapacity, 0f, RV2_Common.VoreStorageCapacityToBeConsideredInfinite, "RV2_Settings_General_MinimumVoreCapacity".Translate(), "RV2_Settings_General_MinimumVoreCapacity_Tip".Translate(), minimumCapacityPresenter);
//            list.Gap();
//            list.CheckboxLabeled("RV2_Settings_General_DisplayUnavailableRMBOptions".Translate(), ref DisplayInvalidRMBOptions, "RV2_Settings_General_DisplayUnavailableRMBOptions_Tip".Translate());
//            list.CheckboxLabeled("RV2_Settings_General_FeederVore".Translate(), ref FeederVore, "RV2_Settings_General_FeederVore_Tip".Translate());
//            //list.CheckboxLabeled("RV2_Settings_General_PassTick".Translate(), ref PassNormalTicksDuringVore, "RV2_Settings_General_PassTick_Tip".Translate());
//            list.CheckboxLabeled("RV2_Settings_General_ForbidCorpse".Translate(), ref ForbidDigestedCorpse, "RV2_Settings_General_ForbidCorpse_Tip".Translate());
//            list.CheckboxLabeled("RV2_Settings_General_AllowBackstoryForcedGenitals".Translate(), ref BackstoriesCanAddSexualParts, "RV2_Settings_General_AllowBackstoryForcedGenitals_Tip".Translate());
//            list.CheckboxLabeled("RV2_Settings_General_CanStealNeedFromPawnWithoutNeed".Translate(), ref CanStealNeedFromPawnWithoutNeed, "RV2_Settings_General_CanStealNeedFromPawnWithoutNeed_Tip".Translate());
//            MaximumTraitsForReformedPawns = (int)list.DoLabelledSlider(MaximumTraitsForReformedPawns, 0, 8, "RV2_Settings_General_ReformationTraits".Translate(), "RV2_Settings_General_ReformationTraits_Tip".Translate());
//            list.CheckboxLabeled("RV2_Settings_General_ReformHumanoidAsNewBorn".Translate(), ref ReformHumanoidsAsNewborns, "RV2_Settings_General_ReformHumanoidAsNewBorn_Tip".Translate());
//            DefaultInternalTemperature = list.DoLabelledSlider(DefaultInternalTemperature, -100, 100, "RV2_Settings_General_DefaultInternalTemperature".Translate(), "RV2_Settings_General_DefaultInternalTemperature_Tip".Translate(DefaultInternalTemperature), UIUtility.TemperaturePresenter);
//            list.CheckboxLabeled("RV2_Settings_General_PreventPreyFromStarving".Translate(), ref PreventPreyFromStarving, "RV2_Settings_General_PreventPreyFromStarving_Tip".Translate());
//            list.CheckboxLabeled("RV2_Settings_General_ShowVoreTab".Translate(), ref ShowVoreTab, "RV2_Settings_General_ShowVoreTab_Tip".Translate());
//            list.CheckboxLabeled("RV2_Settings_General_SpawnEmptyVoreContainers".Translate(), ref SpawnEmptyVoreContainers, "RV2_Settings_General_SpawnEmptyVoreContainers_Tip".Translate());
//#if v1_2
//            list.EndScrollView(ref generalScrollHeight, ref generalScrollHeightStale, ref innerRect);
//            list.End();
//#else
//            list.EndScrollView(ref generalScrollHeight, ref generalScrollHeightStale);
//#endif
//        }

//        private static void DoScatBonesToggleSettings(Listing_Standard list)
//        {
//            bool needToValidate = false;
//            bool originalScatEnabled = ScatEnabled;
//            list.CheckboxLabeled("RV2_Settings_General_ScatEnabled".Translate(), ref ScatEnabled, "RV2_Settings_General_ScatEnabled_Tip".Translate());
//            needToValidate |= originalScatEnabled != ScatEnabled;
//            if (needToValidate) RV2Log.Message("Need to revalidate containers due to changed scat flag");
//            bool originalBonesEnabled = BonesEnabled;
//            list.CheckboxLabeled("RV2_Settings_General_BonesEnabled".Translate(), ref BonesEnabled, "RV2_Settings_General_BonesEnabled_Tip".Translate());
//            needToValidate |= originalBonesEnabled != BonesEnabled;
//            if (needToValidate) RV2Log.Message("Need to revalidate containers due to changed bones flag");
//            if (needToValidate)
//            {
//                Settings_Rules.ValidateContainers();
//            }
//        }

//        private static void DoStrippingDropDown(Listing_Standard list)
//        {
//            string label = "RV2_Settings_General_StrippingPreferenceDefaultBehaviour".Translate();
//            string labelTooltip = "RV2_Settings_General_StrippingPreferenceDefaultBehaviour_Tip".Translate();
//            void action(DefaultStripSetting s) => DefaultStrippingBehaviour = s;
//            Dictionary<DefaultStripSetting, string> optionLabels = new Dictionary<DefaultStripSetting, string>()
//            {
//                { DefaultStripSetting.Random, "RV2_Settings_General_StrippingRandom".Translate() },
//                { DefaultStripSetting.AlwaysStrip, "RV2_Settings_General_StrippingAlways".Translate() },
//                { DefaultStripSetting.NeverStrip, "RV2_Settings_General_StrippingNever".Translate() }
//            };
//            Dictionary<DefaultStripSetting, string> optionTooltips = new Dictionary<DefaultStripSetting, string>()
//            {
//                { DefaultStripSetting.Random, "RV2_Settings_General_StrippingRandom_Tip".Translate() },
//                { DefaultStripSetting.AlwaysStrip, "RV2_Settings_General_StrippingAlways_Tip".Translate() },
//                { DefaultStripSetting.NeverStrip, "RV2_Settings_General_StrippingNever_Tip".Translate() }
//            };
//            UIUtility.CreateLabelledDropDownForEnum<DefaultStripSetting>(list, label, action, DefaultStrippingBehaviour, null, labelTooltip, null, optionLabels, optionTooltips);
//        }

//        protected static void DoSoundSettings(Rect inRect)
//        {
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            outerList.Begin(inRect);
//            outerList.CheckboxLabeled("RV2_Settings_General_EnabledVoreSounds".Translate(), ref SoundsEnabled);
//            if (SoundsEnabled)
//            {
//                outerList.GapLine();
//                outerList.Label("RV2_Settings_General_SoundVolumeModifier".Translate() + ": " + Math.Round(SoundVolumeMultiplier * 100) + "%");
//                SoundVolumeMultiplier = outerList.Slider(SoundVolumeMultiplier, 0f, 1f);
//                Rect outerRect = outerList.GetRect(inRect.height - outerList.CurHeight);
//#if v1_2
//                outerList.MakeAndBeginScrollView(outerRect, soundsScrollHeight, ref soundsScrollPosition, out Rect innerRect);
//                Listing_Standard innerList = outerList;
//#else
//                UIUtility.MakeAndBeginScrollView(outerRect, soundsScrollHeight, ref soundsScrollPosition, out Listing_Standard innerList);
//#endif
//                foreach (SoundDef sound in RV2_Common.VoreSounds)
//                {
//                    string key = sound.defName;
//                    if (!EnabledVoreSounds.ContainsKey(key))
//                    {
//                        EnabledVoreSounds.Add(key, true);
//                    }
//                    // I refuse to create a class with one bool just to be able to pass a ref to the function directly from the dictionary. Ugly workaround.
//                    bool flag = EnabledVoreSounds[key];
//                    // cut off the RV2_ part of the sound defName
//                    string displayLabel = sound.defName.Substring(4);
//                    innerList.CheckboxLabeled(displayLabel, ref flag);
//                    EnabledVoreSounds[key] = flag;
//                }
//#if v1_2
//                outerList.EndScrollView(ref soundsScrollHeight, ref soundsScrollHeightStale, ref innerRect);
//#else
//                outerList.EndScrollView(ref soundsScrollHeight, ref soundsScrollHeightStale);
//#endif
//            }
//            outerList.End();
//        }
//        public static bool IsSoundEnabled(SoundDef soundDef)
//        {
//            if (!SoundsEnabled)
//            {
//                return false;
//            }
//            string soundDefName = soundDef.defName;

//            if (!EnabledVoreSounds.ContainsKey(soundDef.defName))
//            {
//                RV2Log.Message("Sound setting for " + soundDef.defName + " was missing, RV2 has added and set it to ON");
//                EnabledVoreSounds.Add(soundDef.defName, true);
//                return false;
//            }
//            return EnabledVoreSounds[soundDefName];
//        }

//        public override void ExposeData()
//        {
//            base.ExposeData();
//            Scribe_Values.Look(ref Logging, "Logging", Logging, true);
//            Scribe_Values.Look(ref VerboseLogging, "VerboseLogging", VerboseLogging, true);
//            Scribe_Values.Look(ref DesignatorGizmoEnabled, "DesignatorGizmoEnabled", DesignatorGizmoEnabled, true);
//            Scribe_Values.Look(ref RMBVoreMenuEnabled, "RMBVoreMenuEnabled", RMBVoreMenuEnabled, true);
//            Scribe_Values.Look(ref AnimalsCanVore, "AnimalsCanVore", AnimalsCanVore, true);
//            Scribe_Values.Look(ref FatalVoreEnabled, "FatalVoreEnabled", FatalVoreEnabled, true);
//            Scribe_Values.Look(ref EndoVoreEnabled, "EndoVoreEnabled", EndoVoreEnabled, true);
//            Scribe_Values.Look(ref QuirksEnabled, "QuirksEnabled", EndoVoreEnabled, true);
//            Scribe_Values.Look(ref VoreSpeedUserMultiplier, "VoreSpeedUserMultiplier", VoreSpeedUserMultiplier, true);
//            Scribe_Values.Look(ref ScatEnabled, "ScatEnabled", ScatEnabled, true);
//            Scribe_Values.Look(ref BonesEnabled, "BonesEnabled", BonesEnabled, true);
//            Scribe_Values.Look(ref SoundsEnabled, "SoundsEnabled", SoundsEnabled, true);
//            Scribe_Values.Look(ref SoundVolumeMultiplier, "SoundVolumeMultiplier", SoundVolumeMultiplier, true);
//            Scribe_Values.Look(ref DefaultStrippingBehaviour, "DefaultStrippingBehaviour", DefaultStrippingBehaviour, true);
//            Scribe_Values.Look(ref MinimumVoreCapacity, "MinimumVoreCapacity", MinimumVoreCapacity, true);
//            Scribe_Values.Look(ref DisplayInvalidRMBOptions, "DisplayInvalidRMBOptions", DisplayInvalidRMBOptions, true);
//            Scribe_Values.Look(ref FeederVore, "FeederVore", FeederVore, true);
//            Scribe_Values.Look(ref ForbidDigestedCorpse, "ForbidDigestedCorpse", ForbidDigestedCorpse, true);
//            Scribe_Values.Look(ref BackstoriesCanAddSexualParts, "BackstoriesCanAddSexualParts", BackstoriesCanAddSexualParts, true);
//            Scribe_Values.Look(ref CanStealNeedFromPawnWithoutNeed, "CanStealNeedFromPawnWithoutNeed", CanStealNeedFromPawnWithoutNeed, true);
//            Scribe_Values.Look(ref MaximumTraitsForReformedPawns, "MaximumTraitsForReformedPawns", MaximumTraitsForReformedPawns, true);
//            Scribe_Values.Look(ref ReformHumanoidsAsNewborns, "ReformHumanoidsAsNewborns", ReformHumanoidsAsNewborns, true);
//            Scribe_Values.Look(ref DefaultInternalTemperature, "DefaultInternalTemperature", DefaultInternalTemperature, true);
//            Scribe_Values.Look(ref PreventPreyFromStarving, "PreventPreyFromStarving", PreventPreyFromStarving, true);
//            Scribe_Values.Look(ref ShowVoreTab, "ShowVoreTab", ShowVoreTab, true);
//            Scribe_Values.Look(ref SpawnEmptyVoreContainers, "SpawnEmptyVoreContainers", SpawnEmptyVoreContainers, true);


//            ScribeUtilities.ScribeVariableDictionary<string, bool>(ref EnabledVoreSounds, "EnabledVoreSounds");
//        }
//    }
//}