﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using UnityEngine;
//using Verse;
//using RimWorld;

//namespace RimVore2
//{
//    public class Settings_Quirks : ModSettings
//    {
//        // will guarantee that any time the settings are opened, the enabled quirk pools are freshly checked for valid Pools;
//        private static bool enabledQuirkPoolsStale = true;
//        private static bool enabledQuirksStale = true;

//        private static Dictionary<string, bool> enabledQuirkPools = new Dictionary<string, bool>();
//        private static Dictionary<string, bool> enabledQuirks = new Dictionary<string, bool>();
//        private static Dictionary<string, QuirkRarity> quirkRarityOverrides = new Dictionary<string, QuirkRarity>();

//        public static void DefsLoaded()
//        {
//            List<string> quirkDefNames = DefDatabase<QuirkDef>.AllDefsListForReading.ConvertAll(d => d.defName);
//            List<string> quirkPoolDefNames = DefDatabase<QuirkPoolDef>.AllDefsListForReading.ConvertAll(d => d.defName);
//            ScribeUtilities.SyncKeys(ref enabledQuirks, quirkDefNames, true);
//            ScribeUtilities.SyncKeys(ref enabledQuirkPools, quirkPoolDefNames, true);
//        }

//        public static void Reset()
//        {
//            enabledQuirkPools.Clear();
//            enabledQuirks.Clear();
//            quirkRarityOverrides.Clear();
//            RefreshEnabledQuirkPools();
//            RefreshEnabledQuirks();
//        }

//        public static bool QuirkPoolEnabled(QuirkPoolDef pool)
//        {
//            if (enabledQuirkPools == null)
//            {
//                Log.Warning("The settings enabledQuirkPools was null. If this happens again, something is causing the default values for your settings to be overwritten.");
//                enabledQuirkPools = new Dictionary<string, bool>();
//            }
//            if (!enabledQuirkPools.ContainsKey(pool.defName))
//            {
//                RefreshEnabledQuirkPools();
//            }
//            // I have no idea how this would happen, but better to be safe
//            if (!enabledQuirkPools.ContainsKey(pool.defName))
//            {
//                Log.Error("Key " + pool.defName + " is missing even after Refreshing! Will always be false!");
//            }
//            return enabledQuirkPools.TryGetValue(pool.defName, false);
//        }
//        public static bool QuirkEnabled(QuirkDef quirk)
//        {
//            if(enabledQuirks == null)
//            {
//                Log.Warning("The settings enabledQuirks was null. If this happens again, something is causing the default values for your settings to be overwritten.");
//                enabledQuirks = new Dictionary<string, bool>();
//            }
//            if (!enabledQuirks.ContainsKey(quirk.defName))
//            {
//                RefreshEnabledQuirks();
//            }
//            // I have no idea how this would happen, but better to be safe
//            if (!enabledQuirks.ContainsKey(quirk.defName))
//            {
//                Log.Error("Key " + quirk.defName + " is missing even after Refreshing! Will always be false!");
//            }
//            return enabledQuirks.TryGetValue(quirk.defName, false);
//        }

//        public static List<QuirkDef> GetEnabledQuirks(QuirkPoolDef pool)
//        {
//            return pool.quirks.FindAll(quirk => QuirkEnabled(quirk));
//        }

//        private static void SetRarityOverride(QuirkDef quirk, QuirkRarity rarity)
//        {
//            quirkRarityOverrides.SetOrAdd(quirk.defName, rarity);
//        }

//        /// <param name="quirk">The rarity to check for overrides</param>
//        /// <param name="rarity">The retrieved rarity</param>
//        /// <returns>True for successful retrieval, false for empty overrides or key missing</returns>
//        public static QuirkRarity GetRarity(QuirkDef quirk)
//        {
//            if (quirkRarityOverrides.EnumerableNullOrEmpty())
//            {
//                return quirk.rarity;
//            }
//            if (!quirkRarityOverrides.ContainsKey(quirk.defName))
//            {
//                return quirk.rarity;
//            }
//            return quirkRarityOverrides[quirk.defName];
//        }

//        private static void RemoveAllRarityOverrides()
//        {
//            if(quirkRarityOverrides == null)
//            {
//                return;
//            }
//            foreach(string key in quirkRarityOverrides.Keys.ToList())
//            {
//                quirkRarityOverrides.Remove(key);
//            }
//        }
//        private static void RemoveAllRarityOverrides(QuirkPoolDef pool)
//        {
//            if (quirkRarityOverrides.EnumerableNullOrEmpty())
//            {
//                return;
//            }
//            foreach(string key in pool.quirks.ConvertAll(quirk => quirk.defName))
//            {
//                if (quirkRarityOverrides.ContainsKey(key))
//                {
//                    quirkRarityOverrides.Remove(key);
//                }
//            }
//        }

//        public static float CalculateChanceForWeight(QuirkDef quirk, bool considerOverrides = true)
//        {
//            if (considerOverrides)
//            {
//                int totalWeight = GetEnabledQuirks(quirk.GetPool()).Sum(q => (int)GetRarity(q));
//                if (totalWeight <= 0)
//                {
//                    return 0;
//                }
//                return (float)GetRarity(quirk) / totalWeight;
//            }
//            else
//            {
//                int totalWeight = GetEnabledQuirks(quirk.GetPool()).Sum(q => (int)q.rarity);
//                if (totalWeight <= 0)
//                {
//                    return 0;
//                }
//                return (float)quirk.rarity / totalWeight;
//            }
//        }

//        private static void RefreshEnabledQuirkPools()
//        {
//            // take all pools and convert to their defName so Scribe doesn't go nuclear when it loads this class on startup
//            List<string> providedKeys = RV2_Common.SortedQuirkPools.ConvertAll(pool => pool.defName);
//            ScribeUtilities.SyncKeys(ref enabledQuirkPools, providedKeys, true);
//            enabledQuirkPoolsStale = false;
//        }

//        private static void RefreshEnabledQuirks()
//        {
//            // take pools, get all their quirks, then convert to string so Scribe doesn't go nuclear when it loads this class on startup
//            List<string> providedKeys = RV2_Common.SortedQuirkPools.SelectMany(pool => pool.quirks).Select(quirk => quirk.defName).ToList();
//            ScribeUtilities.SyncKeys(ref enabledQuirks, providedKeys, true);
//            enabledQuirksStale = false;
//        }

//        private static QuirkPoolDef currentPool = null;
//        private static QuirkDef currentQuirk = null;
//        public static void DoSettingsWindowContents(Rect inRect)
//        {
//            if (enabledQuirkPoolsStale)
//            {
//                RefreshEnabledQuirkPools();
//            }
//            if (enabledQuirksStale)
//            {
//                RefreshEnabledQuirks();
//            }

//            Listing_Standard list = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            list.Begin(inRect);
//            Rect poolViewRect = list.GetRect(inRect.height - list.CurHeight);

//            int columnCount = 3;
//            List<Rect> columns = UIUtility.CreateColumns(poolViewRect, columnCount, out float columnWidth);
//            DoQuirkPoolList(columns[0], out QuirkPoolDef selectedPool);
//            // if a new pool has been selected, switch current pool to it
//            if(selectedPool != null && selectedPool != currentPool)
//            {
//                currentPool = selectedPool;
//                infoScrollHeightStale = true;
//                currentQuirk = null;
//            }
//            DoQuirkPoolInfo(columns[1], currentPool, out QuirkDef selectedQuirk);
//            if (selectedQuirk != null && selectedQuirk != currentQuirk)
//            {
//                currentQuirk = selectedQuirk;
//            }

//            DoQuirkInfo(columns[2], currentQuirk);
//            list.End();

//            if(currentPool != null)
//            {
//                // if all quirks in a pool are disabled, disable the pool
//                if (currentPool.quirks.TrueForAll(quirk => enabledQuirks[quirk.defName] == false))
//                {
//                    enabledQuirkPools[currentPool.defName] = false;
//                }
//                // if any quirk in a pool is enabled, enable the pool
//                if(currentPool.quirks.Any(quirk => enabledQuirks[quirk.defName] == true))
//                {
//                    enabledQuirkPools[currentPool.defName] = true;
//                }
//            }
//        }

//        private static void SetAllPools(bool value)
//        {
//            foreach(string key in enabledQuirkPools.Keys.ToList())
//            {
//                enabledQuirkPools.SetOrAdd(key, value);
//                SetAllQuirks(key, value);
//            }
//        }
//        private static void SetAllQuirks(string poolName, bool value)
//        {
//            QuirkPoolDef pool = DefDatabase<QuirkPoolDef>.GetNamed(poolName, false);
//            if(pool == null)
//            {
//                RV2Log.Warning("Tried to set quirk pool for name that was not found in Defs: " + poolName);
//                return;
//            }
//            SetAllQuirks(pool, value);
//        }
//        private static void SetAllQuirks(QuirkPoolDef pool, bool value)
//        {
//            foreach(string key in pool.quirks.ConvertAll(quirk => quirk.defName))
//            {
//                enabledQuirks.SetOrAdd(key, value);
//            }
//        }

//        private static Vector2 poolScrollPosition;
//        private static float poolScrollHeight = 0f;
//        private static bool poolScrollHeightStale = true;
//        public static void DoQuirkPoolList(Rect inRect, out QuirkPoolDef selectedPool)
//        {
//            selectedPool = null;
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            outerList.Begin(inRect);
//            outerList.HeaderLabel("RV2_Settings_Quirks_Pools_Header".Translate());
//            Rect scrollableRect = outerList.GetRect(inRect.height - outerList.CurHeight);
//#if v1_2
//            outerList.MakeAndBeginScrollView(scrollableRect, poolScrollHeight, ref poolScrollPosition, out Rect innerRect);
//            Listing_Standard innerList = outerList;
//#else
//            UIUtility.MakeAndBeginScrollView(scrollableRect, poolScrollHeight, ref poolScrollPosition, out Listing_Standard innerList);
//#endif
//            if (innerList.ButtonText("RV2_Settings_Quirks_ResetQuirks".Translate()))
//            {
//                Reset();
//            }
//            if (innerList.ButtonText("RV2_Settings_ResetAllRaritiesPoolList".Translate()))
//            {
//                RemoveAllRarityOverrides();
//            }
//            List<string> enableDisableLabels = new List<string>() { "RV2_Settings_EnableAll".Translate(), "RV2_Settings_DisableAll".Translate() };
//            innerList.ButtonRow(enableDisableLabels, out int enableDisableClicked, PairAlignment.Equal);
//            if (enableDisableClicked != -1)
//            {
//                switch (enableDisableClicked)
//                {
//                    case 0:
//                        SetAllPools(true);
//                        break;
//                    case 1:
//                        SetAllPools(false);
//                        break;
//                }
//            }
//            innerList.GapLine();
//            foreach (QuirkPoolDef pool in RV2_Common.SortedQuirkPools.OrderBy(pool => pool))
//            {
//                bool poolEnabled = QuirkPoolEnabled(pool);
//                PrepareOnOffButton(poolEnabled, out string onOffLabel, out float onOffWidth);
//                List<string> rowLabels = new List<string>() { pool.label, onOffLabel };
//                // we want the on-off button to always have the same size, looks more uniform
//                List<float> fixedSizes = new List<float>() { -1, onOffWidth };
//                bool customButton(Rect rect, string label) => UIUtility.ToggleButton(rect, label, poolEnabled);
//                innerList.ButtonRow(rowLabels, out int indexClicked, PairAlignment.Proportional, fixedSizes, null, customButton);
//                if (indexClicked != -1)
//                {
//                    switch (indexClicked)
//                    {
//                        case 0:
//                            selectedPool = pool;
//                            break;
//                        case 1:
//                            selectedPool = pool;  // could use fallthrough here, but fallthrough is bad practice.
//                            enabledQuirkPools[pool.defName] = !poolEnabled;
//                            foreach(string key in pool.quirks.ConvertAll(quirk => quirk.defName))
//                            {
//                                enabledQuirks[key] = !poolEnabled;
//                            }
//                            break;
//                    }
//                }
//            }
//            /*if (poolScrollHeightStale)
//            {
//                poolScrollHeight = outerList.CurHeight;
//                poolScrollHeightStale = false;
//            }*/
//#if v1_2
//            outerList.EndScrollView(ref poolScrollHeight, ref poolScrollHeightStale, ref innerRect);
//#else
//            innerList.EndScrollView(ref poolScrollHeight, ref poolScrollHeightStale);
//#endif
//            outerList.End();
//        }

//        private static Vector2 infoScrollPosition;
//        private static float infoScrollHeight = 0f;
//        private static bool infoScrollHeightStale = true;
//        public static void DoQuirkPoolInfo(Rect inRect, QuirkPoolDef pool, out QuirkDef selectedQuirk)
//        {
//            RV2Log.Message("poolinfo required height: " + infoScrollHeight, true, true, "Settings");
//            selectedQuirk = null;
//            Listing_Standard outerList = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            outerList.Begin(inRect); ;
//            outerList.HeaderLabel("RV2_Settings_Quirks_PoolInfo_Header".Translate());
//            if (pool == null)
//            {
//                Rect labelRect = outerList.GetRect(inRect.height - outerList.CurHeight);
//                string displayLabel = "RV2_Settings_Quirks_PoolInfo_NoPoolSelected".Translate();
//                UIUtility.LabelInCenter(labelRect, displayLabel, GameFont.Medium);
//            }
//            else
//            {
//                Rect outerRect = outerList.GetRect(inRect.height - outerList.CurHeight);
//#if v1_2
//                outerList.MakeAndBeginScrollView(outerRect, infoScrollHeight, ref infoScrollPosition, out Rect innerRect);
//                Listing_Standard innerList = outerList;
//#else
//                UIUtility.MakeAndBeginScrollView(outerRect, infoScrollHeight, ref infoScrollPosition, out Listing_Standard innerList);
//#endif
//                innerList.LeftRightLabels("RV2_Settings_Quirks_PoolInfo_PoolName".Translate(), pool.label);

//                innerList.LeftRightLabels("RV2_Settings_Quirks_PoolInfo_PoolType".Translate(), pool.poolType.ToString());
//                innerList.LeftRightLabels("RV2_Settings_Quirks_PoolInfo_GenerationOrder".Translate(), pool.generationOrder.ToString());
//                if(pool.description != null)
//                {
//                    innerList.Label("RV2_Settings_Quirks_PoolInfo_Description".Translate());
//                    innerList.Label(pool.description);
//                }
//                innerList.GapLine();
//                innerList.Label("RV2_Settings_Quirks_PoolInfo_Quirks".Translate());
//                if (innerList.ButtonText("RV2_Settings_ResetAllRaritiesPool".Translate()))
//                {
//                    RemoveAllRarityOverrides(currentPool);
//                }
//                List<string> enableDisableLabels = new List<string>() { "RV2_Settings_EnableAll".Translate(), "RV2_Settings_DisableAll".Translate() };
//                innerList.ButtonRow(enableDisableLabels, out int enableDisableClicked, PairAlignment.Equal);
//                if (enableDisableClicked != -1)
//                {
//                    switch (enableDisableClicked)
//                    {
//                        case 0:
//                            SetAllQuirks(currentPool, true);
//                            break;
//                        case 1:
//                            SetAllQuirks(currentPool, false);
//                            break;
//                    }
//                }
//                innerList.GapLine();
//                foreach (QuirkDef quirk in pool.quirks)
//                {
//                    bool quirkEnabled = QuirkEnabled(quirk);
//                    PrepareOnOffButton(quirkEnabled, out string onOffLabel, out float onOffWidth);
//                    List<string> rowLabels = new List<string>() { quirk.label, onOffLabel };
//                    // we want the on-off button to always have the same size, looks more uniform
//                    List<float> fixedSizes = new List<float>() { -1, onOffWidth };
//                    bool customButton(Rect rect, string label) => UIUtility.ToggleButton(rect, label, quirkEnabled);
//                    innerList.ButtonRow(rowLabels, out int indexClicked, PairAlignment.Proportional, fixedSizes, null, customButton);
//                    if(indexClicked != -1)
//                    {
//                        switch (indexClicked)
//                        {
//                            case 0:
//                                selectedQuirk = quirk;
//                                break;
//                            case 1:
//                                selectedQuirk = quirk;  // could use fallthrough here, but fallthrough is bad practice. 
//                                enabledQuirks[quirk.defName] = !quirkEnabled;
//                                break;
//                        }
//                    }
//                }
//                innerList.GapLine();
//                DoConstraints(innerList, pool);
//#if v1_2
//                outerList.EndScrollView(ref infoScrollHeight, ref infoScrollHeightStale, ref innerRect);
//#else
//                innerList.EndScrollView(ref infoScrollHeight, ref infoScrollHeightStale);
//#endif
//            }
//            outerList.End();
//        }

//        public static void DoQuirkInfo(Rect inRect,  QuirkDef quirk)
//        {
//            if (quirkRarityOverrides == null)
//            {
//                quirkRarityOverrides = new Dictionary<string, QuirkRarity>();
//            }
//            Listing_Standard list = new Listing_Standard()
//            {
//                ColumnWidth = inRect.width
//            };
//            list.Begin(inRect);
//            list.HeaderLabel("RV2_Settings_Quirks_QuirkInfo_Header".Translate());
//            Rect borderRect = new Rect(inRect.x, inRect.y + list.CurHeight, inRect.width, inRect.height - list.CurHeight);
//            //Widgets.DrawRectFast(borderRect, Color.blue);
//            if(quirk == null)
//            {
//                Rect labelRect = list.GetRect(inRect.height - list.CurHeight);
//                string displayLabel = "RV2_Settings_Quirks_QuirkInfo_NoQuirkSelected".Translate();
//                UIUtility.LabelInCenter(labelRect, displayLabel, GameFont.Medium);
//            }
//            else
//            {
//                list.LeftRightLabels("RV2_Settings_Quirks_PoolInfo_PoolName".Translate(), quirk.label);

//                void rarityAction(QuirkRarity r) => SetRarityOverride(quirk, r);
//                bool currentQuirkHasRarityOverride = quirk.rarity != GetRarity(quirk);
//                string buttonTooltip = currentQuirkHasRarityOverride ? "RV2_Settings_Quirks_PoolInfo_Rarity_TipOverriden".Translate() : "RV2_Settings_Quirks_PoolInfo_Rarity_TipDefault".Translate();
//                bool customButton(Rect localRect, string localLabel) => UIUtility.ToggleButton(localRect, localLabel, !currentQuirkHasRarityOverride);
//                float chance;
//                if (!QuirkEnabled(quirk))
//                {
//                    chance = 0;
//                }
//                else
//                {
//                    switch (currentPool.poolType)
//                    {
//                        case QuirkPoolType.RollForEach:
//                            chance = (float)GetRarity(quirk);
//                            break;
//                        case QuirkPoolType.PickOne:
//                            chance = (float)Math.Round(CalculateChanceForWeight(quirk) * 100);
//                            break;
//                        default:
//                            chance = 0f;
//                            break;
//                    }
//                }
//                QuirkRarity rarity = GetRarity(quirk);
//                string rarityLabel = "RV2_Settings_Quirks_PoolInfo_Rarity".Translate();
//                list.CreateLabelledDropDownForEnum<QuirkRarity>(rarityLabel, rarityAction, rarity, customButton, null, buttonTooltip);
//                list.LeftRightLabels("RV2_Settings_Quirks_PoolInfo_Chance".Translate(), chance + "%");
//                if(GetRarity(quirk) == QuirkRarity.Invalid)
//                {
//                    enabledQuirks.SetOrAdd(quirk.defName, false);
//                    if (quirkRarityOverrides.ContainsKey(quirk.defName))
//                    {
//                        quirkRarityOverrides.Remove(quirk.defName);
//                    }
//                }
//                list.GapLine();
//                list.Label(quirk.description.ExpandPlaceholders("This pawn", Gender.None));
//                list.GapLine();
//                DoConstraints(list, quirk);
//            }
//            list.End();
//        }

//        private static void PrepareOnOffButton(bool enabled, out string label, out float width)
//        {
//            string onLabel = "RV2_Settings_On".Translate();
//            string offLabel = "RV2_Settings_Off".Translate();
//            width = Math.Max(Text.CalcSize(onLabel).x, Text.CalcSize(offLabel).x) + 10f;

//            if (enabled)
//            {
//                label = onLabel;
//            }
//            else
//            {
//                label = offLabel;
//            }
//        }

//        private static void DoConstraints(Listing_Standard list, QuirkPoolDef pool)
//        {
//            DoConstraints(list, pool.requiredQuirks, pool.blockingQuirks, pool.requiredTraits, pool.blockingTraits, pool.requiredKeywords, pool.blockingKeywords, false);
//        }

//        private static void DoConstraints(Listing_Standard list, QuirkDef quirk)
//        {
//            DoConstraints(list, quirk.requiredQuirks, quirk.blockingQuirks, quirk.requiredTraits, quirk.blockingTraits, quirk.requiredKeywords, quirk.blockingKeywords, false);
//        }

//        private static void DoConstraints(Listing_Standard list, List<QuirkDef> requiredQuirks, List<QuirkDef> blockingQuirks, List<TraitDef> requiredTraits, List<TraitDef> blockingTraits, List<string> requiredKeywords, List<string> blockingKeywords, bool showEmpty = true)
//        {
//            DoConstraint(list, "RV2_Settings_Quirks_RequiredQuirks".Translate(), requiredQuirks?.ConvertAll(quirk => quirk.label), showEmpty);
//            DoConstraint(list, "RV2_Settings_Quirks_BlockingQuirks".Translate(), blockingQuirks?.ConvertAll(quirk => quirk.label), showEmpty);
//            DoConstraint(list, "RV2_Settings_Quirks_RequiredTraits".Translate(), requiredTraits?.ConvertAll(trait => trait.label), showEmpty);
//            DoConstraint(list, "RV2_Settings_Quirks_BlockingTraits".Translate(), blockingTraits?.ConvertAll(trait => trait.label), showEmpty);
//            DoConstraint(list, "RV2_Settings_Quirks_RequiredKeywords".Translate(), requiredKeywords, showEmpty);
//            DoConstraint(list, "RV2_Settings_Quirks_BlockingKeywords".Translate(), blockingKeywords, showEmpty);
//        }

//        private static void DoConstraint(Listing_Standard list, string label, List<string> entries, bool showEmpty = true)
//        {
//            if(!showEmpty && entries.NullOrEmpty())
//            {
//                return;
//            }
//            list.Label(label);
//            list.Indent();
//            if (entries.NullOrEmpty())
//            {
//                list.Label("RV2_Settings_Quirks_ListNone".Translate());
//            }
//            else
//            {
//                foreach (string entry in entries)
//                {
//                    list.Label(entry);
//                }
//            }
//            list.Outdent();
//        }

//        public override void ExposeData()
//        {
//            base.ExposeData();
//            ScribeUtilities.ScribeVariableDictionary(ref enabledQuirkPools, "enabledQuirkPools");
//            ScribeUtilities.ScribeVariableDictionary(ref enabledQuirks, "enabledQuirks");
//            ScribeUtilities.ScribeVariableDictionary(ref quirkRarityOverrides, "quirkRarityOverrides");
//        }
//    }
//}
