﻿using System;
using System.Collections.Generic;
using System.Linq;
using Verse;
using RimWorld;

namespace RimVore2
{
    public static class QuirkUtility
    {
        public static QuirkManager QuirkManager(this Pawn pawn, bool initializeIfNull = true)
        {
            if(!RV2Mod.settings.features.VoreQuirksEnabled)
            {
                return null;
            }
            return SaveStorage.DataStore?.GetPawnData(pawn)?.QuirkManager(initializeIfNull);
        }

        public static QuirkPoolDef GetPool(this QuirkDef quirk)
        {
            // Log.Message(string.Join(", ", RV2_Common.SortedQuirkPools.ConvertAll(q => q.defName)));
            return RV2_Common.SortedQuirkPools.Find(pool => pool.quirks.Contains(quirk));
        }

        public static bool IsEnabled(this QuirkDef quirk)
        {
            return RV2Mod.settings.quirks.IsQuirkEnabled(quirk);
        }

        public static bool IsEnabled(this QuirkPoolDef pool)
        {
            return RV2Mod.settings.quirks.IsPoolEnabled(pool);
        }

        /// <summary>
        /// Adapter method for Settings_Quirks. Factors in potentially set overrides of rarity by the user
        /// </summary>
        /// <param name="quirk">Quirk to retrieve rarity for</param>
        /// <returns>Default quirk rarity or whatever the user has set as overwritten rarity</returns>
        public static QuirkRarity GetRarity(this QuirkDef quirk)
        {
            return RV2Mod.settings.quirks.GetRarity(quirk);
        }

        private static bool AreQuirksValid(List<QuirkDef> existingQuirks, List<QuirkDef> requiredQuirks, List<QuirkDef> blockingQuirks, out string reason)
        {
            return IsValid<QuirkDef>(existingQuirks, requiredQuirks, blockingQuirks, out reason);
        }

        private static bool AreTraitsValid(Pawn pawn, List<TraitDef> requiredTraits, List<TraitDef> blockingTraits, out string reason, List<TraitDef> cachedTraits = null)
        {
            if (pawn.story?.traits == null)
            {
                reason = null;
                return true;
            }
            List<TraitDef> pawnTraits;
            if (cachedTraits == null)
            {
                pawnTraits = pawn.story.traits.allTraits.ConvertAll(trait => trait.def);
            }
            else
            {
                pawnTraits = cachedTraits;
            }
            return IsValid<TraitDef>(pawnTraits, requiredTraits, blockingTraits, out reason);
        }

        private static bool AreKeywordsValid(Pawn pawn, List<string> requiredKeywords, List<string> blockingKeywords, out string reason, List<string> cachedKeywords = null)
        {
            List<string> pawnKeywords;
            // if the calling method already calculated the keywords, use them instead, recalculating takes a lot of resources!
            if (cachedKeywords == null)
            {
                pawnKeywords = pawn.PawnKeywords(true);
            }
            else
            {
                pawnKeywords = cachedKeywords;
            }
            return IsValid<string>(pawnKeywords, requiredKeywords, blockingKeywords, out reason);
        }

        private static bool IsValid<T>(List<T> existing, List<T> required, List<T> blocking, out string reason)
        {
            if (required != null)
            {
                List<T> requiredButMissing = required.FindAll(require => !existing.Contains(require));
                if (!requiredButMissing.NullOrEmpty())
                {
                    reason = "Invalid because required " + typeof(T).ToString() + "'s are missing: " + string.Join(", ", requiredButMissing);
                    return false;
                }
            }
            if(blocking != null)
            {
                List<T> blockingInExisting = blocking.FindAll(block => existing.Contains(block));
                if (!blockingInExisting.NullOrEmpty())
                {
                    reason = "Invalid because blocking " + typeof(T).ToString() + "'s exist: " + string.Join(", ", blockingInExisting);
                    return false;
                }
            }
            reason = null;
            return true;
        }

        public static IEnumerable<KeyValuePair<QuirkDef, int>> FilterOutTempQuirksInSamePool(this IEnumerable<KeyValuePair<QuirkDef, int>> tempQuirks)
        {
            Dictionary<QuirkDef, int> quirks = new Dictionary<QuirkDef, int>();
            Dictionary<QuirkPoolDef, KeyValuePair<QuirkDef, int>> pickOneQuirksByPool = new Dictionary<QuirkPoolDef, KeyValuePair<QuirkDef, int>>();
            foreach (KeyValuePair<QuirkDef, int> kvp in tempQuirks)
            {
                QuirkPoolDef pool = kvp.Key.GetPool();
                // RollForEach quirks have no collisions, add them and go to next temp quirk
                if(pool.poolType == QuirkPoolType.RollForEach)
                {
                    quirks.Add(kvp.Key, kvp.Value);
                    continue;
                }
                
                // quirk is a PickOne and the first of its pool, add it as the dominant quirk, then go to next temp quirk
                if (!pickOneQuirksByPool.ContainsKey(pool))
                {
                    pickOneQuirksByPool.Add(pool, kvp);
                    continue;
                }

                // is the current quirks duration longer than the currently dominant quirk for the pool?
                if(kvp.Value > pickOneQuirksByPool[pool].Value)
                {
                    pickOneQuirksByPool.SetOrAdd(pool, kvp);
                }
                // if the current quirk is not more dominant, just ignore it
            }

            return quirks.AsEnumerable();
        }

        public static List<QuirkDef> GetAllForcedQuirks(this Pawn pawn)
        {
            List<QuirkDef> quirks = new List<QuirkDef>();
            quirks.AddRange(pawn.GetForcedQuirksByHediffs());
            quirks.AddRange(pawn.GetForcedQuirksFromBackstory());
            return quirks;
        }

        public static List<QuirkDef> GetAllBlockedQuirks(this Pawn pawn)
        {
            List<QuirkDef> quirks = new List<QuirkDef>();
            quirks.AddRange(pawn.GetBlockedQuirksFromBackstory());
            return quirks;
        }

        public static List<QuirkDef> GetForcedQuirksByHediffs(this Pawn pawn)
        {
            IEnumerable<QuirkDef> forcedQuirksByHediffs = pawn.health?.hediffSet?
                    .GetAllComps()?
                    .Where(comp => comp is HediffComp_QuirkForcer)?
                    .Cast<HediffComp_QuirkForcer>()?
                    .SelectMany(comp => comp.ForcedQuirks);
            if(forcedQuirksByHediffs == null)
            {
                return new List<QuirkDef>();
            }
            return forcedQuirksByHediffs.ToList();
        }

        public static bool HasAllQuirks(Pawn pawn, List<QuirkDef> requiredQuirks, bool generateQuirkManager = false)
        {
            QuirkManager quirks = pawn.QuirkManager(generateQuirkManager);
            if (quirks == null)
            {
                return false;
            }
            return requiredQuirks
                .TrueForAll(quirk => quirks.HasQuirk(quirk));
        }

        public static IEnumerable<string> QuirkPoolAssignmentErrors()
        {
            List<QuirkDef> quirks = DefDatabase<QuirkDef>.AllDefsListForReading;
            // Log.Message(string.Join(", ", quirks.ConvertAll(q => q.defName)));
            foreach(QuirkDef quirk in quirks)
            {
                if(quirk.GetPool() == null)
                {
                    yield return "QuirkDef " + quirk.defName + " is not assigned to a QuirkPoolDef";
                } 
            }
        }

        public static void ForceVorenappingQuirks(Pawn pawn)
        {
            QuirkManager predatorQuirks = pawn.QuirkManager();
            // predator can't have quirks, oh well
            if (predatorQuirks == null)
            {
                return;
            }
            predatorQuirks.TryPostInitAddQuirk(QuirkDefOf.Enablers_Core_Type_Oral, out _);
            predatorQuirks.TryPostInitAddQuirk(QuirkDefOf.Enablers_Core_Goal_Longendo, out _);
        }

        public static void ForceInstantSwallowQuirk(Pawn pawn)
        {
            QuirkManager predatorQuirks = pawn.QuirkManager();
            if (predatorQuirks == null)
            {
                return;
            }
            predatorQuirks.TryPostInitAddQuirk(QuirkDefOf.Cheat_InstantSwallow, out _);
        }

        public static float ModifyOffsetWithQuirks(float value, Pawn pawn, PawnCapacityDef capacity)
        {
            if (pawn == null || capacity == null || value == 0)
            {
                return value;
            }
            QuirkManager quirks = pawn.QuirkManager(false);
            if (quirks == null)
            {
                return value;
            }
            float modifier = quirks.CapModOffsetModifierFor(capacity);
            float newValue = modifier * value;
            // Log.Message("TRANSPILER - calculated offset modifier for " + pawn.LabelShort + "|" + capacity.label + ": " + modifier + " - new value: " + newValue);
            return newValue;
        }
    }
}
