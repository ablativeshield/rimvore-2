﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public class Quirk : IExposable
    {
        public QuirkDef def;
        public QuirkPoolDef Pool => def.GetPool();

        public Quirk(QuirkDef def)
        {
            this.def = def;
        }

        public Quirk() { }

        public virtual void ExposeData()
        {
            Scribe_Defs.Look(ref def, "def");
        }

        public bool IsValid()
        {
            if(def == null)
            {
                return false;
            }
            return true;
        }
    }

    public class TempVoreQuirk : Quirk
    {
        public TempVoreQuirk(QuirkDef def, int duration) : base(def)
        {
            totalDuration = duration;
            durationLeft = duration;
        }

        public TempVoreQuirk() : base() { }

        public Quirk originalQuirk;
        public int totalDuration;
        public int durationLeft;

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Deep.Look(ref originalQuirk, "originalQuirk", new object[0]);
            Scribe_Values.Look(ref totalDuration, "totalDuration");
            Scribe_Values.Look(ref durationLeft, "durationLeft");
        }
    }
}
