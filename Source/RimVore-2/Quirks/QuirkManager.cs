﻿using System;
using System.Collections.Generic;
using System.Linq;
using Verse;
using RimWorld;

namespace RimVore2
{
    public class QuirkManager : IExposable
    {
        private Pawn pawn;

        private QuirkHelper_Persistent persistentQuirkHelper;
        private QuirkHelper_Persistent PersistentQuirkHelper
        {
            get
            {
                if(persistentQuirkHelper == null)
                {
                    persistentQuirkHelper = new QuirkHelper_Persistent(pawn);
                }
                return persistentQuirkHelper;
            }
        }
        //private QuirkHelper_Temporary temporaryQuirkHelper;
        //private QuirkHelper_Temporary TemporaryQuirkHelper
        //{
        //    get
        //    {
        //        if(temporaryQuirkHelper == null)
        //        {
        //            temporaryQuirkHelper = new QuirkHelper_Temporary(pawn);
        //        }
        //        return temporaryQuirkHelper;
        //    }
        //}

        public QuirkManager(Pawn pawn)
        {
            this.pawn = pawn;
        }

        public QuirkManager()
        {
            if(pawn == null)
            {
                RV2Log.Warning("Tried to construct quirk manager without pawn! This will cause issues!", "Quirks");
                return;
            }
        }

        public bool ActiveQuirksStale => PersistentQuirkHelper.IsStale/* || TemporaryQuirkHelper.IsStale*/;
        private List<Quirk> activeQuirks;
        public List<Quirk> ActiveQuirks
        {
            get
            {
                if (activeQuirks == null || ActiveQuirksStale)
                {
                    RV2Log.Message("Active quirks are stale, refreshing", true, false, "Quirks");
                    activeQuirks = PersistentQuirkHelper.Quirks();
                    //activeQuirks.AddRange(TemporaryQuirkHelper.Quirks());
                    ResolveConflicts();
                    if (!activeQuirks.NullOrEmpty())
                    {
                        //RV2Log.Message("persistent quirks: " + string.Join(", ", PersistentQuirkHelper.Quirks().ConvertAll(q => q.def.defName)), true, false, "Quirks");
                        //RV2Log.Message("temporary quirks: " + string.Join(", ", TemporaryQuirkHelper.Quirks().ConvertAll(q => q.def.defName)), true, false, "Quirks");
                        RV2Log.Message("Active quirks: " + string.Join(", ", activeQuirks.ConvertAll(q => q.def.defName)), true, false, "Quirks");
                    }
                    else
                    {
                        RV2Log.Message("Active quirks are empty!", true, false, "Quirks");
                    }
                    PersistentQuirkHelper.IsStale = false;
                    //TemporaryQuirkHelper.IsStale = false;
                    areGroupedQuirksStale = true;
                }
                return activeQuirks;
            }
        }

        private SortedDictionary<QuirkPoolDef, List<Quirk>> groupedQuirks;
        private bool areGroupedQuirksStale = true;
        public SortedDictionary<QuirkPoolDef, List<Quirk>> GroupedQuirks
        {
            get
            {
                // this calls the property creator, which may or may not set the grouped quirks to stale. It is important to do this before checking for areGroupedQuirksStale, because that value is only set within the ActiveQuirks property itself!
                List<Quirk> activeQuirks = ActiveQuirks;

                // grouped quirks should never be null, but it doesn't hurt to check and set them if they are
                if (areGroupedQuirksStale || groupedQuirks == null)
                {
                    RV2Log.Message("Quirks are stale, regrouping", true, false, "Quirks");
                    Dictionary<QuirkPoolDef, List<Quirk>> quirks = activeQuirks
                        .GroupBy(quirk => quirk.Pool)
                        .ToDictionary(
                            group => group.Key,
                            group => group.AsEnumerable().ToList()
                        );
                    groupedQuirks = new SortedDictionary<QuirkPoolDef, List<Quirk>>(quirks);
                    areGroupedQuirksStale = false;
                }
                return groupedQuirks;
            }
        }

        public bool HasQuirk(QuirkDef quirkDef)
        {
            return ActiveQuirks.Any(quirk => quirk.def == quirkDef);
        }

        public void Tick()
        {
            PersistentQuirkHelper.Tick();
            //TemporaryQuirkHelper.Tick();
        }

        public void RollPersistentQuirks()
        {
            PersistentQuirkHelper.CalculateQuirks();
        }

        public void RerollPersistentQuirkPool(QuirkPoolDef pool)
        {
            PersistentQuirkHelper.CalculatePool(pool);
        }

        public bool HasAllQuirksInPersistentPool(QuirkPoolDef pool)
        {
            return PersistentQuirkHelper.HasAllQuirksInPool(pool);
        }

        public void RemovePersistentQuirk(QuirkDef quirk)
        {
            PersistentQuirkHelper.RemoveQuirk(quirk);
        }
        public void RemovePersistentQuirk(Quirk quirk)
        {
            PersistentQuirkHelper.RemoveQuirk(quirk);
        }

        public bool CanPostInitAddQuirk(QuirkDef quirk, out string reason)
        {
            return PersistentQuirkHelper.IsQuirkApplicable(quirk, out reason);
        }

        public bool TryPostInitAddQuirk(QuirkDef quirk, out string reason)
        {
            if(!PersistentQuirkHelper.IsQuirkApplicable(quirk, out reason))
            {
                return false;
            }
            PersistentQuirkHelper.ForceQuirk(quirk);
            return true;
        }

        private void ResolveConflicts()
        {
            List<Quirk> resolvedQuirks = new List<Quirk>();
            List<QuirkPoolDef> resolvedPools = new List<QuirkPoolDef>();
            if(activeQuirks == null)
            {
                return;
            }
            foreach (Quirk activeQuirk in activeQuirks)
            {
                if(!activeQuirk.IsValid())
                {
                    RV2Log.Warning("invalid quirk, removing from quirks, usually caused by updates", "Quirks");
                    RemovePersistentQuirk(activeQuirk);
                    continue;
                }
                QuirkPoolDef pool = activeQuirk.Pool;
                if (pool.poolType == QuirkPoolType.RollForEach)
                {
                    resolvedQuirks.Add(activeQuirk);
                    continue;
                }
                // first of its pool, add it
                if (!resolvedPools.Contains(pool))
                {
                    resolvedQuirks.Add(activeQuirk);
                    resolvedPools.Add(pool);
                    continue;
                }
                else
                {
                    Quirk oldQuirk = resolvedQuirks.Find(quirk => quirk.Pool == pool);
                    if (activeQuirk is TempVoreQuirk activeQuirkTemp)
                    {
                        if(oldQuirk is TempVoreQuirk oldQuirkTemp)
                        {
                            // two temp quirks colliding (this should have been prevented by the ResolveConflicts() in the QuirkHelper_Temporary
                            RV2Log.Warning("Two temporary PickOne quirks are colliding!", "Quirks");
                            if(activeQuirkTemp.durationLeft > oldQuirkTemp.durationLeft)
                            {
                                resolvedQuirks.Replace(oldQuirk, activeQuirk);
                            }
                            continue;
                        }
                        else
                        {
                            // temp quirk and persistent quirk, temp quirk is always dominant
                            resolvedQuirks.Replace(oldQuirk, activeQuirk);
                            continue;
                        }
                    }
                    else{
                        // this should never happen, we have a PickOne pool with two persistent quirks, simply ignore the current activeQuirk
                        Log.Error("Collision of persistent quirks, removing quirk " + activeQuirk.def.defName);
                    }
                }
            }
            activeQuirks.Clear();
            activeQuirks.AddRange(resolvedQuirks);
        }

        // ---------------- UI methods ------------------
        public void PickQuirkDialogue(QuirkPoolDef pool)
        {
            PersistentQuirkHelper.PickQuirkDialogue(pool);
        }
        public void AddQuirkDialogue(QuirkPoolDef pool)
        {
            PersistentQuirkHelper.AddQuirkDialogue(pool);
        }

        // ---------------- comp methods ------------------

        public ThoughtDef GetOverriddenThought(ThoughtDef originalThought)
        {
            List<QuirkComp_ThoughtOverride> overridingThoughtComps = GetAllCompsByType<QuirkComp_ThoughtOverride>()?
                .Where(comp => comp.originalThought == originalThought)
                .ToList();
            ThoughtDef overrideThought;
            if (overridingThoughtComps.NullOrEmpty())
            {
                // Log.Message("No override thoughts found");
                overrideThought = originalThought;
            }
            else
            {
                // Log.Message("Multiple override thought found");
                overrideThought = overridingThoughtComps.MaxBy(comp => comp.priority).overrideThought;
            }

            if (overrideThought != null)
            {
                RV2Log.Message("Overriding thought " + originalThought.defName + " with " + overrideThought.defName, true, false, "Quirks");
                return overrideThought;
            }
            else
            {
                return originalThought;
            }
        }

        public IEnumerable<ThoughtDef> GetPostVoreMemories(List<string> keywords)
        {
            IEnumerable<QuirkComp_PostVoreMemory> postVoreThoughtComps = GetAllCompsByType<QuirkComp_PostVoreMemory>();
            // for each thought comp check the keywords for applicable thoughts
            IEnumerable<ThoughtDef> memoriesForComp = postVoreThoughtComps
                .Where(comp => comp.keywords  // take each comps keywords
                    .TrueForAll(keyword => keywords.Contains(keyword))) // and check if the current vores keywords has them all
                .Select(comp => comp.memory);   // then use the memory
            RV2Log.Message("keywords " + string.Join(",", keywords) + " generate these memories: " + string.Join(",", memoriesForComp.Select(memory => memory.defName)), "Quirk");
            return memoriesForComp;
        }

        public bool HasSpecialFlag(string flag)
        {
            return GetAllCompsByType<QuirkComp_SpecialFlag>().Any(quirk => quirk.flag == flag);
        }

        public bool HasVoreEnabler(VoreTargetSelectorRequest request)
        {
            return GetAllCompsByType<QuirkComp_VoreEnabler>().Any(comp => comp.selector.Matching(request));
        }

        public IEnumerable<T> GetAllCompsByType<T>() where T : QuirkComp
        {
            return ActiveQuirks
                .SelectMany(quirk => quirk.def.GetComps<T>());
        }

        public bool HasComp<T>() where T : QuirkComp
        {
            return ActiveQuirks.Any(quirk => quirk.def.HasComp<T>());
        }

        public bool HasValueModifier(string modifierName)
        {
            return ActiveQuirks.Any(quirk => quirk.def.HasValueModifiersFor(modifierName));
        }

        public bool TryGetValueModifier(string modifierName, ModifierOperation operation, out float modifierValue)
        {
            var comps = GetAllCompsByType<QuirkComp_ValueModifier>()
                .Where(comp => comp.modifierName == modifierName   // limit to those that actually affect the value we are looking at
                    && comp.operation == operation)  // limit to those that do the same operation as what we are looking for
                .OrderBy(comp => comp.priority); // make sure we follow priority, so mathematical operations are done in-order of priority
            if (comps.EnumerableNullOrEmpty())
            {
                modifierValue = 0;
                return false;
            }
            IEnumerable<float> values = comps.Select(comp => comp.modifierValue); // get the float value
            if (values.EnumerableNullOrEmpty())
            {
                modifierValue = 0;
                return false;
            }
            modifierValue = values.Aggregate((x, y) => operation.Aggregate(x, y));    // aggregate based on operation
            RV2Log.Message("pawn " + pawn.LabelShort + " has modifiers for " + modifierName + ": " + modifierValue, true, true);
            return true;
        }

        public float ModifyValue(string modifierName, float value)
        {
            IEnumerable<QuirkComp_ValueModifier> modifierComps = GetAllCompsByType<QuirkComp_ValueModifier>()
                .Where(comp => comp.modifierName == modifierName)
                .OrderBy(comp => comp.priority);
            if (!modifierComps.EnumerableNullOrEmpty())
            {
                foreach(QuirkComp_ValueModifier comp in modifierComps)
                {
                    value = comp.Modify(value);
                }
            }
            return value;
        }

        public bool HasTotalSelectorModifier(VoreTargetSelectorRequest request)
        {
            IEnumerable<float> values = GetAllCompsByType<QuirkComp_VoreTargetSelectorModifier>()
                .Where(comp => comp.ModifierValid(request))
                .Select(comp => comp.GetModifierValue(request))
                .Where(value => value != float.MinValue);
            return !values.EnumerableNullOrEmpty();
        }

        public float GetTotalSelectorModifier(VoreTargetSelectorRequest request, ModifierOperation modifierOperation = ModifierOperation.Add)
        {
            IEnumerable<float> values = GetAllCompsByType<QuirkComp_VoreTargetSelectorModifier>()
                .Where(comp => comp.ModifierValid(request))
                .Select(comp => comp.GetModifierValue(request))
                .Where(value => value != float.MinValue);
            if (!values.EnumerableNullOrEmpty())
            {
                return values.Aggregate((a, b) => modifierOperation.Aggregate(a, b));    // aggregate all modifier values
            }
            else
            {
                return modifierOperation.DefaultModifierValue();
            }
        }
        public float GetTotalSelectorModifier(VoreRole role, ModifierOperation modifierOperation = ModifierOperation.Add)
        {
            VoreTargetSelectorRequest request = new VoreTargetSelectorRequest(true)
            {
                role = role
            };
            return GetTotalSelectorModifier(request, modifierOperation);
        }
        public float GetTotalSelectorModifier(RaceType raceType, ModifierOperation modifierOperation = ModifierOperation.Add)
        {
            VoreTargetSelectorRequest request = new VoreTargetSelectorRequest(true)
            {
                raceType = raceType
            };
            return GetTotalSelectorModifier(request, modifierOperation);
        }
        public bool HasDesignationBlock(DesignationDef designation)
        {
            IEnumerable<QuirkComp_DesignationBlock> blocks = GetAllCompsByType<QuirkComp_DesignationBlock>();
            // Log.Message("blocks: " + string.Join(", ", blocks.Select(b => b.ToString())));
            return blocks.Any(block => block.designation == designation);
        }

        private Dictionary<PawnCapacityDef, float> capacityModifiers = new Dictionary<PawnCapacityDef, float>();
        public float CapModOffsetModifierFor(PawnCapacityDef capDef, IEnumerable<QuirkComp_CapacityOffsetModifier> modifiers = null)
        {
            if (!capacityModifiers.ContainsKey(capDef))
            {
                if (modifiers == null)
                {
                    modifiers = GetAllCompsByType<QuirkComp_CapacityOffsetModifier>();
                }
                if (!modifiers.Any(m => m.capacity == capDef))
                {
                    return 1;
                }
                IEnumerable<float> modifierValues = modifiers
                    .Where(m => m.capacity == capDef)
                    .Select(m => m.modifierValue);
                float modifier = modifierValues.Aggregate((a, b) => ModifierOperation.Multiply.Aggregate(a, b));
                capacityModifiers.Add(capDef, modifier);
            }
            return capacityModifiers.TryGetValue(capDef, 1);
        }

        public void ExposeData()
        {
            Scribe_Deep.Look(ref persistentQuirkHelper, "persistentQuirkHelper", pawn);
            //Scribe_Deep.Look(ref temporaryQuirkHelper, "temporaryQuirkHelper", pawn);
            Scribe_Collections.Look(ref activeQuirks, "activeQuirks", LookMode.Deep);
            Scribe_References.Look(ref pawn, "pawn");
            // when loading, validate that all quirks are valid, remove invalid ones
            if (Scribe.mode == LoadSaveMode.LoadingVars)
            {
                if (!activeQuirks.NullOrEmpty())
                {
                    int removedQuirks = activeQuirks.RemoveAll(quirk => !quirk.IsValid());
                    if(removedQuirks > 0)
                    {
                        RV2Log.Message("Had to remove " + removedQuirks + " quirks due to being invalid", "Quirks");
                    }
                }
            }
        }
    }
}
