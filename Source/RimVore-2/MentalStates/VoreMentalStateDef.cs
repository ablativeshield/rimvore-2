﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public class VoreMentalStateDef : MentalStateDef
    {
        public VoreTargetRequest request;
        public string targetChangeMessageKey;
        public MentalStateDef fallbackMentalState;
        public VoreRole initiatorRole = VoreRole.Predator;
        public int targetCountToVore = 1;
        public List<VoreGoalDef> goalWhitelist;
        public List<VoreGoalDef> goalBlacklist;
        public List<VoreTypeDef> typeWhitelist;
        public List<VoreTypeDef> typeBlacklist;
        public List<DesignationDef> designationWhitelist;
        public List<DesignationDef> designationBlacklist;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (stateClass == typeof(MentalState_VoreTargeter) && request == null)
            {
                yield return "Required field \"request\" is not set";
            }
            if (fallbackMentalState == null)
            {
                yield return "Required field \"fallbackMentalState\" is not set";
            }
        }
    }
}
