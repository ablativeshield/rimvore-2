﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace RimVore2
{
    public class MentalState_VoreTargeter : MentalState
    {
        public Pawn CurrentTarget;
        public VoreMentalStateDef VoreMentalStateDef => (VoreMentalStateDef)base.def;
        public virtual VoreTargetRequest Request => VoreMentalStateDef.request;
        protected string TargetChangeMessageKey => VoreMentalStateDef.targetChangeMessageKey;
        protected MentalStateDef FallbackMentalState => VoreMentalStateDef.fallbackMentalState;
        public VoreRole InitiatorRole => VoreMentalStateDef.initiatorRole;
        protected List<Pawn> voredTargets = new List<Pawn>();
        protected virtual int TargetsToVoreCount => VoreMentalStateDef.targetCountToVore;
        public bool HasVoredEnoughTargets => voredTargets.Count() >= TargetsToVoreCount;
        private bool hasTriggeredFallback = false;

        public void CallFallbackMentalState()
        {
            hasTriggeredFallback = true;
            base.pawn?.mindState?.mentalStateHandler?.TryStartMentalState(stateDef : FallbackMentalState);
        }

        public override void PreStart()
        {
            base.PreStart();
            //Log.Message("prestart hook, trying to determine target");
            if (!TryDetermineTarget())
            {
                //Log.Message("prestart target did not work, falling back to " + FallbackMentalState.defName);
                CallFallbackMentalState();
            }
        }

#if v1_2
        public override string GetBeginLetterText()
#else
        public override TaggedString GetBeginLetterText()
#endif
        {
            string beginLetter = base.def.beginLetter;  //hasTriggeredFallback ? FallbackMentalState.beginLetter : 
            // we abuse base game behaviour here. If we are falling back to the fallback mental state, we don't want to display a letter
            // Base game does a check on the letter text that we can set to null to prevent the letter from being created altogether
            if (beginLetter.NullOrEmpty() || hasTriggeredFallback)
            {
                return null;
            }
            return beginLetter.Formatted(base.pawn.LabelShort, CurrentTarget?.LabelShort).AdjustedFor(base.pawn, "PAWN", true).CapitalizeFirst();
        }

        public override void MentalStateTick()
        {
            if (base.pawn.IsHashIntervalTick(150))
            {
                if (NeedToRecalculateTarget())
                {
                    if (!TryDetermineTarget())
                    {
                        base.RecoverFromState();
                        return;
                    }
                    if(TargetChangeMessageKey != null)
                    {
                        string message = TargetChangeMessageKey.Translate(base.pawn.LabelShort, CurrentTarget.LabelShort);
                        Messages.Message(message, base.pawn, MessageTypeDefOf.NeutralEvent);
                    }
                }
            }
            base.MentalStateTick();
        }

        private bool NeedToRecalculateTarget()
        {
            if(InitiatorRole == VoreRole.Predator)
            {
                bool currentTargetVored = CurrentTarget != null
                           && GlobalVoreTrackerUtility.IsPreyOf(CurrentTarget, base.pawn);

                if (currentTargetVored)
                {
                    voredTargets.AddDistinct(CurrentTarget);
                }
                bool targetStillValid = IsTargetStillValidAndReachable();
                return !(HasVoredEnoughTargets || targetStillValid);
            }
            else
            {
                if(GlobalVoreTrackerUtility.IsPreyOf(base.pawn, CurrentTarget))
                {
                    return false;
                }
                return !IsTargetStillValidAndReachable();
            }
        }

        private bool IsTargetStillValidAndReachable()
        {
            if (!base.pawn.CanReach(CurrentTarget, PathEndMode.ClosestTouch, Danger.Deadly))
            {
                return false;
            }
            if (!Request.IsValid(CurrentTarget))
            {
                return false;
            }
            return true;
        }

        public override void PostEnd()
        {
            base.PostEnd();
            // TODO: overwrite base behaviour for recovery message here?
        }

        protected virtual bool TryDetermineTarget()
        {
            IEnumerable<Pawn> targets = TargetUtility.GetVorablePawns(base.pawn, Request, 1);
            if (targets.EnumerableNullOrEmpty())
            {
                return false;
            }
            CurrentTarget = targets.RandomElement();
            //Log.Message("Determined target " + CurrentTarget.LabelShort);
            return true;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Collections.Look(ref voredTargets, "targetsVored", LookMode.Reference);
            Scribe_References.Look(ref CurrentTarget, "CurrentTarget", false);
        }
    }

    public class MentalState_ForbiddenFruit : MentalState_VoreTargeter
    {
        readonly Func<Pawn, bool> validator = delegate (Pawn pawn)
        {
            DeathActionWorker deathAction = pawn.RaceProps?.DeathActionWorker;
            return deathAction is DeathActionWorker_SmallExplosion
                || deathAction is DeathActionWorker_BigExplosion;
        };

        public override VoreTargetRequest Request => new VoreTargetRequest
        (
            isAnimal : true,
            canBeFatalVored : true,
            validator : validator
        );
    }

    public class MentalState_VoreTargeter_Kill : MentalState_VoreTargeter
    {
        public override void MentalStateTick()
        {
            base.MentalStateTick();

            if (base.pawn.IsHashIntervalTick(150))
            {
                bool allTargetsDead = voredTargets.All(pawn => pawn.Dead);
                if (allTargetsDead && HasVoredEnoughTargets)
                {
                    pawn.MentalState.RecoverFromState();
                }
            }
                
        }
    }
}
