﻿using RimWorld;
using System.Collections.Generic;
using System;
using Verse;
using Verse.Sound;

namespace RimVore2
{
    public static class SoundManager
    {
        public static void PlaySingleSound(Pawn pawn, SoundDef soundDef)
        {
            if(pawn.Map == null)
            {
                return;
            }
            if (!RV2Mod.settings.sounds.SoundsEnabled)
            {
                return;
            }
            string defName = soundDef.defName;
            if(defName.Contains("Moan") || defName.Contains("Orgasm"))
            {
                if (pawn.RaceProps?.Animal == true)
                {
                    return;
                }
                switch (pawn.gender)
                {
                    case Gender.Male:
                        defName = defName + "Male";
                        soundDef = DefDatabase<SoundDef>.GetNamed(defName, false);
                        break;
                    case Gender.Female:
                        defName = defName + "Female";
                        soundDef = DefDatabase<SoundDef>.GetNamed(defName, false);
                        break;
                    case Gender.None:
                        RV2Log.Message("No moans and orgasms for genderless pawns.", true);
                        return;
                }
            }
            if (soundDef == null)
            {
                RV2Log.Message("No soundDef found for " + defName + ", not playing sound", true);
                return;
            }
            // check if sound is enabled in settings
            if (!RV2Mod.settings.sounds.IsEnabled(soundDef))
            {
                RV2Log.Message("Not playing blocked sound (or missing soundDef in settings)", true);
                return;
            }
            RV2Log.Message("Playing sound: " + soundDef.defName, true);
#pragma warning disable IDE0017 // Simplify object initialization
            SoundInfo sinfo = new TargetInfo(pawn.Position, pawn.Map);
#pragma warning restore IDE0017 // Simplify object initialization
            sinfo.volumeFactor = RV2Mod.settings.sounds.SoundVolumeModifier;
            soundDef.PlayOneShot(sinfo);
        }
    }
}
