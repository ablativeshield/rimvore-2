﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public abstract class QuirkFactor
    {
        public abstract bool CanGetFactor(Pawn pawn);
        public abstract float GetFactor(Pawn pawn, ModifierOperation operation);
        public abstract float ModifyValue(Pawn pawn, float value);
        public abstract string GetDescription(Pawn pawn);

        public abstract IEnumerable<string> ConfigErrors();
    }

    public class QuirkFactor_ValueModifier : QuirkFactor, IExposable
    {
        string modifierName;

        public override bool CanGetFactor(Pawn pawn)
        {
            QuirkManager pawnQuirks = pawn.QuirkManager(false);
            if(pawnQuirks == null)
            {
                return false;
            }
            if (!pawnQuirks.HasValueModifier(modifierName))
            {
                return false;
            }
            return true;
        }

        public override float GetFactor(Pawn pawn, ModifierOperation operation)
        {
            QuirkManager quirks = pawn.QuirkManager();
            quirks.TryGetValueModifier(modifierName, operation, out float factor);
            //Log.Message("Retrieving modifier " + modifierName + " for " + pawn.LabelShort + ": " + factor);
            return factor;
        }

        public override float ModifyValue(Pawn pawn, float value)
        {
            QuirkManager quirks = pawn.QuirkManager();
            return quirks.ModifyValue(modifierName, value);
        }

        public override string GetDescription(Pawn pawn)
        {
            IEnumerable<Quirk> factorQuirks = pawn.QuirkManager().ActiveQuirks
                .Where(quirk => quirk.def.HasValueModifiersFor(modifierName));
            List<string> descriptions = new List<string>();
            List<ModifierOperation> modifierOperations = GeneralUtility.GetValues<ModifierOperation>();
            foreach (Quirk factorQuirk in factorQuirks)
            {
                foreach(ModifierOperation operation in modifierOperations)
                {
                    if(factorQuirk.def.TryGetValueModifierFor(modifierName, operation, out float factor))
                    {
                        descriptions.Add(
                            "    " +
                            factorQuirk.def.label +
                            ": " +
                            operation.OperationSymbol() +
                            " " + 
                            factor
                        );
                    }
                }
            }
            return string.Join("\n", descriptions);
        }

        public void ExposeData()
        {
            Scribe_Values.Look(ref modifierName, "modifierName");
        }

        public override IEnumerable<string> ConfigErrors()
        {
            if(modifierName == null)
            {
                yield return "Required field \"modifierName\" is not set";
            }
        }
    }

    public class RV2StatDef : StatDef
    {
        public List<QuirkFactor> quirkFactors;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (quirkFactors.NullOrEmpty())
            {
                yield return "Required list \"quirkFactors\" is not set or empty";
            }
        }
    }

    public class RV2StatWorker : StatWorker
    {
        RV2StatDef RV2Stat => (RV2StatDef)stat;

        public override bool IsDisabledFor(Thing thing)
        {
            if (base.IsDisabledFor(thing))
            {
                return true;
            }
            if (!(thing is Pawn pawn))
            {
                return true;
            }
            if (pawn.QuirkManager(false) == null)
            {
                return true;
            }
            return false;
        }

        public override bool ShouldShowFor(StatRequest req)
        {
            bool isVisible = base.ShouldShowFor(req)
                && req.Thing is Pawn
                && !IsDisabledFor(req.Thing);
            //Log.Message("should show stat for " + req.Thing + "? : " + isVisible);
            return isVisible;
        }

        public override float GetValueUnfinalized(StatRequest req, bool applyPostProcess = true)
        {
            float statValue = base.GetValueUnfinalized(req, applyPostProcess);
            Pawn pawn = (Pawn)req.Thing;
            IEnumerable<QuirkFactor> quirkFactors = RV2Stat.quirkFactors;
            if (!quirkFactors.EnumerableNullOrEmpty())
            {
                foreach(QuirkFactor quirkFactor in quirkFactors)
                {
                    statValue = quirkFactor.ModifyValue(pawn, statValue);
                    //statValue *= quirkFactor.GetFactor(pawn, ModifierOperation.Multiply);
                }
            }
            return statValue;
        }

        public override string GetExplanationUnfinalized(StatRequest req, ToStringNumberSense numberSense)
        {
            string description = base.GetExplanationUnfinalized(req, numberSense);
            Pawn pawn = (Pawn)req.Thing;
            IEnumerable<QuirkFactor> quirkFactors = RV2Stat.quirkFactors
                .Where(quirkFactor => quirkFactor.CanGetFactor(pawn));
            if (!quirkFactors.EnumerableNullOrEmpty())
            {
                description += "\nQuirks:\n";
                foreach (QuirkFactor quirkFactor in quirkFactors)
                {
                    description += quirkFactor.GetDescription(pawn);
                }
            }
            return description;
        }
    }
}
