﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld.Planet;

// base game Pawn_CarryTracker implements carried things similarly, adapted from there

namespace RimVore2
{
    public class VoreContainer : IThingHolder, IExposable
    {
        public IThingHolder ParentHolder => predator;

        private Pawn predator;
        private ThingOwner<Thing> innerContainer;
        public bool IsEmpty => ThingsInContainer.NullOrEmpty();

        public List<Thing> ThingsInContainer => innerContainer.Cast<Thing>().ToList();
        public VoreProductContainer VoreProductContainer => (VoreProductContainer)innerContainer.InnerListForReading.Find(thing => thing is VoreProductContainer);

        public VoreContainer() { }

        public VoreContainer(Pawn predator)
        {
            this.predator = predator;
            innerContainer = new ThingOwner<Thing>(this);
        }

        public ThingOwner GetDirectlyHeldThings()
        {
            return this.innerContainer;
        }

        public void GetChildHolders(List<IThingHolder> outChildren)
        {
            ThingOwnerUtility.AppendThingHoldersFromThings(outChildren, this.GetDirectlyHeldThings());
        }

        public void DestroyAllRottableItems()
        {
            List<Thing> rottableThings = innerContainer.ToList().FindAll(thing => thing.TryGetComp<CompRottable>() != null);
            foreach(Thing thing in rottableThings)
            {
                // the pawn is either decomposed or destroyed later, destroying it now would cause the later part of the code to break
                if(thing is Pawn || thing is Corpse)
                {
                    RV2Log.Warning("DestroyAllRottableItems() tried to destroy a pawn! This was most likely caused by the prey dying before being digested completely");
                    continue;
                }
                thing.Destroy();
                innerContainer.Remove(thing);
                RV2Log.Message("Item " + thing.Label + " has been digested");
            }
        }

        public void DigestAllApparel(DamageDef damageDef)
        {
            IEnumerable<Apparel> allApparel = innerContainer
                .Where(thing => thing is Apparel)
                .Cast<Apparel>();
            if (allApparel.EnumerableNullOrEmpty())
            {
                return;
            }
            foreach(Apparel apparel in allApparel.ToList())
            {
                QuirkManager predatorQuirks = predator.QuirkManager();
                if(predatorQuirks != null)
                {
                    if (predatorQuirks.HasSpecialFlag("DestroyApparel"))
                    {
                        RV2Log.Message("Predator always destroys apparel, destroying " + apparel.LabelShort, true);
                        apparel.Destroy();
                        continue;
                    }
                }

                int apparelQuality = apparel.GetQualityOrFallback();
                SimpleCurve qualityDamageCurve = new SimpleCurve()
                {
                    Points =
                    {
                        new CurvePoint(0, 0.9f),
                        new CurvePoint(1, 0.75f),
                        new CurvePoint(2, 0.5f),
                        new CurvePoint(3, 0.45f),
                        new CurvePoint(4, 0.4f),
                        new CurvePoint(5, 0.3f),
                        new CurvePoint(6, 0.15f),
                        new CurvePoint(7, 0.05f)    // this quality doesn't exist, but defines the max value in case mods add more
                    }
                };
                float baseDamage = qualityDamageCurve.Evaluate(apparelQuality);
                RV2Log.Message("Calculated apparel damage: " + baseDamage, true);
                if(predatorQuirks != null)
                {
                    baseDamage = predatorQuirks.ModifyValue("ApparelDigestionStrength", baseDamage);
                }
                float apparelDamage = baseDamage * apparel.HitPoints;
                DamageInfo damage = new DamageInfo(damageDef, apparelDamage, float.MaxValue, -1, predator);
                apparel.TakeDamage(damage);

                bool taintApparel = true;
                if(predatorQuirks != null)
                {
                    if (predatorQuirks.HasSpecialFlag("NoTaintedApparel"))
                    {
                        taintApparel = false;
                    }
                }

                if (taintApparel)
                {
                    // sets the "tainted" status of the apparel (even though the pawn is not dead at this point in time)
                    apparel.Notify_PawnKilled();
                }
            }
        }

        public void DigestAllImplants(Pawn pawn, DamageDef damageDef)
        {
            float damage = RV2Mod.settings.cheats.AcidImplantDamage;
            bool destroyImplants = false;
            QuirkManager predatorQuirks = predator.QuirkManager();
            if (predatorQuirks != null)
            {
                damage = predatorQuirks.ModifyValue("ImplantDigestionStrength", damage);
                destroyImplants |= predatorQuirks.HasSpecialFlag("DestroyImplants");
            }
            IEnumerable<Hediff_Implant> implants = pawn.health?.hediffSet?.hediffs
                .Where(hediff => hediff is Hediff_Implant)
                .Cast<Hediff_Implant>();
            if (implants.EnumerableNullOrEmpty())
            {
                return;
            }
            foreach(Hediff_Implant implant in implants.ToList())
            {
                RV2Log.Message("Digesting implant " + implant.Label, true);
                if (!destroyImplants)
                {
                    ThingDef thingDef = implant.def.spawnThingOnRemoved;
                    if (thingDef != null)
                    {
                        Thing thing = ThingMaker.MakeThing(thingDef);
                        float implantDamage = damage * thing.HitPoints;
                        DamageInfo dinfo = new DamageInfo(damageDef, implantDamage, float.MaxValue, -1, predator);
                        thing.TakeDamage(dinfo);
                        innerContainer.TryAdd(thing);
                        RV2Log.Message("Made implant-item " + thing.LabelShort, true);
                    }
                }
                pawn.health.RemoveHediff(implant);
            }
            
        }

        public bool TryAddPrey(Pawn prey)
        {
            prey.holdingOwner?.Remove(prey);
            if (this.innerContainer.TryAdd(prey) == true)
            {
                if (prey?.Spawned == true)
                {
                    prey.DeSpawn();
                    RV2Log.Message("Despawned pawn", true);
                }
                return true;
            }
            return false;
        }

        public bool TryAddItems(List<Thing> items)
        {
            // if we have a product container, any items need to be added to that container
            if(VoreProductContainer != null)
            {
                RV2Log.Message("Adding items to product container: " + string.Join(", ", items), true);
                return VoreProductContainer.AddItems(items);
            }
            else
            {
                RV2Log.Message("Adding items: " + string.Join(", ", items), true);
                return items.All(item => innerContainer.TryAdd(item));
            }
        }

        public bool TryAddItem(Thing item)
        {
            List<Thing> items = new List<Thing>() { item };
            return TryAddItems(items);
        }

        public bool TryDropAllThings(bool simulateNullMap = false)
        {
            // if predator is also a prey of another predator, pass to that predator
            if (GlobalVoreTrackerUtility.GetVoreRecord(predator) != null)
            {
                return TryDropForCascadingPredator();
            }
            // if predator is in caravan, pass to caravan inventory
            if (CaravanUtility.IsCaravanMember(predator))
            {
                 return TryPassToCaravan(predator, ThingsInContainer);
            }
            // normal drop behaviour on predator map
            return TryDropWithMap(predator, simulateNullMap);
        }

        private bool TryDropForCascadingPredator()
        {
            RV2Log.Message("Predator is trying to remove all items, but is currently inside of another predator, passing to upper predator");
            PassToUpperPredator();
            return innerContainer.NullOrEmpty();
        }

        public void PassToUpperPredator()
        {
            VoreTrackerRecord record = predator.GetVoreRecord();
            innerContainer.TryTransferAllToContainer(record.VoreContainer.innerContainer);
        }

        public bool TryPassToCaravan(Pawn predator, List<Thing> items)
        {
            Caravan caravan = CaravanUtility.GetCaravan(this.predator);
            if(caravan == null)
            {
                RV2Log.Warning("Pawn " + predator.LabelShort + " should have been in a caravan");
                return false;
            }
            foreach(Thing item in items)
            {
                innerContainer.Remove(item);
                if(item is Pawn pawn)
                {
                    Faction playerFaction = Find.FactionManager.OfPlayer;
                    bool forcePlayerFaction = pawn.Faction == playerFaction;
                    Find.WorldPawns.PassToWorld(pawn);
                    if(playerFaction != null && forcePlayerFaction)
                    {
                        pawn.SetFactionDirect(playerFaction);
                    }
                }
                caravan.AddPawnOrItem(item, false);
            }
            return true;
        }

        /// <summary>
        /// </summary>
        /// <param name="predator"></param>
        /// <param name="simulateNullMap">null map can be simulated, this is usually done when kidnapping pawns. Due to the way raider jobs work, the vore can't be ended off-map, it must be ended while the kidnapping predator is still on the map</param>
        /// <returns></returns>
        private bool TryDropWithMap(Pawn predator, bool simulateNullMap = false)
        {
            bool success = true;
            Map map = predator.Map;
            bool isMapNull = map == null
                || simulateNullMap;
            bool hasActualItemsToDrop = ThingsInContainer    // take all things in vore
                .FindAll(thing => thing.def.filth == null   // remove all "things" that are just filth items
                && thing.def.HasModExtension<VoreContainerExtension>() == false)    // remove all things that function as an openable container
                .Count() > 0;
            foreach (Thing thing in ThingsInContainer)
            {
                /*if (!hasActualItemsToDrop && !RV2Mod.settings.fineTuning.SpawnEmptyVoreContainer)
                {
                    // if there are no "real" items and the user does not want to spawn empty openable containers, skip dropping
                    thing.Destroy();
                }
                else */if (isMapNull)
                {
                    success &= TryDropToNullMap(thing);
                }
                else
                {
                    success &= innerContainer.TryDrop(thing, predator.Position, map, ThingPlaceMode.Near, out _);
                }
            }
            return success;
        }

        private bool TryDropToNullMap(Thing thing)
        {
            if (thing is Pawn pawn)
            {
                // what exactly the world pawns are I truly do not know, but that's where pawns without a map are supposed to go
                if (!Find.WorldPawns.Contains(pawn))
                {
                    Find.WorldPawns.PassToWorld(pawn);
                    return true;
                }
            }
            else
            {
                // nowhere to go for items without a map, so destroy them
                thing.Destroy();
                return true;
            }
            return false;
        }

        public void Tick()
        {
            innerContainer.ThingOwnerTick();
        }

        public void TickRare()
        {
            innerContainer.ThingOwnerTickRare();
        }

        public override string ToString()
        {
            return string.Join(", ", this.innerContainer.InnerListForReading.ConvertAll(thing => thing.ToString()));
        }

        public void ExposeData()
        {
            Scribe_Deep.Look(ref innerContainer, "innerContainer", new object[]
            {
                this
            });
            Scribe_References.Look(ref predator, "owningPredator");
        }
    }
}