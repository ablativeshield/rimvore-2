﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public class VoreInteraction
    {
        public readonly Pawn Initiator;
        public readonly Pawn Target;
        public readonly Pawn SecondaryTarget;
        public Pawn Predator;
        public Pawn Prey;
        public readonly Dictionary<VorePathDef, string> Paths = new Dictionary<VorePathDef, string>();
        public readonly Dictionary<VoreGoalDef, string> Goals = new Dictionary<VoreGoalDef, string>();
        public readonly Dictionary<VoreTypeDef, string> Types = new Dictionary<VoreTypeDef, string>();
        // the following variables are used for concrete calculations when initiator & target were confirmed
        private List<VoreGoalDef> goalBlacklist;
        private List<VoreGoalDef> goalWhitelist;
        private List<VoreTypeDef> typeBlacklist;
        private List<VoreTypeDef> typeWhitelist;
        private List<DesignationDef> designationBlacklist;
        private List<DesignationDef> designationWhitelist;
        private VoreRole pickedInitiatorRole;
        private VoreGoalDef pickedGoal;
        private VoreTypeDef pickedType;
        public VorePathDef PickedPath;

        public Dictionary<VoreTypeDef, string> TypesForGoal(VoreGoalDef goal) => Paths
            .Where(kvp => kvp.Key.voreGoal == goal)
            .ToDictionary(kvp => kvp.Key.voreType, kvp => kvp.Value);
        public VorePathDef GetPathDef(VoreTypeDef type, VoreGoalDef goal) => Paths.EnumerableNullOrEmpty() ? null : Paths
            .First(kvp => 
                kvp.Key.voreGoal == goal 
                && kvp.Key.voreType == type)
            .Key;
        public IEnumerable<VoreGoalDef> ValidGoals() => Goals
            .Where(g => g.Value == null)
            .Select(g => g.Key)
            .ToList();
        public IEnumerable<VoreGoalDef> ValidGoals(VoreTypeDef type) => ValidPaths(type)
            .Select(p => p.voreGoal);
        public IEnumerable<VoreTypeDef> ValidTypes() => Types
            .Where(t => t.Value == null)
            .Select(t => t.Key);
        public IEnumerable<VoreTypeDef> ValidTypes(VoreGoalDef goal) => ValidPaths(goal)
            .Select(p => p.voreType);
        public IEnumerable<VorePathDef> ValidPaths() => Paths
            .Where(p => p.Value == null)
            .Select(p => p.Key);
        public IEnumerable<VorePathDef> ValidPaths(VoreGoalDef goal) => Paths
            .Where(p => p.Key.voreGoal == goal
            && p.Value == null)
            .Select(p => p.Key);
        public IEnumerable<VorePathDef> ValidPaths(VoreTypeDef type) => Paths
            .Where(p => p.Key.voreType == type
            && p.Value == null)
            .Select(p => p.Key);
        public IEnumerable<string> InvalidReasons(VoreGoalDef goal) => Paths
            .Where(p => p.Key.voreGoal == goal
            && p.Value != null)
            .Select(p => p.Value)
            .Distinct();
        public IEnumerable<string> InvalidReasons(VoreTypeDef type) => Paths
            .Where(p => p.Key.voreType == type
            && p.Value != null)
            .Select(p => p.Value)
            .Distinct();

        public VoreInteraction(Pawn initiator, Pawn target, Pawn secondaryTarget = null,
            VoreRole predeterminedRole = VoreRole.Invalid, bool isForAuto = false,
            List<VoreTypeDef> typeBlacklist = null, List<VoreTypeDef> typeWhitelist = null,
            List<VoreGoalDef> goalBlacklist = null, List<VoreGoalDef> goalWhitelist = null,
            List<DesignationDef> designationBlacklist = null, List<DesignationDef> designationWhitelist = null)
        {
            Initiator = initiator;
            Target = target;
            SecondaryTarget = secondaryTarget;
            this.typeBlacklist = typeBlacklist;
            this.typeWhitelist = typeWhitelist;
            this.goalBlacklist = goalBlacklist;
            this.goalWhitelist = goalWhitelist;
            this.designationBlacklist = designationBlacklist;
            this.designationWhitelist = designationWhitelist;
            if (predeterminedRole != VoreRole.Invalid)
            {
                pickedInitiatorRole = predeterminedRole;
                SetPawns();
                if(CanCalculate(out _))
                {
                    Calculate(isForAuto);
                }
            }
        }

        public void SetPawns()
        {
            switch (pickedInitiatorRole)
            {
                case VoreRole.Predator:
                    Predator = Initiator;
                    Prey = Target;
                    return;
                case VoreRole.Prey:
                    Predator = Target;
                    Prey = Initiator;
                    return;
                case VoreRole.Feeder:
                    Predator = Target;
                    Prey = SecondaryTarget;
                    return;
            }
            ArePawnsValid();
        }

        public bool ArePawnsValid()
        {
            if (Predator == null)
            {
                return false;
            }
            if (Prey == null)
            {
                return false;
            }
            return true;
        }
        /*public VoreInteraction(Pawn predator, Pawn prey, Pawn initiator, bool isForAuto = false)
        {
            this.Predator = predator;
            this.Prey = prey;
            this.Initiator = initiator;
            Calculate(isForAuto);
        }*/

        public bool CanCalculate(out string reason)
        {
            if (!Predator.CanVore(Prey, out reason))
            {
                return false;
            }
            return true;
        }

        private void Calculate(bool isForAuto = false)
        {
            if (!CanCalculate(out string reason))
            {
                RV2Log.Message("Interaction pred " + Predator.LabelShort + " prey " + Prey.LabelShort + " not valid, aborting interaction calculation, Reason: " + reason, true, true);
                return;
            }
            Paths.Clear();
            Goals.Clear();
            Types.Clear();
            foreach (VorePathDef path in RV2_Common.VorePaths)
            {
                //Log.Message("Calculating " + path.defName);
                CalculatePathValid(path, isForAuto, out reason);
                Paths.Add(path, reason);
            }
            foreach (VoreGoalDef goal in RV2_Common.VoreGoals)
            {
                //Log.Message("Calculating " + goal.defName);
                CalculateGoalValid(goal, out reason);
                Goals.Add(goal, reason);
            }
            foreach (VoreTypeDef type in RV2_Common.VoreTypes)
            {
                //Log.Message("Calculating " + type.defName);
                CalculateTypeValid(type, out reason);
                Types.Add(type, null);
            }
            RV2Log.Message("Calculated interaction:" + ToString(), true, true);
        }

        bool CalculatePathValid(VorePathDef path, bool isForAuto, out string reason)
        {
            if (!WhiteBlackListValid(path, out reason))
            {
                return false;
            }
            else if(!path.IsValid(Predator, Prey, out reason, isForAuto))
            {
                return false;
            }
            reason = null;
            return true;
        }
        bool CalculateGoalValid(VoreGoalDef goal, out string reason)
        {
            // goal blacklisted or not whitelisted
            if (!WhiteBlackListValid(goal, out reason))
            {
                return false;
            }
            // goal itself is invalid
            else if (!goal.IsValid(Predator, Prey, out reason))
            {
                return false;
            }
            // goal itself is valid, but there may be no paths!;
            else if (ValidPaths(goal).Count() <= 0)
            {
                IEnumerable<string> goalInvalidReasons = InvalidReasons(goal);
                reason = "RV2_VoreInvalidReasons_NoPaths".Translate(string.Join(", ", goalInvalidReasons));
                return false;
            }
            // goal is valid
            reason = null;
            return true;
        }
        bool CalculateTypeValid(VoreTypeDef type, out string reason)
        {
            if (!WhiteBlackListValid(type, out reason))
            {
                return false;
            }
            if (!type.IsValid(Predator, Prey, out reason))
            {
                return false;
            }
            if (ValidPaths(type).Count() <= 0)
            {
                IEnumerable<string> typeInvalidReasons = InvalidReasons(type);
                reason = "RV2_VoreInvalidReasons_NoPaths".Translate(string.Join(", ", typeInvalidReasons));
                return false;
            }
            reason = null;
            return true;
        }

        bool WhiteBlackListValid(VorePathDef path, out string reason)
        {
            if (!WhiteBlackListValid(path.voreGoal, out reason))
            {
                return false;
            }
            if (!WhiteBlackListValid(path.voreType, out reason))
            {
                return false;
            }
            reason = null;
            return true;
        }

        bool WhiteBlackListValid(VoreTypeDef type, out string reason)
        {
            if(typeBlacklist != null && typeBlacklist.Contains(type))
            {
                reason = "RV2_VoreInvalidReasons_TypeBlacklisted".Translate();
                return false;
            }
            if(typeWhitelist != null && !typeWhitelist.Contains(type))
            {
                reason = "RV2_VoreInvalidReasons_TypeNotWhitelisted".Translate();
                return false;
            }
            reason = null;
            return true;
        }

        bool WhiteBlackListValid(VoreGoalDef goal, out string reason)
        {
            if (goalBlacklist != null && goalBlacklist.Contains(goal))
            {
                reason = "RV2_VoreInvalidReasons_GoalBlacklisted".Translate();
                return false;
            }
            if (goalWhitelist != null && !goalWhitelist.Contains(goal))
            {
                reason = "RV2_VoreInvalidReasons_GoalNotWhitelisted".Translate();
                return false;
            }
            if (designationBlacklist != null)
            {
                bool anyDesignationBlacklisted = goal.requiredDesignations
                    .Where(des => des.assignedTo != IdentifierRole.Predator)
                   .Any(des => designationBlacklist.Contains(des));
                if (anyDesignationBlacklisted)
                {
                    reason = "RV2_VoreInvalidReasons_DesignationBlacklisted".Translate();
                    return false;
                }
            }
            if (designationWhitelist != null)
            {
                bool allDesignationsWhitelisted = goal.requiredDesignations
                    .Where(des => des.assignedTo != IdentifierRole.Predator)
                   .All(des => designationWhitelist.Contains(des));
                if (!allDesignationsWhitelisted)
                {
                    reason = "RV2_VoreInvalidReasons_DesignationNotWhitelisted".Translate();
                    return false;
                }
            }
            reason = null;
            return true;
        }


        // --------------------- Preference calculations after Pawns have been confirmed ---------------------
        public bool TryCalculatePreferredPath(bool isForAuto = false)
        {
            if(!TryRollForRole(out pickedInitiatorRole))
            {
                RV2Log.Message("Early exit: no initiator role determinable", true, false, "AutoVore");
                return false;
            }
            SetPawns();
            if (!ArePawnsValid())
            {
                RV2Log.Error("Tried to call preference calculation, but ArePawnsValid() failed");
                return false;
            }
            if (!CanCalculate(out string reason))
            {
                RV2Log.Error("Tried to call preference calculation for invalid VoreInteraction: " + reason);
                return false;
            }
            Calculate(isForAuto);
            pickedGoal = RollForGoal();
            if (pickedGoal == null)
            {
                RV2Log.Message("Early exit: no goal determinable", true, false, "AutoVore");
                return false;
            }
            pickedType = RollForType();
            if(pickedType == null)
            {
                RV2Log.Message("Early exit: no type determinable", true, false, "AutoVore");
                return false;
            }
            PickedPath = GetPathDef(pickedType, pickedGoal);
            if(PickedPath == null)
            {
                RV2Log.Warning("Tried to create vore-proposal, but no VorePathDef is valid! Aborting", true, false, "AutoVore");
                return false;
            }
            return true;
        }

        private bool TryRollForRole(out VoreRole role)
        {
            role = VoreRole.Invalid;
            List<VoreRole> validRoles = new List<VoreRole>()
            {
                VoreRole.Predator,
                VoreRole.Prey
                //,VoreRole.Feeder
            };

            RemoveRolesByValidatorChecks(ref validRoles);
            RemoveRolesByInvalidInteractions(ref validRoles);
            RemoveRolesByInvalidPaths(ref validRoles);

            if (!TryMakeWeightedPreference(validRoles, out Dictionary<VoreRole, float> weightedRoles))
            {
                return false;
            }

            RV2Log.Message("Weighted roles: " + LogUtility.ToString(weightedRoles), true, false, "Preferences");
            role = weightedRoles.RandomElementByWeight(weightedRole => weightedRole.Value).Key;
            RV2Log.Message("Picked " + role, true, false, "Preferences");
            return true;
        }

        private void RemoveRolesByValidatorChecks(ref List<VoreRole> roles)
        {
            // check ability to be predator / prey
            bool initiatorCanBePredator = Initiator.CanBePredator(out _);
            bool initiatorCanBePrey = Initiator.CanBePrey(out _);
            bool targetCanBePredator = Target.CanBePredator(out _);
            bool targetCanBePrey = Target.CanBePrey(out _);
            if (!initiatorCanBePredator || !targetCanBePrey)
            {
                RV2Log.Message("Removing predator: initiator can't be predator ? " +
                    !initiatorCanBePredator +
                    " target can't be prey ? " +
                    !targetCanBePrey, true, false, "Preferences");
                roles.Remove(VoreRole.Predator);
            }
            if (!initiatorCanBePrey || !targetCanBePredator)
            {
                RV2Log.Message("Removing prey: initiator can't be prey ? " +
                    !initiatorCanBePrey +
                    " target can't be predator ? " +
                    !targetCanBePredator, true, false, "Preferences");
                roles.Remove(VoreRole.Prey);
            }

            // check unwillingness to be predator / prey
            bool initiatorNeverWantsToBePredator = Initiator.NeverWantsToBePredator(out _);
            bool initiatorNeverWantsToBePrey = Initiator.NeverWantsToBePrey(out _);
            bool targetNeverWantsToBePredator = Target.NeverWantsToBePredator(out _);
            bool targetNeverWantsToBePrey = Target.NeverWantsToBePrey(out _);

            if (initiatorNeverWantsToBePredator || targetNeverWantsToBePrey)
            {
                RV2Log.Message("Removing predator: initiator doesn't want to be predator ? " +
                    initiatorNeverWantsToBePredator +
                    " target doesn't want to be prey ? " +
                    targetNeverWantsToBePrey, true, false, "Preferences");
                roles.Remove(VoreRole.Predator);
            }
            if (initiatorNeverWantsToBePrey || targetNeverWantsToBePredator)
            {
                RV2Log.Message("Removing prey: initiator doesn't want to be prey ? " +
                    initiatorNeverWantsToBePrey +
                    " target doesn't want to be predator ? " +
                    targetNeverWantsToBePredator, true, false, "Preferences");
                roles.Remove(VoreRole.Prey);
            }
        }
        private void RemoveRolesByInvalidInteractions(ref List<VoreRole> roles)
        {
            VoreInteractionManager.Update(Initiator, Target);
            bool initiatorAsPredatorIsValid = VoreInteractionManager.InitiatorAsPredator.CanCalculate(out _);
            bool initiatorAsPreyIsValid = VoreInteractionManager.InitiatorAsPrey.CanCalculate(out _);
            if (!initiatorAsPredatorIsValid)
            {
                RV2Log.Message("Removing predator: initiator as predator interaction invalid ? " + !initiatorAsPredatorIsValid, true, false, "Preferences");
                roles.Remove(VoreRole.Predator);
            }
            if (!initiatorAsPreyIsValid)
            {
                RV2Log.Message("Removing prey: initiator as prey interaction invalid ? " + !initiatorAsPreyIsValid, true, false, "Preferences");
                roles.Remove(VoreRole.Prey);
            }
        }
        private void RemoveRolesByInvalidPaths(ref List<VoreRole> roles)
        {
            VoreInteractionManager.Update(Initiator, Target);
            bool initiatorHasValidPath = !VoreInteractionManager.InitiatorAsPredator.ValidPaths().EnumerableNullOrEmpty();
            bool targetHasValidPath = !VoreInteractionManager.InitiatorAsPrey.ValidPaths().EnumerableNullOrEmpty();
            if (!initiatorHasValidPath)
            {
                RV2Log.Message("Removing predator: initiator has no valid paths ? " + !initiatorHasValidPath, true, false, "Preferences");
                roles.Remove(VoreRole.Predator);
            }
            else if (!targetHasValidPath)
            {
                RV2Log.Message("Removing prey: target has no valid paths ? " + !targetHasValidPath, true, false, "Preferences");
                roles.Remove(VoreRole.Prey);
            }
        }
        private bool TryMakeWeightedPreference(IEnumerable<VoreRole> roles, out Dictionary<VoreRole, float> weightedPreference)
        {
            weightedPreference = new Dictionary<VoreRole, float>();
            if (roles.EnumerableNullOrEmpty())
            {
                RV2Log.Message("No roles available to make weighted preferences for", true, false, "Preferences");
                return false;
            }
            foreach (VoreRole role in roles)
            {
                float preference = Initiator.PreferenceFor(role, ModifierOperation.Multiply);
                if (preference > 0)
                {
                    weightedPreference.Add(role, preference);
                }
                else
                {
                    RV2Log.Message("Preference for " + role.ToString() + " was 0 or lower, not adding", true, false, "Preferences");
                }
            }
            return !weightedPreference.EnumerableNullOrEmpty();
        }

        private VoreGoalDef RollForGoal()
        {
            Dictionary<VoreGoalDef, float> weightedGoals;
            if (!TryMakeWeightedPreferences<VoreGoalDef>(ValidGoals(), out weightedGoals))
            {
                return null;
            }

            RV2Log.Message("Weighted goals: " + LogUtility.ToString(weightedGoals), true, false, "AutoVore");
            VoreGoalDef pickedGoal = weightedGoals.RandomElementByWeight(weightedGoal => weightedGoal.Value).Key;
            RV2Log.Message("Picked " + pickedGoal, true, false, "AutoVore");
            return pickedGoal;
        }

        private VoreTypeDef RollForType()
        {
            Dictionary<VoreTypeDef, float> weightedTypes;
            if (!TryMakeWeightedPreferences<VoreTypeDef>(ValidTypes(pickedGoal), out weightedTypes))
            {
                return null;
            }

            RV2Log.Message("Weighted types: " + LogUtility.ToString(weightedTypes), true, false, "AutoVore");
            VoreTypeDef pickedType = weightedTypes.RandomElementByWeight(weightedType => weightedType.Value).Key;
            RV2Log.Message("Picked " + pickedType, true, false, "AutoVore");
            return pickedType;
        }
        private bool TryMakeWeightedPreferences<T>(IEnumerable<T> preferenceSource, out Dictionary<T, float> weightedPreferences) where T : IPreferrable
        {
            weightedPreferences = new Dictionary<T, float>();
            if (preferenceSource.EnumerableNullOrEmpty())
            {
                return false;
            }
            foreach (T source in preferenceSource)
            {
                float preference = source.GetPreference(Initiator, pickedInitiatorRole, ModifierOperation.Add);
                if (preference >= 0)
                {
                    //preference++;
                    weightedPreferences.Add(source, preference);
                }
                else
                {
                    RV2Log.Message("Preference for " + typeof(T).ToString() + " is below 0, not adding to valid target list", true, false, "AutoVore");
                }
            }
            return !weightedPreferences.EnumerableNullOrEmpty();
        }

        public VoreRole Role(Pawn pawn)
        {
            if(pawn == Predator)
            {
                return VoreRole.Predator;
            }
            if(pawn == Prey)
            {
                return VoreRole.Prey;
            }
            if(pawn == Initiator)
            {
                return VoreRole.Feeder;
            }
            return VoreRole.Invalid;
        }

        public override string ToString()
        {
            bool isValid = CanCalculate(out string reason);
            string message = "Predator " +
                Predator?.LabelShort +
                " and prey " +
                Prey?.LabelShort +
                " is valid? " +
                isValid;
            if (isValid)
            {
                message += "\nGoals:\n - ";
                if (Goals.EnumerableNullOrEmpty())
                {
                    message += "None";
                }
                else
                {
                    message += string.Join("\n - ", Goals.Select(kvp => kvp.Key.label + " Valid: " + (kvp.Value == null) + " Reason: " + kvp.Value));
                }
                message += "\nPaths:\n - ";
                if (Paths.EnumerableNullOrEmpty())
                {
                    message += "None";
                }
                else
                {
                    message += string.Join("\n - ", Paths.Select(kvp => kvp.Key.label + " Valid: " + (kvp.Value == null) + " Reason: " + kvp.Value));
                }

                message += "\nTypes:\n - ";
                if (Paths.EnumerableNullOrEmpty())
                {
                    message += "None";
                }
                else
                {
                    message += string.Join("\n - ", Types.Select(kvp => kvp.Key.label + " Valid: " + (kvp.Value == null) + " Reason: " + kvp.Value));
                }

            }
            else
            {
                message += " Reason: " +
                    reason;
            }
            return message;
        }
    }
}
