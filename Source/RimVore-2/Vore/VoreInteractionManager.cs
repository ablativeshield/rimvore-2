﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    /// <summary>
    /// This class exists in the current state because I haven't added initiator-target based caching yet
    /// until then this class would be a massive race condition causer - thank god RW isn't multithreading
    /// </summary>
    public static class VoreInteractionManager
    {
        public static Pawn Initiator;
        public static Pawn Target;
        public static bool IsForAuto = false;
        private static VoreInteraction initiatorAsPredator;
        public static VoreInteraction InitiatorAsPredator
        {
            get
            {
                if (needToRecalculate)
                {
                    Recalculate();
                }
                return initiatorAsPredator;
            }
        }
        private static VoreInteraction initiatorAsPrey;
        public static VoreInteraction InitiatorAsPrey
        {
            get
            {
                if (needToRecalculate)
                {
                    Recalculate();
                }
                return initiatorAsPrey;
            }
        }

        private static bool needToRecalculate = true;

        public static void Update(Pawn initiator, Pawn target, bool isForAuto = false)
        {
            // prevent recalculating when parties haven't changed
            if(initiator == Initiator && target == Target)
            {
                return;
            }
            IsForAuto = isForAuto;
            Initiator = initiator;
            Target = target;
            needToRecalculate = true;
        }

        private static void Recalculate()
        {
            initiatorAsPredator = new VoreInteraction(Initiator, Target, null, VoreRole.Predator);
            initiatorAsPrey = new VoreInteraction(Initiator, Target, null, VoreRole.Prey);
            //initiatorAsPredator = new VoreInteraction(Initiator, Target, Initiator, IsForAuto);
            //initiatorAsPrey = new VoreInteraction(Target, Initiator, Initiator, IsForAuto);
            //RV2Log.Message("Recalculated interaction. Initiator as Predator:\n" + initiatorAsPredator.ToString(), true, true);
            RV2Log.Message("Initiator as prey:\n" + initiatorAsPrey.ToString(), true, true);
            needToRecalculate = false;
        }

        public static void Reset()
        {
            if(needToRecalculate == true)
            {
                return;
            }
            needToRecalculate = true;
            Initiator = null;
            Target = null;
            initiatorAsPredator = null;
            initiatorAsPrey = null;
        }
    }

    
}
