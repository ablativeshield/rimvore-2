﻿using System;
using System.Collections.Generic;
using System.Linq;
using Verse;
using RimWorld;

namespace RimVore2
{
    public abstract class RollModifier : IExposable
    {
        public int priority = -1;

        public RollModifier() { }

        public ModifierOperation operation = ModifierOperation.Multiply;

        protected abstract bool TryGetModifier(VoreTrackerRecord record, out float modifier);

        public virtual float ModifyValue(float value, VoreTrackerRecord record)
        {
            if(TryGetModifier(record, out float modifier))
            {
                float newValue = operation.Aggregate(value, modifier);
                RV2Log.Message(Explain(value, modifier, newValue), true, true, "OngoingVore");
                return newValue;
            }
            return value;
        }

        protected virtual string Explain(float oldValue, float modifier, float newValue)
        {
            return this.ToString() + " modified " + oldValue + " with modifier " + modifier + " for new value " + newValue + " operation: " + operation;
        }

        public virtual IEnumerable<string> ConfigErrors()
        {
            if(operation == ModifierOperation.Invalid)
            {
                yield return "Required field \"operation\" must be set";
            }
            if(operation == ModifierOperation.Set && priority == -1)
            {
                yield return "When using \"operation\" Set, the field \"priority\" is required";
            }
        }
        public virtual void ExposeData()
        {
            Scribe_Values.Look(ref operation, "operation");
        }
    }

    public class RollModifier_Value : RollModifier
    {
        float value;

        protected override bool TryGetModifier(VoreTrackerRecord record, out float modifier)
        {
            modifier = value;
            return true;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref value, "value");
        }
    }

    public class RollModifier_VoreSpeed : RollModifier
    {
        protected override bool TryGetModifier(VoreTrackerRecord record, out float modifier)
        {
            modifier = RV2Mod.settings.cheats.VoreSpeedMultiplier;
            return true;
        }
    }

    public class RollModifier_SizeDifference : RollModifier
    {
        VoreRole pawn;
        VoreRole relationTo;

        protected override bool TryGetModifier(VoreTrackerRecord record, out float modifier)
        {
            Pawn pawn1 = record.GetPawnByRole(pawn);
            Pawn pawn2 = record.GetPawnByRole(relationTo);
            modifier = pawn1.BodySize / pawn2.BodySize;
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (pawn == VoreRole.Invalid)
            {
                yield return "Required field \"pawn\" must be set";
            }
            if (relationTo == VoreRole.Invalid)
            {
                yield return "Required field \"relationTo\" must be set";
            }
            if(pawn == relationTo)
            {
                yield return "Setting \"pawn\" and \"relationTo\" to the same target will always result in 1";
            }
        }

        protected override string Explain(float oldValue, float modifier, float newValue)
        {
            return base.Explain(oldValue, modifier, newValue) + pawn + " / " + relationTo;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref pawn, "pawn");
            Scribe_Values.Look(ref relationTo, "relationTo");
        }
    }

    public abstract class TargetedRollModifier : RollModifier
    {
        VoreRole target;

        protected Pawn TargetPawn(VoreTrackerRecord record)
        {
            return record.GetPawnByRole(target);
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (target == VoreRole.Invalid)
            {
                yield return "Required field \"target\" not set";
            }
        }

        protected override string Explain(float oldValue, float modifier, float newValue)
        {
            return base.Explain(oldValue, modifier, newValue) + " target: " + target;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref target, "target");
        }
    }

    public class RollModifier_FoodFallRate : TargetedRollModifier
    {
        protected override bool TryGetModifier(VoreTrackerRecord record, out float modifier)
        {
            Pawn pawn = TargetPawn(record);
            modifier = pawn.GetHungerRate() * 250;
            return true;
        }
    }

    public class RollModifier_Capacity : TargetedRollModifier
    {
        PawnCapacityDef capacity;

        protected override bool TryGetModifier(VoreTrackerRecord record, out float modifier)
        {
            Pawn pawn = TargetPawn(record);
            if(pawn.health.capacities != null)
            {
                modifier = pawn.health.capacities.GetLevel(capacity);
                return true;
            }
            modifier = 0;
            return false;
        }

        protected override string Explain(float oldValue, float modifier, float newValue)
        {
            return base.Explain(oldValue, modifier, newValue) + " capacity: " + capacity.defName;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (capacity == null)
            {
                yield return "Required field \"capacity\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref capacity, "capacity");
        }
    }

    public class RollModifier_Quirk : TargetedRollModifier
    {
        string modifierName;

        protected override bool TryGetModifier(VoreTrackerRecord record, out float modifier)
        {
            Pawn pawn = TargetPawn(record);
            QuirkManager pawnQuirks = pawn.QuirkManager(false);
            if(pawnQuirks == null)
            {
                modifier = 0;
                return false;
            }
            if (!pawnQuirks.HasValueModifier(modifierName))
            {
                RV2Log.Message("pawn " + pawn.LabelShort + "has no modifiers for " + modifierName, true, true);
                modifier = 0;
                return false;
            }
            return pawnQuirks.TryGetValueModifier(modifierName, operation, out modifier);
        }

        protected override string Explain(float oldValue, float modifier, float newValue)
        {
            return base.Explain(oldValue, modifier, newValue) + " modifierName: " + modifierName;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(modifierName == null)
            {
                yield return "Required field \"modifierName\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref modifierName, "modifierName");
        }
    }

    public class RollModifier_Stat : TargetedRollModifier
    {
        StatDef stat;

        protected override bool TryGetModifier(VoreTrackerRecord record, out float modifier)
        {
            Pawn pawn = TargetPawn(record);
            modifier = pawn.GetStatValue(stat);
            return true;
        }

        protected override string Explain(float oldValue, float modifier, float newValue)
        {
            return base.Explain(oldValue, modifier, newValue) + " stat: " + stat.defName;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(stat == null)
            {
                yield return "Required field \"stat\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref stat, "stat");
        }
    }
}
