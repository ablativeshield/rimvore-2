﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public abstract class RollAction : IExposable
    {
        protected VoreRole target = VoreRole.Invalid;
        protected bool invert = false;
#pragma warning disable IDE0044 // Add readonly modifier
        private bool canBlockNextActions = false;
#pragma warning restore IDE0044 // Add readonly modifier
        public virtual bool CanBlockNextActions => canBlockNextActions;
        protected VoreTrackerRecord record;

        protected Pawn TargetPawn => record.GetPawnByRole(target);
        protected Pawn PredatorPawn => record.Predator;
        protected Pawn PreyPawn => record.Prey;

        public RollAction() { }

        // return false if the action wants to block further actions
        public virtual bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            this.record = record;
            return true;
        }
        public virtual void ExposeData() { }
        public virtual IEnumerable<string> ConfigErrors()
        {
            yield break;
        }
    }

    public class RollAction_PlaySound : RollAction
    {
        SoundDef sound;

        public RollAction_PlaySound() : base()
        {
            target = VoreRole.Predator;
        }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if (!TargetPawn.Spawned || sound == null)
            {
                return false;
            }
            SoundManager.PlaySingleSound(TargetPawn, sound);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (sound == null)
            {
                yield return "Required field \"sound\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref sound, "sound");
        }
    }

    public class RollAction_ShowMote : RollAction
    {
#if v1_2
        ThingDef mote;
#else
        FleckDef mote;
#endif

        public RollAction_ShowMote()
        {
            target = VoreRole.Predator;
        }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if (!TargetPawn.Spawned || mote == null)
            {
                return false;
            }
#if v1_2
            MoteMaker.ThrowMetaIcon(TargetPawn.Position, TargetPawn.Map, mote);
#else
            FleckMaker.ThrowMetaIcon(TargetPawn.Position, TargetPawn.Map, mote);
#endif
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (mote == null)
            {
                yield return "Required field \"mote\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref mote, "mote");
        }
    }

    public class RollAction_Digest : RollAction
    {
        public DamageDef damageDef;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            
            AcidUtility.ApplyAcidByDigestionProgress(record, record.CurrentVoreStage.PercentageProgress, damageDef);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (damageDef == null)
            {
                yield return "Required field \"damageDef\" must be set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref damageDef, "damageDef");
        }
    }

    public abstract class RollAction_CreateThings : RollAction
    {
        protected abstract List<ThingDef> ThingDefsToCreate { get; }

        float requiredNutrition = 1f;
        int quantityPerRequiredNutrition = 1;
        bool reducePreyNutrition = true;

        float nutritionReserve = 0;

        public RollAction_CreateThings() : base() { }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            int producedThingCount = 0;
            float currentPreyNutrition = record.PassValues.TryGetValue("PreyNutrition", 0f);
            // reduce rollstrength if strength is higher than leftover nutrition
            rollStrength = Math.Min(currentPreyNutrition, rollStrength);
            nutritionReserve += rollStrength;
            if (requiredNutrition > 0f)
            {
                producedThingCount = (int)Math.Floor(nutritionReserve / requiredNutrition);
            }
            else
            {
                producedThingCount = 1;
            }
            RV2Log.Message("Created " + producedThingCount + " items, current nutrition reserves: " + nutritionReserve, true, false, "OngoingVore");
            producedThingCount *= quantityPerRequiredNutrition;
            if (producedThingCount <= 0)
            {
                return false;
            }
            if (reducePreyNutrition)
            {
                nutritionReserve -= producedThingCount * requiredNutrition;
            }
            if (ThingDefsToCreate.NullOrEmpty())
            {
                RV2Log.Warning("Tried to create items, but list of items to create is empty", "OngoingVore");
                return false;
            }
            producedThingCount = Mathf.CeilToInt(producedThingCount / ThingDefsToCreate.Count());
            foreach (ThingDef thingDef in ThingDefsToCreate)
            {
                Thing createdThing = CreateThing(thingDef, producedThingCount);
                RV2Log.Message("Created " + createdThing.def.label + ", stackCount: " + createdThing.stackCount, true, false, "OngoingVore");
                record.VoreContainer.TryAddItem(createdThing);
            }
            return true;
        }

        protected Thing CreateThing(ThingDef thingDef, int stackCount)
        {
            Thing thing = ThingMaker.MakeThing(thingDef);
            thing.stackCount = stackCount;
            return thing;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (invert)
            {
                yield return "Cannot use \"invert\" for thing creation";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref requiredNutrition, "requiredNutrition");
            Scribe_Values.Look(ref nutritionReserve, "nutritionReserve");
            Scribe_Values.Look(ref quantityPerRequiredNutrition, "quantityPerRequiredNutrition");
            Scribe_Values.Look(ref reducePreyNutrition, "reducePreyNutrition");
        }
    }

    public class RollAction_CreateThingsFromDef : RollAction_CreateThings
    {
        List<ThingDef> things;
        protected override List<ThingDef> ThingDefsToCreate => things;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (things.NullOrEmpty())
            {
                yield return "required list \"things\" is not set or empty";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Collections.Look(ref things, "things", LookMode.Def);
        }
    }

    public class RollAction_CreateThingsFromContainer : RollAction_CreateThings
    {
        protected override List<ThingDef> ThingDefsToCreate => record.VoreContainer?.VoreProductContainer?.ProvideItems();
    }

    public class RollAction_IncreaseNeed : RollAction
    {
        NeedDef need;
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if (invert)
            {
                rollStrength *= -1;
            }
            return RV2PawnUtility.TryIncreaseNeed(TargetPawn, need, rollStrength);
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (target == VoreRole.Invalid)
            {
                yield return "required field \"target\" is not set";
            }
            if (need == null)
            {
                yield return "required field \"need\" is not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref need, "need");
        }
    }

    public class RollAction_Heal : RollAction
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            rollStrength *= 10; // there used to be a completely new roll after leeching food from predator, but with the new setup and no boxing rolls, we need to manually change the rolled value (or implement more RollActions that affect the rollStrength)

            IEnumerable<Hediff> tendableInjuries = TargetPawn.health.hediffSet.GetHediffsTendable();
            if (!tendableInjuries.EnumerableNullOrEmpty())
            {
                return TendPawn(tendableInjuries, rollStrength);
            }
            else
            {
                return HealPawn(rollStrength);
            }
        }

        private bool TendPawn(IEnumerable<Hediff> injuries, float rollStrength)
        {
            float quality = rollStrength;
            Hediff injury = injuries.RandomElement();
#if v1_2
            injury.Tended(quality);
#else
            injury.Tended(quality, quality);
#endif
            RV2Log.Message("Tended injury " + injury.Label + " with quality " + rollStrength);
            return true;
        }

        private bool HealPawn(float rollStrength)
        {
            float quality = rollStrength;
            List<Hediff> injuries = TargetPawn.GetHealableInjuries();

            if (injuries.NullOrEmpty())
            {
                RV2Log.Message("No injuries to heal", "OngoingVore");
                return false;
            }
            Hediff_Injury injury = (Hediff_Injury)injuries.RandomElement();
            injury.Severity -= quality;
            RV2Log.Message("Healed " + TargetPawn.Label + "'s hediff " + injury.def.label + " by " + quality);
            TargetPawn.health.hediffSet.DirtyCache();
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (invert)
            {
                yield return "Cannot use \"invert\" for healing";
            }
        }
    }

    public class RollAction_AddHediff : RollAction
    {
        protected HediffDef hediff;
        protected BodyPartDef partDef;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if (TargetPawn.health.hediffSet.HasHediff(hediff))
            {
                return false;
            }
            // no reason to null check, null body part is whole body and valid
            BodyPartRecord bodyPart = TargetPawn.GetBodyPartByDef(partDef);
            TargetPawn.health.AddHediff(hediff, bodyPart);
            RV2Log.Message("Added hediff " + hediff.label + " to " + TargetPawn.Label);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (hediff == null)
            {
                yield return "required field \"hediff\" is not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref hediff, "hediff");
            Scribe_Defs.Look(ref partDef, "partDef");
        }
    }

    public class RollAction_IncreaseHediffSeverity : RollAction_AddHediff
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            if (invert)
            {
                rollStrength *= -1;
            }
            // do not call base.DoAction()
            Hediff targetHediff = base.TargetPawn.health.hediffSet.hediffs
                .FindAll(hed => hed.def == hediff)
                .RandomElement();
            if (targetHediff == null)
            {
                return false;
            }
            targetHediff.Severity += rollStrength;
            RV2Log.Message("Increased " + base.TargetPawn.Label + "'s" + targetHediff.def.label + " by " + rollStrength + " to " + targetHediff.Severity);
            return true;
        }
    }

    public class RollAction_Reform : RollAction
    {
        PawnKindDef resultingPawnKind;
        ThingDef resultingRace;
        bool usePredatorRace = true;
        float eachPredatorTraitCopyChance = 0.5f;
        float eachPreyTraitCopyChance = 0.5f;
        bool changeFactionToPlayer = true;
        Gender? fixedGender = null;

        Pawn Predator => record.Predator;
        ThingDef PredatorRace => Predator.def;

        Pawn ResultSourcePawn => usePredatorRace ? Predator : TargetPawn;

        PawnKindDef ResultPawnKindDef
        {
            get
            {
                if(resultingPawnKind != null)
                {
                    return resultingPawnKind;
                }
                return ResultSourcePawn.kindDef;
            }
        }
        ThingDef ResultRaceDef
        {
            get
            {
                if(resultingRace != null)
                {
                    return resultingRace;
                }
                return ResultSourcePawn.def;
            }
        }

        Gender ResultGender => fixedGender != null ? fixedGender.Value : TargetPawn.gender;

        public RollAction_Reform()
        {
            target = VoreRole.Prey;
        }

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            VoreTracker tracker = record.Predator.GetVoreTracker();
            if (record.Prey.Dead)
            {
                RV2Log.Message("Prey is dead, aborting reformation", "OngoingVore");
                return false;
            }
            float currentProgress = record.CurrentVoreStage.PercentageProgress;
            if (currentProgress <= 0.95)
            {
                RV2Log.Message("Not reforming pawn because progress is < 95% (" + currentProgress + ")");
                return false;
            }
            if (TargetPawn.Dead)
            {
                NotificationUtility.DoNotification(NotificationType.MessageNeutral, "RV2_Message_ReformationFailed_PawnDead".Translate());
                return false;
            }
            Pawn newPawn = MakeReformedPawn();
            if (newPawn == null)
            {
                return false;
            }
            tracker.SplitOffNewVore(record, newPawn, null, record.VorePathIndex + 1);
            tracker.UntrackVore(record);
            //GenSpawn.Spawn(newPawn, Predator.Position, Predator.Map);
            return true;
        }

        private Pawn MakeReformedPawn()
        {
            if (ResultPawnKindDef == null)
            {
                RV2Log.Error("Can not reform prey " + TargetPawn?.Label + ", pawnKind is NULL");
                return null;
            }
            string originalPawnKindRaceDefName = ResultPawnKindDef.race.defName; // remember the original race to re-set it after generation
            try
            {
                if (ResultRaceDef == null)
                {
                    RV2Log.Error("Can not reform prey " + TargetPawn?.Label + ", target race is NULL");
                    return null;
                }
                ResultPawnKindDef.race = ResultRaceDef;    // temporarily overwrites the global PawnKindDef's race! Will be forced to be reset after generation
                List<Trait> newTraits = MakeNewTraits();
                Faction newFaction = changeFactionToPlayer ? Faction.OfPlayer : TargetPawn.Faction;
                string fixedLastName = null;
                if (Predator.Name is NameTriple tripleName)
                {
                    fixedLastName = tripleName.Last;
                }
                PawnGenerationRequest request = new PawnGenerationRequest(
                    kind: ResultPawnKindDef,
                    fixedGender: ResultGender,
                    faction: newFaction,
                    forceGenerateNewPawn: true,
                    allowDowned: true,
                    canGeneratePawnRelations: false,
                    colonistRelationChanceFactor: 0,
                    allowFood: false,
                    allowAddictions: false,
                    relationWithExtraPawnChanceFactor: 0,
                    forcedTraits: null, // can't be used to pass predator / old pawn traits, due to forcing all degree to be set to 0 (why, tynan)
                                        //fixedBiologicalAge: 0.01f,  // will be overwritten post-generation
                                        //fixedChronologicalAge: TargetPawn.ageTracker?.AgeChronologicalYearsFloat,
                    fixedLastName: fixedLastName
#if v1_2
#else   // forgive me for the weird off-line ',' but the build variable has priority
                , fixedIdeo: base.TargetPawn.ideo?.Ideo
#endif
                );
                // for non-humanoids we set the new pawn to be a baby, or if the user is fine with generating age 0 humanoids, we use that
                bool generateNewborn = !ResultSourcePawn.IsHumanoid()
                    || (ResultSourcePawn.IsHumanoid() && RV2Mod.settings.fineTuning.ReformHumanoidsAsNewborn);
                request.Newborn = generateNewborn;

                Pawn generatedPawn = PawnGenerator.GeneratePawn(request);
                // if the generated pawn is not a newborn, we need to calculate a biological age for them
                if (!request.Newborn)
                {
                    SetReformedBiologicalAge(generatedPawn);
                }
                // the chronological age we always try to set to the previous pawns age
                SetReformedChronologicalAge(generatedPawn);
                // get rid of any apparel the pawn generated with
                generatedPawn.apparel?.DestroyAll();
                ForceReformedName(generatedPawn);
                // base game is retarded. genuinely fucking stupid, the following line is what happens when you pass a forcedTraits via request
                // pawn.story.traits.GainTrait(new Trait(traitDef, 0, true));
                // it just forces degree 0 - which traits with ACTUAL degrees do not have, 
                // so we skip the INTENDED mechanic of forcing traits and do it after the pawn is generated
                // this solution is dogshit, because at this point the traits don't have an impact on other aspects of pawn generation
                RV2PawnUtility.SetTraits(generatedPawn, newTraits);
                ForceParent(generatedPawn);
                return generatedPawn;
            }
            finally
            {
                // this appears to be unnecessary, but not doing it would leave the pawnKind with an incorrect ThingDef reference as race
                ResultPawnKindDef.race = ThingDef.Named(originalPawnKindRaceDefName);
            }
        }

        private void ForceParent(Pawn newPawn)
        {
            if(Predator.gender == Gender.Male)
            {
                newPawn.SetFather(Predator);
            }
            else if(Predator.gender == Gender.Female)
            {
                newPawn.SetMother(Predator);
            }
        }

        private void SetReformedBiologicalAge(Pawn pawn)
        {
            // animals, mechs, all the other stuff is set to age 0, they should have just been tagged as newborn
            if (!pawn.IsHumanoid())
            {
                pawn.ageTracker.AgeBiologicalTicks = 0;
                return;
            }
            // but because humans are weird, we do a whole lot of calculations for a good target age
            //float oldAge = TargetPawn.ageTracker.AgeBiologicalYearsFloat;
            // first get the minimum age required for this pawn to participate in vore
            float targetAge = RV2Mod.settings.rules.GetValidAge(pawn);
            //// if our target age is above the previous age, reduce it to the old age
            //if(targetAge > oldAge)
            //{
            //    targetAge = oldAge;
            //}
            // if our target age is above chronological age, reduce to chronological age
            float chronoAge = pawn.ageTracker.AgeChronologicalYearsFloat;
            if (targetAge > chronoAge)
            {
                targetAge = chronoAge;
            }
            long newAge = Math.Max(0, (long)(targetAge * 3600000L));
            pawn.ageTracker.AgeBiologicalTicks = newAge;
        }

        private void SetReformedChronologicalAge(Pawn pawn)
        {
            // the new generated age MIGHT be larger than the old pawns chronological age, but chrono age must always be higher than bio age
            float newAge = pawn.ageTracker.AgeBiologicalYearsFloat;
            float chronoAge = TargetPawn.ageTracker.AgeChronologicalYears;
            if (newAge > chronoAge)
            {
                chronoAge = newAge;
            }
            pawn.ageTracker.AgeChronologicalTicks = (long)(chronoAge * 3600000L);
        }

        private void ForceReformedName(Pawn pawn)
        {
            // Animals have no real name, so no name to take
            if (TargetPawn.Name == null)
            {
                return;
            }
            // new name is triple name
            if (pawn.Name is NameTriple newName)
            {
                // both pawns have triple names, keep first and nick name, adopt last name
                if (TargetPawn.Name is NameTriple oldName)
                {
                    pawn.Name = new NameTriple(
                        oldName.First,
                        oldName.Nick,
                        newName.Last
                    );
                }
                // only new name is triple, old is single, so set first name to old single name
                else
                {
                    NameSingle oldSingleName = (NameSingle)TargetPawn.Name;
                    pawn.Name = new NameTriple(
                        oldSingleName.Name,
                        newName.Nick,
                        newName.Last
                    );
                }
            }
            // new name is single name, we always want to keep the old name
            else
            {
                pawn.Name = TargetPawn.Name;
            }
        }

        public override IEnumerable<string> ConfigErrors()
        {
            if (!usePredatorRace && resultingRace == null)
            {
                yield return "when using \"usePredatorRace\" = false the field \"resultingRace\" must be set!";
            }
        }

        protected virtual List<Trait> MakeNewTraits(PawnGenerationRequest request = default(PawnGenerationRequest))
        {
            List<Trait> newTraits = new List<Trait>();
            newTraits.AddRange(GetTraitsFromPawn(TargetPawn, eachPreyTraitCopyChance));
            newTraits.AddRange(GetTraitsFromPawn(Predator, eachPredatorTraitCopyChance));
            RV2Log.Message("total new traits: " + string.Join(", ", newTraits.ConvertAll(t => t.Label)), "TraitGeneration");

            int maxTraitsAllowed = RV2Mod.settings.cheats.ReformedPawnTraitCount;
            if (newTraits.Count > maxTraitsAllowed)
            {
                RV2Log.Message("too many new traits, current count: " + newTraits.Count + " allowed: " + maxTraitsAllowed, "TraitGeneration");
                newTraits = newTraits.GetRange(0, maxTraitsAllowed - 1);
            }
            return newTraits;
        }

        protected virtual List<Trait> GetTraitsFromPawn(Pawn pawn, float chance)
        {
            List<Trait> traits;
            traits = pawn.story?.traits?.allTraits?
                .FindAll(t => RandomUtility.RollSuccess(chance));
            if (traits.NullOrEmpty())
            {
                return new List<Trait>();
            }
            RV2Log.Message("Pawn " + pawn.Label + " provides traits: " + string.Join(", ", traits.ConvertAll(t => t.Label)), "TraitGeneration");
            return traits;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref resultingRace, "resultingRace");
            Scribe_Values.Look(ref usePredatorRace, "usePredatorRace");
            Scribe_Values.Look(ref eachPredatorTraitCopyChance, "eachPredatorTraitCopyChance");
            Scribe_Values.Look(ref eachPreyTraitCopyChance, "eachPreyTraitCopyChance");
            Scribe_Values.Look(ref changeFactionToPlayer, "changeFactionToPlayer");
        }
    }

    public abstract class RollAction_Steal : RollAction
    {
        protected VoreRole takeFrom;
        protected VoreRole giveTo;

        protected Pawn TakerPawn => record.GetPawnByRole(giveTo);
        protected Pawn GiverPawn => record.GetPawnByRole(takeFrom);

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (takeFrom == VoreRole.Invalid)
            {
                yield return "required field \"takeFrom\" must be set";
            }
            if (giveTo == VoreRole.Invalid)
            {
                yield return "required field \"giveTo\" must be set";
            }
            if (takeFrom == giveTo)
            {
                yield return "can't use same role in fields \"takeFrom\" and \"giveTo\"";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref takeFrom, "takeFrom");
            Scribe_Values.Look(ref giveTo, "giveTo");
        }
    }

    public class RollAction_StealNeed : RollAction_Steal
    {
        NeedDef need;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if (invert)
            {
                rollStrength *= -1;
            }
            if(rollStrength < 0)
            {
                RV2Log.Warning("rollStrength is negative for stealing need. This may lead to undesired effects", "OngoingVore");
            }
            bool takerCanTakeNeed = TakerPawn.needs?.TryGetNeed(need) != null    // taker has need
                && TakerPawn.needs.TryGetNeed(need).CurLevel + rollStrength < 1;    // and taker has capacity in the need to steal from giver
            if (!takerCanTakeNeed)
            {
                RV2Log.Message("No need to steal need " + need.defName + " - " + TakerPawn.LabelShort + " has no capacity for " + rollStrength, true, false, "OngoingVore");
                return false;
            }
            bool canStealFromGiver = RV2PawnUtility.TryIncreaseNeed(GiverPawn, need, -rollStrength);
            // if we successfully decreased the givers need, or we are allowed to increase needs without needing to decrease the givers need
            if (canStealFromGiver || RV2Mod.settings.cheats.CanStealFromNullNeed)
            {
                return RV2PawnUtility.TryIncreaseNeed(TakerPawn, need, rollStrength);
            }
            return false;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (need == null)
            {
                yield return "required field \"need\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref need, "need");
        }
    }

    public abstract class RollAction_StealComparable : RollAction_Steal
    {
        protected bool useFixed = true;
        protected bool useRandom = false;
        protected bool useTakersBest = false;
        protected bool useTakersWorst = false;
        protected bool useGiversBest = false;
        protected bool useGiversWorst = false;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref useFixed, "useFixed");
            Scribe_Values.Look(ref useRandom, "useRandom");
            Scribe_Values.Look(ref useTakersBest, "useTakersBest");
            Scribe_Values.Look(ref useTakersWorst, "useTakersWorst");
            Scribe_Values.Look(ref useGiversBest, "useGiversBest");
            Scribe_Values.Look(ref useGiversWorst, "useGiversWorst");
        }
    }

    /// One thing to keep in mind is that skills use exponentially more experience points per level, 
    ///   trying to steal someones level whilst at lvl19 will take way more giver skills to 
    ///   sum up the experience points necessary for the takers skill upgrade
    ///   
    public class RollAction_StealSkill : RollAction_StealComparable
    {
        SkillDef skill;
        bool useExperience;
        bool useLevels;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if (TakerPawn?.skills?.skills == null)
            {
                RV2Log.Warning("Can't steal skills for taker with no skills: " + TakerPawn?.Label);
                return false;
            }
            if (GiverPawn?.skills.skills == null)
            {
                RV2Log.Warning("Can't steal skills from giver with no skills: " + GiverPawn?.Label);
                return false;
            }
            SkillDef skillDef = GetSkill();
            if (skillDef == null)
            {
                return false;
            }
            if (useExperience)
            {
                LeechExperience(GiverPawn, TakerPawn, skillDef, rollStrength);
                return true;
            }
            else if (useLevels)
            {
                LeechLevels(GiverPawn, TakerPawn, skillDef, rollStrength);
                return true;
            }
            return false;
        }

        private SkillDef GetSkill()
        {
            Func<Pawn, IEnumerable<SkillDef>> skillGetter = (Pawn p) => p.skills.skills // take all skills
                .Where(s => !s.TotallyDisabled) // limit to all valid skills
                .Select(s => s.def);    // convert to defs
            Func<Pawn, SkillDef, int> SkillLevel = (Pawn p, SkillDef s) => p.skills.GetSkill(s).Level;
            IEnumerable<SkillDef> giverSkills = skillGetter(GiverPawn);
            IEnumerable<SkillDef> takerSkills = skillGetter(TakerPawn);
            // only skills that both giver and taker have are valid for stealing
            IEnumerable<SkillDef> validSkills = giverSkills.Intersect(takerSkills);
            // now that we have a list of valid skills, remove all invalid skills from giver and taker
            //   this allows us to later on determine the best / worst skills
            giverSkills = giverSkills.Intersect(validSkills);
            takerSkills = takerSkills.Intersect(validSkills);
            if (useFixed && skill != null)
            {
                return skill;
            }
            if (useRandom)
            {
                return validSkills.RandomElement();
            }
            if (useGiversBest)
            {
                return giverSkills.MaxBy(s => SkillLevel(GiverPawn, s));
            }
            if (useGiversWorst)
            {
                return giverSkills.MinBy(s => SkillLevel(GiverPawn, s));
            }
            if (useTakersBest)
            {
                return takerSkills.MaxBy(s => SkillLevel(TakerPawn, s));
            }
            if (useTakersWorst)
            {
                return takerSkills.MinBy(s => SkillLevel(TakerPawn, s));
            }
            RV2Log.Warning("Could not determine skill");
            return null;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (useFixed && skill == null)
            {
                yield return "Field \"skill\" is required if field \"useFixed\" is used";
            }
            if (!useExperience && !useLevels)
            {
                yield return "Either \"useExperience\" or \"useLevels\" must be true for skill theft";
            }
            if (useExperience && useLevels)
            {
                yield return "Can't use both \"useExperience\" and \"useLevels\", disable one of them";
            }
        }

        private void LeechLevels(Pawn giver, Pawn taker, SkillDef targetSkill, float amount)
        {
            SkillRecord giverSkill = giver.skills.GetSkill(targetSkill);
            SkillRecord takerSkill = taker.skills.GetSkill(targetSkill);
            ReduceLevels(giverSkill, amount, out float experience);
            ImpactExperience(takerSkill, experience);
        }

        private void ReduceLevels(SkillRecord record, float amount, out float experience)
        {
            experience = 0;
            // remove the current xp progress
            amount -= record.XpProgressPercent;
            experience += record.xpSinceLastLevel;
            record.xpSinceLastLevel = 0;

            // reduce 3 levels if 2.3 provided, then add back 0.7 as percentage progress
            int levelsToReduce = Mathf.CeilToInt(amount);
            int newLevel = record.Level - levelsToReduce;
            // for now, add all the XP between the lost levels, will later be reduced by whatever new progress in the new level the giver has
            experience += GetExperienceBetweenLevels(record.Level, newLevel);
            // effectively reduce levels
            record.Level -= newLevel;
            amount = levelsToReduce - amount;
            // recalculate leftover levels
            record.xpSinceLastLevel = SkillRecord.XpRequiredToLevelUpFrom(record.Level) * amount;
            // and subtract the "regained" leftover XP from the resulting experience
            experience -= record.xpSinceLastLevel;
        }

        private void LeechExperience(Pawn giver, Pawn taker, SkillDef targetSkill, float amount)
        {
            SkillRecord giverSkill = giver.skills.GetSkill(targetSkill);
            SkillRecord takerSkill = taker.skills.GetSkill(targetSkill);
            // limit the leechable experience based on the givers experience
            amount = Math.Min(GetTotalExperience(giverSkill), amount);
            RV2Log.Message("Pawn " + taker.Label + " is leeching " + amount + " experience from " + giver.Label + " skill " + targetSkill.label);
            ImpactExperience(giverSkill, -amount);
            ImpactExperience(takerSkill, amount);
        }

        private void ImpactExperience(SkillRecord record, float amount)
        {
            float newTotalExperience = GetTotalExperience(record) + amount;
            record.Level = GetSkillLevelForExperience(record, amount, out float leftoverExperience);
            record.xpSinceLastLevel = leftoverExperience;
        }

        // for some fucking reason base game does not include the current levels experience in the total
        private float GetTotalExperience(SkillRecord record)
        {
            return record.XpTotalEarned + record.xpSinceLastLevel;
        }

        private float GetExperienceBetweenLevels(int startLevel, int endLevel)
        {
            float experience = 0f;
            for (int i = startLevel; i < endLevel; i++)
            {
                experience += SkillRecord.XpRequiredToLevelUpFrom(i);
            }
            return experience;
        }

        private int GetSkillLevelForExperience(SkillRecord record, float experience, out float leftoverExperience)
        {
            int level = 0;
            int loopLock = 20; // max skill level is 20 anyways
            // while we have experience left
            while ((experience -= SkillRecord.XpRequiredToLevelUpFrom(level)) > 0)
            {
                // increase level
                level++;
                RV2Log.Message("Increased " + record.def.label + " to level " + record.Level + ", xp left: " + experience, true);
                if (loopLock-- < 0) break;
            }
            leftoverExperience = experience;
            RV2Log.Message("Total level possible: " + level + " leftover XP: " + leftoverExperience, true);
            return level;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref skill, "skill");
            Scribe_Values.Look(ref useExperience, "useExperience");
            Scribe_Values.Look(ref useLevels, "useLevels");
        }
    }

    public abstract class RollAction_PassValue : RollAction
    {
        public string name;
        protected float minValue = float.MinValue;
        protected float maxValue = float.MaxValue;

        public override void ExposeData()
        {
            Scribe_Values.Look(ref name, "name");
            Scribe_Values.Look(ref minValue, "minValue");
            Scribe_Values.Look(ref maxValue, "maxValue");
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (name == null)
            {
                yield return "Required field \"name\" not provided";
            }
            if (minValue > maxValue)
            {
                yield return "field \"minValue\" is larger than field \"maxValue\"";
            }
        }
    }

    public class RollAction_PassValue_Set : RollAction_PassValue
    {
        private enum VorePassValueType
        {
            Value,
            PreyNutrition,
            DigestionProgress
        }
        protected float value = 0;

#pragma warning disable IDE0044 // Add readonly modifier
        private VorePassValueType type = VorePassValueType.Value;
#pragma warning restore IDE0044 // Add readonly modifier

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            switch (type)
            {
                case VorePassValueType.Value:
                    record.SetPassValue(name, value);
                    return true;
                case VorePassValueType.PreyNutrition:
                    float preyNutritionValue = VoreCalculationUtility.CalculatePreyNutrition(record.Prey, record.Predator);
                    record.SetPassValue(name, preyNutritionValue);
                    return true;
                case VorePassValueType.DigestionProgress:
                    float digestionProgressValue = DigestionUtility.GetPreviousDigestionProgress(record.Prey);
                    if (digestionProgressValue > 0) RV2Log.Message("Resuming digestion progress at " + digestionProgressValue, "OngoingVore");
                    record.SetPassValue(name, digestionProgressValue);
                    return true;
                default:
                    RV2Log.Error("Unknown VorePassValue type: " + type, "OngoingVore");
                    return false;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref value, "value");
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(type != VorePassValueType.Value && value != 0)
            {
                yield return "WARNING: Field \"value\" is set when \"type\" is not \"Value\". The currently set type will overwrite whichever \"value\" you set, consider removing \"value\" or using \"type\"=\"Value\" instead.";
            }
        }
    }

    public class RollAction_PassValue_Remove : RollAction_PassValue
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            return record.PassValues.Remove(name);
        }
    }

    public class RollAction_PassValue_Add : RollAction_PassValue
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            float newValue = record.PassValues[name] + rollStrength;
            record.ModifyPassValue(name, newValue, minValue, maxValue);
            return true;
        }
    }

    public class RollAction_PassValue_Subtract : RollAction_PassValue
    {
        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            float newValue = record.PassValues[name] - rollStrength;
            record.ModifyPassValue(name, newValue, minValue, maxValue);
            return true;
        }
    }

    public class RollAction_RecordTale : RollAction
    {
#pragma warning disable CS0649 // assigned from DEF
        private TaleDef tale;
#pragma warning restore CS0649 // assigned from DEF
        protected virtual TaleDef Tale => tale;
        protected virtual Def AdditionalDef => null;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            
            if (Tale == null)
            {
                return false;
            }
            TaleRecorder.RecordTale(Tale, new object[]
            {
                record.Predator,
                record.Prey,
                AdditionalDef
            });
            return true;
        }
    }

    public class RollAction_RecordTale_VoreInitiation : RollAction_RecordTale
    {
        protected override TaleDef Tale => record.VorePath.def.initTale;
        protected override Def AdditionalDef => record.VorePath.def;
    }

    public class RollAction_RecordTale_GoalFinish : RollAction_RecordTale
    {
        protected override TaleDef Tale => record.VorePath.VoreGoal.goalFinishTale;
        protected override Def AdditionalDef => record.VorePath.VoreGoal;
    }

    public class RollAction_RecordTale_VoreExit : RollAction_RecordTale
    {
        protected override TaleDef Tale => record.VorePath.def.exitTale;
        protected override Def AdditionalDef => record.VorePath.def;
    }

    public class RollAction_IncrementRecord : RollAction
    {

#pragma warning disable CS0649 // assigned from DEF
        private RecordDef recordDef;
#pragma warning restore CS0649 // assigned from DEF

        protected virtual RecordDef RecordDef => recordDef;

        public override bool TryAction(VoreTrackerRecord record, float rollStrength)
        {
            base.TryAction(record, rollStrength);
            if(TargetPawn.records == null)
            {
                return false;
            }
            TargetPawn.records.Increment(RecordDef);
            return true;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(target == VoreRole.Invalid)
            {
                yield return "required field \"target\" not set";
            }
            if(recordDef == null && (this.GetType() == typeof(RollAction_IncrementRecord))) // only check the recordDef field if we are using the base class
            {
                yield return "required field \"recordDef\" not set";
            }
        }
    }

    /* I don't know about these... went with implementing them directly in the PreVore and PostVore hooks */
    //public class RollAction_IncrementRecord_Type : RollAction_IncrementRecord
    //{
    //    protected override RecordDef RecordDef
    //    {
    //        get
    //        {
    //            switch (target)
    //            {
    //                case VoreRole.Predator:
    //                    return record.VoreType.initiationRecordPredator;
    //                case VoreRole.Prey:
    //                    return record.VoreType.initiationRecordPrey;
    //                default:
    //                    return null;
    //            }
    //        }
    //    }
    //}

    //public class RollAction_IncrementRecord_Goal : RollAction_IncrementRecord
    //{
    //    protected override RecordDef RecordDef
    //    {
    //        get
    //        {
    //            switch (target)
    //            {
    //                case VoreRole.Predator:
    //                    return record.VoreGoal.goalFinishRecordPredator;
    //                case VoreRole.Prey:
    //                    return record.VoreGoal.goalFinishRecordPrey;
    //                default:
    //                    return null;
    //            }
    //        }
    //    }
    //}
}