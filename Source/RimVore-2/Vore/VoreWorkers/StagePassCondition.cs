﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public abstract class StagePassCondition : IExposable
    {
        protected bool decreasing = false;

        protected float CalculateProgress(float curValue, float endValue, float startValue)
        {
            if (startValue == endValue || curValue == endValue)
            {
                return 1;
            }
            float progress;
            // increasing value, take portion of end value
            if (!decreasing)
            {
                progress = curValue / endValue;
                //Log.Message("for increasing, start " + startValue + " end " + endValue + " current: " + curValue + " progress: " + progress);
            }
            // decreasing value, determine total distance and then take current distance in relation
            else
            {
                progress = (startValue - curValue) / (startValue - endValue);
                //Log.Message("for decreasing, start " + startValue + " end " + endValue + ", current: " + curValue + " progress: " + progress);
            }

            return progress.LimitClamp(0, 1);
        }

        public abstract bool IsPassed(VoreTrackerRecord record, out float progress);

        public virtual IEnumerable<string> ConfigErrors()
        {
            yield break;
        }
        public virtual void ExposeData()
        {
            Scribe_Values.Look(ref decreasing, "increasing");
        }
    }

    public class StagePassCondition_Never : StagePassCondition
    {
        public override bool IsPassed(VoreTrackerRecord record, out float progress)
        {
            RV2Log.Message(record.LogLabel + " - PassCondition_Never is always false", true, true, "OngoingVore");
            progress = -1;
            return false;
        }
    }

    public class StagePassCondition_Manual : StagePassCondition
    {
        public override bool IsPassed(VoreTrackerRecord record, out float progress)
        {
            progress = -1;
            return record.IsManuallyPassed;
        }
    }

    public class StagePassCondition_Timed : StagePassCondition
    {
        int duration;

        public override bool IsPassed(VoreTrackerRecord record, out float progress)
        {
            int adaptedDuration = Math.Max(1, (int)(duration / RV2Mod.settings.cheats.VoreSpeedMultiplier));    // prevent divide by 0 exception with math.max
            int currentlyPassed = record.CurrentVoreStage.PassedRareTicks;
            progress = currentlyPassed / adaptedDuration;
            progress = CalculateProgress(currentlyPassed, adaptedDuration, 0);
            bool isPassed = record.CurrentVoreStage.PassedRareTicks >= adaptedDuration;
            RV2Log.Message(record.LogLabel + " - PassCondition_Timed progress: " + progress + " (" + currentlyPassed + "/" + duration + "(" + adaptedDuration + ")), passed: " + isPassed, true, true, "OngoingVore");
            return isPassed;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(duration <= 0)
            {
                yield return "required field \"duration\" must be larger than 0";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref duration, "duration");
        }
    }

    public abstract class TargetedStagePassCondition : StagePassCondition
    {
        VoreRole target;
        protected Pawn TargetPawn(VoreTrackerRecord record) => record.GetPawnByRole(target);

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }
            if (target == VoreRole.Invalid)
            {
                yield return "required field \"target\" must be set";
            }
        }

        public override bool IsPassed(VoreTrackerRecord record, out float progress)
        {
            progress = -1f;
            // the prey may have died during vore, which means they would be stuck in this stage
            if (TargetPawn(record).Dead)
            {
                return true;
            }
            return false;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref target, "target");
        }
    }

    public class StagePassCondition_Need : TargetedStagePassCondition
    {
        NeedDef need;
        float targetLevel = float.MinValue;

        // TODO I think the whole dictionary approach is unnecessary, each instance of vore creates its own instances of VoreStage, 
            // which in turn instance these StagePassConditions, so logically this part of the code exists exactly once for each vore, 
            // meaning we have no collisions and don't need to find individual keys for each vore
        private Dictionary<float, float> InitialNeedLevels = new Dictionary<float, float>();
        public override bool IsPassed(VoreTrackerRecord record, out float progress)
        {
            if (base.IsPassed(record, out progress))
            {
                return true;
            }
            Pawn pawn = TargetPawn(record);
            Need pawnNeed = pawn.needs.TryGetNeed(need);
            if(pawnNeed == null)
            {
                RV2Log.Message(record.LogLabel + " - PassCondition_Need, but target " + pawn?.Label + " does not have need " + need.defName + " passing to prevent being stuck", true, true, "OngoingVore");
                progress = -1;
                return true;
            }
            float currentLevel = pawnNeed.CurLevel;
            
            // get a unique identifier to this pass condition in relation to the record
            //   this way we can persist the level of the very first time we are checking this condition, which is used for progress calculations
            float initialNeedKey = record.Predator.GetHashCode() * record.Prey.GetHashCode() * pawn.GetHashCode() / need.GetHashCode();
            if (!InitialNeedLevels.ContainsKey(initialNeedKey))
            {
                RV2Log.Message("No initial need present yet, setting to " + currentLevel, "OngoingVore");
                InitialNeedLevels.Add(initialNeedKey, currentLevel);
            }
            InitialNeedLevels.TryGetValue(initialNeedKey, out float initialLevel);
            progress = CalculateProgress(currentLevel, targetLevel, initialLevel);
            bool isPassed = currentLevel >= targetLevel;
            RV2Log.Message(record.LogLabel + " - PassCondition_Need " + pawn?.Label + "|" + need.defName + " progress: " + progress + " (" + currentLevel + "/" + targetLevel + "), passed: " + isPassed, true, true, "OngoingVore");
            return isPassed;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(need == null)
            {
                yield return "Required field \"need\" not set";
            }
            if(targetLevel == float.MinValue)
            {
                yield return "Required field \"targetLevel\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Defs.Look(ref need, "need");
            Scribe_Values.Look(ref targetLevel, "targetLevel");
        }
    }

    public class StagePassCondition_PassValue : StagePassCondition
    {
        public string passValueName;
        float targetValue = float.MinValue;

        public override bool IsPassed(VoreTrackerRecord record, out float progress)
        {
            if (!record.PassValues.TryGetValue(passValueName, out float currentValue))
            {
                RV2Log.Message(record.LogLabel + " - PassCondition_PassValue, but record does not have PassValue " + passValueName + " set! Passing to prevent being stuck", true, true, "OngoingVore");
                progress = -1;
                return true;
            }
            if(!record.InitialPassValues.TryGetValue(passValueName, out float initialValue))
            {
                RV2Log.Message(record.LogLabel + " - PassCondition_PassValue, but record does not have InitialPassValue " + passValueName + " set! Passing to prevent being stuck", true, true, "OngoingVore");
                progress = -1;
                return true;
            }
            progress = CalculateProgress(currentValue, targetValue, initialValue);
            bool isPassed = progress == 1;
            RV2Log.Message(record.LogLabel + " - PassCondition_PassValue progress: " + progress + " (" + currentValue+ "/" + targetValue+ "), passed: " + isPassed, true, true, "OngoingVore");
            return isPassed;
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach(string error in base.ConfigErrors())
            {
                yield return error;
            }
            if(passValueName == null)
            {
                yield return "Required field \"passValueName\" not set";
            }
            if(targetValue == float.MinValue)
            {
                yield return "Required field \"targetValue\" not set";
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref passValueName, "passValueName");
            Scribe_Values.Look(ref targetValue, "targetValue");
        }
    }

    public class StagePassCondition_Healed : TargetedStagePassCondition
    {
        float initialTotalInjurySeverity = float.MinValue;

        public override bool IsPassed(VoreTrackerRecord record, out float progress)
        {
            if (base.IsPassed(record, out progress))
            {
                return true;
            }
            List<Hediff> injuries = TargetPawn(record).GetHealableInjuries();
            float totalInjurySeverity = injuries.Sum(injury => injury.Severity);

            if(initialTotalInjurySeverity == float.MinValue)
            {
                initialTotalInjurySeverity = totalInjurySeverity;
            }
            progress = 1 - totalInjurySeverity / initialTotalInjurySeverity;
            return totalInjurySeverity == 0f;
        }
    }
}
