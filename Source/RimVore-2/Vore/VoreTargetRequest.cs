﻿using RimWorld;
using System;
using Verse;
using Verse.AI;

namespace RimVore2
{
    public class VoreTargetRequest
    {
        // single
        bool? isColonist = null;
        bool? isPrisoner = null;
        bool? isSlave = null;
        bool? isAnimal = null;
        bool? isHumanoid = null;
        bool? isDowned = null;
        bool? canFightBack = null;
        Func<Pawn, bool> Validator = null;
        // pair
        bool? isSameFaction = null;
        bool? isHostedByInitiatorFaction = null;
        bool? isHostile = null;
        bool? canBeVored = null;
        bool? canBeFatalVored = null;
        bool? canBeEndoVored = null;
        bool? canDoVore = null;
        bool? canDoFatalVore = null;
        bool? canDoEndoVore = null;
        Func<Pawn, Pawn, bool> PairValidator = null;

        public VoreTargetRequest() { }

        public VoreTargetRequest(
            bool? isColonist = null,
            bool? isPrisoner = null,
            bool? isSlave = null,
            bool? isAnimal = null,
            bool? isHumanoid = null,
            bool? isDowned = null,
            bool? canFightBack = null,
            Func<Pawn, bool> validator = null,
            bool? isSameFaction = null,
            bool? isHostedByInitiatorFaction = null,
            bool? isHostile = null,
            bool? canBeVored = null,
            bool? canBeFatalVored = null,
            bool? canBeEndoVored = null,
            bool? canDoVore = null,
            bool? canDoFatalVore = null, 
            bool? canDoEndoVore = null, 
            Func<Pawn, Pawn, bool> pairValidator = null)
        {
            this.isColonist = isColonist;
            this.isPrisoner = isPrisoner;
            this.isSlave = isSlave;
            this.isAnimal = isAnimal;
            this.isHumanoid = isHumanoid;
            this.isDowned = isDowned;
            this.canFightBack = canFightBack;
            Validator = validator;

            this.isSameFaction = isSameFaction;
            this.isHostedByInitiatorFaction = isHostedByInitiatorFaction;
            this.isHostile = isHostile;
            this.canBeVored = canBeVored;
            this.canBeFatalVored = canBeFatalVored;
            this.canBeEndoVored = canBeEndoVored;
            this.canDoVore = canDoVore;
            this.canDoFatalVore = canDoFatalVore;
            this.canDoEndoVore = canDoEndoVore;
            PairValidator = pairValidator;
        }

        public virtual bool IsValid(Pawn pawn)
        {
            if (isColonist != null && pawn.IsColonist != isColonist)
            {
                return false;
            }
            if (isPrisoner != null && pawn.IsPrisoner != isPrisoner)
            {
                return false;
            }
#if v1_2
#else
            if (isSlave != null && pawn.IsSlave != isSlave)
            {
                return false;
            }
#endif
            if (isAnimal != null && pawn.IsAnimal() != isAnimal)
            {
                return false;
            }
            if (isHumanoid != null && pawn.IsHumanoid() != isHumanoid)
            {
                return false;
            }
            if (isDowned != null && pawn.Downed != isDowned)
            {
                return false;
            }
            if (canFightBack != null && pawn.skills.GetSkill(SkillDefOf.Melee).TotallyDisabled != canFightBack)
            {
                return false;
            }
            if (Validator != null && !Validator(pawn))
            {
                return false;
            }
            return true;
        }

        public virtual bool IsValid(Pawn initiator, Pawn target)
        {
            if (!initiator.CanReach(target, PathEndMode.ClosestTouch, Danger.Some))
            {
                return false;
            }
            if (!IsValid(target))
            {
                return false;
            }
            if (isSameFaction != null && (initiator.Faction == target.Faction) != isSameFaction)
            {
                return false;
            }
            if (isHostedByInitiatorFaction != null && initiator.Faction == target.HostFaction != isHostedByInitiatorFaction)
            {
                return false;
            }
            if (isHostile != null && target.HostileTo(initiator.Faction) != isHostile)
            {
                return false;
            }
            if (canBeVored != null && initiator.CanVore(target, out _) != canBeVored)
            {
                return false;
            }
            if (canBeFatalVored != null && initiator.CanFatalVore(target, out _, true) != canBeFatalVored)
            {
                return false;
            }
            if (canBeEndoVored != null && initiator.CanEndoVore(target, out _, true) != canBeEndoVored)
            {
                return false;
            }
            if (canDoVore != null && target.CanVore(initiator, out _) != canDoVore)
            {
                return false;
            }
            if (canDoFatalVore != null && target.CanFatalVore(initiator, out _, true) != canDoFatalVore)
            {
                return false;
            }
            if (canDoEndoVore != null && target.CanEndoVore(initiator, out _, true) != canDoEndoVore)
            {
                return false;
            }
            if (PairValidator != null && !PairValidator(initiator, target))
            {
                return false;
            }
            return true;
        }
    }
}
