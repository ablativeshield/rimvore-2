﻿using System;
using System.Collections.Generic;
using System.Linq;
using Verse;
using RimWorld;
using RimWorld.Planet;
using UnityEngine;

namespace RimVore2
{
    public class VoreTrackerRecord : IExposable
    {
        public VoreTracker VoreTracker => Predator.GetVoreTracker();
        public Pawn Predator;
        public Pawn Prey;
        public Pawn ForcedBy;
        public VoreContainer VoreContainer;
        public VorePath VorePath;
        public VoreTypeDef VoreType => VorePath.VoreType;
        public VoreGoalDef VoreGoal => VorePath.VoreGoal;
        public int VorePathIndex;
        public bool PreyStartedNaked = false;
        public bool IsInterrupted = false;
        public bool IsManuallyPassed = false;
        public bool IsFinished = false;
        public HediffDef CurrentHediffDef => CurrentVoreStage?.def.predatorHediffDef;
        public bool IsForced => ForcedBy != null;

        public Dictionary<string, float> PassValues = new Dictionary<string, float>();
        // used to calculate progress on values that are not 0.0 -> 1.0 progress
        public Dictionary<string, float> InitialPassValues = new Dictionary<string, float>();

        public int VorePathCount => VorePath.path.Count;
        public BodyPartRecord CurrentBodyPart => BodyPartUtility.GetBodyPartByName(Predator, CurrentVoreStage.def.partName);
        public VoreStage CurrentVoreStage => VorePath?.path[VorePathIndex];
        public VoreStage PreviousVoreStage
        {
            get
            {
                int previousIndex = VorePathIndex - 1;
                // if out of range, return NULL
                if(previousIndex < 0 || previousIndex >= VorePathCount)
                {
                    return null;
                }
                return VorePath.path[previousIndex];
            }
        }
        public VoreStage NextVoreStage
        {
            get
            {
                int nextIndex = VorePathIndex + 1;
                // if out of range, return NULL
                if (nextIndex < 0 || nextIndex >= VorePathCount)
                {
                    return null;
                }
                return VorePath.path[nextIndex];
            }
        }
        public bool HasReachedEntrance => PreviousVoreStage == null;
        public bool HasReachedEnd => NextVoreStage == null;
        public bool CanMoveToNextPart => !HasReachedEnd && CurrentVoreStage.PassConditionsFulfilled(this);
        public bool CanReverse => CurrentVoreStage.def.canReverseDirection;
        // TODO reversal instead of directly exiting
        public bool CanEject
        {
            get
            {
                if (HasReachedEnd)
                {
                    return true;
                }
                if (CanReverse)
                {
                    // check if any predator quirks prevent eject
                    QuirkManager predatorQuirks = Predator.QuirkManager();
                    if(predatorQuirks != null && predatorQuirks.HasSpecialFlag("InescapablePredator"))
                    {
                        return false;
                    }
                    return true;
                }
                return false;
            }
        }

        public bool IsCurrentPartValid => BodyPartUtility.GetBodyPartByName(Predator, CurrentVoreStage.def.partName) != null;
        public bool IsNextPartValid => BodyPartUtility.GetBodyPartByName(Predator, NextVoreStage.def.partName) != null;

        public string LogLabel
        {
            get
            {
                return "Predator: " + Predator?.LabelShort +
                "|Prey: " + Prey?.LabelShort +
                "|Stage: " + CurrentVoreStage?.def.defName;
            }
        }

        public string DisplayLabel
        {
            get
            {
                string name = Prey.Name?.ToStringFull;
                if (name == null) name = Prey.def.label;
                string partGoal = CurrentVoreStage.def.partGoal;
                string partName = CurrentVoreStage.def.DisplayPartName;
                int percentageProgress = (int)(CurrentVoreStage.PercentageProgress * 100);
                string label = $"{name} - [{partGoal}] {partName}";
                if(percentageProgress > 0)
                {
                    label += $" {percentageProgress}%";
                }
                return label;
            }
        }

        public VoreTrackerRecord()
        {
            Predator = null;
            Prey = null;
            VoreContainer = null;
            VorePath = null;
            VorePathIndex = 0;
        }

        public VoreTrackerRecord(VoreTrackerRecord oldRecord)
        {
            Predator = oldRecord.Predator;
            Prey = oldRecord.Prey;
            ForcedBy = oldRecord.ForcedBy;
            VorePath = new VorePath(oldRecord.VorePath.def);
            VorePathIndex = oldRecord.VorePathIndex;
        }

        public void Initialize()
        {
            VoreTracker.SynchronizeHediffs();
            //SetPredatorHediff();
            RV2Log.Message("Added " + Prey.LabelShort + " to predator " + Predator.LabelShort + "'s " + CurrentBodyPart.Label + ". Stage defName: " + CurrentVoreStage.def.defName, "OngoingVore");
            RV2Log.Message("voreTrackerRecord: " + ToString(), "OngoingVore");
            CurrentVoreStage.Start(this);
        }

        public void Tick()
        {
            VoreContainer.Tick();
        }

        public void TickRare()
        {
            CurrentVoreStage.PassedRareTicks++;
            VoreContainer.TickRare();
            // move prey through body if possible
            if (CanMoveToNextPart)
            {
                MovePreyToNextPart();
            }
            CurrentVoreStage.Cycle(this);
            if (RV2Mod.settings.cheats.PreventStarvingPrey)
            {
                PreventPreyFromStarving(Prey);
            }
            EjectIfDumpingOverdue();
        }

        private void EjectIfDumpingOverdue()
        {
            if(HasReachedEnd && CurrentVoreStage.PassConditionsFulfilled(this))
            {
                RV2Log.Message("Prey " + Prey.LabelShort + " is overdue to be released, forcing release");
                VoreTracker.Eject(this);
            }
        }

        private void PreventPreyFromStarving(Pawn pawn)
        {
            if (pawn.Dead)
            {
                return;
            }
            Need_Food need = pawn.needs?.food;
            if (need == null)
            {
                return;
            }
            if (need.Starving)
            {
                RV2Log.Message("Preventing prey " + pawn.LabelShort + " from starving.");
                need.CurLevel = 0.5f;
            }
        }

        public void MovePreyToNextPart()
        {
            // check for validity is done in Tick
            if (!IsCurrentPartValid || !IsNextPartValid)
            {
                RV2Log.Warning("The " + (!IsCurrentPartValid ? "current" : "next") + " body part for the prey is invalid, calling emergency eject.", "OngoingVore");
                VoreTracker.EmergencyEject(this);
                return;
            }
            CurrentVoreStage.End(this);
            VorePathIndex++;
            RV2Log.Message("Moved prey " +
                Prey.LabelShort +
                " from " +
                PreviousVoreStage.def.partName +
                "|" +
                PreviousVoreStage.def.predatorHediffDef.defName +
                " to " +
                CurrentVoreStage.def.partName +
                "|" +
                CurrentVoreStage.def.predatorHediffDef.defName +
                " stage defName: " +
                CurrentVoreStage.def.defName, "OngoingVore");
            VoreTracker.SynchronizeHediffs();
            //SetPredatorHediff();
            CurrentVoreStage.Start(this);
        }

        /*public void SetPredatorHediff()
        {
            HediffDef currentHediffDef = CurrentVoreStage?.def.predatorHediffDef;
            // Log.Message("current stage hediffDef: " + currentHediffDef);
            Hediff_ContainingPrey addingHediff = (Hediff_ContainingPrey)Predator.health?.hediffSet?.GetFirstHediffOfDef(currentHediffDef);
            if (addingHediff == null)
            {
                Hediff_ContainingPrey hediff = HediffMakerUtility.MakeHediffForPart<Hediff_ContainingPrey>(currentHediffDef, Predator, CurrentBodyPart);
                RV2Log.Message("Adding new hediff " + hediff.Label, "OngoingVore");
                Predator.health.AddHediff(hediff);
            }
            else
            {
                addingHediff.UpdateLabel();
                RV2Log.Message("Hediff " + addingHediff.def.defName + " already exists", "OngoingVore");
            }
        }*/

        public void SetPassValue(string passValueName, float setValue)
        {
            PassValues.SetOrAdd(passValueName, setValue);
            InitialPassValues.SetOrAdd(passValueName, setValue);
        }

        public void ModifyPassValue(string passValueName, float modifyValue, float minValue = float.MinValue, float maxValue = float.MaxValue)
        {
            if (!PassValues.ContainsKey(passValueName))
            {
                throw new Exception("Trying to modify key that has not been set before!");
            }
            if (modifyValue < minValue)
            {
                RV2Log.Message("Prevented value to be lower than minValue, forced " + modifyValue + " to become " + minValue, true, false, "OngoingVore");
                modifyValue = minValue;
            }
            if (modifyValue > maxValue)
            {
                RV2Log.Message("Prevented value to be higher than maxValue, forced " + modifyValue + " to become " + maxValue, true, false, "OngoingVore");
                modifyValue = maxValue;
            }

            PassValues[passValueName] = modifyValue;
        }

        public bool IsValid()
        {
            if(Predator == null)
            {
                return false;
            }
            if(Prey == null)
            {
                return false;
            }
            if (CurrentBodyPart == null)
            {
                return false;
            }
            if (VorePath == null)
            {
                return false;
            }
            if(VoreContainer == null)
            {
                return false;
            }
            return true;
        }

        public Pawn GetPawnByRole(VoreRole role)
        {
            switch (role)
            {
                case VoreRole.Predator:
                    return Predator;
                case VoreRole.Prey:
                    return Prey;
                case VoreRole.Feeder:
                    return ForcedBy;
                default:
                    return null;
            }
        }

        public string GetPreyName()
        {
            string preyName = Prey?.LabelShort;
            if (preyName == null)  // should never happen, if it does, make sure we get a unique label
            {
                Log.Error("Prey is null, something is fatally causing issues and set a currently vored pawn to NULL! Trying to remove VoreTrackerRecord");
                VoreTracker?.EmergencyEject(this);
                return null;
            }
            return preyName;
        }

        public void MoveAllPreyItemsToContainer()
        {
            Prey.apparel?.GetDirectlyHeldThings().TryTransferAllToContainer(VoreContainer.GetDirectlyHeldThings());
            Prey.inventory?.GetDirectlyHeldThings().TryTransferAllToContainer(VoreContainer.GetDirectlyHeldThings());
        }

        public override string ToString()
        {
            return "Predator: " + Predator?.LabelShort +
                "|Prey: " + Prey?.LabelShort +
                "|VoreType: " + VoreType?.defName +
                "|VoreGoal: " + VoreGoal?.defName +
                "|CurrentBodyPart: " + CurrentBodyPart?.LabelShort +
                "|ContainedThings: " + VoreContainer?.ToString() +
                //"|VorePathHediffs: " + string.Join(", ", VorePath.path.ConvertAll(e => e.def.predatorHediffDef.ToString())) +
                "|VorePathIndex: " + VorePathIndex;
        }

        public void ExposeData()
        {
            // if we are saving, we must make sure the data we are persisting is correct, otherwise do not scribe it
            if (!(Scribe.mode == LoadSaveMode.Saving && !IsValid()))
            {
                Scribe_References.Look(ref Predator, "Predator", true);
                Scribe_References.Look(ref Prey, "Prey", true);
                Scribe_References.Look(ref ForcedBy, "ForcedBy", true);
                Scribe_Deep.Look(ref VoreContainer, "ContainedThings", new object[0]);
                Scribe_Deep.Look(ref VorePath, "VorePath", new object[0]);
                Scribe_Values.Look(ref VorePathIndex, "VorePathIndex");
                Scribe_Values.Look(ref PreyStartedNaked, "PreyStartedNaked", PreyStartedNaked);
                Scribe_Values.Look(ref IsInterrupted, "IsInterrupted", IsInterrupted);
                Scribe_Values.Look(ref IsManuallyPassed, "IsManuallyPassed", IsManuallyPassed);
                Scribe_Values.Look(ref IsFinished, "IsFinished", IsFinished);
                ScribeUtilities.ScribeVariableDictionary(ref PassValues, "PassValues");
                ScribeUtilities.ScribeVariableDictionary(ref InitialPassValues, "InitialPassValues");
            }
            else
            {
                RV2Log.Error("Error during saving, VoreTrackerRecord " + LogLabel + " is not valid!");
            }
        }
    }
}
