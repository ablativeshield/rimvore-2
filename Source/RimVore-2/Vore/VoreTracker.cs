﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

/// <summary>
/// Keeps track of all pawns containing other pawns inside of any of their bodyparts.
/// Pawn.Tick() is patched to check if a pawn is being tracked, if it is, it passes the tick down to all contained prey pawns
/// </summary>

namespace RimVore2
{
    public class VoreTracker : IExposable
    {
        private Pawn pawn;
        public List<VoreTrackerRecord> VoreTrackerRecords;
        public bool IsSynchronized = false;    // not scribed, so every time the game starts up again, it will force the vore tracker to synchronize the hediffs

        public bool IsTrackingVore => VoreTrackerRecords?.Count > 0;

        public bool HasPreyReadyToRelease => VoreTrackerRecords.Any(record => record.HasReachedEnd);

        public VoreTracker() { }

        public VoreTracker(Pawn pawn) {
            this.pawn = pawn;
            VoreTrackerRecords = new List<VoreTrackerRecord>();
        }

        public void TrackVore(VoreTrackerRecord record)
        {
            if (VoreTrackerRecords.Any(trackedRecord => trackedRecord.Prey == record.Prey))
            {
                RV2Log.Warning("RimVore-2: Tried to vore track a pawn that is already being tracked. Intercepted, no errors should occur.");
                return;
            }
            VoreTrackerRecords.Add(record);
            record.Initialize();
            GlobalVoreTrackerUtility.SetVoreTrackingCacheStale(pawn);
        }

        public void SplitOffNewVore(VoreTrackerRecord originalRecord, Pawn newPrey, VorePath newPath = null, int forcedPathIndex = -1)
        {
            if(newPath == null)
            {
                newPath = originalRecord.VorePath;
            }
            VoreTrackerRecord newRecord = new VoreTrackerRecord(originalRecord)
            {
                Prey = newPrey,
                VorePath = newPath
            };
            PreVoreUtility.PopulateRecord(ref newRecord);
            if(forcedPathIndex >= 0)
            {
                newRecord.VorePathIndex = forcedPathIndex;
            }
            TrackVore(newRecord);
        }

        public void UntrackVore(VoreTrackerRecord record)
        {
            VoreTrackerRecords.Remove(record);
            GlobalVoreTrackerUtility.SetVoreTrackingCacheStale(pawn);
            GlobalVoreTrackerUtility.ClearCachedRecord(record.Prey);
            SynchronizeHediffs();
        }

        public void Tick()
        {
            if(!IsSynchronized)
            {
                SynchronizeHediffs();
            }
            if (!IsTrackingVore)
            {
                return;
            }
            if (pawn.Dead)
            {
                EmergencyEjectAll();
                return;
            }
            foreach (VoreTrackerRecord record in VoreTrackerRecords.ToList())
            {
                try
                {
                    record.Tick();
                }
                catch(Exception e)
                {
                    Log.Error("Exception during predator tick for record " + record.ToString() + "\n error: " + e);
                }
            }
        }

        public void TickRare()
        {
            if (!IsTrackingVore)
            {
                return;
            }
            if (pawn.Dead)
            {
                EmergencyEjectAll();
                return;
            }
            foreach (VoreTrackerRecord record in VoreTrackerRecords.ToList())             
            {
                try
                {
                    record.TickRare();
                }
                catch (Exception e)
                {
                    Log.Error("Exception during predator rare tick for record " + record.ToString() + "\n error: " + e);
                }
            }
        }

        public void SynchronizeHediffs()
        {
            if(pawn.health?.hediffSet == null)
            {
                RV2Log.Message("Pawn " + pawn.LabelShort + " has no health, no vore hediffs to apply");
                return;
            }
            RV2Log.Message("Synchronizing hediffs for " + pawn.LabelShort, true, false, "VoreHediffs");
            ResetHediffConnections();
            foreach(VoreTrackerRecord record in VoreTrackerRecords)
            {
                ConnectHediff(record);
            }
            UpdateHediffs();
            IsSynchronized = true;
        }

        private void ResetHediffConnections()
        {
            IEnumerable<Hediff_ContainingPrey> voreHediffs = pawn.health.hediffSet.hediffs
                .FindAll(hediff => hediff is Hediff_ContainingPrey)
                .Cast<Hediff_ContainingPrey>();
            voreHediffs.ForEach(hediff =>
            {
                RV2Log.Message("Clearing connected records for hediff " + hediff.Label, true, false, "VoreHediffs");
                hediff.ConnectedVoreRecords.Clear();
            });
        }

        private void ConnectHediff(VoreTrackerRecord record)
        {
            HediffDef hediffDef = record.CurrentHediffDef;
            Hediff hediff = pawn.health.hediffSet.GetFirstHediffOfDef(hediffDef);
            if(hediff == null)
            {
                hediff = pawn.health.AddHediff(hediffDef, record.CurrentBodyPart);
            }
            Hediff_ContainingPrey voreHediff = (Hediff_ContainingPrey)hediff;
            voreHediff.ConnectedVoreRecords.Add(record);
            RV2Log.Message("Connected " + record.LogLabel + " to hediff " + voreHediff.Label, true, false, "VoreHediffs");
        }

        private void UpdateHediffs()
        {
            IEnumerable<Hediff_ContainingPrey> voreHediffs = pawn.health.hediffSet.hediffs
                .FindAll(hediff => hediff is Hediff_ContainingPrey)
                .Cast<Hediff_ContainingPrey>();
            foreach(Hediff_ContainingPrey voreHediff in voreHediffs)
            {
                if(voreHediff.ConnectedVoreRecords.Count == 0)
                {
                    RV2Log.Message("Removing vore hediff without VoreTrackerConnections: " + pawn.LabelShort + " | " + voreHediff.Label, true, false, "VoreHediffs");
                    pawn.health.RemoveHediff(voreHediff);
                }
                else
                {
                    voreHediff.UpdateLabel();
                }
            }
        }

        public void EmergencyEjectAll()
        {
            foreach (VoreTrackerRecord record in VoreTrackerRecords?.ToList())
            {
                EmergencyEject(record);
            }
        }

        public void EmergencyEject(VoreTrackerRecord record)
        {
            try
            {
                RV2Log.Message("EMERGENCY EJECT for predator " + record.Predator?.LabelShort + " and prey " + record.Prey?.LabelShort, "OngoingVore");
                record.VoreContainer?.TryDropAllThings();
                //record.ContainedThings.TryRemoveAllThings();
                UntrackVore(record);
                // probably don't want to resolve vore on a broken vore interaction
                // PostVoreUtility.ResolveVore(record, true);
            }
            catch (Exception ex)
            {
                RV2Log.Error("Exception occured during emergency eject: " + ex);
            }
        }

        public void Eject(VoreTrackerRecord record, bool isForcedBySurgery = false, bool simulateNullMap = false)
        {
            if (!isForcedBySurgery && !record.CanEject)
            {
                return;
            }
            // TODO path reversal through pred
            RV2Log.Message("Clearing vore for predator " + record.Predator.LabelShort + " and prey " + record.Prey.LabelShort, "OngoingVore");

            record.VoreContainer.TryDropAllThings(simulateNullMap);
            UntrackVore(record);
            PostVoreUtility.ResolveVore(record);
        }

        public void ExposeData()
        {
            Scribe_Collections.Look(ref VoreTrackerRecords, "TrackedVores", LookMode.Deep, new object[0]);
            Scribe_References.Look(ref pawn, "pawn");
        }

        public override string ToString()
        {
            if(VoreTrackerRecords.Count == 0)
            {
                return "<EmptyVoreTracker>";
            }
            return string.Join("\n", VoreTrackerRecords.ConvertAll(v => v.ToString()));
        }
    }
}
