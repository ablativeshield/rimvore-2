﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public class VoreProposal : IExposable
    {
        public Pawn Predator;
        public Pawn Prey;
        public Pawn Initiator;
        public Pawn Target;
        public VorePathDef VorePath;
        public List<string> Keywords;
        public ProposalStatus status = ProposalStatus.Pending;
        public bool Passed => status == ProposalStatus.Forced || status == ProposalStatus.Accepted;

        public VoreProposal(Pawn predator, Pawn prey, Pawn initiator, Pawn target, VorePathDef path)
        {
            this.Predator = predator;
            this.Prey = prey;
            this.Initiator = initiator;
            this.Target = target;
            this.VorePath = path;
            if (PreferenceUtility.CanBeForced(target))
            {
                this.status = ProposalStatus.Forced;
            }
        }
        public VoreProposal() { }

        private void RollSuccess()
        {
            if (status == ProposalStatus.Forced)
            {
                return;
            }
            float chanceToAccept = PreferenceUtility.GetChanceToAcceptProposal(Initiator, Target, this);
            RV2Log.Message("Chance to accept: " + Math.Round(chanceToAccept * 100) + "%", true, false, "AutoVore");
            bool accepted = RandomUtility.RollSuccess(chanceToAccept);
            status = accepted ? ProposalStatus.Accepted : ProposalStatus.Denied;
        }

        public bool TryProposal()
        {
            RollSuccess();
            if (Passed)
            {
                Accepted();
            }
            else
            {
                Denied();
            }
            DoNotification();
            return Passed;
        }

        private void DoNotification()
        {
            string notificationText;
            switch (status)
            {
                case ProposalStatus.Forced:
                    notificationText = "RV2_Message_VoreProposalForced";
                    break;
                case ProposalStatus.Accepted:
                    notificationText = "RV2_Message_VoreProposalAccepted";
                    break;
                case ProposalStatus.Denied:
                    notificationText = "RV2_Message_VoreProposalDenied";
                    break;
                default:
                    RV2Log.Error("Invalid proposal status: " + status);
                    return;
            }
            notificationText = notificationText.Translate(Target.LabelShort, Initiator.LabelShort);
            NotificationType notificationType;
            if (Passed)
            {
                notificationType = VorePath.voreGoal.IsLethal
                    ? RV2Mod.settings.fineTuning.FatalProposalAcceptedNotification
                    : RV2Mod.settings.fineTuning.ProposalAcceptedNotification;
                string actionDescription = VorePath.actionDescription;
                if (actionDescription != null)
                {
                    notificationText += "\n" + actionDescription.Formatted(Predator.LabelShort, Prey.LabelShort);
                }
            }
            else
            {
                notificationType = RV2Mod.settings.fineTuning.ProposalDeniedNotification;
            }
            NotificationUtility.DoNotification(notificationType, notificationText);
        }

        private void Denied()
        {
            float forcedVoreChance = RV2Mod.settings.fineTuning.ForcedVoreChanceOnFailedProposal;
            QuirkManager initiatorQuirks = Initiator.QuirkManager();
            if(initiatorQuirks == null)
            {
                return;
            }

            forcedVoreChance = initiatorQuirks.ModifyValue("FailedProposalForceVoreChance", forcedVoreChance);
            bool canFightForVore = RV2Mod.settings.rules.CanForceFailedProposal(Initiator, Target) && !Target.Downed;
            if (canFightForVore && RandomUtility.RollSuccess(forcedVoreChance))
            {
                Initiator.mindState.mentalStateHandler.TryStartMentalState(RV2_Common.VoreFighting_Attacker);
                MentalState_ForcedVoreFight_Attacker initiatorMentalState = (MentalState_ForcedVoreFight_Attacker)Initiator.MentalState;
                initiatorMentalState.proposal = this;
                initiatorMentalState.otherPawn = Target;
                Target.mindState.mentalStateHandler.TryStartMentalState(RV2_Common.VoreFighting_Defender);
                MentalState_ForcedVoreFight_Defender targetMentalState = (MentalState_ForcedVoreFight_Defender)Target.MentalState;
                targetMentalState.otherPawn = Initiator;
            }
            VoreThoughtUtility.NotifyDeniedProposal(Initiator, Target, Role(Initiator), VorePath.voreType, VorePath.voreGoal);
        }

        private void Accepted()
        {
            Initiator?.skills?.Learn(SkillDefOf.Social, 500f);
        }

        public VoreRole Role(Pawn pawn)
        {
            if (pawn == Predator)
            {
                return VoreRole.Predator;
            }
            if (pawn == Prey)
            {
                return VoreRole.Prey;
            }
            if (pawn == Initiator)
            {
                return VoreRole.Feeder;
            }
            return VoreRole.Invalid;
        }

        public void ExposeData()
        {
            Scribe_References.Look(ref Predator, "Predator");
            Scribe_References.Look(ref Prey, "Prey");
            Scribe_References.Look(ref Initiator, "Initiator");
            Scribe_References.Look(ref Target, "Target");
            Scribe_Defs.Look(ref VorePath, "VorePath");
            Scribe_Collections.Look(ref Keywords, "KeyWords", LookMode.Value);
            Scribe_Values.Look(ref status, "status");
        }
    }
}
