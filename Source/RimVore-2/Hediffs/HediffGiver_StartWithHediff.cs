﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    /// <remarks
    /// Copied over from the mod JecsTools - which is not officially supported any more
    /// </remarks>
    public class HediffGiver_StartWithHediff : HediffGiver
    {
        public float chance = 1f;
        public float maleCommonality = 1f;
        public float femaleCommonality = 1f;
        public string bodyPartName;

        public void GiveHediff(Pawn pawn)
        {
            //If the random number is not within the chance range, exit.
            if (!RandomUtility.RollSuccess(chance))
            {
                return;
            }
            //If the gender is male, check the male commonality chance, and if it fails, exit.
            if (pawn.gender == Gender.Male && !RandomUtility.RollSuccess(maleCommonality))
            {
                return;
            }
            //If the gender is female, check the female commonality chance, and if it fails, exit.
            if (pawn.gender == Gender.Female && !RandomUtility.RollSuccess(femaleCommonality))
            {
                return;
            }
            // Log.Message("Adding hediff " + this.hediff.defName);
            HediffMakerUtility.AddHediffForPart(hediff, pawn, bodyPartName);
            //pawn.health.AddHediff(appliedHediff);
            //Log.Message("Added hediff " + appliedHediff.def.defName + " with hash " + appliedHediff.GetHashCode());
            //HealthUtility.AdjustSeverity(pawn, this.hediff, 1f);
        }
    }
}