﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;

namespace RimVore2.ToImplement
{
    [HarmonyPatch(typeof(PregnancyHelper))]
    [HarmonyPatch("impregnate")]
    [HarmonyPatch(new Type[] { typeof(Pawn), typeof(Pawn), typeof(xxx.rjwSextype) })]
    public static class Impregnate_Patch
    {

        [HarmonyPrefix]
        public static bool impregnate(Pawn pawn, Pawn partner, xxx.rjwSextype sextype = xxx.rjwSextype.None)
        {
            var (p1, p2) = UBHelper.SortPawns(pawn, partner);

            bool isHost = UBHelper.CanBeHost(p1);
            bool isBaby = UBHelper.CanBeUnbirthed(p2);


            if (isHost && isBaby)
            {
                Log.Message($"try to unbirth {p2}:{p2.Name} with host {p1}:{p1.Name}");
                Hediff_BasePregnancy.Create<Hediff_UnbirthPregnancy>(p1, p2);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
*/