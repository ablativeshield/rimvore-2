﻿/*using RimWorld;
using rjw;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;
namespace Me.Lazy
{
    //[RJWAssociatedHediff("RJW_pregnancy_unbirth")]
    public class Hediff_UnbirthPregnancy : Hediff_BasePregnancy, IThingHolder
    {
        // Handling of custom abort and miscarreing logic is not done on RJW side yet, just disabling them for now

        public override bool canBeAborted
        {
            get
            {
                return false;
            }
        }

        public override bool canMiscarry
        {
            get
            {
                return false;
            }
        }

        public IThingHolder ParentHolder
        {
            get
            {
                return null;
            }
        }

        public override void Abort()
        {
            base.Abort();

            var pawns = container.ToArray();
            container.TryDropAll(pawn.InteractionCell, pawn.Map, ThingPlaceMode.Near, null, null);
            babies.ForEach(x => x.Kill(null));
        }

        public override void Kill()
        {
            base.Kill();

            container.TryDropAll(pawn.InteractionCell, pawn.Map, ThingPlaceMode.Near, null, null);
            babies.ForEach(x => x.Kill(null));
        }

        public override void Initialize(Pawn mother, Pawn dad)
        {
            BodyPartRecord torso = mother.RaceProps.body.AllParts.Find(x => x.def.defName == "Torso");
            mother.health.AddHediff(this, torso);


            father = null;
            babies = new List<Pawn>();
            babies.Add(dad);

            if (container == null)
            {
                container = new ThingOwner<Pawn>(mother);

                dad.DeSpawn(DestroyMode.Vanish); // remove from map (to remove from map container, for some reason direct trasfer is forbidden for it)
                container.TryAdd(dad, true); // preventimg pawn from spawning somewhere
            }
            contractions = 0;
            Severity = 0.5f;
            progress_per_tick = 1.0f / (dad.RaceProps.gestationPeriodDays * TicksPerDay);
        }

        ThingOwner<Pawn> container = null;

        public override void GiveBirth()
        {

            Pawn mother = pawn;
            if (mother == null)
                return;
            if (babies == null)
                return;



            Log.Message("burthing");

            foreach (Pawn p in container)
            {
                p.ageTracker.AgeBiologicalTicks = p.ageTracker.AgeBiologicalTicks + (15 * TicksPerDay);
            }

            container.TryDropAll(mother.InteractionCell, mother.Map, ThingPlaceMode.Direct, null, null); // making appear on map


            // code below is broken because of wrong pawn references

            List<Pawn> siblings = new List<Pawn>();
            foreach (Pawn baby in babies)
            {
                Need_Sex sex_need = mother.needs.TryGetNeed<Need_Sex>();
                if (mother.Faction != null && !(mother.Faction?.IsPlayer ?? false) && sex_need != null)
                {
                    sex_need.CurLevel = 1.0f;
                }


                PostBirth(mother, father, baby);

                if (mother.Spawned)
                {
                    // Move the baby in front of the mother, rather than on top
                    if (mother.CurrentBed() != null)
                    {
                        baby.Position = baby.Position + new IntVec3(0, 0, 1).RotatedBy(mother.CurrentBed().Rotation);
                    }
                    // Spawn guck
                    FilthMaker.MakeFilth(mother.Position, mother.Map, ThingDefOf.Filth_AmnioticFluid, mother.LabelIndefinite(), 5);
                    mother.caller?.DoCall();
                    baby.caller?.DoCall();
                }


                if (xxx.is_human(baby))
                    mother.records.AddTo(xxx.CountOfBirthHuman, 1);
                if (xxx.is_animal(baby))
                    mother.records.AddTo(xxx.CountOfBirthAnimal, 1);


                if ((mother.records.GetAsInt(xxx.CountOfBirthHuman) > 10 || mother.records.GetAsInt(xxx.CountOfBirthAnimal) > 20))
                {
                    mother.Add(Quirk.Breeder);
                    mother.Add(Quirk.ImpregnationFetish);
                }

                // Unbirth quirk?
            }

            mother.health.RemoveHediff(this);

        }

        public override void ExposeData()
        {
            base.ExposeData();

            Scribe_Deep.Look<ThingOwner<Pawn>>(ref this.container, "innerContainer", new object[] { pawn });
        }

        public void GetChildHolders(List<IThingHolder> outChildren)
        {
            ThingOwnerUtility.AppendThingHoldersFromThings(outChildren, this.GetDirectlyHeldThings());
        }

        public ThingOwner GetDirectlyHeldThings()
        {
            return container;
        }
    }
}
*/