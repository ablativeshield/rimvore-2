﻿using Verse;
using System;
using System.Collections.Generic;
using System.Linq;
using RimWorld;
using UnityEngine;

namespace RimVore2
{
    public static class RV2_Common
    {
        public static readonly IEnumerable<PawnKindDef> AnimalRaces = DefDatabase<PawnKindDef>.AllDefsListForReading
            .FindAll(def => def.RaceProps?.Animal == true)
            .OrderBy(animal => animal.label);

        // Reflection to use the enum in ColonyRelation.cs
        public static readonly IEnumerable<string> RelationKinds = Enum.GetValues(typeof(RelationKind))
            .Cast<RelationKind>()
            .Select(x => x.ToString());

        public static readonly List<VoreTypeDef> VoreTypes = DefDatabase<VoreTypeDef>.AllDefsListForReading;
        public static readonly List<VoreGoalDef> VoreGoals = DefDatabase<VoreGoalDef>.AllDefsListForReading;
        public static readonly List<VorePathDef> VorePaths = DefDatabase<VorePathDef>.AllDefsListForReading;

        public static readonly List<DesignationDef> VoreDesignations = DefDatabase<DesignationDef>.AllDefsListForReading;

        public static readonly List<SoundDef> VoreSounds = DefDatabase<SoundDef>.AllDefsListForReading.FindAll(def => def.defName.StartsWith("RV2_"));

        public static readonly ThingDef VoreContainerNone = DefDatabase<ThingDef>.GetNamed("RV2_Container_None");

        public static readonly HediffDef DigestionBookmarkHediff = HediffDef.Named("RV2_DigestionRecovery");

        public static readonly List<AliasDef> AliasDefs = DefDatabase<AliasDef>.AllDefsListForReading;

        public static readonly List<JobDef> VoreInitiatingJobs = new List<JobDef>()
        {
            VoreJobDefOf.RV2_VoreInitAsPredator,
            VoreJobDefOf.RV2_VoreInitAsPrey,
            VoreJobDefOf.RV2_VoreInitAsFeeder
        };

        public static readonly MentalStateDef VoreFighting_Attacker = DefDatabase<MentalStateDef>.GetNamed("RV2_VoreFighting_Attacker");
        public static readonly MentalStateDef VoreFighting_Defender = DefDatabase<MentalStateDef>.GetNamed("RV2_VoreFighting_Defender");

        public static readonly List<QuirkPoolDef> SortedQuirkPools = DefDatabase<QuirkPoolDef>.AllDefsListForReading.OrderBy(pool => pool.generationOrder).ToList();

        public static RecordDef predatorRecordDef = DefDatabase<RecordDef>.GetNamed("RV2_Predator");
        public static RecordDef preyRecordDef = DefDatabase<RecordDef>.GetNamed("RV2_Prey");

        public static readonly List<TemporaryQuirkGiver> TemporaryQuirkGivers = DefDatabase<TemporaryQuirkGiver>.AllDefsListForReading;
        public static List<TemporaryQuirkGiver> TemporaryQuirkGiversForKeyword(string keyword) => TemporaryQuirkGivers?.FindAll(giver => giver.criteriaKeywords?.Contains(keyword) == true);

        public static Dictionary<QuirkRarity, Color> QuirkRarityColors = new Dictionary<QuirkRarity, Color>()
        {
            { QuirkRarity.Guaranteed, Color.white },
            { QuirkRarity.Abundant, Color.white },
            { QuirkRarity.Common, Color.white },
            { QuirkRarity.Uncommon, Color.green },
            { QuirkRarity.Rare, Color.magenta },
            { QuirkRarity.VeryRare, Color.red }
        };

        public static List<RelationKind> relationKindsToNeverStrip = new List<RelationKind>()
        {
            RelationKind.Trader,
            RelationKind.Visitor
        };

        public const float VoreStorageCapacityToBeConsideredInfinite = 50f;

        private static List<VoreTargetSelectorRequest> voreCombinationsRequiringEnablers;
        public static List<VoreTargetSelectorRequest> VoreCombinationsRequiringEnablers
        {
            get
            {
                if(voreCombinationsRequiringEnablers == null)
                {
                    voreCombinationsRequiringEnablers = DefDatabase<QuirkDef>.AllDefsListForReading    // get all quirks
                        .Where(quirk => quirk.comps != null)    // only pick quirks with comps
                        .SelectMany(quirk => quirk.comps)   // get all comps of all quirks
                        .Where(comp => comp is QuirkComp_VoreEnabler)   // limit to PreyEnabler comps
                        .Cast<QuirkComp_VoreEnabler>()  // cast to special comp
                        .Select(comp => comp.selector)  // get the race type that is being enabled
                        .ToList();  // finally cast to list
                }
                return voreCombinationsRequiringEnablers;
            }
        }
    }

    [DefOf]
    public static class VoreJobDefOf
    {
        static VoreJobDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(VoreJobDefOf));
        }
        public static JobDef RV2_VoreInitAsPredator;
        public static JobDef RV2_VoreInitAsPrey;
        public static JobDef RV2_VoreInitAsFeeder;
        public static JobDef RV2_EjectPreySelf;
        public static JobDef RV2_EjectPreyForce;
        public static JobDef RV2_HuntVorePrey;
        public static JobDef RV2_KidnapVorePrey;
        public static JobDef RV2_ProposeVore;
        public static JobDef RV2_ReleasePrey;
    }

    [DefOf]
    public static class VoreGoalDefOf
    {
        static VoreGoalDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(VoreGoalDefOf));
        }

        public static VoreGoalDef Digest;
        public static VoreGoalDef Pleasure;
        public static VoreGoalDef Store;
        public static VoreGoalDef Heal;
    }

#if v1_2
#else
    [DefOf]
    public static class VoreHistoryDefOf
    {
        static VoreHistoryDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(HistoryEventDefOf));
        }

        public static HistoryEventDef RV2_DigestedMember;
    }
#endif
    [DefOf]
    public static class DesignationDefOf
    {
        static DesignationDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(DesignationDefOf));
        }

        public static DesignationDef predator;
        public static DesignationDef fatal;
        public static DesignationDef endo;
    }

    [DefOf]
    public static class QuirkDefOf
    {
        static QuirkDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(QuirkDefOf));
        }

        public static QuirkDef Enablers_Core_Type_Oral;
        public static QuirkDef Enablers_Core_Goal_Longendo;
        public static QuirkDef Cheat_InstantSwallow;
        public static QuirkDef Enablers_Core_Goal_Digest;
    }

    [DefOf]
    public static class VoreThoughtDefOf
    {
        static VoreThoughtDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(VoreThoughtDefOf));
        }

        public static ThoughtDef RV2_DeniedVoreProposal_Social;
        public static ThoughtDef RV2_DeniedVoreProposalObsessed_Social;
        public static ThoughtDef RV2_DeniedVoreProposal_Mood;
        public static ThoughtDef RV2_DeniedVoreProposalObsessed_Mood;
        public static ThoughtDef RV2_FatallyVoredMemory_Social;
        public static ThoughtDef RV2_FatallyVoredMemory_Rival_Social;
        public static ThoughtDef RV2_FatallyVoredMemory_Mood;
        public static ThoughtDef RV2_FatallyVoredMemory_Rival_Mood;
    }

    [DefOf]
    public static class VoreInteractionDefOf
    {
        static VoreInteractionDefOf()
        {
            DefOfHelper.EnsureInitializedInCtor(typeof(VoreInteractionDefOf));
        }

        public static InteractionDef RV2_FatalVore;
        public static InteractionDef RV2_EndoVore;
    }
}