﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2.DebugTools
{
    public static class Reset
    {
        [DebugAction("RimVore-2", "Reset RV2 PawnData", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void HardReset()
        {
            SaveStorage.DataStore.HardReset();
        }

        [DebugAction("RimVore-2", "Add Nabbers Preset To Rules", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void AddNabbersPreset()
        {
            string presetName = "Nabbers' Recommendation";
            if (RV2Mod.settings.rules.Presets.ContainsKey(presetName))
            {
                RV2Mod.settings.rules.Presets.Remove(presetName);
            }
            RV2Mod.settings.rules.Presets.Add(RulePresets.NabbersChoice());
        }

        [DebugAction("RimVore-2", "Resync All Vore Hediffs", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void ResyncAllHediffs()
        {
            IEnumerable<Pawn> pawns = Find.CurrentMap?.mapPawns?.AllPawnsSpawned;
            if (pawns.EnumerableNullOrEmpty())
            {
                return;
            }
            foreach (Pawn pawn in pawns)
            {
                RV2Log.Message("Debug resync for pawn " + pawn.LabelShort, true);
                pawn.GetVoreTracker()?.SynchronizeHediffs();
            }
        }

        [DebugAction("RimVore-2", "Nuke RV-2 Settings", actionType = DebugActionType.Action, allowedGameStates = AllowedGameStates.Playing)]
        public static void NukeSettings()
        {
            SettingsUpdater.NukeSettings();
        }
    }
}
