﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;

namespace RimVore2
{
    public static class Debug_Eject
    {
        [DebugAction("RimVore-2", "Emergency eject", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void EmergencyEject(Pawn p)
        {
            VoreTracker tracker = SaveStorage.DataStore?.GetPawnData(p)?.VoreTracker;
            if (tracker == null || tracker.VoreTrackerRecords.Count <= 0)
            {
                return;
            }
            List<FloatMenuOption> options = tracker.VoreTrackerRecords.ConvertAll(record => new FloatMenuOption("Eject " + record.Prey.Label, () => tracker.EmergencyEject(record)));
            Find.WindowStack.Add(new FloatMenu(options));
        }

        [DebugAction("RimVore-2", "Emergency eject all", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void EmergencyEjectAll(Pawn p)
        {
            VoreTracker tracker = SaveStorage.DataStore?.GetPawnData(p)?.VoreTracker;
            if (tracker == null || tracker.VoreTrackerRecords.Count <= 0)
            {
                return;
            }
            tracker.EmergencyEjectAll();
        }
    }
}
