﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HarmonyLib;
using Verse;
using RimWorld;

namespace RimVore2
{
    [HarmonyPatch(typeof(FoodUtility), "ThoughtsFromIngesting")]
    public static class RV2_Patch_FoodUtility
    {
        [HarmonyPostfix]
#if v1_2
        public static void InterceptSpecialFoodThoughts(Pawn ingester, Thing foodSource, ThingDef foodDef, ref List<ThoughtDef> __result)
        {
            List<ThoughtDef> backupResult = __result;
#else
        public static void InterceptSpecialFoodThoughts(Pawn ingester, Thing foodSource, ThingDef foodDef, ref List<FoodUtility.ThoughtFromIngesting> __result)
        {
            List<FoodUtility.ThoughtFromIngesting> backupResult = __result;
#endif
            try
            {
                if(__result == null)
                {
                    return;
                }
                
                // in order to have shared code between 1.2 and 1.3 this is one of the only times you will see 'var' in RV2.
                foreach(var thoughtOrWrapper in __result.ToList())
                {
#if v1_2
                    ThoughtDef thought = thoughtOrWrapper;
#else            
                    ThoughtDef thought = thoughtOrWrapper.thought;
#endif
                    ThoughtDef overrideThought = ingester?.QuirkManager(false)?.GetOverriddenThought(thought);
                    if(overrideThought == null)
                    {
                        continue;
                    }
                    if(overrideThought != thought)
                    {
                        __result.Remove(thoughtOrWrapper);
#if v1_2
                        ThoughtDef newThought = overrideThought;
#else
                        FoodUtility.ThoughtFromIngesting newThought = new FoodUtility.ThoughtFromIngesting()
                        {
                            fromPrecept = thoughtOrWrapper.fromPrecept,
                            thought = overrideThought
                        };
#endif
                        __result.Add(newThought);
                    }
                }
                // Log.Message("thoughts after overrides: " + string.Join(", ", __result.ConvertAll(thought => thought.defName)));
            }
            catch (Exception e)
            {
                Log.Warning("RimVore-2: Something went wrong when intercepting thoughts from ingesting food, Error:\n" + e);
                __result = backupResult;
            }
        }
    }
}