﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;

namespace RimVore2
{
    [HarmonyPatch(typeof(PawnUtility), "TrySpawnHatchedOrBornPawn")]
    public class Patch_PawnUtility
    {
        [HarmonyPostfix]
        private static void InterceptBirthOfVoredPawn(ref bool __result, ref Pawn pawn, ref Thing motherOrEgg)
        {
            try
            {
                if (!__result)
                {
                    return;
                }
                if(!(motherOrEgg is Pawn))
                {
                    return;
                }
                // GetValidAge will return -1 if no age check exists. In case an age check exists, we don't intercept newborns!
                if(RV2Mod.settings.rules.GetValidAge(pawn, IdentifierRole.Both) > -1)
                {
                    return;
                }
                Pawn parent = (Pawn)motherOrEgg;
                VoreTrackerRecord record = parent.GetVoreRecord();
                if(record == null)
                {
                    return;
                }
                VoreTracker tracker = record.Predator.GetVoreTracker();
                tracker.SplitOffNewVore(record, pawn, null, record.VorePathIndex);
                return;
            }
            catch(Exception e)
            {
                Log.Warning("RimVore-2: Something went wrong when attempting to intercept birth of a pawn: " + e);
                return;
            }
        }

    }
}