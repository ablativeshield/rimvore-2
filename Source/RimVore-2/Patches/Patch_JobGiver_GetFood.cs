﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;
using HarmonyLib;
using Verse.AI;

namespace RimVore2
{
    [HarmonyPatch(typeof(JobGiver_GetFood), "TryGiveJob")]
    public static class Patch_JobGiver_GetFood
    {
        [HarmonyPostfix]
        private static void InterceptPredatorHunting(ref Job __result, Pawn pawn)
        {
            Job backupResult = __result;
            try
            {
                Job job = __result;
                if(job == null)
                {
                    // no idea why this would happen
                    return;
                }
                if (job.def != JobDefOf.PredatorHunt)
                {
                    // if the created job isn't a hunting animal, we don't care to replace it
                    return;
                }
                if (job.targetA == null)
                {
                    // no idea why this sometimes happens on a hunting job that should always require a target...
                    return;
                }
                Pawn predator = pawn;
                Pawn prey = __result.targetA.Pawn;
                
                if (!predator.CanFatalVore(prey, out string reason))
                {
                    RV2Log.Message("Would have replaced hunting job with vore job, but predator " + predator.LabelShort + " can't vore: " + reason, "Jobs");
                    return;
                }
                List<VoreGoalDef> goalWhitelist = new List<VoreGoalDef>()
                {
                    VoreGoalDefOf.Digest
                };
                VoreInteraction interaction = new VoreInteraction(predator, prey, null, VoreRole.Predator, true, null, null, null, goalWhitelist);
                RV2Log.Message(interaction.ToString(), true, false, "Jobs");
                IEnumerable<VorePathDef> validPaths = interaction.ValidPaths();
                
                if (validPaths.EnumerableNullOrEmpty())
                {
                    RV2Log.Message("Would have replaced hunting job with vore job, but predator " + predator.LabelShort + " doesn't have any valid paths for digest vore that would feed them", "Jobs");
                    return;
                }
                if (RandomUtility.RollSuccess(RV2Mod.settings.fineTuning.HuntingAnimalsVoreChance))
                {
                    VorePathDef pathDef = validPaths.RandomElement();
                    RV2Log.Message("Replacing hunting job, picked " + pathDef.label + " as path for vore", true, false, "Jobs");
                    VoreJob jobReplacement = VoreJobMaker.MakeJob(VoreJobDefOf.RV2_HuntVorePrey, job.targetA, job.targetB, job.targetC);
                    jobReplacement.VorePath = new VorePath(pathDef);
                    jobReplacement.killIncappedTarget = false;
                    jobReplacement.Initiator = pawn;
                    __result = jobReplacement;
                    return;
                }
            }
            catch (Exception e)
            {
                Log.Warning("RimVore-2: Something went wrong when trying to intercept the predator hunting job: " + e);
            }
        }
    }
}