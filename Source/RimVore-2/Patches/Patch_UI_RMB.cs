﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;
using Verse.AI;


/// <summary>
/// I am constantly unhappy with how the RMB menu is built. The current approach tries to prevent empty float menus and has the invalid options properly baked in
/// Most menu functions return a true if they offer any options, which is checked in their calling function - this should make the entire system more maintaineable, but I am not sure
/// I will happily accept any suggestions for a more "robust" system that is equally variable
/// </summary>
namespace RimVore2
{
    [HarmonyPatch(typeof(FloatMenuMakerMap), "AddHumanlikeOrders")]
    static class RV2_Patch_UI_RMB_AddHumanlikeOrders
    {
        private static readonly TargetingParameters voreTargetParameters = new TargetingParameters()
        {
            canTargetPawns = true,
            canTargetBuildings = false,
            canTargetItems = true,
            canTargetAnimals = true,
            mapObjectTargetsMustBeAutoAttackable = true
        };

        [HarmonyPostfix]
        private static void AddVoreOptions(Vector3 clickPos, Pawn pawn, List<FloatMenuOption> opts)
        {
            List<FloatMenuOption> backupResult = opts;
            try
            {
                if (!RV2Mod.settings.features.ShowRMBVoreMenu)
                {
                    return;
                }
                if (pawn.jobs == null)
                {
                    return;
                }
                List<FloatMenuOption> voreOptions = new List<FloatMenuOption>();

#if v1_2
                List<LocalTargetInfo> validTargets = GenUI.TargetsAt_NewTemp(clickPos, voreTargetParameters).ToList();
#else
                List<LocalTargetInfo> validTargets = GenUI.TargetsAt(clickPos, voreTargetParameters).ToList();
#endif
                validTargets = validTargets
                    .FindAll(target => target.Pawn != null
                    && !target.Pawn.IsBurning()
                    && !target.Pawn.HostileTo(Faction.OfPlayer) || target.Pawn.Downed
                    && !target.Pawn.InMentalState);

                if (!validTargets.NullOrEmpty())
                {
                    foreach (LocalTargetInfo target in validTargets)
                    {
                        if (target.Pawn == pawn)
                        {
                            if (DoSelfOptions(pawn, out List<FloatMenuOption> optionsToAdd))
                            {
                                voreOptions.AddRange(optionsToAdd);
                            }
                        }
                        else if (pawn.CanReach(target, PathEndMode.ClosestTouch, Danger.Deadly))
                        {
                            if (DoTargetOptions(pawn, target.Pawn, out List<FloatMenuOption> optionsToAdd))
                            {
                                voreOptions.AddRange(optionsToAdd);
                            }
                        }
                    }
                }

                if (!voreOptions.NullOrEmpty())
                {
                    FloatMenuOption voreOption = new FloatMenuOption("RV2_RMB_Home".Translate(), () => Find.WindowStack.Add(new FloatMenu(voreOptions)));
                    
                    opts.Add(voreOption);
                }
            }
            catch (Exception e)
            {
                Log.Error("Something went wrong when RimVore-2 tried to add vore options to RMB:\n" + e);
                opts = backupResult;
            }
        }

        private static bool DoSelfOptions(Pawn pawn, out List<FloatMenuOption> options)
        {
            options = new List<FloatMenuOption>();
            FloatMenuOption option;
            if (DoEjectOption(pawn, pawn, out option))
            {
                options.Add(option);
            }
            if (DoReserveOption(pawn, out option))
            {
                options.Add(option);
            }
            if (DoManualPassConditionOption(pawn, out option))
            {
                options.Add(option);
            }
            if(DoStageJumpOption(pawn, out option))
            {
                options.Add(option);
            }
            return !options.NullOrEmpty();
        }

        private static bool DoTargetOptions(Pawn initiator, Pawn target, out List<FloatMenuOption> options)
        {
            options = new List<FloatMenuOption>();
            FloatMenuOption option;
            List<FloatMenuOption> optionsToAdd;
            if (DoReserveOption(target, out option))
            {
                options.Add(option);
            }
            if (DoEjectOption(initiator, target, out option))
            {
                options.Add(option);
            }
            if (DoVoreProposalOptions(initiator, target, out optionsToAdd))
            {
                options.AddRange(optionsToAdd);
            }
            if (DoDirectVoreOptions(initiator, target, out optionsToAdd))
            {
                options.AddRange(optionsToAdd);
            }
            if (DoFeederVoreOption(initiator, target, out option))
            {
                options.Add(option);
            }
            if (options.NullOrEmpty())
            {
                return false;
            }
            return true;
        }

        private static bool DoEjectOption(Pawn initiator, Pawn target, out FloatMenuOption option)
        {
            option = null;
            bool isForSelf = initiator == target;
            if (DoEjectOptions(initiator, target, out List<FloatMenuOption> options))
            {
                string optionLabel = isForSelf ? "RV2_RMB_EjectPrey_Self".Translate() : "RV2_RMB_EjectPrey_Target".Translate(target.LabelShort);
                option = new FloatMenuOption(optionLabel, () => Find.WindowStack.Add(new FloatMenu(options)));
                return true;
            }
            return false;
        }

        private static bool DoEjectOptions(Pawn initiator, Pawn predator, out List<FloatMenuOption> options)
        {
            bool isForSelf = initiator == predator;
            options = new List<FloatMenuOption>();
            FloatMenuOption option;
            // get the tracker for the predator, then limit the eject options to valid targets
            List<VoreTrackerRecord> records = predator.GetVoreTracker()?.VoreTrackerRecords?.FindAll(r => r.CanEject);
            // no releasable prey exists
            if (records.NullOrEmpty())
            {
                return false;
            }
            foreach (VoreTrackerRecord record in records)
            {
                Pawn prey = record.Prey;
                string optionLabel = record.DisplayLabel;

                option = FloatMenuUtility.DecoratePrioritizedTask(new FloatMenuOption(optionLabel, delegate ()
                {
                    Job ejectJob;
                    if (isForSelf)
                    {
                        ejectJob = JobMaker.MakeJob(VoreJobDefOf.RV2_EjectPreySelf, prey);
                    }
                    else
                    {
                        ejectJob = JobMaker.MakeJob(VoreJobDefOf.RV2_EjectPreyForce, predator, prey);
                    }
                    initiator.jobs.TryTakeOrderedJob(ejectJob);
                }), initiator, predator);
                options.Add(option);
            }

            return true;
        }

        private static bool DoReserveOption(Pawn pawn, out FloatMenuOption option)
        {
            option = null;
            if (!RV2Mod.settings.features.FeederVoreEnabled)
            {
                return false;
            }
            bool isAlreadyReserved = ReservedPreyUtility.IsReserved(pawn);
            bool canBeReservedPrey = ReservedPreyUtility.CanReserve(pawn, out string reason);
            string optionLabel = isAlreadyReserved ? "RV2_RMB_UnReserve".Translate(pawn.LabelShort) : "RV2_RMB_Reserve".Translate(pawn.LabelShort);
            if (canBeReservedPrey)
            {
                option = new FloatMenuOption(optionLabel, () => ReservedPreyUtility.Reserve(pawn));
                return true;
            }
            else if (isAlreadyReserved)
            {
                option = new FloatMenuOption(optionLabel, () => ReservedPreyUtility.UnReserve(pawn));
                return true;
            }
            else
            {
                optionLabel += " (" + reason + ")";
                option = new FloatMenuOption(optionLabel, () => { })
                {
                    Disabled = true
                };
                return true;
            }
        }

        private static bool DoManualPassConditionOption(Pawn pawn, out FloatMenuOption option)
        {
            option = null;
            VoreTracker tracker = SaveStorage.DataStore.GetPawnData(pawn)?.VoreTracker;
            if (tracker == null || !tracker.IsTrackingVore)
            {
                return false;
            }
            List<FloatMenuOption> options = new List<FloatMenuOption>();
            foreach (VoreTrackerRecord record in tracker.VoreTrackerRecords)
            {
                bool recordNeedsManualPass = record.IsManuallyPassed == false   // don't allow option if record is already manually passed
                    && record.CurrentVoreStage.def.passConditions   // take all pass conditions of the current stage
                        .Any(condition => condition is StagePassCondition_Manual);  // and check if any of them is a manual pass

                if (recordNeedsManualPass)
                {
                    options.Add(new FloatMenuOption(record.Prey.LabelShort, () => record.IsManuallyPassed = true));
                }
            }

            if (options.Count > 0)
            {
                option = new FloatMenuOption("RV2_RMB_ManualPass".Translate(), () => Find.WindowStack.Add(new FloatMenu(options)));
            }
            return option != null;
        }

        private static bool DoStageJumpOption(Pawn pawn, out FloatMenuOption option)
        {
            option = null;
            VoreTracker tracker = SaveStorage.DataStore.GetPawnData(pawn)?.VoreTracker;
            if (tracker == null || !tracker.IsTrackingVore)
            {
                return false;
            }
            List<FloatMenuOption> options = new List<FloatMenuOption>();
            foreach (VoreTrackerRecord record in tracker.VoreTrackerRecords)
            {
                string jumpKey = record.CurrentVoreStage.def.jumpKey;
                if(jumpKey == null) // stage has no jump capability, skip
                {
                    continue;
                }
                IEnumerable<VoreJump> jumps = JumpUtility.Jumps(record.Predator, record.Prey, jumpKey);
                if (jumps.EnumerableNullOrEmpty())  // no jump paths valid, skip
                {
                    continue;
                }
                List<FloatMenuOption> jumpOptions = jumps
                    .Where(jump => jump.path != record.VorePath.def // don't present the already running vore as an option
                    && jump.path.voreType == record.VorePath.VoreType)  // limit to paths that have the same type (prevents crossing oral -> anal for stomach e.g.)
                    .Select(jump => new FloatMenuOption(jump.path.voreGoal.label, () => jump.Jump(record)))
                    .ToList();

                options.Add(new FloatMenuOption(record.DisplayLabel, () => Find.WindowStack.Add(new FloatMenu(jumpOptions))));
            }

            if (options.Count > 0)
            {
                option = new FloatMenuOption("RV2_RMB_JumpGoal".Translate(), () => Find.WindowStack.Add(new FloatMenu(options)));
            }
            return option != null;
        }

        private static bool DoDirectVoreOptions(Pawn initiator, Pawn target, out List<FloatMenuOption> options)
        {
            options = new List<FloatMenuOption>();
            FloatMenuOption option;
            if (!Prefs.DevMode)
            {
                return false;
            }
            VoreInteractionManager.Update(initiator, target);
            VoreInteraction initiatorAsPrey = VoreInteractionManager.InitiatorAsPrey;
            VoreInteraction initiatorAsPredator = VoreInteractionManager.InitiatorAsPredator;
            if (DoVoreAsPredatorOption(initiator, target, initiatorAsPredator, out option))
            {
                options.Add(option);
            }
            if (DoVoreAsPreyOption(initiator, target, initiatorAsPrey, out option))
            {
                options.Add(option);
            }

            return !options.NullOrEmpty();
        }

        private static bool DoVoreProposalOptions(Pawn initiator, Pawn target, out List<FloatMenuOption> options)
        {
            options = new List<FloatMenuOption>();

            FloatMenuOption option;

            VoreInteractionManager.Update(initiator, target);
            VoreInteraction initiatorAsPrey = VoreInteractionManager.InitiatorAsPrey;
            VoreInteraction initiatorAsPredator = VoreInteractionManager.InitiatorAsPredator;

            if (DoVoreAsPredatorOption(initiator, target, initiatorAsPredator, out option, true))
            {
                options.Add(option);
            }
            if (DoVoreAsPreyOption(initiator, target, initiatorAsPrey, out option, true))
            {
                options.Add(option);
            }

            return !options.NullOrEmpty();
        }

        private static bool DoVoreAsPredatorOption(Pawn initiator, Pawn target, VoreInteraction interaction, out FloatMenuOption option, bool isForProposal = false)
        {
            option = null;
            Pawn prey = interaction.Prey;
            string optionLabel = isForProposal ? "RV2_RMB_ProposeAsPredator" : "RV2_RMB_InitiateAsPredator";
            optionLabel = optionLabel.Translate(prey.LabelShort);
            if (interaction.CanCalculate(out string reason))
            {
                JobDef jobDef = isForProposal ? VoreJobDefOf.RV2_ProposeVore : VoreJobDefOf.RV2_VoreInitAsPredator;
                Func<VorePath, VoreJob> jobMaker = delegate (VorePath path)
                {
                    VoreJob job = VoreJobMaker.MakeJob(jobDef, target);
                    job.targetA = target;
                    job.VorePath = path;
                    job.Initiator = initiator;
                    if (isForProposal)
                    {
                        job.Proposal = new VoreProposal(initiator, target, initiator, target, path.def);
                    }
                    return job;
                };
                if (DoVoreGoalOptions(initiator, target, interaction, jobMaker, out List<FloatMenuOption> options, isForProposal))
                {
                    option = new FloatMenuOption(optionLabel, () => Find.WindowStack.Add(new FloatMenu(options)));
                    return true;
                }
            }

            if (!RV2Mod.settings.fineTuning.ShowInvalidRMBOptions)
            {
                return false;
            }
            optionLabel += " (" + reason + ")";
            option = UIUtility.DisabledOption(optionLabel);
            return true;
        }

        private static bool DoVoreAsPreyOption(Pawn initiator, Pawn target, VoreInteraction interaction, out FloatMenuOption option, bool isForProposal = false)
        {
            option = null;
            Pawn predator = interaction.Predator;
            string optionLabel = isForProposal ? "RV2_RMB_ProposeAsPrey" : "RV2_RMB_InitiateAsPrey";
            optionLabel = optionLabel.Translate(predator.LabelShort);
            if (interaction.CanCalculate(out string reason))
            {
                JobDef jobDef = isForProposal ? VoreJobDefOf.RV2_ProposeVore : VoreJobDefOf.RV2_VoreInitAsPrey;
                Func<VorePath, VoreJob> jobMaker = delegate (VorePath path)
                {
                    VoreJob job = VoreJobMaker.MakeJob(jobDef, target);
                    job.targetA = target;
                    job.VorePath = path;
                    job.Initiator = initiator;
                    if (isForProposal)
                    {
                        job.Proposal = new VoreProposal(target, initiator, initiator, target, path.def);
                    }
                    return job;
                };
                if (DoVoreGoalOptions(initiator, target, interaction, jobMaker, out List<FloatMenuOption> options, isForProposal))
                {
                    option = new FloatMenuOption(optionLabel, () => Find.WindowStack.Add(new FloatMenu(options)));
                    return true;
                }
            }

            if (!RV2Mod.settings.fineTuning.ShowInvalidRMBOptions)
            {
                return false;
            }
            optionLabel += " (" + reason + ")";
            option = UIUtility.DisabledOption(optionLabel);
            return true;
        }

        private static bool DoVoreGoalOptions(Pawn initator, Pawn target, VoreInteraction interaction, Func<VorePath, VoreJob> jobMaker, out List<FloatMenuOption> options, bool isForProposal = false)
        {
            options = new List<FloatMenuOption>();
            foreach (KeyValuePair<VoreGoalDef, string> goalCheck in interaction.Goals)
            {
                VoreGoalDef goal = goalCheck.Key;
                string reason = goalCheck.Value;
                string optionLabel = goal.RMBMenuTranslationKey.Translate();
                // goal is valid
                if (reason == null)
                {
                    if (DoVoreTypeOptions(initator, target, interaction, jobMaker, goal, out List<FloatMenuOption> goalOptions, isForProposal))
                    {
                        options.Add(new FloatMenuOption(optionLabel, () => Find.WindowStack.Add(new FloatMenu(goalOptions))));
                    }
                }
                // goal is invalid
                else
                {
                    if (RV2Mod.settings.fineTuning.ShowInvalidRMBOptions)
                    {
                        optionLabel += " (" + reason + ")";
                        options.Add(UIUtility.DisabledOption(optionLabel));
                    }
                }
            }

            return !options.NullOrEmpty();
        }

        private static bool DoVoreTypeOptions(Pawn initiator, Pawn target, VoreInteraction interaction, Func<VorePath, VoreJob> jobMaker, VoreGoalDef goal, out List<FloatMenuOption> options, bool isForProposal = false)
        {
            options = new List<FloatMenuOption>();
            foreach (KeyValuePair<VoreTypeDef, string> typeCheck in interaction.TypesForGoal(goal))
            {
                VoreTypeDef type = typeCheck.Key;
                string reason = typeCheck.Value;
                string optionLabel = type.RMBMenuLabelTranslationKey.Translate();
                // type is valid
                if (reason == null)
                {
                    VorePath path = new VorePath(interaction.GetPathDef(type, goal));

                    VoreJob job = jobMaker(path);
                    job.targetA = target;
                    job.VorePath = path;
                    FloatMenuOption option = new FloatMenuOption(optionLabel, () => initiator.jobs.TryTakeOrderedJob(job));
                    // this will disable the option with the "reserved by" text if it can't be executed
                    option = FloatMenuUtility.DecoratePrioritizedTask(option, initiator, target);
                    options.Add(option);
                }
                // type is invalid
                else
                {
                    if (RV2Mod.settings.fineTuning.ShowInvalidRMBOptions)
                    {
                        optionLabel += " (" + reason + ")";
                        options.Add(UIUtility.DisabledOption(optionLabel));
                    }
                }
            }

            return !options.NullOrEmpty();
        }

        private static bool DoFeederVoreOption(Pawn initiator, Pawn target, out FloatMenuOption option)
        {
            option = null;
            string optionLabel = "RV2_RMB_AsFeeder".Translate(target.LabelShort);
            if (!RV2Mod.settings.features.FeederVoreEnabled)
            {
                return false;
            }
            if (ReservedPreyUtility.AnyPawnReservedInMap(initiator.Map))
            {
                if (DoSelectReservedPreyOptions(initiator, target, out List<FloatMenuOption> options))
                {
                    option = new FloatMenuOption(optionLabel, () => Find.WindowStack.Add(new FloatMenu(options)));
                    return true;
                }
            }
            else
            {
                optionLabel += " (" + "RV2_VoreInvalidReasons_NoReservedPrey".Translate() + ")";
                option = UIUtility.DisabledOption(optionLabel);
                return true;
            }
            return false;
        }

        private static bool DoSelectReservedPreyOptions(Pawn feeder, Pawn predator, out List<FloatMenuOption> options)
        {
            options = new List<FloatMenuOption>();
            List<Pawn> validReservedPrey = ReservedPreyUtility.ReservedPrey
                .FindAll(pawn => pawn != feeder)    // the feeder shouldn't feed themselves to the predator, that's what direct vore interactions are for
                .FindAll(pawn => pawn != predator); // the predator can't be fed to itself
            foreach (Pawn prey in validReservedPrey)
            {
                VoreInteraction interaction = new VoreInteraction(feeder, predator, prey, VoreRole.Feeder);
                string optionLabel = prey.LabelShort;
                if (interaction.CanCalculate(out string reason))
                {
                    JobDef jobDef = VoreJobDefOf.RV2_VoreInitAsFeeder;
                    Func<VorePath, VoreJob> jobMaker = delegate (VorePath path)
                    {
                        VoreJob job = VoreJobMaker.MakeJob(jobDef, prey, predator);
                        job.targetA = prey;
                        job.targetB = predator;
                        job.VorePath = path;
                        job.Initiator = feeder;
                        return job;
                    };
                    if (DoVoreGoalOptions(feeder, prey, interaction, jobMaker, out List<FloatMenuOption> goalOptions))
                    {
                        options.Add(new FloatMenuOption(optionLabel, () => Find.WindowStack.Add(new FloatMenu(goalOptions))));
                    }
                }
                else
                {
                    if (RV2Mod.settings.fineTuning.ShowInvalidRMBOptions)
                    {
                        optionLabel += " (" + reason + ")";
                        options.Add(UIUtility.DisabledOption(optionLabel));
                    }
                }

            }

            return !options.NullOrEmpty();
        }
    }
}