﻿using System;
using System.Collections.Generic;
using System.Linq;
using RimWorld;
using Verse;
using Verse.AI;

namespace RimVore2
{
    public static class Toil_Vore
    {
        public const int baseSwallowDuration = 600;

        public static Toil SwallowToil(Pawn predator, Pawn prey, TargetIndex targetIndex, int swallowDuration = baseSwallowDuration)
        {
            List<float> modifiers = new List<float>()
            {
                predator.GetSizeInRelationTo(prey),
                predator.health.capacities.GetLevel(PawnCapacityDefOf.Eating),
                RV2Mod.settings.cheats.VoreSpeedMultiplier
            };
            // remove 0 modifiers because they would cause a DivideByZero exception
            modifiers = modifiers
                .FindAll(mod => mod != 0f);
            if (!modifiers.NullOrEmpty())
            {
                RV2Log.Message("modifying " + swallowDuration + " with: " + string.Join(", ", modifiers), true, false, "Jobs");
                foreach (float modifier in modifiers)
                {
                    swallowDuration = (int)(swallowDuration / modifier);
                }
            }
            QuirkManager predatorQuirks = predator.QuirkManager(false);
            if (predatorQuirks != null)
            {
                swallowDuration = (int)predator.QuirkManager().ModifyValue("SwallowSpeed", swallowDuration);
            }

            // waiting for 0 ticks would throw an error, prevent by waiting for 1 tick at least
            swallowDuration = Math.Max(1, swallowDuration);
            Toil swallowToil = Toils_General.WaitWith(targetIndex, swallowDuration, true, true);
            swallowToil.socialMode = RandomSocialMode.Off;

            return swallowToil;
        }

        public static Toil VomitToil(Pawn predator, Pawn prey, int vomitDuration = baseSwallowDuration)
        {
            List<float> modifiers = new List<float>()
            {
                predator.GetSizeInRelationTo(prey),
                predator.health.capacities.GetLevel(PawnCapacityDefOf.Eating),
                RV2Mod.settings.cheats.VoreSpeedMultiplier
            };
            modifiers = modifiers.FindAll(mod => mod != 0f);
            if (!modifiers.NullOrEmpty())
            {
                RV2Log.Message("modifying " + vomitDuration + " with: " + string.Join(", ", modifiers), true, false, "Jobs");
                foreach (float modifier in modifiers)
                {
                    vomitDuration = (int)(vomitDuration / modifier);
                }
            }
            
            // waiting for 0 ticks would throw an error, prevent by waiting for 1 tick at least
            vomitDuration = Math.Max(1, vomitDuration);
            Toil vomitToil = Toils_General.Wait(vomitDuration);
            vomitToil.socialMode = RandomSocialMode.Off;

            return vomitToil;
        }
        public static Toil ExecutionToil(VoreJob voreJob, Pawn initiator, Pawn predator, Pawn prey, bool isForced = false)
        {
            return new Toil()
            {
                initAction = delegate ()
                {
                    RV2Log.Message("Execution Toil finish action called", "Jobs");
                    VoreTrackerRecord record = new VoreTrackerRecord()
                    {
                        Predator = predator,
                        Prey = prey,
                        ForcedBy = isForced ? initiator : null,
                        VorePath = voreJob.VorePath,
                        VorePathIndex = 0
                    };
                    PreVoreUtility.PopulateRecord(ref record);
                    predator.GetVoreTracker().TrackVore(record);

                    ForcedState forcedState = voreJob.GetForcedState(predator, prey, isForced);

                    if (VoreValidator.StripBeforeVore(predator, prey, forcedState))
                    {
                        prey.Strip();
                        record.PreyStartedNaked = true;
                    }
                },
                defaultCompleteMode = ToilCompleteMode.Instant
            };
        }

        public static Toil EjectToil(Pawn initiatorPawn, Pawn predatorPawn, Pawn ejectPawn, bool isInterrupted = false)
        {
            return new Toil()
            {
                initAction = delegate ()
                {
                    VoreTracker voreTracker = predatorPawn.GetVoreTracker();
                    VoreTrackerRecord record = ejectPawn.GetVoreRecord();
                    if (record != null)
                    {
                        record.IsInterrupted = isInterrupted;
                        voreTracker.Eject(record);
                    }
                    else
                    {
                        Log.Warning("RimVore-2: Tried to eject pawn, but could not find VoreTrackerRecord: " + ejectPawn.LabelShort);
                    }
                },
                defaultCompleteMode = ToilCompleteMode.Instant
            };
        }
    }
}