﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using RimWorld;

namespace RimVore2
{
    public class JobDriver_Vore_Init_AsPredator: JobDriver
    {
        readonly TargetIndex preyIndex = TargetIndex.A;

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            Pawn targetPawn = (Pawn)base.job.GetTarget(preyIndex);
            if (!pawn.HasFreeCapacityFor(targetPawn))
            {
                return false;
            }
            return base.pawn.Reserve(targetPawn, base.job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDespawnedOrNull(preyIndex);
            this.FailOnAggroMentalStateAndHostile(preyIndex);
            this.FailOnMentalState(preyIndex);
            this.FailOnBurningImmobile(preyIndex);
            this.FailOnDestroyedOrNull(preyIndex);

            Pawn predator = base.pawn;
            Pawn prey = (Pawn)TargetA;
            VoreJob voreJob = (VoreJob)base.job;
            voreJob.targetA = base.TargetA;

            this.FailOn(() => VoreValidator.CurrentlyInVoreInitiationJob(prey));

            RV2Log.Message("Job started with prey: " + prey.LabelShort + " and pred " + predator.LabelShort, "Jobs");

            yield return Toils_Goto.GotoThing(preyIndex, PathEndMode.Touch);

            yield return Toil_Vore.SwallowToil(predator, prey, preyIndex);
            yield return Toil_Vore.ExecutionToil(voreJob, predator, predator, prey, voreJob.IsForced);
        }
    }
}