﻿using System.Collections.Generic;
using Verse;
using Verse.AI;

namespace RimVore2
{
    class JobDriver_Vore_EjectPrey_Force : JobDriver
    {
        readonly TargetIndex targetIndex = TargetIndex.A;

        private Pawn ejectPawn;
        private Pawn EjectPawn
        {
            get
            {
                if(ejectPawn == null)
                {
                    ejectPawn = TargetB.Pawn;
                }
                if(ejectPawn == null)
                {
                    this.FailOn(() => true);
                }
                return ejectPawn;
            }
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            return base.pawn.Reserve(base.job.GetTarget(targetIndex), base.job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDespawnedOrNull(targetIndex);

            Pawn initiatorPawn = base.pawn;
            Pawn targetPawn = (Pawn)TargetA;    // TargetA will be scribed correctly, because they are spawned normally - if not, the FailOnDespawnedOrNull will take care of it

            RV2Log.Message("Job started with initiator: " + initiatorPawn.LabelShort + " and target " + targetPawn.LabelShort, "Jobs");


            yield return Toils_Goto.GotoThing(targetIndex, PathEndMode.Touch);

            // toil is named swallow, but fulfills the same purpose as un-swallowing - wait for default duration with target pawn
            yield return Toil_Vore.SwallowToil(targetPawn, ejectPawn, targetIndex);
            yield return Toil_Vore.EjectToil(initiatorPawn, targetPawn, EjectPawn, true);
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_References.Look(ref ejectPawn, "ejectPawn", true);
        }
    }
}
