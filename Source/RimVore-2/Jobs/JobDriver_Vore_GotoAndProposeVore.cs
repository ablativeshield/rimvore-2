﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using RimWorld;

namespace RimVore2
{
    public class JobDriver_Vore_GotoAndProposeVore : JobDriver
    {
        readonly TargetIndex targetIndex = TargetIndex.A;

        VoreJob voreJob;
        VoreJob VoreJob
        {
            get
            {
                if(voreJob == null)
                {
                    if(job is VoreJob parsedVoreJob)
                    {
                        voreJob = parsedVoreJob;
                        voreJob.targetA = this.TargetA;
                        voreJob.targetB = this.TargetB;
                        voreJob.targetC = this.TargetC;
                    }
                    else
                    {
                        throw new Exception("Job for JobDriver_Vore_GotoAndProposeVore is not VoreJob! Aborting!");
                    }
                }
                return voreJob;
            }
        }
        bool InitiatorIsPredator => VoreJob.Proposal.Predator == this.pawn;
        Pawn TargetPawn => this.job.GetTarget(targetIndex).Pawn;
        Pawn Predator => InitiatorIsPredator ? pawn : TargetPawn;
        Pawn Prey => InitiatorIsPredator ? TargetPawn : pawn;

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            if (InitiatorIsPredator)
            {
                if (!pawn.HasFreeCapacityFor(TargetPawn))
                {
                    return false;
                }
            }
            else
            {
                if (!TargetPawn.HasFreeCapacityFor(pawn))
                {
                    return false;
                }
            }
            return this.pawn.Reserve(TargetPawn, this.job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDespawnedOrNull(targetIndex);
            this.FailOnAggroMentalStateAndHostile(targetIndex);
            this.FailOnMentalState(targetIndex);
            this.FailOnBurningImmobile(targetIndex);
            this.FailOnDestroyedOrNull(targetIndex);

            yield return Toils_Goto.GotoThing(targetIndex, PathEndMode.Touch);
            yield return Toils_General.WaitWith(targetIndex, 600, true);
            Toil proposalToil = new Toil() {
                initAction = delegate ()
                {
                    bool proposalPassed = VoreJob.Proposal.TryProposal();
                    if (!proposalPassed)
                    {
                        AddEndCondition(() => JobCondition.Succeeded);
                    }
                }
            };
            proposalToil.socialMode = RandomSocialMode.SuperActive;
            yield return proposalToil;
            Toil voreToil = new Toil()
            {
                initAction = delegate ()
                {
                    JobDef nextJobDef = VoreJob.Proposal.Role(pawn).GetInitJobDefFor();
                    VoreJob nextJob = VoreJobMaker.MakeJob(nextJobDef, this.TargetA, this.TargetB, this.TargetC);
                    nextJob.VorePath = VoreJob.VorePath;
                    RV2Log.Message("Starting vore initiation job: " + nextJobDef.defName, true, false, "Jobs");
                    this.pawn.jobs.StartJob(nextJob, JobCondition.Succeeded);
                }
            };
            yield return voreToil;
            //yield return Toil_Vore.SwallowToil(Predator, Prey, targetIndex);
            //yield return Toil_Vore.ExecutionToil(VoreJob, Predator, Prey);
        }
    }
}
