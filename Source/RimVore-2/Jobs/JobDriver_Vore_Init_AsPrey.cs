﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using RimWorld;

namespace RimVore2
{
    public class JobDriver_Vore_Init_AsPrey: JobDriver
    {
        readonly TargetIndex predatorIndex = TargetIndex.A;

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            Pawn targetPawn = (Pawn)this.job.GetTarget(predatorIndex);
            if (!targetPawn.HasFreeCapacityFor(this.pawn))
            {
                return false;
            }
            return this.pawn.Reserve(targetPawn, this.job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDespawnedOrNull(predatorIndex);
            this.FailOnAggroMentalStateAndHostile(predatorIndex);
            this.FailOnMentalState(predatorIndex);
            this.FailOnBurningImmobile(predatorIndex);
            this.FailOnDestroyedOrNull(predatorIndex);

            Pawn predator = (Pawn)TargetA;
            Pawn prey = this.pawn;
            VoreJob voreJob = (VoreJob)this.job;
            voreJob.targetA = this.TargetA;

            this.FailOn(() => VoreValidator.CurrentlyInVoreInitiationJob(predator));

            RV2Log.Message("Job started with prey: " + prey.LabelShort + " and pred " + predator.LabelShort, "Jobs");


            yield return Toils_Goto.GotoThing(predatorIndex, PathEndMode.Touch);

            yield return Toil_Vore.SwallowToil(predator, prey, predatorIndex);
            yield return Toil_Vore.ExecutionToil(voreJob, prey, predator, prey, voreJob.IsForced);
        }
    }
}