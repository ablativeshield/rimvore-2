﻿using System;
using System.Collections.Generic;
using Verse;

namespace RimVore2
{
    public class InternalTemperatureExtension : DefModExtension
    {
        public float temperature = 36.5f;
    }
}
