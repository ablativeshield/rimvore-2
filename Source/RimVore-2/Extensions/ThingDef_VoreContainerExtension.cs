﻿using System;
using System.Collections.Generic;
using Verse;

namespace RimVore2
{
    public class VoreContainerExtension : DefModExtension
    {
        public List<ThingDef> forcedProducedThings;
        public bool isBones = false;
        public bool isScat = false;

        public bool IsValid()
        {
            bool valid = true;
            if (!RV2Mod.settings.features.BonesEnabled && isBones)
            {
                valid = false;
            }
            if (!RV2Mod.settings.features.ScatEnabled && isScat)
            {
                valid = false;
            }
            return valid;
        }

        public bool ProvidesForcedResource() => forcedProducedThings?.Count > 0 == true;
    }
}
