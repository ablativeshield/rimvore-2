﻿using RimWorld;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Verse;

namespace RimVore2
{
    public static class DigestionUtility
    {
        public static void FinishDigestion(VoreTrackerRecord record, DamageDef appliedDamageDef)
        {
            RV2Log.Message("FinishDigestion called");
            Pawn prey = record.Prey;
            record.MoveAllPreyItemsToContainer();
            record.VoreContainer.DestroyAllRottableItems();
            record.VoreContainer.DigestAllApparel(appliedDamageDef);
            record.VoreContainer.DigestAllImplants(prey, appliedDamageDef);
            DigestPawn(record, appliedDamageDef);
            List<Thing> madeThings = record.VorePath.def.voreProduct?.MakeVoreProducts(record.VoreContainer.GetDirectlyHeldThings(), record.VorePath.def, record.Predator);
            // add everything that was created (including the potential container) to the contained things again to excrete them
            if(madeThings != null)
            {
                record.VoreContainer.TryAddItems(madeThings);
            }
        }

        private static void DigestPawn(VoreTrackerRecord record, DamageDef appliedDamageDef)
        {
            Pawn prey = record.Prey;
            if (prey == null)
            {
                RV2Log.Warning("Can't finish prey, prey was null");
                return;
            }
            if (!prey.Dead)
            {
                KillPreyWithDamage(record, appliedDamageDef);
            }
            Corpse corpse = prey.Corpse;
            if (corpse == null)
            {
                RV2Log.Warning("Corpse was null! Skipping corpse processing");
                return;
            }
            ProcessCorpse(record, appliedDamageDef);
            VoreThoughtUtility.NotifyFatallyVored(record.Predator, record.Prey, record.VorePath.VoreGoal);
        }
        private static void ProcessCorpse(VoreTrackerRecord record, DamageDef appliedDamageDef)
        {
            Pawn prey = record.Prey;

            CorpseProcessingType type = RV2Mod.settings.rules.GetCorpseProcessingType(record.Predator, record.VorePath.def);
            // mechanoids get special treatment
            if (prey.IsMechanoid())
            {
                DigestMechanoid(record, appliedDamageDef);
                return;
            }
            if (RV2Mod.settings.fineTuning.ForbidDigestedCorpse)
            {
                prey.Corpse.SetForbidden(true);
            }
            switch (type)
            {
                case CorpseProcessingType.Fresh:
                    return;
                case CorpseProcessingType.Destroy:
                    if (!prey.Corpse.Destroyed) prey.Corpse.Destroy();
                    break;
                case CorpseProcessingType.Rot:
                    DecomposePawn(prey, 0.5f);
                    break;
                case CorpseProcessingType.Dessicate:
                    DecomposePawn(prey);
                    break;
                default:
                    Log.Error("No CorpseProcessingType support for " + type);
                    break;
            }
        }

        private static void DigestMechanoid(VoreTrackerRecord record, DamageDef damageDef)
        {
            Pawn predator = record.Predator;
            Pawn prey = record.Prey;
            QuirkManager predatorQuirks = predator.QuirkManager();
            if(predatorQuirks == null)
            {
                RV2Log.Warning("Pawn without quirks is trying to digest a mechanoid - only pawns with the appropriate quirks can do so!");
                return;
            }
            VoreTargetSelectorRequest request = new VoreTargetSelectorRequest(true)
            {
                voreGoal = VoreGoalDefOf.Digest,
                raceType = RaceType.Mechanoid
            };
            List<Thing> createdItems = new List<Thing>();
            int itemCount = (int)Mathf.Ceil(prey.BodySize);
            bool produceIntricateItems = predatorQuirks.HasSpecialFlag("NoTaintedApparel");
            // if we have a predator with the spotless digestion quirk, create steel and plasteel
            if (produceIntricateItems)
            {
                // base game returns  15 steel for size 1 (lancer, pikeman, scyther)
                // and 30 steel and 10 plasteel for size 1.8 (centipede)
                // to unify this system, just return 15 steel & 5 plasteel for each full size-point of the mech
                for (int i = 0; i < itemCount; i++)
                {
                    for (int j = 0; j < 15; j++)
                    {
                        createdItems.Add(ThingMaker.MakeThing(ThingDefOf.Steel));
                    }
                    for (int j = 0; j < 5; j++)
                    {
                        createdItems.Add(ThingMaker.MakeThing(ThingDefOf.Plasteel));
                    }
                }
            }
            // otherwise just produce metal slag based on body size
            else
            {
                for(int i = 0; i <= itemCount; i++)
                {
                    createdItems.Add(ThingMaker.MakeThing(ThingDefOf.ChunkSlagSteel));
                }
            }
            prey.Destroy();
            record.VoreContainer.TryAddItems(createdItems);
        }

        public static int GetQualityOrFallback(this Apparel apparel)
        {
            QualityCategory quality;
            bool success = apparel.TryGetQuality(out quality);
            if (success)
            {
                return (int)quality;
            }
            // use reflection to retrieve the amount of values for the QualityCategory enum, then subtract by 1 cause 0 indexed
            int maxQuality = System.Enum.GetValues(typeof(QualityCategory)).EnumerableCount() - 1;
            int fallbackQuality = maxQuality / 3;
            RV2Log.Message("Apparel had no QuirkQuality, returning " + fallbackQuality + " instead");
            return fallbackQuality;
        }

        private static void KillPreyWithDamage(VoreTrackerRecord record, DamageDef appliedDamageDef)
        {
            DoFactionImpact(record);
            Pawn predator = record.Predator;
            Pawn prey = record.Prey;
            DamageInfo dinfo = new DamageInfo(appliedDamageDef, 99999f, 99999f, -1, predator);
            dinfo.SetIgnoreInstantKillProtection(true);
            prey.Kill(dinfo);
            DoFactionImpact(record);
            // would have loved to just call the deathActionWorker.PawnDied() but it requires the corpse, which is currently despawned and the predator can not be used in-place
            bool preyExplodesOnDeath = prey.RaceProps.deathActionWorkerClass == typeof(DeathActionWorker_SmallExplosion) 
                                       || prey.RaceProps.deathActionWorkerClass == typeof(DeathActionWorker_BigExplosion);
            if (preyExplodesOnDeath)
            {
                // sadly base game explosions don't factor in pawn size at all and they are all either Big or Small
                float explosionSize = prey.BodySize * 2;
                GenExplosion.DoExplosion(predator.Position, predator.Map, explosionSize, DamageDefOf.Flame, predator);
            }
        }

        private static void DoFactionImpact(VoreTrackerRecord record)
        {
            // if vore was willing, no faction impact)
            if(record.ForcedBy == null)
            {
                return;
            }
            Pawn prey = record.Prey;
#if v1_2
#else
            // guilty pawns can be fatal vored without repercussions
            if (prey.guilt?.IsGuilty == true)
            {
                return;
            }
#endif
            Faction preyFaction = prey.Faction;
            if(preyFaction == null)
            {
                return;
            }
            if(preyFaction != Faction.OfPlayer)
            {
#if v1_2
                preyFaction.TrySetRelationKind(Faction.OfPlayer, FactionRelationKind.Hostile, true, null, new RimWorld.Planet.GlobalTargetInfo(record.Predator));
#else
                Faction.OfPlayer.TryAffectGoodwillWith(preyFaction, Faction.OfPlayer.GoodwillToMakeHostile(preyFaction), true, !preyFaction.temporary, VoreHistoryDefOf.RV2_DigestedMember, null);
#endif
            }
        }

        private static void DecomposePawn(Pawn pawn, float rotModifier = 1f)
        {
            Corpse corpse = pawn.Corpse;
            CompRottable rotComp = corpse?.GetComp<CompRottable>();
            if (rotComp == null)
            {
                RV2Log.Warning("No CompRottable found for corpse. Skipping decomposition.");
                return;
            }
            rotComp.RotProgress = rotComp.PropsRot.TicksToDessicated * rotModifier;
        }

        public static void ApplyDigestionBookmark(VoreTrackerRecord record)
        {
            float progress = record.PassValues.TryGetValue("DigestionProgress");
            if(progress <= 0)
            {
                return;
            }
            Pawn prey = record.Prey;
            if(prey.health?.hediffSet?.hediffs == null)
            {
                return;
            }
            HediffDef bookmarkDef = RV2_Common.DigestionBookmarkHediff;
            // pawn might have hediff already
            Hediff existingHediff = prey.health.hediffSet.hediffs.Find(hediff => hediff.def == bookmarkDef);
            // if the pawn doesn't have the hediff yet, add it
            if (existingHediff == null)
            {
                existingHediff = prey.health.AddHediff(bookmarkDef);
            }
            RV2Log.Message("Digestion recovery hediff was at value " + existingHediff.Severity + " -> adding " + progress, true);
            existingHediff.Severity += progress;
        }

        public static float GetPreviousDigestionProgress(Pawn pawn)
        {
            Hediff bookmarkHediff = pawn.health?.hediffSet?.hediffs?
                .Find(hediff => hediff.def == RV2_Common.DigestionBookmarkHediff);
            if(bookmarkHediff == null)
            {
                return 0f;
            }
            return bookmarkHediff.Severity;
        }
    }
}