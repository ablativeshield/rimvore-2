﻿using System;
using System.Collections.Generic;
using RimWorld;
using Verse;

namespace RimVore2
{
    public static class PostVoreUtility
    {
        public static void ResolveVore(VoreTrackerRecord record)
        {
            List<string> keywords = record.RecordKeywords();
            record.CurrentVoreStage.End(record);
            ApplyPostVore(record, keywords, true);
            ApplyPostVore(record, keywords, false);
            record.IsFinished = true;
        }

        private static void ApplyPostVore(VoreTrackerRecord record, List<string> originalKeywords, bool isForPredator)
        {
            // do not modify referenced list, otherwise predator keywods will show up in prey keywords
            List<string> keywords = new List<string>(originalKeywords);
            // Log.Message("Calculated keywords: " + string.Join(", ", keywords));
            Pawn pawn;
            Pawn otherPawn;
            if (isForPredator)
            {
                RV2Log.Message("Applying post vore to predator.");
                keywords.Add("PawnIsPredator");
                pawn = record.Predator;
                otherPawn = record.Prey;
            }
            else
            {
                RV2Log.Message("Applying post vore to prey.");
                keywords.Add("PawnIsPrey");
                pawn = record.Prey;
                otherPawn = record.Predator;
            }
            if(pawn == null)
            {
                Log.Error("PAWN IS NULL");
                return;
            }
            if (pawn.Dead)
            {
                RV2Log.Message("Pawn dead, skipping post vore.");
                // if the pawn died, remove their "ReservedPrey" status if they had one
                ReservedPreyUtility.UnReserve(pawn, false);
                return;
            }
            // call the pawn based post-vore
            if (isForPredator)
            {
                ApplyPredatorPostVore(record);
            }
            else
            {
                ApplyPreyPostVore(record);
            }
            bool pawnHasThoughts = pawn.needs.mood != null;
            // only apply thoughts if the pawn can take them and the vore wasn't interrupted
            if (pawnHasThoughts && !record.IsInterrupted)
            {
                QuirkManager pawnQuirks = pawn.QuirkManager();
                if(pawnQuirks != null)
                {
                    IEnumerable<ThoughtDef> memories = pawnQuirks.GetPostVoreMemories(keywords);
                    // Log.Message("Calculated comp memories: " + string.Join(", ", memories.ConvertAll(memory => memory.defName)));
                    if (memories != null)
                    {
                        foreach (ThoughtDef memory in memories)
                        {
                            pawn.needs.mood.thoughts.memories.TryGainMemory(memory, otherPawn);
                            Scribe_Deep.Look(ref otherPawn, "otherPawn");
                        }
                    }
                }
            }
            if (!record.IsInterrupted)
            {
                IncrementRecords(record);
            }
        }

        private static void ApplyPredatorPostVore(VoreTrackerRecord record)
        {
            // in case the vore would have fed the predator, but the digestion was interrupted, empty the predators food
            bool resetPredatorFood = record.IsInterrupted && record.VorePath.def.feedsPredator;
            if (resetPredatorFood)
            {
                record.Predator.SetFoodNeed(0f);
            }
        }

        private static void ApplyPreyPostVore(VoreTrackerRecord record)
        {
            //Log.Message("Applying real prey post vore");
            //Log.Message("goal: " + record.VoreGoal.defName + " - interrupted: " + record.IsInterrupted);
            // apply acid "bookmark" hediff if digestion was interrupted
            if(record.VoreGoal.IsLethal && record.IsInterrupted)
            {
                DigestionUtility.ApplyDigestionBookmark(record);
            }
        }

        private static void IncrementRecords(VoreTrackerRecord record)
        {
            record.VoreGoal.IncrementRecords(record.Predator, record.Prey);
        }
    }
}
