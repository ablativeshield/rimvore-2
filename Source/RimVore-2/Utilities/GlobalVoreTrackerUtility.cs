﻿using System.Collections.Generic;
using Verse;
using System.Linq;

namespace RimVore2
{
    public static class GlobalVoreTrackerUtility
    {
        private static Dictionary<Pawn, bool> pawnsTrackingVoreCache = new Dictionary<Pawn, bool>();
        private static List<Pawn> stalePawns = new List<Pawn>();

        public static bool IsTrackingVore(this Pawn pawn)
        {
            // if the pawns vores have changed
            if (stalePawns.Contains(pawn))
            {
                RV2Log.Message("Pawn " + pawn.Label + " has stale tracking state, removing", true);
                // check if the cache contains the pawns current status
                if (pawnsTrackingVoreCache.ContainsKey(pawn))
                {
                    // and remove the status
                    pawnsTrackingVoreCache.Remove(pawn);
                }
                stalePawns.Remove(pawn);
            }
            // if stale hasn't removed the key and we previously cached it
            if (pawnsTrackingVoreCache.ContainsKey(pawn))
            {
                // return the cached value
                return pawnsTrackingVoreCache.TryGetValue(pawn);
            }
            // no cached value, recalculate
            VoreTracker voreTracker = pawn.GetVoreTracker();
            if (voreTracker?.IsTrackingVore == true)
            {
                pawnsTrackingVoreCache.Add(pawn, true);
            }
            // on null tracker or false result
            else
            {
                pawnsTrackingVoreCache.Add(pawn, false);
            }
            RV2Log.Message("Calculated " + pawnsTrackingVoreCache.TryGetValue(pawn, false) + " tracking state for " + pawn.Label, true);
            // the fallback value here should never be used, but just to be sure
            return pawnsTrackingVoreCache.TryGetValue(pawn, false);
        }

        public static void SetVoreTrackingCacheStale(Pawn pawn)
        {
            if (stalePawns.Contains(pawn))
            {
                return;
            }
            stalePawns.Add(pawn);
        }

        public static bool IsPreyOf(this Pawn prey, Pawn predator)
        {
            VoreTrackerRecord record = prey.GetVoreRecord();
            return record != null && record.Predator == predator;
        }

        public static List<VoreTracker> AllActiveVoreTrackers => 
            SaveStorage.DataStore?.PawnData?
                .Select(pawnData => pawnData.Value.VoreTracker) // get all vore trackers
                .Where(voreTracker => voreTracker.IsTrackingVore)   // limit to active vore trackers
                .ToList();
        public static List<VoreTrackerRecord> AllVoreTrackerRecords => 
            AllActiveVoreTrackers
                .SelectMany(voreTracker => voreTracker.VoreTrackerRecords)
                .ToList();

        public static List<Pawn> AllVoredPrey => 
            AllVoreTrackerRecords
                .Select(record => record.Prey)
                .ToList();
        public static List<Pawn> AllVoringPredators => 
            AllActiveVoreTrackers
                .Select(tracker => tracker.VoreTrackerRecords[0].Predator)
                .ToList();

        private static Dictionary<Pawn, VoreTrackerRecord> cachedRecords = new Dictionary<Pawn, VoreTrackerRecord>();
        public static void ClearCachedRecord(Pawn pawn)
        {
            if (cachedRecords.ContainsKey(pawn))
            {
                cachedRecords.Remove(pawn);
            }
        }
        public static void ClearCachedRecords()
        {
            cachedRecords.Clear();
        }
        public static VoreTrackerRecord GetVoreRecord(this Pawn prey)
        {
            if (cachedRecords.ContainsKey(prey) && cachedRecords[prey] != null)
            {
                //Log.Message("Retrieving cached record: " + cachedRecords[prey]);
                return cachedRecords[prey];
            }
            VoreTrackerRecord record = AllVoreTrackerRecords?.Find(r => r.Prey == prey);
            if(record == null)
            {
                return null;
            }
            cachedRecords.SetOrAdd(prey, record);
            //Log.Message("Caching record: " + cachedRecords[prey]);
            return record;
        }

        public static VoreTracker GetVoreTracker(this Pawn predator) 
        {
            PawnData pawnData = SaveStorage.DataStore?.GetPawnData(predator);
            if(pawnData == null)
            {
                return null;
            }
            return pawnData.VoreTracker;
        }
    }
}
