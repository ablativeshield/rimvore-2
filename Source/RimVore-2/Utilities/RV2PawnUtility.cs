﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public static class RV2PawnUtility
    {
        public static Trait GenerateTrait(Pawn pawn, PawnGenerationRequest request = default(PawnGenerationRequest))
        {
            Func<TraitDef, float> weightSelector = (TraitDef t) => t.GetGenderSpecificCommonality(pawn.gender);
            Func<Trait> traitPicker = delegate ()
            {
                TraitDef def = DefDatabase<TraitDef>.AllDefsListForReading.RandomElementByWeight(weightSelector);
                if(def.degreeDatas.Count > 0)
                {
                    //Log.Message("degree data: " + string.Join(", ", def.degreeDatas.ConvertAll(dd => dd.degree + ", " + dd.label)));
                    int randomDegree = PawnGenerator.RandomTraitDegree(def);
                    //Log.Message("Set degree to " + randomDegree);
                    return new Trait(def, randomDegree);
                }
                else
                {
                    //Log.Message("No need to set degree");
                    return new Trait(def);
                }
            };
                
            Trait trait = traitPicker();
            int recursionLock = 10;
            int originalRecursionLock = recursionLock;
            while (!TraitIsValid(pawn, trait, request))
            {
                trait = traitPicker();
                
                if(recursionLock-- == 0)
                {
                    RV2Log.Error("Tried " + originalRecursionLock + " different traitDefs, all were invalid.");
                    return null;
                }
            }
            return trait;
        }

        public static void SetTraits(Pawn pawn, List<Trait> newTraits)
        {
            TraitSet pawnTraits = pawn.story?.traits;
            if(pawnTraits == null)
            {
                RV2Log.Message("Tried to set traits, but pawn " + pawn.LabelShort + " does not have traits");
                return;
            }
            foreach (Trait newTrait in newTraits)
            {

                Trait existingTrait = pawnTraits.GetTrait(newTrait.def);
                if (existingTrait != null)
                {
                    if (existingTrait.Degree != newTrait.Degree)
                    {
                        RemoveTrait(pawnTraits, existingTrait);
                    }
                    else
                    {
                        // the trait already exists in its intended degree, skip
                        continue;
                    }
                }

                if (!TraitIsValid(pawn, newTrait))
                {
                    RV2Log.Message("Tried to set trait " + newTrait.Label + " but it is not valid, skipping");
                    continue;
                }

                if(pawnTraits.allTraits.Count >= RV2Mod.settings.cheats.ReformedPawnTraitCount)
                {
                    // the pawn has too many traits, remove a random one
                    Trait randomTrait = pawnTraits.allTraits.RandomElement();
                    RV2Log.Message("removing trait " + randomTrait.def.defName + " due to overflow");
                    RemoveTrait(pawnTraits, randomTrait);
                }
                RV2Log.Message("adding trait " + newTrait.def.defName + " with degree " + newTrait.Degree);
                pawnTraits.GainTrait(newTrait);
            }
        }

        public static void RemoveTrait(TraitSet set, Trait trait)
        {
            // 1.3 does additional operations whenever you remove a trait, need to stay backwards compatible though
#if v1_2
            set.allTraits.Remove(trait);
#else
            set.RemoveTrait(trait);
#endif
        }

        public static void GenerateOrTrimTraits(Pawn pawn, List<Trait> traits, int targetCount, PawnGenerationRequest request = default(PawnGenerationRequest))
        {
            if (traits.Count > targetCount)
            {
                traits = traits.GetRange(0, targetCount - 1);
                RV2Log.Message("Trimmed traits down to " + string.Join(", ", traits.ConvertAll(t => t.def.defName)));
            }
            if (traits.Count < targetCount)
            {
                int loopLock = 3 * (targetCount - traits.Count);  // each missing trait has 3 generation attempts
                while (traits.Count < targetCount && loopLock-- > 0)
                {
                    Trait newTrait = RV2PawnUtility.GenerateTrait(pawn, request);
                    RV2Log.Message("Created new trait " + newTrait.def.defName);
                    if (newTrait != null)
                    {
                        traits.Add(newTrait);
                    }
                }
                RV2Log.Message("Generated traits to: " + string.Join(", ", traits.ConvertAll(t => t.def.defName)));
            }
        }

        public static bool TraitIsValid(Pawn pawn, Trait trait, PawnGenerationRequest request = default(PawnGenerationRequest))
        {
            if(!TraitIsValid(pawn, trait.def, trait.Degree, request))
            {
                return false;
            }

            // base game requires a maximum min mental break threshold of 0.5 (don't ask me why)
                // in fact, this check is so fucking dumb that I disabled it
            /*if (pawn.mindState?.mentalBreaker != null)
            {
                float newMentalBreakMinThreshold = pawn.mindState.mentalBreaker.BreakThresholdMinor
                    + trait.OffsetOfStat(StatDefOf.MentalBreakThreshold) * trait.MultiplierOfStat(StatDefOf.MentalBreakThreshold);
                if(newMentalBreakMinThreshold <= 0.5f)
                {
                    RV2Log.Message("trait invalid - would set mental break threshold below 0.5", "TraitGeneration");
                    return false;
                }
            }*/
            return true;
        }
        public static bool TraitIsValid(Pawn pawn, TraitDef traitDef, int degree = 0, PawnGenerationRequest request = default(PawnGenerationRequest))
        {
            if (traitDef == null)
            {
                RV2Log.Error("Could not create trait for " + pawn?.Label + ", resulting TraitDef is NULL");
                return false;
            }
            // pawn already has trait
            if (pawn.story?.traits?.HasTrait(traitDef) == true)
            {
                RV2Log.Message("trait invalid - pawn already has trait", "TraitGeneration");
                return false;
            }
            // request prohibits trait
            if (request.ProhibitedTraits?.Contains(traitDef) == true)
            {
                RV2Log.Message("trait invalid - request prohibits", "TraitGeneration");
                return false;
            }
            if (request.KindDef != null)
            {
                // the requests PawnKind blocks the trait
                if (request.KindDef?.disallowedTraits?.Contains(traitDef) == true)
                {
                    RV2Log.Message("trait invalid - request pawnkind disallows", "TraitGeneration");
                    return false;
                }
                // the requests PawnKind required a workTag that is blocked by the trait
                if ((request.KindDef.requiredWorkTags & traitDef.disabledWorkTags) != WorkTags.None)
                {
                    RV2Log.Message("trait invalid - would block required workTag", "TraitGeneration");
                    return false;
                }
                if (traitDef == TraitDefOf.Gay && !request.AllowGay)
                {
                    RV2Log.Message("trait invalid - gae not allowed", "TraitGeneration");
                    return false;
                }
            }
            // trait is blocked for this currently hostile pawns
            if (!traitDef.allowOnHostileSpawn && pawn.Faction?.HostileTo(Faction.OfPlayer) == true)
            {
                RV2Log.Message("trait invalid - not allowed on hostile", "TraitGeneration");
                return false;
            }
            // any of the pawns existing traits conflicts with the new trait
            if (pawn.story?.traits?.allTraits?.Any(t => t.def.conflictingTraits.Contains(traitDef)) == true)
            {
                RV2Log.Message("trait invalid - conflicts with another trait", "TraitGeneration");
                return false;
            }
            // trait requires work type that the pawn is not capable of
            if (traitDef.requiredWorkTypes != null && pawn.OneOfWorkTypesIsDisabled(traitDef.requiredWorkTypes))
            {
                RV2Log.Message("trait invalid - requires worktype the pawn can't do", "TraitGeneration");
                return false;
            }
            // trait requires work tag that the pawn is not capable of
            if (pawn.WorkTagIsDisabled(traitDef.requiredWorkTags))
            {
                RV2Log.Message("trait invalid - required worktag that pawn can't do", "TraitGeneration");
                return false;
            }

            // if trait has forced worktype passions and pawn can work
            if(traitDef.forcedPassions != null && pawn.workSettings != null)
            {
                Predicate<SkillDef> skillCheck = (SkillDef s) => s.IsDisabled(pawn.story.DisabledWorkTagsBackstoryAndTraits, pawn.GetDisabledWorkTypes(true));
                // the trait tries to set a passion for a work type that the pawn is not capable of
                if (traitDef.forcedPassions.Any(skill => !skillCheck(skill)))
                {
                    RV2Log.Message("trait invalid - sets passion for worktype that pawn can't do", "TraitGeneration");
                    return false;
                }
            }
            // degree of trait is disallowed by backstories
            if (pawn.story?.childhood?.DisallowsTrait(traitDef, degree) == true)
            {
                RV2Log.Message("trait invalid - childhood disallows", "TraitGeneration");
                return false;
            }
            if (pawn.story?.adulthood?.DisallowsTrait(traitDef, degree) == true)
            {
                RV2Log.Message("trait invalid - adulthood disallows", "TraitGeneration");
                return false;
            }

            RV2Log.Message("trait valid", "TraitGeneration");
            return true;
        }

        public static bool TryIncreaseNeed(Pawn pawn, NeedDef needDef, float value)
        {
            Need pawnNeed = pawn?.needs?.TryGetNeed(needDef);
            if (pawnNeed == null)
            {
                RV2Log.Message("Pawn " + pawn?.Label + " does not have need " + needDef.label + " - nothing to increase/decrease");
                return false;
            }
            float originalLevel = pawnNeed.CurLevel;
            pawnNeed.CurLevel += value;
            // if the need did not change, return false
            if(pawnNeed.CurLevel == originalLevel)
            {
                return false;
            }
            RV2Log.Message("Changed " + pawn.Label + "'s need " + needDef.label + " by " + value + ", new level: " + pawnNeed.CurLevel);
            return true;
        }

        public static List<Hediff> GetHealableInjuries(this Pawn pawn)
        {
            return pawn.health?.hediffSet?.hediffs?
                .FindAll(hed => hed is Hediff_Injury
                && !hed.IsPermanent());
        }

        public static bool HasValidMentalStateForVore(this Pawn pawn, VoreRole role)
        {
            MentalStateDef stateDef = pawn.MentalStateDef;
            Extension_ValidMentalStateForVore extension = stateDef.GetModExtension<Extension_ValidMentalStateForVore>();
            if(extension == null)
            {
                return false;
            }
            return extension.IsValid(role);
            //List<MentalStateDef> allowedMentalStatesForVore = new List<MentalStateDef>()
            //    {
            //        MentalStateDefOf.Manhunter,
            //        MentalStateDefOf.ManhunterPermanent
            //    };

            //if (allowedMentalStatesForVore.Contains(pawn.MentalStateDef))
            //{
            //    return true;
            //}
            //if (pawn.MentalState is MentalState_VoreTargeter)
            //{
            //    return true;
            //}
            //return false;
        }

        public static float GetHungerRate(this Pawn pawn)
        {
            float lifeStageFactor = pawn.ageTracker.CurLifeStage.hungerRateFactor;
            float racePropsFactor = pawn.RaceProps.baseHungerRate;
            float hediffsFactor = pawn.health.hediffSet.GetHungerRateFactor(HediffDefOf.Malnutrition);
            float storyFactor = ((pawn.story == null || pawn.story.traits == null)
                ? 1f
                : pawn.story.traits.HungerRateFactor);
            float statFactor = pawn.GetStatValue(StatDefOf.HungerRateMultiplier, true);
            float totalFactor = lifeStageFactor * racePropsFactor * hediffsFactor * storyFactor * statFactor;
#if v1_2
            float totalHunger = Need_Food.BaseFoodFallPerTick * totalFactor;
#else
            float totalHunger = Need_Food.BaseFoodFallPerTickAssumingCategory(HungerCategory.Fed, totalFactor);
#endif
            RV2Log.Message("life stage: " + lifeStageFactor
                + "race props: " + racePropsFactor
                + "hediffs: " + hediffsFactor
                + "story: " + storyFactor
                + "stat: " + statFactor
                + "total factor: " + totalFactor
                + "total food fall " + totalHunger
            );
            return totalHunger;
        }

        public static float GetInternalTemperature(this Pawn pawn)
        {
            if (!pawn.def.HasModExtension<InternalTemperatureExtension>())
            {
                return RV2Mod.settings.cheats.InternalTemperature;
            }
            return pawn.def.GetModExtension<InternalTemperatureExtension>().temperature;
        }
    }
}
