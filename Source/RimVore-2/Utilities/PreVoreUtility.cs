﻿using RimWorld;
using Verse;
using System;
using System.Collections.Generic;

namespace RimVore2
{
    public static class PreVoreUtility
    {
        public static void PopulateRecord(ref VoreTrackerRecord record)
        {
            Pawn prey = record.Prey;
            Pawn predator = record.Predator;

            record.PreyStartedNaked = prey.IsNaked();

            record.VoreContainer = new VoreContainer(predator);
            record.VoreContainer.TryAddPrey(prey);

            if (record.VorePath.def.feedsPredator)
            {
                predator.AddFood(VoreCalculationUtility.CalculatePreyNutrition(prey, predator));
            }
            IncrementRecords(record);
            TriggerInteractions(record);
        }
        public static void IncrementRecords(VoreTrackerRecord record)
        {
            record.Predator.records?.Increment(RV2_Common.predatorRecordDef);
            record.Prey.records?.Increment(RV2_Common.preyRecordDef);
            record.VoreType.IncrementRecords(record.Predator, record.Prey);
        }

        public static void TriggerInteractions(VoreTrackerRecord record)
        {
            InteractionDef interactionDef = record.VoreGoal.IsLethal ? VoreInteractionDefOf.RV2_FatalVore : VoreInteractionDefOf.RV2_EndoVore;

            // the list at the end is a free selection of rule packs! We can use this to insert the goal / type of vore!
            // This kinda creates messy rules, but interactions are an overcomplicated mess anyways...
            List<RulePackDef> rulePacks = ExtraRulePacks(record);
            PlayLogEntry_Interaction logEntry = new PlayLogEntry_Interaction(interactionDef, record.Predator, record.Prey, rulePacks);
            Find.PlayLog.Add(logEntry);
        }
        private static List<RulePackDef> ExtraRulePacks(VoreTrackerRecord record)
        {
            List<RulePackDef> list = new List<RulePackDef>();
            List<RulePackDef> typeRulePacks = record.VorePath.VoreType.relatedRulePacks;
            if (typeRulePacks != null)
            {
                list.AddRange(typeRulePacks);
            }
            List<RulePackDef> goalRulePacks = record.VorePath.VoreGoal.relatedRulePacks;
            if(goalRulePacks != null)
            {
                list.AddRange(goalRulePacks);
            }
            return list;
        }
    }
}
