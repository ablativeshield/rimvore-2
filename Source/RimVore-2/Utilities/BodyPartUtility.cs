﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public static class BodyPartUtility
    {
        public static BodyPartRecord GetBodyPartByDef(this Pawn pawn, BodyPartDef partDef)
        {
            return pawn.GetBodyPartsByDef(partDef).RandomElement();
        }

        public static List<BodyPartRecord> GetBodyPartsByDef(this Pawn pawn, BodyPartDef partDef)
        {
            return pawn.health.hediffSet.GetNotMissingParts()
                .Where(part => part.def == partDef)
                .ToList();
        }

        public static BodyPartRecord GetBodyPartByName(this Pawn pawn, string bodyPartName)
        {
            return GetBodyPartsByName(pawn, bodyPartName).RandomElementWithFallback();
        }

        public static List<BodyPartRecord> GetBodyPartsByName(this Pawn pawn, string bodyPartName)
        {
            List<BodyPartRecord> bodyParts = pawn.health?.hediffSet?.GetNotMissingParts().ToList();
            if (bodyParts.NullOrEmpty())
            {
                return null;
            }
            List<string> bodyPartAliases = bodyPartName.GetAliases("BodyPart");
            // Log.Message("aliases: " + string.Join(", ", aliasNames));
            bodyParts = bodyParts.FindAll(bodyPart => bodyPart.def.defName.ContainsAnyAsSubstring(bodyPartAliases));
            //if (RV2Settings_General.VerboseLogging && pawn.Label != lastLoggedPawn) Log.Message("Called search for voreType " + bodyPartName);
            //if (RV2Settings_General.VerboseLogging && pawn.Label != lastLoggedPawn) Log.Message("Found bodyparts: " + String.Join(",", bodyParts.ConvertAll(e=>e.Label)));
            if (!bodyParts.NullOrEmpty())
            {
                return bodyParts;
            }
            return new List<BodyPartRecord>();
        }

        public static Hediff GetHediffOnBodyPartByNames(Pawn pawn, string bodyPartName, string hediffName)
        {
            BodyPartRecord bodyPart = pawn.GetBodyPartByName(bodyPartName);
            if(bodyPart == null)
            {
                return null;
            }
            List<string> hediffAliases = hediffName.GetAliases();
            return pawn.health.hediffSet.hediffs    // all hediffs
                .FindAll(h => h.Part == bodyPart)   // all hediffs for our body part
                .Find(hediff => hediff.def.defName.ContainsAnyAsSubstring(hediffAliases)); // contains any alias
        }

        public static List<BodyPartRecord> GetAllAcidVulnerableBodyParts(Pawn pawn)
        {
            List<BodyPartRecord> bodyParts = pawn.health.hediffSet.GetNotMissingParts()
                .ToList()
                .FindAll(part => part.def.IsSkinCovered(part, pawn.health.hediffSet));   // this means we don't apply to organs, bones or mechanical parts
            return bodyParts;
        }

        public static float GetAcidModifier(BodyPartRecord bodyPart)
        {
            int depth = GetBodyPartDepth(bodyPart);
            // allow maximum depth of 5 for 100% acid strength
            depth = Math.Min(depth, 5);
            float acidModifier = depth * 0.2f;
            return acidModifier;
        }

        public static int GetBodyPartDepth(BodyPartRecord bodyPart)
        {
            int depth = 1;
            BodyPartRecord searchPart = bodyPart;
            while (!searchPart.IsCorePart)
            {
                depth++;
                searchPart = searchPart.parent;
            }
            return depth;
        }

        public static bool IsVitalOrHasVitalChildren(BodyPartRecord bodyPart)
        {
            List<BodyPartTagDef> vitalTags = DefDatabase<BodyPartTagDef>.AllDefsListForReading.FindAll(tag => tag.vital);
            return vitalTags.Any(tag => bodyPart.HasChildParts(tag));
        }
    }
}
