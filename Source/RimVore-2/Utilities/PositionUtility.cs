﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public static class PositionUtility
    {
        public static IntVec3 GetPreyReleasePosition(Pawn pawn)
        {
            List<IntVec3> possiblePositions = new List<IntVec3>();
            for (int i = 0; i < 10; i++)
            {
                Predicate<IntVec3> cellValidator = delegate (IntVec3 cell)
                {
                    return pawn.CanReach(new LocalTargetInfo(cell), Verse.AI.PathEndMode.OnCell, Danger.Some);
                };
                bool foundCell = CellFinder.TryFindRandomCellNear(pawn.Position, pawn.Map, 15, cellValidator, out IntVec3 position);
                if (foundCell && !possiblePositions.Contains(position))
                {
                    possiblePositions.Add(position);
                }
            }
            if (possiblePositions.NullOrEmpty())
            {
                return pawn.Position;
            }
            // create scores for each position and track position and its score in a dictionary
            Dictionary<IntVec3, float> scoredPositions = possiblePositions.ToDictionary(
                position => position,
                position => GetPositionScore(pawn, position)
            );
            RV2Log.Message("calculated positions: " + LogUtility.ToString(scoredPositions));

            IntVec3 chosenPosition = scoredPositions
                .Where(kvp => kvp.Value == scoredPositions.Max(kvp2 => kvp2.Value))  // get all entries where position score is max
                .RandomElement()    // get any of the best positions
                .Key;   // discard score
            return chosenPosition;
        }

        private static float GetPositionScore(Pawn pawn, IntVec3 position)
        {
            float score = 0;
            Danger positionDanger = position.GetDangerFor(pawn, pawn.Map);
            if (positionDanger != Danger.None)
            {
                return -float.MaxValue;
            }
            Room room = position.GetRoom(pawn.Map);
            if(room != null)
            {
                if (room.IsDoorway) return -float.MaxValue;    //doorways are a horrible place to release a prey in
#if v1_2
                if (room.isPrisonCell) score -= 5;
#else
                if (room.IsPrisonCell) score -= 5;
#endif
                if (room.PsychologicallyOutdoors) score += 5;   // outdoors is good (not too sure about this)
                if (room.Owners.Contains(pawn)) score += 20;    // if room is pawn bedroom it's probably private
                RoomRoleDef role = room.Role;
                if (role == RoomRoleDefOf.Hospital) score -= 20; // do not act like an animal
                else if (role == RoomRoleDefOf.DiningRoom) score -= 15; // do not act like an animal
                else if (role == RoomRoleDefOf.ThroneRoom) score -= 10; // do not act like an animal
                int numberOfDoors = room.Cells
                    .SelectMany(cell => cell.GetThingList(pawn.Map))
                    .Count(thing => thing.def.IsDoor);
                score -= numberOfDoors * 5; // try to use a room that has few doors - less chance of pawns walking in
            }
            if (!pawn.ComfortableTemperatureRange().Includes(position.GetTemperature(pawn.Map))) score -= 10;   // stick to comfortable temperature
            if (position.GetSurfaceType(pawn.Map) == SurfaceType.Eat) score -= 10;  // do not release on the table

            return score;
        }
    }
}
