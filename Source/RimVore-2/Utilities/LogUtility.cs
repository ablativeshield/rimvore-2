﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public static class LogUtility
    {
        public static string ToString<K, V>(this Dictionary<K, V> dictionary)
        {
            List<string> stringifiedEntries = new List<string>();
            foreach(KeyValuePair<K, V> kvp in dictionary)
            {
                string entry = kvp.Key.ToString() + " : " + kvp.Value.ToString();
                stringifiedEntries.Add(entry);
            }
            return "key type: " + 
                typeof(K).ToString() + 
                " | value type: " + 
                typeof(V).ToString() + 
                "\n" + 
                string.Join("\n", stringifiedEntries);
        }

        public static string PresentFloatRange(float value1, float value2)
        {
            return PresentFloat(value1) + " - " + PresentFloat(value2);
        }
        public static string PresentFloat(float value)
        {
            if (value == float.MaxValue) return "∞";
            else if (value == float.MinValue) return "-∞";
            return value.ToString();
        }
        public static string PresentFloatRange(FloatRange floatRange)
        {
            return PresentFloatRange(floatRange.min, floatRange.max);
        }

        public static void PresentConfigErrors<T>(IEnumerable<string> errors)
        {
            foreach (string text in errors)
            {
                Log.Error(string.Concat(new object[]
                {
                    "Config error in ",
                    typeof(T).ToString(),
                    ": ",
                    text
                }));
            }
        }
    }
}
