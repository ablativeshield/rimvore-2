﻿using System;
using System.Collections.Generic;
using Verse;
using RimWorld;

namespace RimVore2
{
    public static class VoreCalculationUtility
    {
        public static float CalculatePreyNutrition(Pawn prey, Pawn predator = null)
        {
            ThingDef foodDef = FoodUtility.GetFinalIngestibleDef(prey);
            if(foodDef != null)
            {
                return FoodUtility.GetNutrition(prey, foodDef);
            }
            RV2Log.Message("Pawn does not have FinalingestibleDef, falling back to manual food calculation");
            float meatAmount = prey.GetStatValue(StatDefOf.MeatAmount, true);
            ThingDef meatDef = prey?.def?.race?.meatDef;
            float totalNutrition;
            if(meatAmount > 0 && meatDef != null)
            {
                float meatNutrition = meatDef.GetStatValueAbstract(StatDefOf.Nutrition);
                totalNutrition = meatAmount * meatNutrition;
                return totalNutrition;
            }
            else
            {
                totalNutrition = RV2Mod.settings.cheats.FallbackNutritionValue;
                RV2Log.Error("RimVore-2 was unable to determine how much nutrition pawn " + prey.LabelShort + " would have, returning default fallback: " + totalNutrition);
            }
            // quirk influence on nutrition
            QuirkManager preyQuirks = prey.QuirkManager(false);
            if(preyQuirks != null)
            {
                totalNutrition = preyQuirks.ModifyValue("PreyNutritionMultiplierAsPrey", totalNutrition);
            }
            QuirkManager predatorQuirks = predator?.QuirkManager(false);
            if(predatorQuirks != null)
            {
                totalNutrition = predatorQuirks.ModifyValue("PreyNutritionMultiplierAsPredator", totalNutrition);
            }
            return totalNutrition;
        }

        public static float GetSizeInRelationTo(this Pawn pawn1, Pawn pawn2)
        {
            if (pawn1 == null || pawn2 == null)
            {
                RV2Log.Warning("Either pawn for size difference calculation was null, forcing modifier 1f!");
                return 1f;
            }
            return pawn1.BodySize / pawn2.BodySize;
        }

        public static JobDef GetInitJobDefFor(this VoreRole role)
        {
            switch (role)
            {
                case VoreRole.Predator:
                    return VoreJobDefOf.RV2_VoreInitAsPredator;
                case VoreRole.Prey:
                    return VoreJobDefOf.RV2_VoreInitAsPrey;
                case VoreRole.Feeder:
                    return VoreJobDefOf.RV2_VoreInitAsFeeder;
                default:
                    return null;
            }
        }

        public static float DefaultModifierValue(this ModifierOperation operation)
        {
            switch (operation)
            {
                case ModifierOperation.Add:
                case ModifierOperation.Subtract:
                    return 0f;
                case ModifierOperation.Multiply:
                case ModifierOperation.Divide:
                    return 1f;
                default:
                    throw new Exception("Unknown operation \"" + operation + "\" to provide default modifier for");
            }
        }

        public static string OperationSymbol(this ModifierOperation operation)
        {
            switch (operation)
            {
                case ModifierOperation.Add: return "+";
                case ModifierOperation.Subtract: return "-";
                case ModifierOperation.Multiply: return "x";
                case ModifierOperation.Divide: return "/";
                case ModifierOperation.Set: return "=";
                default:
                    throw new Exception("Unknown operation \"" + operation + "\" to provide symbol for");
            }
        }

        public static Func<float, float, float> AggregationOperation(this ModifierOperation operation)
        {
            switch (operation)
            {
                case ModifierOperation.Add:
                case ModifierOperation.Subtract:
                    return (x, y) => x + y;
                case ModifierOperation.Multiply:
                    return (x, y) => x * y;
                case ModifierOperation.Divide:
                    return (x, y) => x / y;
                case ModifierOperation.Set:
                    return (x, y) => y;
                default:
                    throw new Exception("Unknown operation \"" + operation + "\" to aggregate roll modifiers with");
            }
        }
        public static float Aggregate(this ModifierOperation type, float arg1, float arg2)
        {
            Func<float, float, float> operation = type.AggregationOperation();
            return operation(arg1, arg2);
        }
    }
}
