﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public static class ReservedPreyUtility
    {
        public static List<Pawn> ReservedPrey { get; } = new List<Pawn>();

        public static bool IsReserved(Pawn pawn, bool checkForDead = true)
        {
            if(pawn == null)
            {
                return false;
            }
            if (checkForDead && pawn.Dead)
            {
                UnReserve(pawn);
            }
            return SaveStorage.DataStore?.GetPawnData(pawn)?.IsReservedPrey == true;
        }

        public static bool AnyPawnReserved()
        {
            return !ReservedPrey.NullOrEmpty();
        }
        public static bool AnyPawnReservedInMap(Map map)
        {
            return !ReservedPrey.FindAll(pawn => pawn.Map == map).NullOrEmpty();
        }

        public static bool CanReserve(Pawn pawn, out string reason)
        {
            if (pawn.Dead)
            {
                reason = "RV2_VoreInvalidReasons_PawnDead".Translate();
                return false;
            }
            if (IsReserved(pawn))
            {
                reason = "RV2_VoreInvalidReasons_AlreadyReserved".Translate();
                return false;
            }
            if (!pawn.CanBePrey(out reason))
            {
                return false;
            }
            reason = null;
            return true;
        }

        public static void Reserve(Pawn pawn)
        {
            if (!CanReserve(pawn, out string reason))
            {
                RV2Log.Warning("Can't reserve pawn: " + reason);
                return;
            }
            SaveStorage.DataStore.GetPawnData(pawn).IsReservedPrey = true;
            ReservedPrey.Add(pawn);
        }

        public static void UnReserve(Pawn pawn, bool checkForDead = false)
        {
            if (!IsReserved(pawn, checkForDead))
            {
                return;
            }
            SaveStorage.DataStore.GetPawnData(pawn).IsReservedPrey = false;
            ReservedPrey.Remove(pawn);
        }

        // called when map is loaded
        public static void LoadAllReservedPrey(IEnumerable<Pawn> pawns)
        {
            if (pawns.EnumerableNullOrEmpty())
            {
                RV2Log.Warning("Tried to load reserved prey for empty pawn list, this usually happens on map generation");
                return;
            }
            pawns = pawns
                .Where(pawn => pawn != null 
                && SaveStorage.DataStore.HasPawnData(pawn) 
                && SaveStorage.DataStore.GetPawnData(pawn).IsReservedPrey);
            if (pawns.EnumerableNullOrEmpty())
            {
                return;
            }
            // take all pawns on the map ( probably pretty resource intensive...) and add the preys that have the pawnData flag to the reserved prey
            ReservedPrey.AddRange(pawns);
            string logMessage = "Reserved prey have been loaded: ";
            if (ReservedPrey.NullOrEmpty())
            {
                logMessage += "[None]";
            }
            else
            {
                logMessage += "Count: " + ReservedPrey.Count + ", Names: " + string.Join(", ", ReservedPrey.ConvertAll(pawn => pawn.LabelShort));
            }
            RV2Log.Message(logMessage);
        }
    }
}
