﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld.Planet;
using HugsLib;

namespace RimVore2
{
    class DataStore : WorldComponent
    {
        public DataStore(World world) : base(world)
        { }

        public Dictionary<int, PawnData> PawnData = new Dictionary<int, PawnData>();

        public override void ExposeData()
        {
            if(Scribe.mode == LoadSaveMode.Saving)
            {
                PawnData.RemoveAll(item => item.Value == null || !item.Value.IsValid);
            }
            base.ExposeData();
            Scribe_Collections.Look(ref PawnData, "Data", LookMode.Value, LookMode.Deep);
            if(Scribe.mode == LoadSaveMode.LoadingVars)
            {
                if(PawnData == null)
                {
                    PawnData = new Dictionary<int, PawnData>();
                }
            }
        }
        public void HardReset()
        {
            foreach(Pawn predator in GlobalVoreTrackerUtility.AllVoringPredators)
            {
                Debug_Eject.EmergencyEjectAll(predator);
            }
            PawnData.Clear();
        }

        public bool HasPawnData(Pawn pawn)
        {
            return PawnData.ContainsKey(pawn.thingIDNumber);
        }

        public PawnData GetPawnData(Pawn pawn)
        {
            if(pawn == null)
            {
                RV2Log.Error("Tried to get pawn data for NULL pawn!");
                return null;
            }
            PawnData data = PawnData.TryGetValue(pawn.thingIDNumber);
            if (data?.IsValid == false)
            {
                PawnData.Remove(pawn.thingIDNumber);
                data = null;
            }
            if(data == null)
            {
                data = new PawnData(pawn);
                PawnData.Add(pawn.thingIDNumber, data);
            }
            return data;
        }

        private void SetPawnData(Pawn pawn, PawnData data)
        {
            PawnData.Add(pawn.thingIDNumber, data);
        }
    }
}
