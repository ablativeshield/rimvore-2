﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;
using HugsLib;

namespace RimVore2
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses")]
    class SaveStorage : ModBase
    {
        public override string ModIdentifier => ModId;
        public static string ModId => "RV2";

        public static DataStore DataStore;

        // don't let harmony run amok and patch files at this point
        protected override bool HarmonyAutoPatch { get => false; }

        public override void WorldLoaded()
        {
            base.WorldLoaded();
            DataStore = Find.World.GetComponent<DataStore>();
            ReservedPreyUtility.LoadAllReservedPrey(Find.CurrentMap?.mapPawns?.AllPawns);
            DebugTools.Reset.ResyncAllHediffs();    // redirect to debugtools because I am L A Z Y
        }

        public override void DefsLoaded()
        {
            base.DefsLoaded();
            RV2Mod.mod.DefsLoaded();
            SettingsUpdater.UpdateSettings();
            //Settings_General.Setup();
            //LoadedModManager.GetMod<SettingsController_General>().WriteSettings();
            //LoadedModManager.GetMod<SettingsController_Quirks>().WriteSettings();
            //LoadedModManager.GetMod<SettingsController_Rules>().WriteSettings();
            //LoadedModManager.GetMod<SettingsController_AutoVore>().WriteSettings();
            //Settings_Rules.DefsLoaded();
            //Settings_Quirks.DefsLoaded();
            IEnumerable<string> poolAssignmentErrors = QuirkUtility.QuirkPoolAssignmentErrors();
            LogUtility.PresentConfigErrors<QuirkDef>(poolAssignmentErrors);
        }
    }
}
