﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace RimVore2
{
    public class PawnData : IExposable
    {
        public Pawn Pawn = null;
        public bool IsReservedPrey = false;
        public VoreTracker VoreTracker;
        private QuirkManager quirkManager;

        public bool HasQuirks => QuirkManager(false) != null;
        private Dictionary<DesignationDef, Designation> designations;
        public Dictionary<DesignationDef, Designation> Designations
        {
            get
            {
                if (designations.EnumerableNullOrEmpty())
                {
                    InitializeDesignations();
                }
                return designations;
            }
        }

        public PawnData() { }

        public PawnData(Pawn pawn)
        {
            this.Pawn = pawn;
            VoreTracker = new VoreTracker(pawn);
            InitializeDesignations();
        }

        public QuirkManager QuirkManager(bool initializeIfNull = true)
        {
            // if quirk manager hasn't been created yet and the pawn qualifies for quirks, create it
            // Log.Message("quirk manager null? " + (quirkManager == null) + " pawn can have quirks? " + pawn.CanHaveQuirks());
            if (quirkManager == null && initializeIfNull)
            {
                if (Pawn.CanHaveQuirks(out _))
                {
                    RV2Log.Message("Creating quirk manager for " + Pawn.Label, "Quirks");
                    quirkManager = new QuirkManager(Pawn);
                }
            }
            return quirkManager;
        }

        private void InitializeDesignations()
        {
            if(Pawn == null)
            {
                RV2Log.Warning("Tried to initialize designations, but Pawn is NULL");
                return;
            }
            if(designations == null)
            {
                designations = new Dictionary<DesignationDef, Designation>();
            }
            foreach(DesignationDef designation in RV2_Common.VoreDesignations)
            {
                designations.Add(designation, new Designation(Pawn, designation));
            }

            ScribeUtilities.SyncKeys(ref designations, RV2_Common.VoreDesignations);
        }

        public void ExposeData()
        {
            Scribe_References.Look(ref Pawn, "Pawn", true);
            Scribe_Values.Look(ref IsReservedPrey, "IsReservedPrey", false, true);
            Scribe_Deep.Look(ref VoreTracker, "VoreTracker", new object[0]);
            Scribe_Deep.Look(ref quirkManager, "quirkManager", Pawn);
            if (Scribe.mode == LoadSaveMode.Saving)
            {
                ScribeUtilities.SyncKeys(ref designations, RV2_Common.VoreDesignations);
            }
            ScribeUtilities.ScribeVariableDictionary(ref designations, "designations", LookMode.Def, LookMode.Deep);
            if (Scribe.mode == LoadSaveMode.LoadingVars)
            {
                ScribeUtilities.SyncKeys(ref designations, RV2_Common.VoreDesignations);
            }
        }
        
        public bool IsValid
        {
            get
            {
                return Pawn != null && VoreTracker != null && !Pawn.Discarded;
            }
        }
    }
}
