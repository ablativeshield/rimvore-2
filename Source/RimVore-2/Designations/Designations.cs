﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using UnityEngine;

namespace RimVore2
{
    public class Designations : Command
    {
        private const float GizmoPadding = 1f;
        private const float IconPadding = 3f;
        private const float IconSize = 32f;
        private IconGrid iconGrid;

        private readonly Pawn pawn;

        //private static SubIcon lastClicked;

        public void CalculateIconGrid(float topLeftX = 0, float topLeftY = 0)
        {

            // quirk button + all designations
            int iconCount = 1 + RV2_Common.VoreDesignations.Count;
            int rowCount = 2;
            // Log.Message("setting grid for " + iconCount + " icons");
            // e.g. 7 icons means 4 icons per row for 2 rows
            int columnCount = (int)Mathf.Ceil((float)iconCount / rowCount);
            iconGrid = new IconGrid(columnCount, rowCount, topLeftX, topLeftY, IconSize, IconPadding);
        }

        private readonly List<SubIcon> subIcons = new List<SubIcon>();
        private List<SubIcon> SubIcons
        {
            get
            {
                if(subIcons.NullOrEmpty())
                {
                    if(iconGrid == null)
                    {
                        CalculateIconGrid();
                    }

                    if (VoreValidator.CanHaveQuirks(pawn, out _))
                    {
                        subIcons.Add(new QuirkButton(iconGrid.GetCurrentGridPosition(true), pawn));
                    }
                    foreach(DesignationDef designation in RV2_Common.VoreDesignations)
                    {
                        subIcons.Add(new DesignationIcon(iconGrid.GetCurrentGridPosition(true), pawn, designation));
                    }
                    //if (Settings_General.EndoVoreEnabled || Settings_General.FatalVoreEnabled)
                    //{
                    //    subIcons.Add(new Predator());
                    //}
                    //if (Settings_General.EndoVoreEnabled)
                    //{
                    //    subIcons.Add(new EndoPrey());
                    //}
                    //if (Settings_General.FatalVoreEnabled)
                    //{
                    //    subIcons.Add(new FatalPrey());
                    //}
                }
                return subIcons;
            }
        }

        public override float GetWidth(float maxWidth)
        {
            if(iconGrid == null)
            {
                CalculateIconGrid();
            }
            float width = Mathf.Min(maxWidth, iconGrid.size.x);
            //RV2Log.Message("GetWidth returns " + iconGrid.size.x + " with maxWidth " + maxWidth);
            return width;
        }

        public Designations(Pawn pawn)
        {
            this.pawn = pawn;
            //Settings_Rules.SetAutoDesignations(pawn);
            //if (Settings_General.VerboseLogging) LogUtility.MessageOnce("Pawn " + pawn.LabelShort + " is role " + pawn.GetRelationKind());
            defaultLabel = "RV2designations";
            defaultDesc = "RV2designations";
        }

#if v1_2
        public override GizmoResult GizmoOnGUI(Vector2 topLeft, float maxWidth)
#else
        public override GizmoResult GizmoOnGUI(Vector2 topLeft, float maxWidth, GizmoRenderParms parms)
#endif
        {
            if(iconGrid == null)
            {
                CalculateIconGrid(topLeft.x, topLeft.y);
            }
            // topLeft needs to consider the padding to the outermost box
            topLeft = topLeft + new Vector2(GizmoPadding, GizmoPadding);
            Rect innerGizmoRect = new Rect(topLeft, iconGrid.size);
            Rect outerGizmoRect = innerGizmoRect.ExpandedBy(GizmoPadding);
            //gizmoRect = gizmoRect.ContractedBy(GizmoPadding);
            iconGrid.startX = innerGizmoRect.x;
            iconGrid.startY = innerGizmoRect.y;
            Widgets.DrawWindowBackground(outerGizmoRect);
            foreach (SubIcon icon in SubIcons)
            {
                Rect iconRect = iconGrid.GetRect(icon.gridPosition);
                if (icon.Draw(iconRect))
                {
                    //lastClicked = icon;
                    return new GizmoResult(GizmoState.Interacted, Event.current);
                }
            }
            return new GizmoResult(GizmoState.Clear);
        }

        public abstract class SubIcon
        {
            public Vector2Int gridPosition;
            protected Pawn pawn;

            protected abstract Texture2D CurrentTexture { get; }
            protected abstract string CurrentTip { get; }
            public abstract void Action();

            public SubIcon(Vector2Int gridPosition, Pawn pawn)
            {
                // Log.Message("Set subicon with gridPosition " + gridPosition);
                this.gridPosition = gridPosition;
                this.pawn = pawn;
            }

            public bool Draw(Rect iconRect)
            {
                TooltipHandler.TipRegion(iconRect, CurrentTip);
                GUI.DrawTexture(iconRect, CurrentTexture);

                if(Widgets.ButtonInvisible(iconRect, false))
                {
                    Action();
                    return true;
                }
                return false;
            }

        }

        public class DesignationIcon : SubIcon
        {
            protected Designation activeDesignation;

            public DesignationIcon(Vector2Int gridPosition, Pawn pawn, DesignationDef def) : base(gridPosition, pawn)
            {
                activeDesignation = SaveStorage.DataStore?.GetPawnData(pawn)?.Designations?.TryGetValue(def);
                if (activeDesignation == null)
                {
                    RV2Log.Error("SubIcon has activeDesignation NULL, can't draw gizmo");
                    return;
                }
            }

            protected override Texture2D CurrentTexture => activeDesignation.CurrentIcon();
            protected override string CurrentTip => activeDesignation.CurrentTip();

            public override void Action()
            {
                activeDesignation.Cycle();
            }
        }

        [StaticConstructorOnStartup]
        public class QuirkButton : SubIcon
        {
            public QuirkButton(Vector2Int gridPosition, Pawn pawn) : base(gridPosition, pawn) { }

            protected override Texture2D CurrentTexture => ContentFinder<Texture2D>.Get("Widget/quirk_menu");

            protected override string CurrentTip => "RV2_QuirkWindow_Header".Translate();

            public override void Action()
            {
                Find.WindowStack.Add(new Window_Quirks(pawn));
                VoreInteractionManager.Reset();
            }
        }


        //[StaticConstructorOnStartup]
        //public class EndoPrey : SubIcon
        //{
        //    public override Vector2 Offset => new Vector2(0, IconGap + IconSize);

        //    public override Texture2D IconAutoOn => ContentFinder<Texture2D>.Get("Widget/endo_prey_auto_on");
        //    public override Texture2D IconAutoOff => ContentFinder<Texture2D>.Get("Widget/endo_prey_auto_off");
        //    public override Texture2D IconManualOn => ContentFinder<Texture2D>.Get("Widget/endo_prey_manual_on");
        //    public override Texture2D IconManualOff => ContentFinder<Texture2D>.Get("Widget/endo_prey_manual_off");
        //    public override Texture2D IconDeny => ContentFinder<Texture2D>.Get("Widget/endo_prey_deny");

        //    public override string IsXAutoOnDesc => "RV2_Widget_IsEndoPreyAutoOnDesc";
        //    public override string IsXAutoOffDesc => "RV2_Widget_IsEndoPreyAutoOffDesc";
        //    public override string IsXManualOnDesc => "RV2_Widget_IsEndoPreyManualOnDesc";
        //    public override string IsXManualOffDesc => "RV2_Widget_IsEndoPreyManualOffDesc";
        //    public override string IsNeverXDesc => "RV2_Widget_IsNeverEndoPreyDesc";

        //    public override bool CanBeX(Pawn pawn) => pawn.CanBeEndoPrey(out _);
        //    public override bool XManuallySet(Pawn pawn) => PawnData(pawn).EndoPreyManuallySet;
        //    public override bool IsXManual(Pawn pawn) => PawnData(pawn).IsEndoPreyManual;
        //    public override bool IsXAuto(Pawn pawn) => PawnData(pawn).IsEndoPreyAuto;

        //    public override void Cycle(Pawn pawn)
        //    {
        //        PawnData pawnData = PawnData(pawn);
        //        if (pawnData.EndoPreyManuallySet)
        //        {
        //            if (pawnData.IsEndoPreyManual)
        //            {
        //                pawnData.IsEndoPreyManual = false;
        //            }
        //            else
        //            {
        //                pawnData.EndoPreyManuallySet = false;
        //            }
        //        }
        //        else
        //        {
        //            pawnData.EndoPreyManuallySet = true;
        //            pawnData.IsEndoPreyManual = true;
        //        }
        //        VoreInteractionManager.Reset();
        //    }
        //}
        //[StaticConstructorOnStartup]
        //public class FatalPrey : SubIcon
        //{
        //    public override Vector2 Offset => new Vector2(IconGap + IconSize, IconGap + IconSize);

        //    public override Texture2D IconAutoOn => ContentFinder<Texture2D>.Get("Widget/fatal_prey_auto_on");
        //    public override Texture2D IconAutoOff => ContentFinder<Texture2D>.Get("Widget/fatal_prey_auto_off");
        //    public override Texture2D IconManualOn => ContentFinder<Texture2D>.Get("Widget/fatal_prey_manual_on");
        //    public override Texture2D IconManualOff => ContentFinder<Texture2D>.Get("Widget/fatal_prey_manual_off");
        //    public override Texture2D IconDeny => ContentFinder<Texture2D>.Get("Widget/fatal_prey_deny");

        //    public override string IsXAutoOnDesc => "RV2_Widget_IsFatalPreyAutoOnDesc";
        //    public override string IsXAutoOffDesc => "RV2_Widget_IsFatalPreyAutoOffDesc";
        //    public override string IsXManualOnDesc => "RV2_Widget_IsFatalPreyManualOnDesc";
        //    public override string IsXManualOffDesc => "RV2_Widget_IsFatalPreyManualOffDesc";
        //    public override string IsNeverXDesc => "RV2_Widget_IsNeverFatalPreyDesc";

        //    public override bool CanBeX(Pawn pawn) => pawn.CanBeFatalPrey(out _);
        //    public override bool XManuallySet(Pawn pawn) => PawnData(pawn).FatalPreyManuallySet;
        //    public override bool IsXManual(Pawn pawn) => PawnData(pawn).IsFatalPreyManual;
        //    public override bool IsXAuto(Pawn pawn) => PawnData(pawn).IsFatalPreyAuto;

        //    public override void Cycle(Pawn pawn)
        //    {
        //        PawnData pawnData = PawnData(pawn);
        //        if (pawnData.FatalPreyManuallySet)
        //        {
        //            if (pawnData.IsFatalPreyManual)
        //            {
        //                pawnData.IsFatalPreyManual = false;
        //            }
        //            else
        //            {
        //                pawnData.FatalPreyManuallySet = false;
        //            }
        //        }
        //        else
        //        {
        //            pawnData.FatalPreyManuallySet = true;
        //            pawnData.IsFatalPreyManual = true;
        //        }
        //        VoreInteractionManager.Reset();
        //    }
        //}
    }
}