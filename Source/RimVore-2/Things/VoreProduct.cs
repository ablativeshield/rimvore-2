﻿using System;
using System.Collections.Generic;
using Verse;

namespace RimVore2
{
    /// <summary>
    /// Re-pack digested remains and/or add new items
    /// </summary>
    /// <remarks>
    /// Following possibilities: 
    /// * items set, container and containerItems not set: The normal contents of the VoreContainer are excreted alongside any Thing items provided
    /// * items, container set and containerItems not set: The normal contents of the VoreContainer are moved the container item, which is excreted alongside other items (like spawned filth)
    /// * items, container and containerItems set: The normal contents of the VoreContainer are moved to the container item along with whatever items were provided in containerItems. The container alongside the provided items will be excreted
    /// </remarks>
    public class VoreProduct
    {
        public bool dessicatePrey = true;
        public bool destroyPrey = false;

        public List<ThingDef> items;
        public List<ThingDef> containerItems;
        //public ContainerSelectorDef containerSelector;
        public List<ThingDef> selectableContainers;

        public List<ThingDef> ValidContainers => selectableContainers.FindAll(container => container.GetModExtension<VoreContainerExtension>().IsValid());

        public List<Thing> MakeVoreProducts(ThingOwner previousContainer, VorePathDef vorePathDef, Pawn predator)
        {
            if(items == null && ValidContainers.NullOrEmpty())
            {
                return new List<Thing>();
            }
            List<Thing> producedItems = new List<Thing>();
            if (items != null)
            {
                producedItems.AddRange(items.ConvertAll(thingDef => ThingMaker.MakeThing(thingDef)));
                RV2Log.Message("producedItems: " + string.Join(", ", producedItems.ConvertAll(t => t.ToString())));
            }
            if(!ValidContainers.NullOrEmpty())
            {
                VoreProductContainer producedContainer = CreateProductContainer(vorePathDef, predator);
                if (producedContainer == null)
                {
                    RV2Log.Warning("The container was null, skipping");
                    return producedItems;
                }
                RV2Log.Message("producedContainer: " + producedContainer.def.defName, true);
                bool containerHasProducedItems = producedContainer.def != RV2_Common.VoreContainerNone;
                if (!containerHasProducedItems)
                {
                    return producedItems;
                }
                List<Thing> producedContainerItems = containerItems?.ConvertAll(thingDef => ThingMaker.MakeThing(thingDef));
                if(producedContainerItems != null)
                {
                    RV2Log.Message("producedContainerItems: " + string.Join(", ", producedContainerItems.ConvertAll(t => t.ToString())), true);
                    producedContainer.GetDirectlyHeldThings().TryAddRangeOrTransfer(producedContainerItems);
                }
                previousContainer.TryTransferAllToContainer(producedContainer.GetDirectlyHeldThings());
                producedItems.Add(producedContainer);
            }
            return producedItems;
        }

        public VoreProductContainer CreateProductContainer(VorePathDef vorePathDef, Pawn predator)
        {
            ThingDef containerDef = RV2Mod.settings.rules.GetContainer(predator, vorePathDef);
            if(containerDef == null)
            {
                RV2Log.Warning("ContainerDef for CreateProductContainer was null, most liked caused by rules not being set yet, falling back to first container");
                containerDef = vorePathDef.FirstValidContainer;
                if(containerDef == null)
                {
                    RV2Log.Warning("ContainerDef is still null, this is most likely caused because no selectable containers are available");
                    return null;
                }
            }
            VoreProductContainer productContainer = (VoreProductContainer) ThingMaker.MakeThing(containerDef);
            productContainer.Initialize();
            return productContainer;
        }
    }
}
