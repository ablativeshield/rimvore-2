﻿using Verse;
using RimWorld;
using System.Collections.Generic;
using System.Linq;

namespace RimVore2
{
    public class VoreProductContainer : ThingWithComps, IOpenable, IThingHolder, IExposable
    {
        private ThingOwner<Thing> innerContainer;

        public VoreProductContainer() { }

        public void Initialize()
        {
            innerContainer = new ThingOwner<Thing>();
        }

        public bool CanOpen => true;

#if v1_2
#else
        public int OpenTicks => 50;
#endif

        public void GetChildHolders(List<IThingHolder> outChildren)
        {
            ThingOwnerUtility.AppendThingHoldersFromThings(outChildren, this.GetDirectlyHeldThings());
        }

        public ThingOwner GetDirectlyHeldThings()
        {
            return innerContainer;
        }

        public List<ThingDef> ProvideItems()
        {
            VoreContainerExtension voreContainerExtension = def.GetModExtension<VoreContainerExtension>();
            if(voreContainerExtension != null)
            {
                if (!voreContainerExtension.ProvidesForcedResource())
                {
                    RV2Log.Message("VoreContainerExtension exists for " + def.defName + " but there are no items to provide.");
                    return new List<ThingDef>();
                }
                RV2Log.Message("VoreContainerExtension providing additional items: " +
                     string.Join(", ", voreContainerExtension.forcedProducedThings.ConvertAll(thing => thing.defName)), true);
                return voreContainerExtension.forcedProducedThings;
            }
            RV2Log.Message("No VoreContainerExtension, so no additional items provided");
            return new List<ThingDef>();
        }

        public bool AddItems(List<Thing> items)
        {
            return items.All(item => innerContainer.TryAdd(item));
        }

        public void Open()
        {
            if(innerContainer.TryDropAll(this.Position, this.Map, ThingPlaceMode.Near))
            {
                this.Destroy();
                return;
            }
            Log.Error("Could not drop items from vore product.");
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Deep.Look<ThingOwner<Thing>>(ref this.innerContainer, "innerContainer", new object[]
            {
                this
            });
        }
    }
}
