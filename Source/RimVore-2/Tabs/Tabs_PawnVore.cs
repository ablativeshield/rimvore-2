﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using Verse.AI;

namespace RimVore2
{
    class Tabs_PawnVore : ITab
    {

        private Vector2 mainSize = new Vector2(500f, 450f);
        private Vector2 scrollPosition;
        private float currentPreyHeight = 0;
        private bool currentPreyHeightDirty = true;
        private readonly int TextPadding = 10;
        private readonly int UseableTypeIconPadding = 2;
        private readonly int RecursionPadding = 25;
        private readonly int IconSize = 20;

        public override bool IsVisible => RV2Mod.settings.features.ShowVoreTab && SelPawn.IsTrackingVore();

        public Tabs_PawnVore()
        {
            size = new Vector2(mainSize.x, mainSize.y);
            labelKey = "RV2_PawnVoreTab";
        }

        protected override void FillTab()
        {
            ResetGUI();
            try
            {

                Vector2 startPosition = new Vector2(10, 10);
                Vector2 rectSize = new Vector2(size.x - 10, size.y - 10);

                Rect contentRect = new Rect(startPosition, rectSize).ContractedBy(16);
                //Listing_Standard contentList = new Listing_Standard();
                //contentList.Begin(contentRect);

                //Icons showing the avaliable vore types pred can use
                //Rect UseableRect = contentList.GetRect(IconSize);
                //DrawUseableTypes(UseableRect);

                //Current prey list
                DrawCurrentList(contentRect);
                //contentList.End();

                
            } catch(Exception e)
            {
                RV2Log.Error($"Uncaught exception while drawing pawn tab for {this.SelPawn}. \n Exception: {e}");
            }
            ResetGUI();
        }

        //Still experimenting with useable type display. if going to have it at all
        private void DrawUseableTypes(Rect useableRect)
        {
            for (int i = 0; i < RV2_Common.VoreTypes.Count; i++)
            {
                VoreTypeDef VoreType = RV2_Common.VoreTypes[i];
                Texture2D Icon = VoreType.Icon;
                Rect rect = new Rect(useableRect.x + i * IconSize + UseableTypeIconPadding, useableRect.y, IconSize, IconSize);

                bool CanUseType = true;//todo check if pawn can actuall use this type
                Color IconColor = Color.white;
                if (!CanUseType)
                {
                    IconColor = Color.grey;
                }

                TooltipHandler.TipRegion(rect, VoreType.label);
                Widgets.ButtonImage(rect, Icon, IconColor);//May want to add ability to turn off specific types of vore for individual pawn
            }
        }

        private void DrawCurrentList(Rect inRect)
        {
            IEnumerable<IGrouping<VoreTypeDef, VoreTrackerRecord>> groupedTrackers = GetGroupedTrackers(SelPawn);
            if (groupedTrackers == null || groupedTrackers.Count() == 0)
            {
                return;
            }
#if v1_2
            Listing_Standard list = new Listing_Standard()
            {
                ColumnWidth = inRect.width
            };
            list.Begin(inRect);
            Rect outerRect = list.GetRect(inRect.height - list.CurHeight); ;
            list.MakeAndBeginScrollView(outerRect, currentPreyHeight, ref scrollPosition, out Rect innerRect);
#else
            UIUtility.MakeAndBeginScrollView(inRect, currentPreyHeight, ref scrollPosition, out Listing_Standard list);
#endif

            try
            {
                //thought about having a check for it to only display so many values but if your eating so much this becomes a problem its on you
                foreach (IGrouping<VoreTypeDef, VoreTrackerRecord> group in groupedTrackers)
                {
                    foreach (VoreTrackerRecord record in group)
                    {
                        if (record.Prey == null) continue;
                        DrawPreyAndChildrenRecursivly(list, record, 50);
                    }
                }
            }
            catch (Exception e)
            {
                list.Label("Error in draw current list " + e.Message);
                list.Label(e.StackTrace);
            }

            currentPreyHeightDirty = true;
#if v1_2
            list.EndScrollView(ref currentPreyHeight, ref currentPreyHeightDirty, ref innerRect);
#else
            UIUtility.EndScrollView(list, ref currentPreyHeight, ref currentPreyHeightDirty);
#endif

        }
        private void DrawPreyAndChildrenRecursivly(Listing_Standard list, VoreTrackerRecord record, float pictureSize, int xOffset = 0, int depth = 0)
        {
            //Draw current prey
            DrawRecord(list, record, pictureSize, xOffset, depth == 0);

            //Draw prey's prey
            IEnumerable<IGrouping<VoreTypeDef, VoreTrackerRecord>> groupedTrackers = GetGroupedTrackers(record.Prey);
            if (groupedTrackers == null || groupedTrackers.Count() == 0) return;

            //Only going to 3 levels deep to avoid running off the tab window
            //Grabbing the count of additional prey and letting them know how many more there is
            if (depth >= 3)
            {
                int additionalPreyCount = GetChildrenCountRecursive(record.Prey);
                Rect rect = list.GetRect(IconSize);
                DrawIcon(rect.x + xOffset, rect.y, IconSize, UIUtility.HiddenButtonTexture, () => { }, "RV2_Tab_HiddenPreyCount".Translate(additionalPreyCount));
                return;
            }

            foreach (IGrouping<VoreTypeDef, VoreTrackerRecord> group in groupedTrackers)
            {
                foreach (VoreTrackerRecord childRecord in group)
                {
                    if (childRecord.Prey == null) continue;
                    DrawPreyAndChildrenRecursivly(list, childRecord, pictureSize, (depth + 1) * RecursionPadding, depth + 1);
                }
            }
        }

        private int GetChildrenCountRecursive(Pawn pred)
        {
            int count = 0;
            VoreTracker tracker = pred.GetVoreTracker();
            if (tracker == null) return count;
            foreach (VoreTrackerRecord record in tracker.VoreTrackerRecords)
            {
                count += 1 + GetChildrenCountRecursive(record.Prey);
            }
            return count;
        }

        private void DrawRecord(Listing_Standard list, VoreTrackerRecord record, float recordSize, int xOffset, bool isTopLevel)
        {
            recordSize = Math.Min(recordSize, IconSize * 2);//Need a min amount of vertical space for the information area to display 2 rows

            Rect recordRect = list.GetRect(recordSize); //Allocated space for this record in list

            //Type information followed by portait followed by information area
            
            DrawIcon(recordRect.x + xOffset,//Allocated x along with the indentations for recursive prey
                recordRect.y + (.5f * recordSize) - (IconSize * .5f),//Center vore icon based on height of allocated space
                IconSize,
                record.VoreType.Icon,
                tooltip: record.VoreType.label
            );

            //Portait area
            bool doDrawPicture = ShouldDrawProfileTexture(record);
            bool didDrawPawn = false;//Needed for correct positioning of information passed the picture if it draws one or not
            if (doDrawPicture)
            {
#if v1_2
                RenderTexture pawnTexture = PortraitsCache.Get(record.Prey, new Vector2(recordSize, recordSize));
#else
                RenderTexture pawnTexture = PortraitsCache.Get(record.Prey, new Vector2(recordSize, recordSize), Rot4.South);
#endif
                if (pawnTexture != null)
                {
                    Rect PortraitRect = new Rect(recordRect.x + xOffset + IconSize,//Allocated x start with recursive prey offset and vore type icon
                        recordRect.y,
                        pawnTexture.width,
                        pawnTexture.height
                    );
                    GUI.DrawTexture(PortraitRect, pawnTexture);
                    didDrawPawn = true;
                }
            }

            //Information area divided in 2 rows top row text information bottom the icon information

            //Define the start of the information
            //Allocated X start + Recursive prey indentation + type info icon size + additional buffer padding between information start and picture + if applicable the offset caused by picture
            float InfoAreaXOffset = recordRect.x + xOffset + IconSize + TextPadding;
            if (didDrawPawn)
            {
                InfoAreaXOffset += recordSize;
            }

            //Text information area
            Rect labelRect = new Rect(InfoAreaXOffset, recordRect.y, recordRect.width - InfoAreaXOffset, recordSize);
            GUI.Label(labelRect, record.DisplayLabel);

            //Icon information area
            float curIconX = 0;
            float IconY = recordRect.y + recordRect.height - IconSize;

            //Each DrawLabelIcon returns the change in cur x position so the next one knows where to draw
            curIconX += DrawIconConditionally(InfoAreaXOffset + curIconX, IconY, record.VoreGoal.Icon, tooltip: record.VoreGoal.label); // Digestion or endo
            curIconX += DrawIconConditionally(InfoAreaXOffset + curIconX, IconY, UIUtility.BanishButton, () => DoEject(record), "RV2_RMB_EjectPrey_Self".Translate(), () => CanDoEject(record, isTopLevel));// Eject

        }

        private bool CanDoEject(VoreTrackerRecord record, bool isTopLevel)
        {
            return isTopLevel && CanPlayerGiveCommand(record.Predator) && record.CanEject;
        }
        private bool CanPlayerGiveCommand(Pawn predator)
        {
            return predator.IsColonist && !predator.InMentalState && !predator.Downed && !predator.IsBurning();
        }

        private void DoEject(VoreTrackerRecord record)
        {
            Job ejectJob = JobMaker.MakeJob(VoreJobDefOf.RV2_EjectPreySelf, record.Prey);
            record.Predator.jobs.TryTakeOrderedJob(ejectJob);
        }

        //Returns the distance that x should be offset after adding for additional icons
        private int DrawIconConditionally(float totalXOffset, float y, Texture2D icon, Action OnClick = null, string tooltip = null, Func<bool> conditionalDraw = null)
        {
            if (conditionalDraw != null && !conditionalDraw()) return 0;

            DrawIcon(totalXOffset, y, IconSize, icon, OnClick, tooltip);

            return IconSize;
        }

        private IEnumerable<IGrouping<VoreTypeDef, VoreTrackerRecord>> GetGroupedTrackers(Pawn pawn)
        {
            return pawn.GetVoreTracker()?.VoreTrackerRecords.GroupBy(t => t.VoreType);
        }

        //Currently only supporting humanoids, may or may not want to add support to named animals, and in future pawnmorpher former human status might want to be checked
        private bool ShouldDrawProfileTexture(VoreTrackerRecord record)
        {
            return record.Prey.IsHumanoid();
        }
        private void DrawIcon(float x, float y, float size, Texture2D iconTexture, Action OnClick = null, string tooltip = null)
        {
            Rect rect = new Rect(x, y, size, size);
            if (tooltip != null && Mouse.IsOver(rect))
            {
                TooltipHandler.TipRegion(rect, tooltip);
            }
            if (Widgets.ButtonImage(rect, iconTexture))
            {
                OnClick?.Invoke();
            }
        }

        public static void ResetGUI()
        {
            Text.Font = GameFont.Small;
            Text.Anchor = TextAnchor.UpperLeft;
            GUI.color = Color.white;
        }
    }
}
