﻿using System.Reflection;
using HarmonyLib;
using Verse;

namespace RimVore2
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            //RV2Log.Message("RV2 startup", true);
            Harmony.DEBUG = false;
            Harmony harmony = new Harmony("rv2");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            ReflectionUtility.StartUp();
        }
    }
}
