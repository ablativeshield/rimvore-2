﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using rjw;
using RimVore2;

namespace RJWGenitalAccess
{
    [HarmonyPatch(typeof(rjw.Sexualizer), "SexualizeGenderedPawn")]
    public class PatchTemplate
    {
        [HarmonyPostfix]
        private static void AddRV2BackstoryGenitals(Pawn pawn)
        {
            try
            {
                if(!pawn.TryGetRV2Backstory(out RV2_BackstoryDef adultBackstory, out RV2_BackstoryDef childBackstory))
                {
                    return;
                }
                if(adultBackstory != null)
                {
                    AddSexualPartForBackstory(pawn, adultBackstory);
                }
                if(childBackstory != null)
                {
                    AddSexualPartForBackstory(pawn, childBackstory);
                }
            }
            catch(Exception e)
            {
                Log.Warning("RimVore-2: Something went wrong while trying to add RV2Backstory enforced genitalia: " + e);
                return;
            }
        }

        private static void AddSexualPartForBackstory(Pawn pawn, RV2_BackstoryDef backstory)
        {
            if (!Settings_General.BackstoriesCanAddSexualParts)
            {
                return;
            }
            List<SexualPart> forcedParts = backstory.forcedSexualParts;
            if (forcedParts.NullOrEmpty())
            {
                return;
            }
            foreach(SexualPart part in forcedParts)
            {
                Func<Pawn, bool> partCheck;
                Action<Pawn> partAdder;
                switch (part)
                {
                    case SexualPart.Penis:
                        partCheck = (Pawn p) => GenitalUtility.GenitalAccess.HasPenis(p);
                        partAdder = (Pawn p) => GenitalUtility.GenitalAccess.AddSexualPart(p, SexualPart.Penis);
                        break;
                    case SexualPart.Vagina:
                        partCheck = (Pawn p) => GenitalUtility.GenitalAccess.HasVagina(p);
                        partAdder = (Pawn p) => GenitalUtility.GenitalAccess.AddSexualPart(p, SexualPart.Vagina);
                        break;
                    case SexualPart.Breasts:
                        partCheck = (Pawn p) => GenitalUtility.GenitalAccess.HasBreasts(p);
                        partAdder = (Pawn p) => GenitalUtility.GenitalAccess.AddSexualPart(p, SexualPart.Breasts);
                        break;
                    default:
                        throw new NotImplementedException("Unknown SexualPart: " + part.ToString());
                }
                if (partCheck(pawn))
                {
                    continue;
                }
                partAdder(pawn);
            }
        }
    }
}